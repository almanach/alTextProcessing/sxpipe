<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output
        indent="yes"
        method="html"
        encoding="utf-8"
        doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
        doctype-public="-//W3C//DTD XHTML 1.1//EN"
    />
    
    <xsl:param name="cpt_pers">
        <xsl:value-of select="count(//PERSON/ENTITY)+count(//PERSON_m/ENTITY)+count(//PERSON_f/ENTITY)"/>
    </xsl:param>
<!--    <xsl:param name="cpt_org">
        <xsl:value-of select="count(//ORGANIZATION/ENTITY)+count(//ORG2/ENTITY)+count(//PERSON_f/ENTITY)"/>
    </xsl:param>-->
    
    <xsl:template match="ENTITIES">
        <html>
            <head>
                <title>
                    ENTITIES
                </title>
            </head>
            <body>
                <table>
                    <!--<caption>CODE COULEURS</caption>-->
                    <tr style="background-color:#15317E;"><td>PERSON</td></tr>
                    <tr style="background-color:#347235;"><td>LOCATION</td></tr>
                    <tr style="background-color:#E56717;"><td>ORGANIZATION</td></tr>
                    <tr style="background-color:#6C2DC7;"><td>COMPANY</td></tr>
                    <tr style="background-color:#808000;"><td>PRODUCT</td></tr>
                    <tr style="background-color:gray;"><td>WORK</td></tr>
                </table>    
                <xsl:apply-templates/>
            </body>                
        </html>
    </xsl:template>
    <xsl:template match="ENTITIES/PERSON">
        <h3 style="background-color:#15317E;text-align:center;">
            <xsl:value-of select="concat('PERSONS (',count(//PERSON/ENTITY),'/', $cpt_pers,')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <xsl:template match="ENTITIES/PERSON_m">
        
        <h3 style="background-color:#15317E;text-align:center;">
            <xsl:value-of select="concat('PERSONS M (',count(//PERSON_m/ENTITY),'/', $cpt_pers,')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <xsl:template match="ENTITIES/PERSON_f">
        <h3 style="background-color:#15317E;text-align:center;">
            <xsl:value-of select="concat('PERSONS F (',count(//PERSON_f/ENTITY),'/', $cpt_pers,')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <xsl:template match="ENTITIES/LOCATION">
        <h3 style="background-color:#347235;text-align:center;">
            <xsl:value-of select="concat('LOCATIONS (',count(//LOCATION/ENTITY),')')"/> 
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <xsl:template match="ENTITIES/ORGANIZATION">
        <h3 style="background-color:#E56717;text-align:center;">
            <!--<xsl:value-of select="concat('ORGANIZATIONS (',count(//ORGANIZATION/ENTITY),'/',$cpt_org,')')"/>-->
            <xsl:value-of select="concat('ORGANIZATIONS (',count(//ORGANIZATION/ENTITY),')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <!--<xsl:template match="ENTITIES/ORG2">
        <h3 style="background-color:#E56717;text-align:center;">
            <xsl:value-of select="concat('ORGANIZATIONS-ESTAB. (',count(//ORG2/ENTITY),'/',$cpt_org,')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>-->
    <xsl:template match="ENTITIES/COMPANY">
        <h3 style="background-color:#6C2DC7;text-align:center;">
            <xsl:value-of select="concat('COMPANIES (',count(//COMPANY/ENTITY),')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <xsl:template match="ENTITIES/PRODUCT">
        <h3 style="background-color:#808000;text-align:center;">
            <xsl:value-of select="concat('PRODUCT (',count(//PRODUCT/ENTITY),')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    <xsl:template match="ENTITIES/WORK">
        <h3 style="background-color:GRAY;text-align:center;">
            <xsl:value-of select="concat('WORKS (',count(//WORK/ENTITY),')')"/>
        </h3>
        <table>
            <xsl:attribute name="border">1</xsl:attribute>
            <tr><th>SCREEN</th><th>NAME</th><th>OCC</th><th>INFO</th></tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template> 
    <xsl:template match="ENTITY">
        <tr>
            <td>
                <xsl:attribute name="style">font-weight:bold;</xsl:attribute>
                <xsl:value-of select="./@print"/>
            </td>
            <td>
            <xsl:choose>
                <xsl:when test="./@href">
                    <a>
                        <xsl:attribute name="class"><xsl:value-of select="parent::node()"/></xsl:attribute>
                        <xsl:attribute name="target">_blank</xsl:attribute>
                        <xsl:attribute name="style">color:#302226;</xsl:attribute>
                        <xsl:attribute name="href">
                            <xsl:value-of select="./@href"/>
                        </xsl:attribute>
                        <xsl:value-of select="./@name"/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <span>
                        <xsl:attribute name="class"><xsl:value-of select="parent::node()"/></xsl:attribute>
                        <xsl:attribute name="title">NO HREF</xsl:attribute>
                        <xsl:attribute name="style">color:#302226;</xsl:attribute>
                        <xsl:value-of select="./@name"/>
                    </span>
                </xsl:otherwise>
            </xsl:choose>
            </td>
            <td><xsl:value-of select="./@occ"/></td>
            <td><xsl:value-of select="./@infos"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>

