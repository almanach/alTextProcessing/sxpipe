<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output indent="yes" method="html" encoding="utf-8"
    doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
    doctype-public="-//W3C//DTD XHTML 1.1//EN"/>

  <xsl:template match="news_set">
    <html>
      <head>
        <title> SXPIPE2 2 HTML </title>
      </head>
      <body>
        <table>
          <!--<caption>CODE COULEURS</caption>-->
          <tr style="background-color:#15317E;">
            <td>PERSON</td>
            <td style="font-weight:bold;">PERSON + QUOTE AUTHOR</td>
          </tr>
          <tr style="background-color:#347235;">
            <td>LOCATION</td>
          </tr>
          <tr style="background-color:#E56717;">
            <td>ORGANIZATION</td>
            <td style="font-weight:bold;">ORGANIZATION + QUOTE AUTHOR</td>
          </tr>
          <tr style="background-color:#6C2DC7;">
            <td>COMPANY</td>
          </tr>
          <tr style="background-color:#808000;">
            <td>PRODUCT</td>
          </tr>
          <tr style="background-color:gray;">
            <td>WORK</td>
          </tr>
        </table>
        <xsl:apply-templates select="./news"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="news_set/news/title">
    <h3>
      <xsl:value-of select="./@text"/>
    </h3>
  </xsl:template>

  <xsl:template match="news_set/news">
    <h4 style="background-color:#6D7B8D;text-align:center">
      <xsl:value-of select="concat(./@number,' | ',./@date,' | ', ./@id)"/>
    </h4>
    <xsl:apply-templates/>
    <p/>
  </xsl:template>

  <xsl:template match="news_set/news/data/p">
    <xsl:param name="para" select="./@rank"/>
    <div>
      <span>
        <xsl:value-of select="$para"/>
      </span>
      <xsl:text>/ </xsl:text>
      <xsl:value-of select="./@text"/>
    </div>
  </xsl:template>
  <xsl:template match="news_set/news/sxpipe">
    <h3>SXPIPE</h3>
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="news_set/news/sxpipe/title">
    <div style="font-weight:bold">
      <xsl:apply-templates/>
    </div>
  </xsl:template>
  <xsl:template match="news_set/news/sxpipe/p">
    <xsl:param name="sent" select="./@rank"/>
    <div>
      <span>
        <xsl:value-of select="$sent"/>
      </span>
      <xsl:text>/ </xsl:text>
      <xsl:apply-templates/>
    </div>
  </xsl:template>
  <xsl:template match="SQ">
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;SQ(&lt;/sup&gt;</span>
    <span style="background-color:#FFF8C6">
      <xsl:apply-templates/>
    </span>
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;)SQ&lt;/sup&gt;</span>
  </xsl:template>
  <xsl:template match="SHQ">
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;SHQ(&lt;/sup&gt;</span>
    <span style="background-color:#FFF8C6">
      <xsl:apply-templates/>
    </span>
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;)SHQ&lt;/sup&gt;</span>
  </xsl:template>
  <xsl:template match="STQ">
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;STQ(&lt;/sup&gt;</span>
    <span style="background-color:#FFF8C6">
      <xsl:apply-templates/>
    </span>
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;)STQ&lt;/sup&gt;</span>
  </xsl:template>
  <xsl:template match="HQ">
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;HQ(&lt;/sup&gt;</span>
    <span style="background-color:#FFF8C6">
      <xsl:apply-templates/>
    </span>
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;)HQ&lt;/sup&gt;</span>
  </xsl:template>
  <xsl:template match="TQ">
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;TQ(&lt;/sup&gt;</span>
    <span style="background-color:#FFF8C6">
      <xsl:apply-templates/>
    </span>
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;)TQ&lt;/sup&gt;</span>
  </xsl:template>
  <xsl:template match="DQ">
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;DQ(&lt;/sup&gt;</span>
    <span style="background-color:#FFF8C6">
      <xsl:apply-templates/>
    </span>
    <span style="color:maroon;background-color:#FFF8C6"
      >&lt;sup&gt;)DQ&lt;/sup&gt;</span>
  </xsl:template>
  <xsl:template match="PERSON">
    <a>
      <xsl:attribute name="class">PERSON</xsl:attribute>
      <xsl:attribute name="style">color:#15317E;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>
  <xsl:template match="PERSON_m">
    <a>
      <xsl:attribute name="class">PERSON_M</xsl:attribute>
      <xsl:attribute name="style">color:#15317E;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="concat(child::text(),'&lt;sup&gt;m&lt;/sup&gt;')"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>
  <xsl:template match="PERSON_f">
    <a>
      <xsl:attribute name="class">PERSON_F</xsl:attribute>
      <xsl:attribute name="style">color:#15317E;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="concat(child::text(),'&lt;sup&gt;f&lt;/sup&gt;')"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>
  <xsl:template match="LOCATION">
    <a>
      <xsl:attribute name="class">LOCATION</xsl:attribute>
      <xsl:attribute name="style">color:#347235;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>

  <xsl:template match="ADRESSE">
    <span>
      <xsl:attribute name="style">color:#347235;</xsl:attribute>
      <xsl:attribute name="title">ADDRESS</xsl:attribute>
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

  <xsl:template match="ORGANIZATION">
    <a>
      <xsl:attribute name="class">ORGANIZATION</xsl:attribute>
      <xsl:attribute name="style">color:#E56717;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>

  <xsl:template match="ORG2">
    <span>
      <xsl:attribute name="class">ORGANIZATION-ESTABLISHMENT</xsl:attribute>
      <xsl:attribute name="style">color:#E56717;</xsl:attribute>
      <xsl:attribute name="title">ORGANIZATION-ESTABLISHMENT</xsl:attribute>
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

  <xsl:template match="COMPANY">
    <a>
      <xsl:attribute name="class">COMPANY</xsl:attribute>
      <xsl:attribute name="style">color:#6C2DC7;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>

  <xsl:template match="PRODUCT">
    <a>
      <xsl:attribute name="class">PRODUCT</xsl:attribute>
      <xsl:attribute name="style">color:#808000;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>

  <xsl:template match="WORK">
    <a>
      <xsl:attribute name="class">WORK</xsl:attribute>
      <xsl:attribute name="style">color:gray;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="./@info"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>

  <xsl:template match="AUT/PERSON">
    <a>
      <xsl:attribute name="class">PERSON_AUTHOR</xsl:attribute>
      <xsl:attribute name="style"
        >color:#15317E;font-weight:bold;background-color:yellow;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="concat(./@info, ' // AUTEUR')"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">

        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>
  <xsl:template match="AUT/PERSON_m">
    <a>
      <xsl:attribute name="class">PERSON_AUTHOR_M</xsl:attribute>
      <xsl:attribute name="style"
        >color:#15317E;font-weight:bold;background-color:yellow;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="concat(./@info, ' // AUTEUR')"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="concat(child::text(),'&lt;sup&gt;m&lt;/sup&gt;')"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>
  <xsl:template match="AUT/PERSON_f">
    <a>
      <xsl:attribute name="class">PERSON_AUTHOR_F</xsl:attribute>
      <xsl:attribute name="style"
        >color:#15317E;font-weight:bold;background-color:yellow;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="concat(./@info, ' // AUTEUR')"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="concat(child::text(),'&lt;sup&gt;f&lt;/sup&gt;')"/>
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>
  <xsl:template match="AUT/ORGANIZATION">
    <a>
      <xsl:attribute name="class">ORGANIZATION_AUTHOR</xsl:attribute>
      <xsl:attribute name="style"
        >color:#E56717;font-weight:bold;background-color:yellow;</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="concat(./@info, ' // AUTEUR')"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./a[1]/@href"/>
      </xsl:attribute>
      <xsl:value-of select="child::text()"/>
      <!--             <xsl:apply-templates/> -->
    </a>
    <span>&amp;nbsp;</span>
  </xsl:template>

  <xsl:template match="AUT_CL">
    <span>
      <xsl:attribute name="class">AUTHOR_CL</xsl:attribute>
      <xsl:attribute name="style">background-color:yellow;</xsl:attribute>
      <xsl:attribute name="title">AUTEUR</xsl:attribute>
      <!--             <xsl:apply-templates/> -->
    </span>
  </xsl:template>
  <xsl:template match="NP">
    <span>
      <xsl:attribute name="class">np</xsl:attribute>
      <xsl:attribute name="style">color:orange</xsl:attribute>
      <xsl:value-of select="."/>
    </span>
  </xsl:template>


</xsl:stylesheet>
