#!/usr/bin/env perl

use warnings;

$| = 1;

use strict;
use File::Find;
use Encode;

#SI DEPECHE AFP EST EN LATIN-1
#binmode(STDIN, ':encoding(iso-8859-1)');

my @dir = shift or die "$0 <dir> <file_out>\n";
my $file_out = shift or die "$0 <dir> <file_out>\n";

#open(G, ">encoding(iso-8859-1)", $file_out) or die "Can't open $file_out: $!\n";
open(G, ">encoding(utf8)", $file_out) or die "Can't open $file_out: $!\n";
print G "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<news_set>\n";


my $cpt_news = 0;
find(\&process_files, @dir);

print G "</news_set>\n";
close(G);


sub process_files
{
	my $file = $File::Find::name;
	my $current_dir = $File::Find::dir;
	unless(-d $file)
	{
		$file =~ /.+?\/([^\/]+)$/;
		my $file_name = $1;
		unless($file_name =~ /^\.\.?/)
		{	
			#SI DEPECHE AFP EST EN LATIN-1
			#open(F, "<encoding(iso-8859-1)", $file) or die "Can't open $file: $!\n";
		  open(F, "<encoding(utf8)", $file) or die "Can't open $file: $!\n";
			$cpt_news++;
			my $news = "";
			my $date = "";
			my $id = "";
			my $title_text = "";
			my $title_xml = "";			
			while(<F>){$news .= $_;}
			close(F);
			$news =~ s/\n//g;
			$news =~ s/�/=YEN=/g;
			
			if($news =~ /<DateId>([^<]+)<\/DateId>/){$date = $1;}
			if($news =~ /<NewsItemId>([^<]+)<\/NewsItemId>/){$id = $1;}
			print G "<news date=\"$date\" id=\"$id\" number=\"$cpt_news\">\n";
			if($news =~ /<HeadLine>([^<]+)<\/HeadLine>/)
			{
				$title_text = $1;
				$title_xml = $1;
				$title_xml =~ s/"/&quot;/g;
				#INUTILE ET MEME FATAL SI FICHIER DE SORTIE EXPLICITEMENT OUVERT EN UTF8
				#$title = Encode::decode('utf-8', $title);
				print G "<title text=\"$title_xml\"/>\n";
			}
			my $data = "";
			my $copy = "";
			my $cpt_p = 0;
			my $text = "";
			my $text_xml = "";
			if($news =~ /<DataContent>/)
			{
				print G "<data>\n";
				$data = $&.$';
				$data =~ s/<quote>//g;
				$data =~ s/<\/quote>//g;
				while($data =~ /<p>([^<]+)<\/p>/)
				{
					$cpt_p++;
					my $para = $1;
					$data = $';
					if($para =~ /^\s*[-a-z]+\s*(\/\s*[-a-z]+)+\s*$/)
					{
						$text .= "<p rank=\"$cpt_p\" signature=\"true\" text=\"$para\"/>\n";
						$text_xml = $para;
						$text_xml =~ s/"/&quot;/g;
						print G "<p rank=\"$cpt_p\" signature=\"true\" text=\"$text_xml\"/>\n";							
					}
					else
					{
						$text .= "<p rank=\"$cpt_p\">"."\n".$para."\n"."</p>"."\n";
						$text_xml = $para;
						$text_xml =~ s/"/&quot;/g;
						print G "<p rank=\"$cpt_p\" text=\"$text_xml\"/>\n";	
					}					
				}
				print G "</data>\n";
				print G "<sxpipe>\n";
				print G "<title>\n$title_text\n</title>\n";
				print G $text;
				print G "</sxpipe>\n";
				print G "</news>\n";
			}
		}
	}		
}


__END__

