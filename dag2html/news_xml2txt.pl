#!/usr/bin/env perl
# $Id: modifs sur xml2txt par Stern 300509
use warnings;
use strict;
# Transforme un corpus XML dont TOUTES les balises forment une ligne à elles toutes seules

$| = 1;

my $line = "";
while (<>) 
{
#	print STDERR $_;<STDIN>;
	$line = $_;
	$line =~ s/[­·]/-/g;
	$line =~ s/œ/oe/g;
	$line =~ s/€/EUR/g;
	$line =~ s/¥/YEN/g;
	$line =~ s/(<[^>]+>)/{$1} _XML/g;
	print $line;
}

