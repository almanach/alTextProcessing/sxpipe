#!/usr/bin/env perl

# Ce nombre contient trois z�ros et deux deux
# douze douzaines
# vingt et uni�me
# million de milliardi�mes
# quinze milli�mes

$| = 1;

$lang="fr";

# variables
$number	= qr/{([^}]+)} *_NUMBER/o ;
$link	= qr/{([^}]+)} *(?:et|de)/oi ;
$ieme   = qr/{([^}]+)} *(?:z�roi�mes?|uni�mes?|deuxi�mes?|troisi�mes?|quatri�mes?|cinqui�mes?|sixi�mes?|septi�mes?|huiti�mes?|neuvi�mes?|dixi�mes?|onzi�mes?|douzi�mes?|treizi�mes?|quatorzi�mes?|quinzi�mes?|seizi�mes?|vingti�mes?|trenti�mes?|quaranti�mes?|cinquanti�mes?|soixanti�mes?|quatre-vingti�mes?|centi�mes?|milli�mes?|millioni�mes?|milliardi�mes?|tiers|quarts?)/oi;   # ni�me ?
$aine   = qr/{([^}]+)} *(?:huitaines?|dixaines?|douzaines?|quinzaines?|vingtaines?|trentaines?|quarantaines?|cinquantaines?|soixantaines?|centaines?|milliers?)/oi;


while (<>) {
  chomp;

  # normalisation
  s/_NUM /_NUMBER /g ;
  while (s/(^| )$number $number( |$)/$1\{$2 $3} _NUMBER$4/goi) {}
  
  # reconnaissance
  ### -i�mes et -aines
  s/(^| )$number $ieme( |$)/$1\{$2 $3} _NUMBER$4/goi;
  s/(^| )$number $link $ieme( |$)/$1\{$2 $3 $4} _NUMBER$5/goi;
  s/(^| )$number $aine( |$)/$1\{$2 $3} _NUMBER$4/goi;
  s/(^| )$ieme( |$)/$1\{$2} _NUMBER$3/goi;
  s/(^| )$aine( |$)/$1\{$2} _NUMBER$3/goi;
  
  ### petite verrue pour reconna�tre les dates comportant des mois corrig�s par sxspeller
  $month = qr/{([^}]+)} *(?:janvier|f�vrier|mars|avril|mai|juin|juillet|ao�t|septembre|octobre|novembre|d�cembre)/oi ;
  s/(^| )$number $month $number( |$)/$1\{$2 $3 $4} _DATE_arto$5/goi;
  s/(^| )$number $month( |$)/$1\{$2 $3} _DATE_arto$4/goi;
  s/(^| )$month $number( |$)/$1\{$2 $3} _DATE_year$4/goi;

  s/  +/ /g;
  print "$_\n";
}


# GRAMMAIRE ORIGINELLE
# **** not  L A L R ( 1 ) ****
# *Conflicts | Shift-Reduce  | Reduce-Reduce
# *-----------------------------------------
# *          | User | System | User | System
# *          -------------------------------
# *   112    | 0    |  112   | 0    |  0  

# <axiome>		= <entities> ; 3

# <entities>		= <entities> <entity> ;
# <entities>		= <entity> ;

# <entity>		= <word> ;
# * @2 sort _NUMBER
# <entity>		= <number> ; 2
# * @5 sort _DATE
# <entity>		= <date> ; 5

# <word>			= de ; 1
# <word>			= et ; 1
# <word>			= %WORD ; 1
# <word>			= "/" ; 1
# <word>			= janvier ; 1
# <word>			= f�vrier ; 1
# <word>			= mars ; 1
# <word>			= avril ; 1
# <word>			= mai ; 1
# <word>			= juin ; 1
# <word>			= juillet ; 1
# <word>			= ao�t ; 1
# <word>			= septembre ; 1
# <word>			= octobre ; 1
# <word>			= novembre ; 1
# <word>			= d�cembre ; 1

# * &1 : TRUE ssi le terminal suivant est un <ieme>

# * Ce nombre contient trois z�ros et deux deux
# <number>		= <_NUMBERs> ;
# * vingt et uni�me
# * @4 : Permet de concatener les commentaires
# <number>		= <_NUMBERs> et &1 <ieme> ; 4
# * million de milliardi�mes
# <number>		= <_NUMBERs> de &1 <ieme> ; 4
# * quinze milli�mes
# * &1 : TRUE ssi le terminal suivant est un <ieme>
# *<number>		= <_NUMBERs> &1 <ieme> ; 4
# * On laisse la prio shift systeme s'en charger
# <number>		= <_NUMBERs> <ieme> ; 4
# * douze douzaines
# * &4 : TRUE ssi le terminal suivant est un <aine>
# *<number>		= <_NUMBERs> &4 <aine> ; 4
# * On laisse la prio shift systeme s'en charger
# <number>		= <_NUMBERs> <aine> ; 4
# <number>		= <ieme> ;
# <number>		= <aine> ;

# * &2 : TRUE ssi le terminal suivant est un _NUMBER
# <_NUMBERs>		= <_NUMBERs> &2 <_NUMBER> ; 4
# <_NUMBERs>		= <_NUMBER> ;

# * en lettres ...
# <_NUMBER>		= _NUMBER ;
# * en chiffres ...
# <_NUMBER>		= _NUM ;

# *<ieme>			= ni�me ; !!

# <ieme>			= z�roi�me ;
# <ieme>			= z�roi�mes ;
# <ieme>			= uni�me ;
# <ieme>			= uni�mes ;
# <ieme>			= deuxi�me ;
# <ieme>			= deuxi�mes ;
# <ieme>			= troisi�me ;
# <ieme>			= troisi�mes ;
# <ieme>			= quatri�me ;
# <ieme>			= quatri�mes ;
# <ieme>			= cinqui�me ;
# <ieme>			= cinqui�mes ;
# <ieme>			= sixi�me ;
# <ieme>			= sixi�mes ;
# <ieme>			= septi�me ;
# <ieme>			= septi�mes ;
# <ieme>			= huiti�me ;
# <ieme>			= huiti�mes ;
# <ieme>			= neuvi�me ;
# <ieme>			= neuvi�mes ;

# <ieme>			= dixi�me ;
# <ieme>			= dixi�mes ;
# <ieme>			= onzi�me ;
# <ieme>			= onzi�mes ;
# <ieme>			= douzi�me ;
# <ieme>			= douzi�mes ;
# <ieme>			= treizi�me ;
# <ieme>			= treizi�mes ;
# <ieme>			= quatorzi�me ;
# <ieme>			= quatorzi�mes ;
# <ieme>			= quinzi�me ;
# <ieme>			= quinzi�mes ;
# <ieme>			= seizi�me ;
# <ieme>			= seizi�mes ;

# <ieme>			= vingti�me ;
# <ieme>			= vingti�mes ;
# <ieme>			= trenti�me ;
# <ieme>			= trenti�mes ;
# <ieme>			= quaranti�me ;
# <ieme>			= quaranti�mes ;
# <ieme>			= cinquanti�me ;
# <ieme>			= cinquanti�mes ;
# <ieme>			= soixanti�me ;
# <ieme>			= soixanti�mes ;
# <ieme>			= quatre-vingti�me ;
# <ieme>			= quatre-vingti�mes ;

# <ieme>			= centi�me ;
# <ieme>			= centi�mes ;

# <ieme>			= milli�me ;
# <ieme>			= milli�mes ;

# <ieme>			= millioni�me ;
# <ieme>			= millioni�mes ;

# <ieme>			= milliardi�me ;
# <ieme>			= milliardi�mes ;

# <aine>			= huitaine ;
# <aine>			= huitaines ;
# <aine>			= dixaine ;
# <aine>			= dixaines ;
# <aine>			= douzaine ;
# <aine>			= douzaines ;
# <aine>			= quinzaine ;
# <aine>			= quinzaines ;
# <aine>			= vingtaine ;
# <aine>			= vingtaines ;
# <aine>			= trentaine ;
# <aine>			= trentaines ;
# <aine>			= quarantaine ;
# <aine>			= quarantaines ;
# <aine>			= cinquantaine ;
# <aine>			= cinquantaines ;
# <aine>			= soixantaine ;
# <aine>			= soixantaines ;

# <aine>			= centaine ;
# <aine>			= centaines ;

# <aine>			= millier ;
# <aine>			= milliers ;

# * fait par benoit ...
# *<date>			= _NUM "/" _NUM "/" _NUM ; 4
# *<date>			= _NUM "/" _NUM ; 4
# <date>			= _NUM <month _NUM> ; 4
# <date>			= <month _NUM> ; 4
# <date>			= _NUM <month> ; 4

# <month>			= janvier ;
# <month>			= f�vrier ;
# <month>			= mars ;
# <month>			= avril ;
# <month>			= mai ;
# <month>			= juin ;
# <month>			= juillet ;
# <month>			= ao�t ;
# <month>			= septembre ;
# <month>			= octobre ;
# <month>			= novembre ;
# <month>			= d�cembre ;

# <month _NUM>		= janvier _NUM ; 4
# <month _NUM>		= f�vrier _NUM ; 4
# <month _NUM>		= mars _NUM ; 4
# <month _NUM>		= avril _NUM ; 4
# <month _NUM>		= mai _NUM ; 4
# <month _NUM>		= juin _NUM ; 4
# <month _NUM>		= juillet _NUM ; 4
# <month _NUM>		= ao�t _NUM ; 4
# <month _NUM>		= septembre _NUM ; 4
# <month _NUM>		= octobre _NUM ; 4
# <month _NUM>		= novembre _NUM ; 4
# <month _NUM>		= d�cembre _NUM ; 4
