#!/usr/bin/env perl

use strict;

my $P;
my @A;
my $no_prios = 1;

while (<>) {
    chomp;
    s/^#.*//;
    /^\s*[0-9]+\s+([0-9]+).*: [^*].*> *= */ || next;
    $P = $1;
    if (/;\s*(-?[0-9]+)\s*$/) {
	$A[$P] = $1;
	if ($1 != 0) {
	  $no_prios = 0;
	}
    } elsif (/;\s*$/) {
	$A[$P] = 0;
    }
    last if (/CODE  NON_TERMINAL/);
}

if ($no_prios) {
  print "#define has_prios 0\n";
} else {
  print "#define has_prios 1\n";
  print "int prod2prio[]={0,\n";
  for $P (1..$#A) {
    if ($A[$P] != 0) {
      print $A[$P].", ";
    } else {
      print "0, ";
    }
    if ($P % 10 == 0) {
      print "\n";
    }
  }
  if ($#A % 10 != 0) {
    print "\n";
  }
  print "};\n";
}

