=pod
    PROGRAMME      unitex2sxpipe - lex2
    DESCRIPTION    ajoute les terminaux fléchis au lexique
    AUTEUR         Antoine Désir
=cut
use strict;

my $quote = "'";
my $dquote = '"';
my $lim = "===";
my $fileIn;
my $fileOut;
my $conf;

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-in$/) { $fileIn = shift ; }
	elsif (/^-out$/) { $fileOut = shift ; }
	elsif (/^-lim$/) { $lim = shift ; }
	elsif (/^-quote$/) { $quote = shift ; }
	elsif (/^-config$/) { $conf = shift ; }
}

open(IN,"<$fileIn") or die ("err in");
open(OUT,">$fileOut") or die ("err out");

while(<IN>){
	chomp;
	if (/<T> = $dquote=/) { next; }
	/<T> = $dquote(.+?)$dquote *;/;
	if (defined $1){
		my $out1 = "$1";
		my $out2 = "$1";
		$out2 =~ s/\\"/"/g;   # hack pour """
		$out2 =~ s/'/\\'/g;   # hack pour "'"
		print OUT "\"$out1\"\t\t$quote$out2$quote\t\[\] ;\n";
	}
}
close IN;

# ajouts du fichier de config
open (CONF,"<$conf") or die ("err conf");
my $num;
my $sousNum;
while (<CONF>){
	chomp;
	if ($_ eq "") { next;}
	if (/^____(.+?) /) { 
		$num = $1 ;
		next ;
	}
	if ($num == 1) {
		/\t(.+?)\t(.+)/;
		print OUT "\"$1\"\t\t$2\t[] ;\n";
	}
	if ($num == 4) {
		if (/^----(.+?) /) { 
			$sousNum = $1 ;
			next ;
		}
		/(.+)\t(.+)/;
		if ($sousNum == 2){
			print OUT "\"$1\"\t\t$quote$lim$2$lim$quote\t[] ;\n";
		}
		else {
			print OUT "\"$1\"\t\t$2\t[] ;\n";
		}
	}
}

# fioriture
print OUT "\"__ANY__\"\t\t__any__\t[] ;\n" ;

close OUT;
print "lex2 ok\n";
