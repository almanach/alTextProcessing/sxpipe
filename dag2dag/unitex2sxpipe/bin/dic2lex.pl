#!/usr/bin/env perl

use strict;

my $repDicUser = "dic";
my $repDicUnitex = "../unitex2sxpipe/Dela";
my $fileOut = "Fdic.lextmp";
my $fileIn;
my $conf;

my $lim1 = "===";
my $quote = "'";

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-usrdir$/) { $repDicUser = shift ; }
	elsif (/^-unitexdir$/) { $repDicUnitex = shift ; }
	elsif (/^-in$/) { $fileIn = shift ; }
	elsif (/^-out$/) { $fileOut = shift ; }
	elsif (/^-lim$/) { $lim1 = shift ; }
	elsif (/^-quote$/) { $quote = shift ; }
	elsif (/^-config$/) { $conf = shift ; }
}
my $lim2 = $lim1;

open(CONF,"<$conf") or die ("err conf");
my %newPOS;
my $num;
while (<CONF>){
	chomp;
	if ($_ eq "") {next;}
	if (/^____(.+?) /) { $num = $1 ; next ; }
	if ($num == 7)	{
		$newPOS{$_} = 1;
		# ex : $newPOS{"PERSONNE"} = 1
	}
}
$num = undef;

#open(DIC,"cat ${repDicUser}/*.dic ${repDicUnitex}/*.dic | recode unicode..l1 | grep \"^\\(lentement\\|arbre\\|semaine\\)\" |") or die ("err dic");
open(DIC,"cat ${repDicUser}/*.dic ${repDicUnitex}/*.dic | recode unicode..l1 |") or die ("err dic");

my %dic;
while (<DIC>) { # stocke toutes les entr�es des .dic dans %dic
  chomp;
  next if /^\//;
  my $tmp = "tmp> $_ <tmp";
  my $mot;
  if (/^(.+?),/) {
    $mot = $1;
  }
  # else { print "$tmp\n"; }
  $mot =~ s/\\([-.])/\1/g;
  if ($mot =~ /\\/) {
    print "$mot\n";
  }
  $mot =~ s/\\/\\\\/g;		# hack pour backslash
  $mot =~ s/\"/\\\"/g;		# hack pour double quote
  my $pos;
  if (/\.([A-Z]+?)(\+|:|$)/) {
    $pos = $1;
  }
  $mot = underscore($mot);
  my $num = 1;
  while (exists $dic{$pos}{"${mot}___$num"}) {
    $num++ ;
  }
	
  if (/\+([A-Za-z0-9+]+)/) {
    my $t = $1 ;
    $t =~ s/\+/__/g ;
    foreach my $trait (split("__",$t)) {
      $dic{$pos}{"${mot}___$num"}{"Traits"}{$trait} = 1;
      # ex : $dic{"N"}{"Zimbabw�en___1"}{"Traits"}{"Hum"} = 1
    }
  }
  while (/:([A-Za-z0-9]+)/g) {
    $dic{$pos}{"${mot}___$num"}{"Flexion"}{$1} = 1;
    # ex : $dic{"N"}{"Zimbabw�en___1"}{"Flexion"}{"ms"} = 1
    foreach my $flex (split("",$1)) {
      $dic{$pos}{"${mot}___$num"}{"Flexion"}{$flex} = 1;
      # ex : $dic{"N"}{"Zimbabw�en___1"}{"Flexion"}{"m"} = 1
    }
  }
}
close DIC;
print "dic\tok\n";

# for my $t (keys %{$dic{ADV}{"lentement___1"}}) {
#   print STDERR "$t\n";
#   for (keys %{$dic{ADV}{"lentement___1"}{$t}}) {
#     print STDERR "\t$_\t".$dic{ADV}{"lentement___1"}{$t}{$_}."\n";
#   }
# }

open(TMP,">>$fileIn") or die ("err tmp");
foreach (keys %newPOS) {
  print TMP "$lim1$_$lim2\n";
}
close TMP;

open(IN,"<$fileIn") or die ("err in");

my %plusMoins;
my %tousTraits;
my %traitNeg;

while (<IN>) {
  /$lim1(.+?)([+-].+?)?(:.+)?$lim2/;
  my $pos = $1;
  my $traits = $2;
  my $flexion = $3;
  my $term = $& ;
  $flexion =~ s/:// ;
  $plusMoins{$term}{"POS"} = $pos;
  $plusMoins{$term}{"Flexion"} = $flexion;

  while ($traits =~ /([+-])([A-Za-z0-9]+)/g) {
    my $signe;
    my $trait = $2;
    if ($1 eq "\+") {
      $signe = 1;
    }
    if ($1 eq "-") {
      $signe = -1;
      $traitNeg{$pos}{$flexion} = 1;
    }
    $plusMoins{$term}{"Traits"}{$trait} = $signe;
    $tousTraits{$pos}{$flexion}{$trait} = 1;
  }
}
close IN;

open(OUT,">$fileOut.tmp") or die ("err out");

foreach my $term (keys %plusMoins) {
  my $pos = $plusMoins{$term}{"POS"};
  my $flexion = $plusMoins{$term}{"Flexion"};
  print STDERR "$term\t$pos\t$flexion\n";
 mot:foreach my $mot (sort keys %{$dic{$pos}}){
    if ($flexion ne "" && !exists($dic{$pos}{$mot}{"Flexion"}{$flexion})) {
      next mot;
    }
    my $term2 = $pos;
    if ($traitNeg{$pos}{$flexion}) {
      foreach my $trait (sort keys %{$tousTraits{$pos}{$flexion}}) {
	if (exists($dic{$pos}{$mot}{"Traits"}{$trait})) {
	  if ($plusMoins{$term}{"Traits"}{$trait} == -1) {
	    next mot;
	  }
	  $term2 = $term2."\+".$trait ; 
	} elsif (!exists($dic{$pos}{$mot}{"Traits"}{$trait})) {
	  $term2 = $term2."-".$trait ;
	} else {
	  next mot;
	}
      }
      if ($flexion ne "") {
	$term2 = $term2.":".$flexion ;
      }
      $term2 = $lim1.$term2.$lim2 ;
    } else {
      $term2 = $term ;
    }
    $mot =~ /(.+)___/;
    print OUT "\"$1\"\t\t$quote$term2$quote\t\[\] ;\n";
  }
}
close OUT;

system ("LC_ALL=C sort -u $fileOut.tmp > $fileOut");

print "dic2lex\tOK\n";

sub underscore {
  my ($sortie) = @_;
  $sortie =~ s/ /_/g;
  return $sortie ;
}


__END__

=pod

=head1 NAME

    unitex2sxpipe - dic2lex

=head1 DESCRIPTION

    ajoute au lexique (.lex) les entr�es utiles du type <ADV+neg>, � partir des lexiques d'Unitex (.dic)

=head1 AUTHOR

    Antoine D�sir & Beno�t Sagot

=cut
