#!/usr/bin/env perl
use DBI;
use Encode;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $lang = shift || die "Please specify a language";
my $dir = shift || ".";

my $vars_dbh = DBI->connect("dbi:SQLite:$dir/ne_vars.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $vars_sth = $vars_dbh->prepare('select key,variant from data order by key;');
my $refs_dbh = DBI->connect("dbi:SQLite:$dir/ne_refs.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $refs_sth = $refs_dbh->prepare('select key,type,weight,country_code,geonames_type from data order by key;');

my %lang_country = (
		 "fr" => { "FR" => 1 },
		 "en" => { "US" => 1, "GB" => 1, "IE" => 1 },
		 "es" => { "ES" => 1 },
		 "de" => { "DE" => 1 },
		);

my $n = 0;
$refs_sth->execute();
while (my @tmpinfo = $refs_sth->fetchrow) {
  $id = sprintf("%.0f", $tmpinfo[0]);
  $id =~ s/^0*//;
  $n++;
  print STDERR "$id\r" if ($n % 1000 == 0);
  $data = Encode::decode("utf8",$tmpinfo[1]);
  $data =~ s/^_/\1/ || next;
  $weight = Encode::decode("utf8",$tmpinfo[2]);
  $country_code = $tmpinfo[3];
  $geonames_type = $tmpinfo[4];
  unless (
	  ($weight <= 4 && $data =~ /^PERSON/)
	  || ($lang eq "en" && $weight <= 10 && $data =~ /^PERSON/)
	  || ($lang eq "fr" && $weight <= 1 && $data !~ /^(COMPANY|ORGANIZATION)/ && !defined($lang_country{$lang}{$country_code}) && $geonames_type !~ /^(L\.[CR]|PPLC|S|A)/)
	  || ($lang eq "en" && $weight <= 3)
	  || ($lang eq "en" && $data =~ /^(WORK|PRODUCT)/)
	 ) {
    $id2type{sprintf("%.0f", $id)} = $data;
  }
}
print STDERR "\n";

$n = 0;
$vars_sth->execute();
while (my @tmpinfo = $vars_sth->fetchrow) {
  $id = sprintf("%.0f", $tmpinfo[0]);
  $id =~ s/^0*//;
  $n++;
  print STDERR "$id\r" if ($n % 1000 == 0);
  $data = Encode::decode("utf8",$tmpinfo[1]);
  $type = $id2type{sprintf("%.0f", $tmpinfo[0])};
  $data =~ s/^\s+//;
  $data =~ s/\s+$//;
#  next if $data !~ /^\S*[A-ZÉÀÈÊÂÎÛÔÇ0-9]/;
  next if $data !~ /[A-ZÉÀÈÊÂÎÛÔÇ]/;
  $data =~ s/"//g;
  $data =~ s/\`/'/g;
  $data =~ s/ /_/g;
  if ($type =~ /^PERSON/) {
    if ($data =~ /^[a-zé]+_/) {
      print "\"$data\"\t\tx".lc($type)."\t[] ;\n" if ($data =~ /..../);
    } else {
      print "\"$data\"\t\t".lc($type)."\t[] ;\n" if ($data =~ /.../);
    }
  } elsif ($type =~ /^(?:LOCATION)$/) {
    next if ($data =~ /^[DL]es$/);
    next unless ($id =~ /^2000.{12}$/);
    print "\"$data\"\t\t".lc($type)."\t[] ;\n" if ($data =~ /.../);
  } elsif ($type =~ /^(?:ORGANIZATION|COMPANY|PRODUCT)$/) {
    print "\"$data\"\t\t".lc($type)."\t[] ;\n" if ($data =~ /../);
  }
}
