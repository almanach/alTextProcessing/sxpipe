#!/usr/bin/env perl
use DBI;
use Encode;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-d$/) {$dir=shift;}
}

my $dbh = DBI->connect("dbi:SQLite:$dir/ne.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $sth = $dbh->prepare('select value from data where key=?;');
my $lefff_dbh = DBI->connect("dbi:SQLite:$dir/lefff.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth = $lefff_dbh->prepare('select value from data where key=?;');
my $first_names_dbh = DBI->connect("dbi:SQLite:$dir/first_names.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $first_names_sth = $first_names_dbh->prepare('select value from data where key=?;');
my $lefff_locations_dbh = DBI->connect("dbi:SQLite:$dir/lefff_locations.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_locations_sth = $lefff_locations_dbh->prepare('select value from data where key=?;');


my %is_in_lefff;
my %is_in_lefff_locations;

my %cache;
my %entity;
my $contains_first_token = 0;
$country_context{FR} = 1;
$city_context{Paris} = 1;

while (<>) {
  chomp;

#  print STDERR "="x40 ."\n";

  if (is_new_doc($_)) {
    for (keys %entity) {
      $entity{$_} /= 2;
    }
    %country_context = ();
    $country_context{FR} = 1;
    %city_context = ();
    $city_context{Paris} = 1;
    %temporal_context = ();
  }

  while (s/([\(\|]\s*)\((\s*{([^}]*)}\s*\S+\s*(?:\|\s*{\3}\s*\S+\s*)+)\)(\s*[\|\)])/\1\2\4/g) {}

  %best_choice_count = ();
  %best_choice = ();
  %best_choice_type = ();
  %seen_tokens = ();
  %ttc2tokens_content = ();
  %ttc2tokens = ();
  %seen = ();

  while (/{([^}]*)}\s*(_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
    next if (defined($seen{"$1 ### $2"}));
    $seen{"$1 ### $2"} = 1;
    $tokens = $1;
    $semlex = $4;
    $subdag = $5;
    $tokens_content = $tokens;
    $type = $2;
    $type_ext = $3;
    $tokens_content =~ s/<F.*?>//g;
    $tokens_content =~ s/<\/F>//g;
    if ($subdag ne "" && $tokens_content =~ /\b($subdag)\b/i) {
      $tokens_content = $1;
    }
    ### ROSA SAYS ?
    $is_safe = ($type_ext ne "");
    if ($is_safe) {$re = quotemeta($tokens); s/{$re}(\s*$type)\d+/{$tokens}$1/;}
    ### END
    $tokens_and_tokens_content = $tokens ." ### ". $tokens_content;
    $ttc2tokens_content{$tokens_and_tokens_content} = $tokens_content;
    $ttc2tokens{$tokens_and_tokens_content} = $tokens;
    $re = quotemeta ("{$tokens}")."\\s*".$type."\\s*".quotemeta($semlex);
    s/$re/{$tokens} $type/g;
    if ($tokens =~ /\"E\d+F1\"/) {$contains_first_token = 1} else {$contains_first_token = 0}
    my $tmp = "$tokens_content ### $type";
    if (!defined($cache{$tmp})) {
      fill_cache ($tokens_content,$type,$tmp);
      if ($tokens_content =~ /^M\. (.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
      ### ROSA SAYS ? ON (lieutenant-)général
      if ($type =~ /^_PERSON/ && $tokens_content =~ /^(?:(?:Mr|Mll?e|Mme|Me|Pr|Dr)s?\.?|(?:lieutenant-)?général) (.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
      ### END
      ### ROSA SAYS ? WHY NOT PUT THIS IN THE BNF? (AND CONTROL WHAT FOLLOWS?)
      elsif ($tokens_content =~ /^(?:ville|village|agglomération|cité|localité|district|commune|région|département) (?:du|de l[a']|de|d')(.*)$/i) {
	fill_cache ($1,$type,$tmp);
      } elsif ($tokens_content =~ /^(?:.*?'|[a-zé'].*?-)(.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
    }
    if (!defined($cache{$tmp})) { # && ($tmp =~ /^[A-Z­°²\´'ÂÇÈÊÉÎÏÖÜŸ -]+###/ || $tmp =~ /^[a-z­°²\´'àáâäçèéêëíîïñóôöùûüÿ -]+###/)
      $tmp2 = $tokens_content;
      $tmp2 =~ s/(^| )(.)([^ ]*)/$1.uc($2).lc($3)/ge;

      fill_cache ($tmp2,$type,$tmp);
      if ($tmp2 =~ /^M\. (.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
      if ($type =~ /^_PERSON/ && $tmp2 =~ /^(?:(?:Mr|Mll?e|Mme|Me|Pr|Dr)s?\.?|(?:lieutenant-)?général) (.*)$/) {
	fill_cache ($1,$type,$tmp);
      } elsif ($tmp2 =~ /^(?:ville|village|agglomération|cité|localité|district|commune|région|département) (?:du|de l[a']|de|d')(.*)$/i) {
	fill_cache ($1,$type,$tmp);
      } elsif ($tmp2 =~ /^(?:.*?'|ex-)(.*)$/i) {
	fill_cache ($1,$type,$tmp);
      }
    }
    if (!defined($cache{$tmp})) {
      $type =~ /^_(.*)$/;
      $tmpinfo = "$tokens_content\t\\(UNKNOWN $1\\)";
      $tmpinfo =~ s/\t/___/g;
      $tmpinfo =~ s/ /_/g;
      $cache{$tmp}{$tmpinfo} = 0.5;
    }
#    $changed_best_choice = 0;
    if (!defined ($best_choice{$tokens_and_tokens_content})) {$best_choice{$tokens_and_tokens_content} = ""}
    for (keys %{$cache{$tmp}}) {
      my $local_weight = $cache{$tmp}{$_};
      if ($type eq "_LOCATION" && /Geoname_\d+_([A-Z]{2,})_/ && defined($country_context{$1})) {$local_weight += 0.001}
#      print STDERR "\$entity{$_} = $entity{$_} + $local_weight\n";
      if ($best_choice_count{$tokens_and_tokens_content} <= $entity{$_} + $local_weight) {
#	  print STDERR "is_dubious($tokens_content, $type, $_, $local_weight, $is_safe) = ".is_dubious($tokens_content, $type, $_, $local_weight, $is_safe)."\n";
	if ($entity{$_} == 0 && is_dubious($tokens_content, $type, $_, $local_weight, $is_safe)) {
	  next;
	}
	$best_choice{$tokens_and_tokens_content} = $_;
	$best_choice_count{$tokens_and_tokens_content} = $entity{$_} + $local_weight;
	$best_choice_type{$tokens_and_tokens_content} = $type;

#	$changed_best_choice = 1;
      }
    }
#    if ($changed_best_choice == 1) {
#      print STDERR "$best_choice_count{$tokens_and_tokens_content}\n";
#      print STDERR "\$entity{$best_choice{$tokens_and_tokens_content}} = $entity{$best_choice{$tokens_and_tokens_content}}\t$best_choice_count{$tokens_and_tokens_content}\n";
#    }
  }
  for $tokens_and_tokens_content (keys %best_choice) {
    $tokens_content = $ttc2tokens_content{$tokens_and_tokens_content};
    $tokens = $ttc2tokens{$tokens_and_tokens_content};
    #print STDERR "\$best_choice{$tokens_and_tokens_content} = $best_choice{$tokens_and_tokens_content}\n";
    if ($best_choice{$tokens_and_tokens_content} ne "") {
      my $type = $best_choice_type{$tokens_and_tokens_content};
      $suff = "";
      if ($best_choice{$tokens_and_tokens_content} =~ /\(UNKNOWN_/) {
	$suff = "__0"; # ça veut dire qu'on n'aime pas trop cette interprétation
      }
      $re = quotemeta($tokens);
      s/\((?:\s*{$re}\s*[\w\d_]+\s*\|\s*)*{$re}(\s*$type)\d*(?:__0)?\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$suff \[|$best_choice{$tokens_and_tokens_content}|]/g;#print "HERE1:\n$_\n";
      s/{$re}(\s*$type)\d*(?:__0)?($|[^ _]| [^\[]| \[[^\|])/{$tokens}$1$suff \[|$best_choice{$tokens_and_tokens_content}|]$2/g;#print "HERE2:\n$_\n$best_choice{$tokens_and_tokens_content}\n";
    } else {
      my $tmp = $tokens_content;
      $tmp =~ s/ /_/g;
      $tmp =~ s/([+?()\[\]])/\\\1/g;
      $tmp .= "__0"; # ça veut dire qu'on n'aime pas trop cette interprétation
      $re = quotemeta($tokens);
      s/\(\s*{$re}(\s*)_[\w\d_]+\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$tmp /g;#print "HERE3\n";
      s/{$re}(\s*)_[\w\d_]+([^\w\d_:]|$)/{$tokens}$1$tmp $2/g;#print "HERE4\n";
    }
  }
  # on enlève les chemins passant par des __0 s'il y en a d'autres
  while (s/(<F id[^>]*>[^<]*)(?<=[^\\])([(|)])/\1\\\2/g){}
  s/(^|[^\\])([(|)])/\1 \2 /g;
  s/(^|[^\\])([(|)])/\1 \2 /g;
  s/ *\[ +\| +/ \[\|/g;
  s/ +\| +\] */\|\] /g;
  s/  +/ /g;
#  print STDERR "$_\n";
### on vire d'abord ce qui n'a pas de [|...|], pour garder un truc avec [|...|] s'il y en a mais qu'il n'y a que des __0
  $linear_path_elt = qr/(?:\[\||\|\]|[^(|)]|\\[(|)])/o;
  while (s/ \| $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \| / \| /g) {}
  while (s/ \( $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \| / \( /g) {s/ \( ($linear_path_elt+?) \)/ \1/g;}
  while (s/ \| $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \) / \) /g) {s/ \( ($linear_path_elt+?) \)/ \1/g;}
#  print STDERR "$_\n";
  ### on vire
  while (s/ \| $linear_path_elt*?__0$linear_path_elt*? \| / \| /g) {}
  while (s/ \( $linear_path_elt*?__0$linear_path_elt*? \| / \( /g) {s/ \( ($linear_path_elt+?) \)/ \1/g;}
  while (s/ \| $linear_path_elt*?__0$linear_path_elt*? \) / \) /g) {s/ \( ($linear_path_elt+?) \)/ \1/g;}
  s/  +/ /g;
  s/__0 / /g;
  %seen = ();
  while (/{([^}]*)}\s*(_ADDRESS|_ADRESSE)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
    next if (defined($seen{"$1 ### $2"}));
    $seen{"$1 ### $2"} = 1;
    $tokens = $1;
    $type = $2;
    $tokens_content = $tokens;
    $tokens_content =~ s/<F.*?>//g;
    $tokens_content =~ s/<\/F>//g;
    $google_query = $tokens_content.", ";
    for (sort {$city_context{$b} <=> $city_context{$a}} keys %city_context) {
      $google_query .= "$_";
      last;
    }
    $google_query =~ s/ /+/g;
    $href = "http://maps.google.com/maps?q=$google_query&hl=fr";
    $re = quotemeta($tokens);
    s/{$re}\s*$type\d*(?: *(\[\|(.*?)\|\]))?/{$tokens} _LOCATION \[\|(address)____<a_href="$href">Google_maps<\/a>\|\]/g;
  }
  while (/{([^}]*)}\s*(_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
    next if (defined($seen{"$1 ### $2"}));
    $seen{"$1 ### $2"} = 1;
    $tokens = $1;
    $semlex = $4;
    $subdag = $5;
    $tokens_content = $tokens;
    $type = $2;
    $type_ext = $3;
    $tokens_content =~ s/<F.*?>//g;
    $tokens_content =~ s/<\/F>//g;
    $tokens_and_tokens_content = $tokens ." ### ". $tokens_content;
    $entity{$best_choice{$tokens_and_tokens_content}}+=$cache{$tokens_content." ### ".$type}{$best_choice{$tokens_and_tokens_content}};
#    print STDERR ">>>> \$entity{$best_choice{$tokens_and_tokens_content}} = $entity{$best_choice{$tokens_and_tokens_content}}\n";
    if ($type eq "_LOCATION") {
      if ($best_choice{$tokens_and_tokens_content} =~ /Geoname_\d+_([A-Z]{2,})_A\.PCLI/) {
	$country_context{$1}++;
	#      print STDERR "Spatial context: $1\n";
      } elsif ($best_choice{$tokens_and_tokens_content} =~ /Geoname_\d+_([A-Z]{2,})_A\.PCL/) {
	$city_context{$1}++;
      }
    }
    if ($best_choice{$tokens_and_tokens_content} =~ /\(UNKNOWN_(.*?)\)/) {
      fill_cache_subproc ($tokens_content, $best_choice{$tokens_and_tokens_content}, $type, "$tokens_content ### $type");
      $tokens_content =~ /(^| )([^ ]+)$/;
      my $ending = $2;
      unless (defined($new_entities_endings{$ending}) && defined($new_entities_endings{$ending}{$best_choice_type{$tokens_and_tokens_content}})) {
	$new_entities{"$tokens_content ### $type"} = $best_choice{$tokens_and_tokens_content};
	$new_entities_endings{$ending}{$type} = "$tokens_content ### $type";
      }
    }
  }
  print "$_\n";
}
$sth->finish;
$lefff_sth->finish;
$first_names_sth->finish;
$lefff_locations_sth->finish;
undef $sth;
undef $lefff_sth;
undef $first_names_sth;
undef $lefff_locations_sth;
$lefff_locations_dbh->disconnect;
$first_names_dbh->disconnect;
$lefff_dbh->disconnect;
$dbh->disconnect;


sub fill_cache {
  my $tokens_content = shift;
  my $type = shift;
  my $key = shift;
  return if (defined($cache{$key}));
  $sth->execute($tokens_content." ### ".$type);
  while (my $tmpinfo = $sth->fetchrow) {
    $tmpinfo = Encode::decode("utf8",$tmpinfo);
    fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key);
  }
  $sth->finish;
  $tokens_content =~ /(^| )([^ ]+)$/;
  my $ending = $2;
  if (defined($new_entities_endings{$ending}) && defined($new_entities_endings{$ending}{$type})) {
    fill_cache_subproc ($tokens_content, $new_entities{$new_entities_endings{$ending}{$type}}, $type, $key);
  }
  if ($type =~ /^_PERSON_[mf]$/) {
    $key =~ s/_[mf]$//;
    fill_cache ($tokens_content, "_PERSON", $key, 2);
  }
}

sub fill_cache_subproc {
  my $tokens_content = shift;
  my $tmpinfo = shift;
  my $type = shift;
  my $key = shift;
  $tmpinfo =~ s/\t#(?: (\d+))?/\t/;
  $tmpinfo =~ s/([(|)])/\\\1/g;
  $weight = $1 || 1;
  if ($weight > 0) {$weight = (log (1+$weight))/1000}
  if ($tmpinfo =~ /\(UNKNOWN_/) {$weight +=0.5}
  if ($tmpinfo =~ /Geoname \d+ \w* P\./) {$weight += 0.05}
    elsif ($tmpinfo =~ /Geoname \d+ \w* A\./) {$weight += 0.04}
  $tmpinfo =~ s/\t/___/g;
  $tmpinfo =~ s/ /_/g;
  $weight = 1 + $weight unless /^<unknown>$/;
  if (($tokens_content !~ / / && $type =~ /^_PERSON/)
      || ($tokens_content =~ / / && $type eq "_LOCATION")) {
    $weight -=0.1;
  }
  if ($type eq "_ORGANIZATION") {$weight -= 0.05}
  $cache{$key}{$tmpinfo} = $weight;
  if ($key =~ /_PERSON$/) {
    $cache{$key."_f"}{$tmpinfo} = $weight-0.001;
    $cache{$key."_m"}{$tmpinfo} = $weight-0.001;
  }
}


sub is_dubious {
  my $tokens_content = shift;
  my $type = shift;
  my $def = shift;
  my $weight = shift;
  my $is_safe = shift;
  if ($is_safe) {return 0}
  if ($type =~ /^_PERSON/) {
    if ($tokens_content =~ /^([^ ]+) /) {
      fill_is_in_lefff ($1);
      if (defined($is_in_lefff_locations{$1})) {return 1}
    }
    if ($tokens_content =~ /^M\. /) {return 1}
    if ($tokens_content =~ / / && $tokens_content !~ /^[a-z]+ [^ ]+$/) {return 0}
    if ($weight > 0.9061 && $tokens_content !~ /^[a-z]+ [^ ]+$/) {return 0}
    fill_is_in_lefff ($tokens_content);
    if ($tokens_content =~ /^[a-z]+ ([^ ]+)$/) {fill_is_in_lefff ($1);}
    if (defined($is_in_lefff{$tokens_content}) >= 2) {return 1}
    elsif ($tokens_content =~ /^[a-z]+ ([^ ]+)$/ && defined($is_in_lefff{$1}) && $is_in_lefff{$1} >= 2) {return 1}
    $tokens_content_re = quotemeta ($tokens_content);
    if ($def =~ /^$tokens_content_re\_/) {return 1}
    $def =~ /^(.)(.*)$/;
    if (remove_diacritics ($1) ne $1) {
      $def =~ s/^(.)/remove_diacritics($1)/e;
      if ($def =~ /^$tokens_content_re\_/) {return 1}
    }
    $tokens_content =~ /^(.)(.*)$/;
    if (remove_diacritics ($1) ne $1) {
      $tokens_content =~ s/^(.)/remove_diacritics($1)/e;
      $tokens_content_re = quotemeta ($tokens_content);
      if ($def =~ /^$tokens_content_re\_/) {return 1}
    }
    if ($tokens_content !~ / /) {return 1} # violent, on va tenter
  } elsif ($type =~ /_LOCATION/ || $type =~ /_ORGANIZATION/) {
    if ($tokens_content =~ / /) {return 0}
    if ($tokens_content =~ /^(?:Paris)$/) {return 0}
    if ($tokens_content =~ /^(?:Chirac)$/) {return 1}
#    if ($tokens_content =~ /^(?:[DLdl'])?(?:Paris)$/) {return 0}
    fill_is_in_lefff ($tokens_content);
    if ($is_in_lefff{$tokens_content} == 2) {return 1}
    if ($is_in_lefff{$tokens_content} >= 4) {return 1}
    if ($tokens_content =~ s/^[LDld]'//) {
      fill_is_in_lefff ($tokens_content);
      if ($is_in_lefff{$tokens_content} == 2) {return 1}
      if ($is_in_lefff{$tokens_content} >= 4) {return 1}
    }
  }
  return 0;
}

sub fill_is_in_lefff {
  my $tokens_content = shift;
  if (!defined($is_in_lefff{$tokens_content})) {
    $lefff_locations_sth->execute($tokens_content);
    if ($lefff_locations_sth->fetchrow) {
      $lefff_locations_sth->finish;
      $is_in_lefff_locations{$tokens_content} = 1;
      $is_in_lefff{$tokens_content} = 0;
      return;
    }
    $lefff_locations_sth->finish;
    $lefff_sth->execute($tokens_content);
    if ($lefff_info = $lefff_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 1;
    }
    $lefff_sth->finish;
    $lefff_sth->execute(lc($tokens_content));
    if ($lefff_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 2;
    }
    $lefff_sth->finish;
    $first_names_sth->execute($tokens_content);
    if ($first_names_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 4;
    }
    $first_names_sth->finish;
  }
}

sub is_new_doc {
  my $line = shift;
  if ($line =~ /<news_item/ || $line =~ /<item/){
    #print STDERR "Changement de contexte: $_\n"; 
    return 1;
  }
  return 0;
}

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔ]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
