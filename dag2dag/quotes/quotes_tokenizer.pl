#!/usr/bin/env perl

use warnings;

use strict;

my $pair = 0;
my $quote_impair = 0;

while(<>)
  {

    $pair = 0;
    $quote_impair = 0;

    while(/{<F id=".+?">"<\/F>} "/)
      {
	$pair++;
	if($pair % 2 == 0)
	  {
	    s/({<F id=".+?">)(?:"|&quot;)(<\/F>}) (_EPSILON|"|&quot;)/${1}QUOTE_NO_SPACE_LEFT$2 _QUOTE_NO_SPACE_LEFT/;
	    $quote_impair = 0;
	  }
	else
	  {
	    s/({<F id=".+?">)(?:"|&quot;)(<\/F>}) (_EPSILON|"|&quot;)/${1}QUOTE_NO_SPACE_RIGHT$2 _QUOTE_NO_SPACE_RIGHT/;
	    $quote_impair = 1;
	  }
      }

    if ($quote_impair == 1)
      {
	s/QUOTE_NO_SPACE_LEFT/"/g;
	s/QUOTE_NO_SPACE_RIGHT/"/g;
	print;
	next;
      }

    print;

  }
