#!/usr/bin/env perl

use warnings;

use strict;

my @blacklist = map{chomp; $_;}<DATA>;

while(<>) {

    foreach my $el (@blacklist) {
      if ($_ =~ /$el/) {
	s/(_PERSON(?:_[mf]\d?)?)( \[|\d+:$el|\])/$1__notaqauthor$2/g;
      }
    }


    # guillemets entre parenth�ses -> notaquote
    while (s/(\([^\)]*_QUOTE_NO_SPACE_RIGHT) /$1__notaquote /g) {}

    # un seul mot entre guillemets -> notaquote
    while (s/(_QUOTE_NO_SPACE_RIGHT) ({[^}]+} [^{]+{[^}]+} _QUOTE_NO_SPACE_LEFT)/$1__notaquote $2/g) {}


    # person entre parenth�ses -> notaqauthor
    while (s/(\([^\)]*_PERSON(?:_[mf]\d?)?) /$1__notaqauthor /g) {}

    # person pr�c�d�e d'une pr�position etc. -> notaqauthor
    s/(\s+)(o�|de|d'|des|�|a|par|anti-_|pro-_|ex-_|_-_|affaire|que|qu'|contre|depuis|derri�re|devant|avec|apr�s|apr�s-_|avant|avant-_|chez|entre|envers|hormis|parmi|sans|sauf|sous|suivant|sur|vers|via|jusqu'�|si|lorsque|lorsqu'|et|comme|quand|dont|un|une|le|la|du)((?:\s+{[^}]+}\s+(?:M.|Mme))?)(\s+{[^}]+}\s+)(_PERSON(?:_[mf]\d?)?)(\s+)/$1$2$3$4$5__notaqauthor$6/gi;
    s/(_de|_d'|_que|_qu'|_�|_si|_o�)((?:\s+{[^}]+}\s+(?:M.|Mme))?)(\s+{[^}]+}\s+)(_PERSON(?:_[mf]\d?)?)(\s+)/$1$2$3$4__notaqauthor$5/g;

    # person pr�c�d�e d'une pr�position et d'une fonction ('de la ministre X') -> notaqauthor
    s/(\s+)(du|de_la)(\s+{[^}]+}\s+)(ministre|d�put�|d�put�e)((?:\s+{[^}]+}\s+(?:M.|Mme))?)(\s+{[^}]+}\s+)(_PERSON(?:_[mf]\d?)?)(\s+)/$1$2$3$4$5$6$7__notaqauthor$8/g;

    # person se trouvant dans une proposition introduite par "qui" -> notaqauthor
    s/(\s+qui\s+)([^,]*?)((?:\s+{[^}]+}\s+(?:M.|Mme))?)(_PERSON(?:_[mf]\d?)?)(\s+)/$1$2$3$4__notaqauthor$5/g;


    # verbe pr�c�d� d'un article, d'une pr�position etc. -> notaqverb
    s/(\s+)(le|la|de_la|l'|un|une|de|d'|par|ex-_|anti-_|_-_|point|sa|son|ce|cette)(\s+{[^}]+}\s+)(communiqu�|juge|formule|c�l�bre|presse|annonce|mesure|oppos�|temp�te|risque|analyse|attaque|commande|�bauche|menace|note|riposte|signe|demande|r�plique|titre)(\s+)/$1$2$3$4__notaqverb$5/gi;

    # verbe se trouvant dans une proposition introduite par "qui", "que", "dont" ou dont le sujet est "on" -> notaqverb
    s/(\s+)(qui|que|qu'|que_si|dont|on)(\s+)([^,]*?)(\s+)(soutient|soutenait|accuse|appelle|oppose|opposera|pr�sente|pr�sentait|d�fend|d�fendait|qualifie|qualifiait|demandait|opposait|demande|soutenu|accus�|appel�|oppos�|qualifi�|qualifiait|dit|dite|d�clar�|d�clar�e|d�menti|affirm�|�voque|r�pond)(\s+)/$1$2$3$4$5$6__notaqverb$7/gi;

    print;
    
}

__DATA__
Michel Debr�
Victor Hugo
Liliane Bettencourt
Sully-Andr� Peyre
Tina Turner
