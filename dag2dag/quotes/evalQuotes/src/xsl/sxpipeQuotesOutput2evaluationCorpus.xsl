<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Dec 9, 2009</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
  <xsl:output indent="yes" method="xml" encoding="utf-8"/>
    
  <xsl:template match="/child::*">
    <quotes_corpus>
      <xsl:apply-templates select="./news_item"/>
    </quotes_corpus>
  </xsl:template>
  
  
    <xsl:template match="news_item">
        <news_item>
            <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
            <xsl:attribute name="ref"><xsl:value-of select="./@ref"/></xsl:attribute>       
            <xsl:attribute name="eval_type"><xsl:value-of select="./@eval_type"/></xsl:attribute>
            <xsl:apply-templates select="./para"/>
        </news_item>
    </xsl:template>
      
  <!-- TEMPLATES FOR PARAGRAPHS DISPLAY -->
    <xsl:template match="para">
        <xsl:choose>
            <xsl:when test="./@signature">
                <para>
                    <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
                    <xsl:attribute name="signature"><xsl:value-of select="./@signature"/></xsl:attribute>                    
                </para>
            </xsl:when>
            <xsl:otherwise>                
                <para>
                    <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>      
                    <xsl:apply-templates/>
                </para> 
            </xsl:otherwise>
        </xsl:choose>       
    </xsl:template>
  
  <!--
Quote : <Q>
Auteurs : <AUT>, <AUT_CL>, <AUT_DEF>, <AUT_ORG>, <AUT_PRO>
Discours direct à l'initiale : <DDIQ> + <DDI_QV>
Discours indirect : <DIQ> + <DI_QV>
Se dire : <DQ> + <DQV>
Incise : <IQ> + <INC_QV>
Head verbatim avec IQ : <HQ>
Tail verbatim avec IQ : <TQ>
Prédicat prépositionnel : <PRQ> + <QPREP>
Verbtim isolé : <SQ>
Guillemets non citationnels: <NQ>
-->

    <xsl:template match="para/ENAMEX">
        <ENAMEX>
            <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
            <xsl:value-of select="."/>
        </ENAMEX>
    </xsl:template>
    
    <xsl:template match="NO_PRED/ENAMEX|IQ/ENAMEX|DIQ/ENAMEX|DDIQ/ENAMEX|DQ/ENAMEX|PRQ/ENAMEX|GUILLQ/ENAMEX|INTRANSQ/ENAMEX|HQ/ENAMEX|TQ/ENAMEX">
        <ENAMEX>
            <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
            <xsl:value-of select="."/>
        </ENAMEX>
    </xsl:template>    
    
    <xsl:template match="AUT/ENAMEX">
        <ENAMEX>
            <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
            <xsl:value-of select="."/>
        </ENAMEX>
    </xsl:template>    

    <xsl:template match="Q|SQ|NQ">
        <xsl:element name="{local-name()}">
            <xsl:if test="./@id"><xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute></xsl:if>
            <xsl:apply-templates/>
        </xsl:element>        
    </xsl:template>

    <xsl:template match="NO_PRED|IQ|DIQ|DDIQ|DQ|PRQ|GUILLQ|INTRANSQ|HQ|TQ|INC_QV|DI_QV|DDI_QV|GER_QV|DQV|QPREP|GUILL_QV|INTRANS_QV|AUT|AUT_PRO">
        <xsl:element name="{local-name()}">
            <xsl:if test="./@id"><xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute></xsl:if>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
</xsl:stylesheet>






