<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output indent="yes" method="html" encoding="utf-8"
    doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
    doctype-public="-//W3C//DTD XHTML 1.1//EN"/>

  <xsl:template match="/child::*">
    <html>
      <head>
        <title> AFP News - Metadata &amp; Quotes </title>
      </head>
      <style type="text/css">
        h4{background-color:#6D7B8D;text-align:center}
        h3{background-color:#E8E8E8;}
        table.head{background-color:#E8E8E8;}
        td.slugs{font-style:bold;}
        td.iptc{font-style:bold;color:#254117}    
        a.Person{color:#FE0101;}
        a.Person_Author{color:#FE0101;background-color:yellow;}
        <!--a.Person_inquote{color:#FE0101;}-->
        a.Organization{color:#D400FF;}
        a.Location{color:green;}
        a.Company{color:#823B05;}
        a.Product{color:#6B6B30;}
        span.Anaphoric_Author{background-color:yellow;}
        span.GN_Author{background-color:orange;}
        span.quote{color:#495CFF;}
        span.notaquote{color:#8FA2FF;}
        span.quote_verb{text-decoration:underline;}
        span.url{text-decoration:underline;color:blue;}
      </style>
      <body>
        <table>
          <!--<caption>CODE COULEURS</caption>-->
          <tr style="font-weight:bold;color:#495CFF;">
            <td>QUOTE</td>
          </tr>
          <tr style="font-weight:bold;color:#FE0101;">
            <td>PERSON</td>
          </tr>
          <tr style="font-weight:bold;color:#D400FF;">
            <td>ORGANIZATION</td>
          </tr>
          <tr style="font-weight:bold;color:green;">
            <td>LOCATION</td>
          </tr>
          <tr style="font-weight:bold;color:#823B05;">
            <td>COMPANY</td>
          </tr>
          <tr style="font-weight:bold;color:#6B6B30;">
            <td>PRODUCT</td>
          </tr>
          <!--<tr style="font-weight:bold;color:gray;">
            <td>WORK</td>
            </tr>-->
          <tr style="font-weight:bold;background-color:yellow;">
            <td>QUOTE AUTHOR</td>
          </tr>
        </table>
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>


  <xsl:template match="news_item|item">
    <h4>
      <xsl:value-of select="concat(./@rank,' | ', ./@ref)"/>
    </h4>
    <xsl:apply-templates select="./news_head"/>
    <xsl:apply-templates select=".//para"></xsl:apply-templates>
    <p/>
  </xsl:template>
  
  <xsl:template match="news_head">
    <table class="head" border="1">
      <tr>      
        <xsl:apply-templates select="parent::*/iptc_code"/>
        <xsl:apply-templates select="parent::*/news_slugs"/>
      </tr>
    </table>
    <div/><div/>
    <h3><xsl:value-of select="concat(./@dateline, ' ', ./@headline)"/></h3>
  </xsl:template>
  
  <xsl:template match="/child::*/news_item/news_slugs">
    <td class="slugs"><xsl:value-of select="concat('[', ./@text, ']')"/></td>
  </xsl:template>
  
  <xsl:template match="/child::*/news_item/iptc_code">
    <td class="iptc"><xsl:value-of select="./@value"/></td>
  </xsl:template>

  <xsl:template match="para">
    <xsl:param name="rank" select="./@rank"/>
    <div>
      <span>
        <xsl:value-of select="concat($rank, '/')"/>
      </span>
      <xsl:choose>
          <xsl:when test="./@signature">
            <!-- pretend this is in a template -->
            <xsl:variable name="myString" select="./@signature"/>
            <xsl:variable name="myNewString">
              <xsl:call-template name="replaceCharsInString">
                <xsl:with-param name="stringIn" select="string($myString)"/>
                <xsl:with-param name="charsIn" select="'__'"/>
                <xsl:with-param name="charsOut" select="'/'"/>
              </xsl:call-template>              
            </xsl:variable>
            <xsl:variable name="myNewNewString">
              <xsl:call-template name="replaceCharsInString">
                <xsl:with-param name="stringIn" select="string($myNewString)"/>
                <xsl:with-param name="charsIn" select="'_'"/>
                <xsl:with-param name="charsOut" select="'-'"/>
              </xsl:call-template>              
            </xsl:variable>
            <!-- $myNewString is a result tree fragment, which should be OK. -->
            <!-- If you really need a string object, do this: -->
            <xsl:variable name="myNewRealString" select="string($myNewNewString)"/>
            <xsl:value-of select="$myNewRealString"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates/>
          </xsl:otherwise>
        </xsl:choose>     
  </div>
  </xsl:template>

<!-- here is the template that does the replacement -->
<xsl:template name="replaceCharsInString">
  <xsl:param name="stringIn"/>
  <xsl:param name="charsIn"/>
  <xsl:param name="charsOut"/>
  <xsl:choose>
    <xsl:when test="contains($stringIn,$charsIn)">
      <xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
      <xsl:call-template name="replaceCharsInString">
        <xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
        <xsl:with-param name="charsIn" select="$charsIn"/>
        <xsl:with-param name="charsOut" select="$charsOut"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$stringIn"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

  <xsl:template match="SQ|NO_PRED|IQ|DIQ|DDIQ|DQ|PRQ|GUILLQ|INTRANSQ|TQ|HQ">
    <sup><xsl:value-of select="local-name()"/></sup>
    <span class="quote">
      <xsl:attribute name="title">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </span>
  </xsl:template>
  
  <xsl:template match="NQ">
    <sup><xsl:value-of select="local-name()"/></sup>
    <span class="notaquote">
      <xsl:attribute name="title">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </span>
  </xsl:template>
  
  <xsl:template match="INC_QV|DI_QV|GER_QV|DDI_QV|DQV|QPREP|GUILL_QV|INTRANS_QV">    
    <span class="quote_verb">
      <xsl:attribute name="title">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </span>    
  </xsl:template>
  
  <xsl:template match="URL">    
    <span class="url">
      <xsl:attribute name="title">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </span>    
  </xsl:template>
  
  <xsl:template match="ENAMEX[@type='Person']">
    <a>     
      <xsl:attribute name="class">Person</xsl:attribute>             
      <xsl:attribute name="title">
        <xsl:variable name="name_gender">
        <xsl:choose>
          <xsl:when test="./@gender and ./@gender!='null'">
            <xsl:value-of select="concat(./@name, ' (', ./@gender, ')')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="./@name"/>
          </xsl:otherwise>
        </xsl:choose>     
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name_gender, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>
  </xsl:template>

 
   <xsl:template match="AUT/ENAMEX[@type='Person']">
    <a> 
      <xsl:attribute name="class">Person_Author</xsl:attribute>                 
            <xsl:attribute name="title">
        <xsl:variable name="name_gender">
        <xsl:choose>
          <xsl:when test="./@gender and ./@gender!='null'">
            <xsl:value-of select="concat(./@name, ' (', ./@gender, ')')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="./@name"/>
          </xsl:otherwise>
        </xsl:choose>     
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name_gender, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>
   </xsl:template>

<!--	<xsl:template match="IQ/ENAMEX[@type='Person']">
    <a> 
      <xsl:attribute name="class">Person_inquote</xsl:attribute>                 
      <xsl:attribute name="title">
        <xsl:variable name="name_gender">
          <xsl:choose>
            <xsl:when test="./@gender and ./@gender!='null'">
              <xsl:value-of select="concat(./@name, ' (', ./@gender, ')')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="./@name"/>
            </xsl:otherwise>
          </xsl:choose>     
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name_gender, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>
  </xsl:template>-->
  
  <xsl:template match="ENAMEX[@type='Location']">
    <a>
      <xsl:attribute name="class">Location</xsl:attribute>
       <xsl:attribute name="title">
        <xsl:variable name="name">        
            <xsl:value-of select="./@name"/>
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>    
  </xsl:template>
  
  <xsl:template match="ENAMEX[@type='Organization']">
    <a>
      <xsl:attribute name="class">Organization</xsl:attribute>
        <xsl:attribute name="title">
        <xsl:variable name="name">        
            <xsl:value-of select="./@name"/>
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>
  </xsl:template>
  

  <xsl:template match="ENAMEX[@type='Company']">
    <a>
      <xsl:attribute name="class">Company</xsl:attribute>
        <xsl:attribute name="title">
        <xsl:variable name="name">        
            <xsl:value-of select="./@name"/>
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>
  </xsl:template>

  <xsl:template match="ENAMEX[@type='Product']">
    <a>
      <xsl:attribute name="class">Product</xsl:attribute>
        <xsl:attribute name="title">
        <xsl:variable name="name">        
            <xsl:value-of select="./@name"/>
        </xsl:variable>
        <xsl:variable name="known">
          <xsl:choose>
            <xsl:when test="./@href='none'"><xsl:text>[undefined]</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('[', ./@eid, ']')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($name, ' ', $known)"/>
      </xsl:attribute>
      <xsl:attribute name="target">_blank</xsl:attribute>
      <xsl:attribute name="href">        
        <xsl:value-of select="./@href"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </a>
  </xsl:template>


  <xsl:template match="AUT_PRO|INC_QV/AUT_PRO">
    <span>
      <xsl:attribute name="class">Anaphoric_Author</xsl:attribute>
      <xsl:attribute name="title">Anaphoric Author</xsl:attribute>
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  
  <xsl:template match="AUT_GN">
    <span>
      <xsl:attribute name="class">GN_Author</xsl:attribute>
      <xsl:attribute name="title">GN Author</xsl:attribute>
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  


</xsl:stylesheet>
