#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
from xml.dom.minidom import *
import codecs
from optparse import *
from subprocess import *
import collections


class ResultsChart:
    def __init__(self):
        self.found_quotes = self.correct_quotes = self.long_quotes = self.short_quotes = self.different_quotes = self.false_quotes = self.missed_quotes = self.found_authors = self.correct_author = self.incorrect_author = self.false_author = self.missed_author = self.authors = 0
    def updateQuotesResults(self, result_tuple):
        quote_result = result_tuple[0]
        author_result = result_tuple[1]
        if quote_result == 0:
            self.false_quotes +=1
        else:
            if quote_result == 1: self.correct_quotes += 1
            elif quote_result == 2: self.long_quotes += 1
            elif quote_result == 3: self.short_quotes += 1
            elif quote_result == 5: self.different_quotes += 1
            elif quote_result == 4: self.missed_quotes += 1
        if author_result == 1: self.correct_author += 1; self.authors += 1; self.found_authors += 1
        elif author_result == 2: self.incorrect_author += 1; self.authors += 1; self.found_authors += 1
        elif author_result == 3: self.false_author += 1
        elif author_result == 4: self.missed_author += 1; self.authors += 1
        
def main():
    results = ResultsChart()
    options, args = getOptions()
    auto_dom = parse(options.auto)
    gold_dom = parse(options.gold)
    gold_items_to_parse = selectNodes(gold_dom.getElementsByTagName('news_item'), 'attr_value', ['eval_type', options.eval_type])
    gold_paras = filter(
        (lambda para: len(para.getElementsByTagName('Q')) > 0 or len(para.getElementsByTagName('SQ')) > 0 or len(para.getElementsByTagName('NQ')) > 0),
        mergeLists(map((lambda item: item.getElementsByTagName('para')), gold_items_to_parse)))
    gold_paras_ids = map((lambda para: (para.parentNode.getAttribute('rank'), para.getAttribute('rank'))), gold_paras)
    auto_paras = filter((lambda para: (para.parentNode.getAttribute('rank'), para.getAttribute('rank')) in gold_paras_ids), auto_dom.getElementsByTagName('para'))
    # assert len(gold_paras) == len(auto_paras)
    # print 'Gold paras: %d Auto paras: %d'%(len(gold_paras), len(auto_paras))

    # gold_quotes = found_quotes = correct_quotes = long_quotes = short_quotes = missed_quotes = found_authors = correct_author = incorrect_author = false_author = missed_author = authors = 0
    # results = (gold_quotes, found_quotes, correct_quotes, long_quotes, short_quotes, false_quotes, missed_quotes, found_authors, correct_author, incorrect_author, false_author, missed_author, authors)
    # results = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    sq = found_sq = correct_sq = false_sq = missed_sq = 0
    nq = found_nq = correct_nq = false_nq = missed_nq = 0
    total_gold_quotes = 0
    total_auto_quotes = 0
    for para_id in gold_paras_ids:
        print 'Paragraph %s'%('#'.join(para_id))
        # print >> sys.stderr, 'Paragraph %s'%('#'.join(para_id))
        gold_para = filter((lambda para: (para.parentNode.getAttribute('rank'), para.getAttribute('rank')) == para_id), gold_paras)[0]
        auto_para = filter((lambda para: (para.parentNode.getAttribute('rank'), para.getAttribute('rank')) == para_id), auto_paras)[0]
        total_gold_quotes += len(gold_para.getElementsByTagName('Q'))
        total_auto_quotes += len(auto_para.getElementsByTagName('Q'))
        if len(gold_para.getElementsByTagName('Q')) == 0:
            if len(auto_para.getElementsByTagName('Q')) > 0:
                for auto_quote in auto_para.getElementsByTagName('Q'):
                    result = (0, None)
                    print '>>>>>',; print result
                    print 'Auto Quote: '+auto_quote.toxml().encode('utf-8')+'\n'
                    # print >> sys.stderr, "no gold quotes and %d auto quotes => false (0, None)"%(len(auto_para.getElementsByTagName('Q')))
                    results.updateQuotesResults(result)
        elif len(gold_para.getElementsByTagName('Q')) > 0:
            if len(auto_para.getElementsByTagName('Q')) == len(gold_para.getElementsByTagName('Q')):
                i = 0
                for gold_quote in gold_para.getElementsByTagName('Q'):
                    auto_quote = auto_para.getElementsByTagName('Q')[i]
                    result = evaluate(gold_quote, auto_quote)
                    print '>>>>>',; print result
                    print 'Gold Quote: '+gold_quote.toxml().encode('utf-8')+'\n'
                    print 'Auto Quote: '+auto_quote.toxml().encode('utf-8')+'\n'
                    results.updateQuotesResults(result)
                    i += 1
            elif len(auto_para.getElementsByTagName('Q')) == 0:
                for gold_quote in gold_para.getElementsByTagName('Q'):
                    result = (4, None)
                    print '>>>>>',; print result
                    print 'Gold Quote: '+gold_quote.toxml().encode('utf-8')+'\n'
                    # print >> sys.stderr, "no auto quotes and %d gold quotes => missed (4, None)"%(len(gold_para.getElementsByTagName('Q')))
                    results.updateQuotesResults(result)
            else:
                # print >> sys.stderr, 'Gold para has %d Quotes, Auto para has %d'%(len(gold_para.getElementsByTagName('Q')), len(auto_para.getElementsByTagName('Q')))
                for auto_quote in auto_para.getElementsByTagName('Q'):
                    j = 0
                    r = {}
                    for gold_quote in gold_para.getElementsByTagName('Q'):
                        result = evaluate(gold_quote, auto_quote)
                        r[j] = result
                        j += 1
                    try:
                        best_match = filter((lambda x: r[x][0] == 1 or r[x][0] == 2 or r[x][0] == 3), r.keys())[0]
                        print '>>>>>',; print r[best_match]
                        print 'Gold Quote: '+gold_para.getElementsByTagName('Q')[best_match].toxml().encode('utf-8')+'\n'
                        print 'Auto Quote: '+auto_quote.toxml().encode('utf-8')+'\n'
                        results.updateQuotesResults(r[best_match])
                    except IndexError:
                        result = (4, None)
                        print '>>>>>',; print result
                        print 'Gold Quote: '+gold_quote.toxml().encode('utf-8')+'\n'
                        results.updateQuotesResults(result)
                        continue
        found_sq += len(auto_para.getElementsByTagName('SQ'))
        if len(gold_para.getElementsByTagName('SQ')) == 0:
            if len(auto_para.getElementsByTagName('SQ')) > 0:
                for auto_quote in auto_para.getElementsByTagName('SQ'):
                    false_sq += 1
                    print '>>>>> False SQ'
                    print 'Auto SQ: '+auto_quote.toxml().encode('utf-8')+'\n'
        elif len(gold_para.getElementsByTagName('SQ')) > 0:
            sq += len(gold_para.getElementsByTagName('SQ'))
            if len(auto_para.getElementsByTagName('SQ')) == len(gold_para.getElementsByTagName('SQ')):
                correct_sq += len(gold_para.getElementsByTagName('SQ'))
                i = 0
                for gold_quote in gold_para.getElementsByTagName('SQ'):
                    auto_quote = auto_para.getElementsByTagName('SQ')[i]
                    print '>>>>> Correct SQ'
                    print 'Gold SQ: '+gold_quote.toxml().encode('utf-8')+'\n'
                    print 'Auto SQ: '+auto_quote.toxml().encode('utf-8')+'\n'
                    i += 1
            elif len(auto_para.getElementsByTagName('SQ')) == 0:
                missed_sq += len(gold_para.getElementsByTagName('SQ'))
                for gold_quote in gold_para.getElementsByTagName('SQ'):
                    print '>>>>> Missed SQ'
                    print 'Gold SQ: '+gold_quote.toxml().encode('utf-8')+'\n'
            else:
                print 'Gold para has %d SQ, Auto para has %d (pass)'%(len(gold_para.getElementsByTagName('SQ')), len(auto_para.getElementsByTagName('SQ')))
        found_nq += len(auto_para.getElementsByTagName('NQ'))
        if len(gold_para.getElementsByTagName('NQ')) == 0:
            if len(auto_para.getElementsByTagName('NQ')) > 0:
                for auto_quote in auto_para.getElementsByTagName('NQ'):
                    false_nq += 1
                    print '>>>>> False NQ'
                    print 'Auto NQ: '+auto_quote.toxml().encode('utf-8')+'\n'
        elif len(gold_para.getElementsByTagName('NQ')) > 0:
            nq += len(gold_para.getElementsByTagName('NQ'))
            if len(auto_para.getElementsByTagName('NQ')) == len(gold_para.getElementsByTagName('NQ')):
                correct_nq += len(gold_para.getElementsByTagName('NQ'))
                i = 0
                for gold_quote in gold_para.getElementsByTagName('NQ'):
                    auto_quote = auto_para.getElementsByTagName('NQ')[i]
                    print '>>>>> Correct NQ'
                    print 'Gold NQ: '+gold_quote.toxml().encode('utf-8')+'\n'
                    print 'Auto NQ: '+auto_quote.toxml().encode('utf-8')+'\n'
                    i += 1
            elif len(auto_para.getElementsByTagName('NQ')) == 0:
                missed_nq += len(gold_para.getElementsByTagName('NQ'))
                for gold_quote in gold_para.getElementsByTagName('NQ'):
                    print '>>>>> Missed NQ'
                    print 'Gold NQ: '+gold_quote.toxml().encode('utf-8')+'\n'
            else:
                print 'Gold para has %d NQ, Auto para has %d (pass)'%(len(gold_para.getElementsByTagName('NQ')), len(auto_para.getElementsByTagName('NQ')))
    values = (total_gold_quotes, total_auto_quotes, results.missed_quotes, results.false_quotes, results.correct_quotes, results.long_quotes, results.short_quotes, results.different_quotes)
    print 'Gold quotes: %d\nFound: %d | Missed: %d | False: %d\nCorrect: %d | Long: %d | Short: %d | Different: %d'%(values)
    values = (results.authors, results.found_authors, results.missed_author, results.correct_author, results.incorrect_author, results.false_author)
    print '\tAuthors: %d Found: %d | Missed: %s\n\tCorrect: %d | Incorrect: %d | False: %d'%(values)
    print 'Gold SQ: %d\nFound: %d | Correct: %d | False: %d | Missed: %s'%(sq, found_sq, correct_sq, false_sq, missed_sq)
    print 'Gold NQ: %d\nFound: %d | Correct: %d | False: %d | Missed: %s'%(nq, found_nq, correct_nq, false_nq, missed_nq)


def updateResults(results, result):
    (found_quotes, correct_quotes, long_quotes, short_quotes, false_quotes, missed_quotes, found_authors, correct_author, incorrect_author, false_author, missed_author, authors) = results
    if result[0] > 0:
        if result[0] == 1: correct_quotes += 1; found_quotes += 1
        elif result[0] == 2: long_quotes += 1; found_quotes += 1
        elif result[0] == 3: short_quotes += 1; found_quotes += 1
        elif result[0] == 4: missed_quotes += 1
        if result[1] == 1: correct_author += 1; authors += 1; found_authors += 1
        elif result[1] == 2: incorrect_author += 1; authors += 1; found_authors += 1
        elif result[1] == 3: false_author += 1
        elif result[1] == 4: missed_author += 1; authors += 1
    elif result[0] == 0:
        false_quotes += 1
    results = (found_quotes, correct_quotes, long_quotes, short_quotes, false_quotes, missed_quotes, found_authors, correct_author, incorrect_author, false_author, missed_author, authors)
    return results


def evaluate(gold_quote, auto_quote):
    quote_result = author_result = None
    quote_result = compareQuotes(gold_quote, auto_quote)
    (auto_author_type, auto_author_data) = getAuthor(auto_quote)
    (gold_author_type, gold_author_data) = getAuthor(gold_quote)
    author_result = compareAuthor((gold_author_type, gold_author_data), (auto_author_type, auto_author_data))
    return (quote_result, author_result)

def compareQuotes(gold_quote, auto_quote):
    gold_data = re.sub(' +', ' ', re.sub('^\s+', '', re.sub('\s+$', '', getQuotedPart(gold_quote))))
    auto_data = re.sub(' +', ' ', re.sub('^\s+', '', re.sub('\s+$', '', getQuotedPart(auto_quote))))
    if gold_data == auto_data:
        # print >> sys.stderr, "gold data == auto data => 1"
        return 1
    elif len(gold_data) < len(auto_data) and (auto_data.endswith(gold_data) or auto_data.startswith(gold_data)):
        # print >> sys.stderr, "gold data < auto data => 2"
        return 2
    elif len(gold_data) > len(auto_data) and (gold_data.endswith(auto_data) or gold_data.startswith(auto_data)):
        # print >> sys.stderr, "gold data > auto data => 3"
        return 3
    else:
        # print >> sys.stderr, "gold data != auto data => 5"
        return 5

def getQuotedPart(quote_node):
    # QUOTE MARKUPS: 
    # <SQ> <Q> <IQ> <DIQ> <DDIQ> <TQ> <HQ> <DQ> <PRQ>
    # => <QUOTE_IQ> <QUOTE_DIQ> <QUOTE_DDIQ> <QUOTE_DQ> <QUOTE_COMPLEX> <QUOTE_PREP>
    # <DQV> <INC_QV> <DI_QV> <DDI_QV> <QPREP>
    # <AUT> <AUT_ORG> <AUT_CL> <AUT_PRO>     
    # data = ""
    # try:
    # IF <Q> HAS CHILDREN SUCH AS IQ, DIQ, DDIQ OR DQ, THEN IT'S A "COMPLEX" QUOTE => GET THE DATA
    # data = "###".join(map((lambda node: getNodeData(node)), selectNodes(quote_node.childNodes, 'node_type', ('element', ['IQ', 'DIQ', 'DDIQ', 'DQ', 'PRQ']))))
    data = getNodeData(quote_node)
    # IF <Q> HAS A <HQ> OR <TQ> CHILD, CONCAT DATA
    # try:
    #     data = '###'.join(map((lambda node: getNodeData(node)), selectNodes(quote_node.childNodes, 'node_type', ('element', ['HQ'])))) + "###" + data
    # except IndexError:
    #     pass
    # try:
    #     data += '###' + '###'.join(map((lambda node: getNodeData(node)), selectNodes(quote_node.childNodes, 'node_type', ('element', ['TQ']))))
    # except IndexError:
    #     pass
    # except IndexError:
        # IF IT HAS NO SUCH CHILDREN, IT'S A "SIMPLE" QUOTE => <SQ>
        # data += getNodeData(quote_node)
    return data

def compareAuthor((gold_author_type, gold_author_data), (auto_author_type, auto_author_data)):
    if gold_author_type == None and auto_author_type != None:
        # print >> sys.stderr, "gold author == None and auto author != None => 3"
        return 3
    if gold_author_type != None and auto_author_type == None:
        # print >> sys.stderr, "gold author != None and auto author == None => 4"
        return 4
    if gold_author_type == None and auto_author_type == None:
        # print >> sys.stderr, "gold author == None and auto author == None => None"
        return None
    if gold_author_type == auto_author_type:
        # print >> sys.stderr, "gold author == auto author => 1"
        if gold_author_data == auto_author_data: return 1
        else: return 2
    else:
        # print >> sys.stderr, "gold author != auto author => 2"
        return 2

def getAuthor(quote):
    flag = False
    for author_type in ['AUT', 'AUT_CL', 'AUT_PRO', 'AUT_DEF', 'AUT_ORG']:
        try:
            author = quote.getElementsByTagName(author_type)[0]
            flag = True
            return author_type, getNodeData(author)
        except IndexError:
            continue
    if not flag:
        return None, None
                                   

def getNodeData(node):
    data = ""
    if node.nodeType == node.TEXT_NODE:
        data += node.data
    else:
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                data += child.data
            elif child.nodeType == child.ELEMENT_NODE:
                for sub_child in child.childNodes:
                    data += getNodeData(sub_child)
    return data

def selectNodes(nodes_list, select_mode, select_data):
    if select_mode == 'node_type':
        if select_data[0] == 'text':
            return filter(isTextNode, nodes_list)
        elif select_data[0] == 'element':
            return filter((lambda node: isElementNode(node) and (select_data[1] == None or node.tagName in select_data[1])), nodes_list)
    elif select_mode == 'attr_value':
        return filter((lambda node: node.getAttribute(select_data[0]) == select_data[1]), nodes_list)
    else:
        return nodes_list

def isElementNode(node):
    return node.nodeType == node.ELEMENT_NODE

def mergeLists(listOfLists):
    mergedList = []
    for list2merge in listOfLists:
        mergedList.extend(list2merge)
    return mergedList    
    
def getOptions():
    parser = OptionParser()
    eval_choices = ['dev', 'test']
    strict_choices = ['precision', 'recall']
    parser.add_option("--auto", action="store", dest="auto", default=None, help="automatic annotation")
    parser.add_option("--gold", action="store", dest="gold", default=None, help="gold annotation")
    parser.add_option("--eval", action="store", dest="eval_type", default=None, type="choice", choices=eval_choices, help="evaluate on 'test' or 'dev' corpus")
    # parser.add_option("--out", "-o", action="store", dest="out", default=None, help="output file")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.auto == None or options.gold == None: sys.stderr.write("You must specify automatic and gold annotations\n"); parser.print_help(); sys.exit()
    if options.eval_type == None: sys.stderr.write('You must specify the evaluation type\n'); parser.print_help(); sys.exit()
    # if options.out == None: sys.stderr.write("You must specify the output file name\n"); parser.print_help(); sys.exit()
    return options, args


if __name__ == '__main__':
    main()
