#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;

$|=1; 

my $lib_dir = ""; # folder (typically /usr/local/lib/aleda) that contains aleda databases (e.g., lefff.dat)
my $moredic_dir = "";  #Dictionnaires ajoutés
my $lang = "fr"; #langue du lexique utilisé par défaut fr
my $morelex=0;
while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
elsif (/^-(-)?h(elp)?$/) {
	print "Le script gl_autocensure a pour but de détecter les mots inconnus qui correspondent à de la réticence de plume, soit, à des mots de type m∗∗∗e ou m@#$. Pour bien fonctionner, ce script prend les options suivantes:
\t-l\tpermet de selectionner la langue voulu (optionnel: par défaut la langue selectionnée sera le français
\t-ad\tcorrespond au répertoire dans lequel est stocké le dictionnaire de référence(Lefff)
\t-addLex\tpermet d'ajouter un lexique (un mot par ligne)\n";
}
elsif (/^-d$/) {$lib_dir=shift;}
elsif (/^-l$/) {$lang = shift;}
elsif (/^-addLex$/) {
	$moredic_dir=shift;
	$morelex = 1;
}
}

if ($lib_dir eq "") {
  die "### ERROR: gl_onomatopee.pl requires using option -ad followed by the folder that contains aleda library";
}

## hash de cache
my %is_in_lefff_cache;

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$lib_dir/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth = $lefff_dbh->prepare('select EXISTS (select * from data where wordform=?);');

## hash de cache
my %is_in_morelex_cache;

#on ouvre la base de donnée des autres lexiques
my $morelex_dbh;
my $morelex_sth;
if ($morelex){
	$morelex_dbh = DBI->connect("dbi:SQLite:$moredic_dir/dic_viavoo.dat", "", "", {RaiseError => 1, AutoCommit => 1});
	$morelex_sth = $morelex_dbh->prepare('select EXISTS (select * from data where wordform=?);');
}

while(<>){ #on parcourt le doc
# formattage
chomp;
if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
}   
s/^\s*/ /o;
s/\s$/ /o;


#Pré-correction
$tmp = "$_ ";
$_="";
while ($tmp =~ s/^(\s*(?:{[^{}]+})?\s*)([^ ]+)//) { #pour chaque mot sxpipe
	$comment = $1;
	$mot = $2;
	$_ .= $comment;
	#	print STDERR "-$comment----$mot-\n";
	$mot_mod = lc($mot);
	$mot_mod =~ s/_/ /g;
	#si mot non étiqueté et pas ds dico : 
	if ($comment =~ /^\s*$/ && $mot !~ m/^_/ && $mot !~ m/^\s*$/ && isInconnu($mot,$morelex) && (!is_in_lefff(lc $1,$lefff_sth) ||!is_in_lefff(lc $2,$lefff_sth))){
		##si mot autocensure:
		$motq = quotemeta ($mot);
		if ($mot =~ m/^[a-z]+([#\@&\$\%§\*£§!\?\.]|_UNDERSCORE){2,}[a-z#\@&\$\%§\*£§]+[^!\?\.]+$/){ #pourquoi ne pas prendre tout les caractères non alphabétique?!
			$mot =~ s/$motq/{$motq} _AUTOCENSURE/g;
			##Si mot décomposé
		}elsif ($mot =~ m/^([a-z]+([#\@&\$\%§\*£§!\?\.]|_UNDERSCORE){2,}[a-z#\@&\$\%§\*£§]+)([!\?\.]+)$/ and $tmp=~m/^\s*([a-zA-Z]|$)/){ #pourquoi ne pas prendre tout les caractères non alphabétique?!
			$mot =~ m/^([a-z]+([#\@&\$\%§\*£§!\?\.]|_UNDERSCORE){2,}[a-z]*)([!\?\.])$/;
			my $newmotq=($1);
			my $ponct=$3;
			$mot =~ s/$motq/{$newmotq} _AUTOCENSURE $ponct/g;
		}
	}
	$_ .="$mot";			 
}

# sortie 
s/^ //;
s/ $//;
print "$_\n";
}
#print STDERR "Correcteur: Autocensure: done\n";


#on ferme la base de donnée du lefff
$lefff_sth->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.

################################################################################
#  MÉTHODES
################################################################################



sub is_in_lefff {
my $word = shift;
if (defined($is_in_lefff_cache{$word})) {
  return $is_in_lefff_cache{$word};
}
$lefff_sth->execute(lc $word);
my $reponse = $lefff_sth->fetchrow;
#print "$reponse \n";
$is_in_lefff_cache{$word} = $reponse;
return $reponse;
}

sub is_in_morelex {
my $word = shift;
if (defined($is_in_morelex_cache{$word})) {
  return $is_in_morelex_cache{$word};
}
#print $word."\n";
$morelex_sth->execute(lc $word);
my $reponse = $morelex_sth->fetchrow;
$is_in_morelex_cache{$word} = $reponse;
return $reponse;
}

sub isInconnu {
	my ($mot,$morelex)=@_;
	if($mot =~ m/^\\[\(\)\*\+\?]$/) {
		return 0 ;#pas inconnu car symbole regex
	}elsif ($mot =~ m/^_/) {
		return 0 ;#pas inconnu car déjà étiqueté
	}elsif ($mot =~ m/^\s*$/) {
		return 0 ;#pas inconnu car vide
	}elsif(is_in_lefff(lc $mot) || is_in_lefff($mot)) {
		return 0 ;#pas inconnu car in lefff
	}elsif($morelex){
		return 0 if(is_in_morelex($mot)) ; #pas inconnu car in autres lexiques
		$mot =~ m/(.*)__.*/;
		return 0 if(is_in_lefff($1));#pas inconnu car in lefff
		my $mot_mod = lc($mot);
		$mot_mod =~ s/_/ /g;
		if(is_in_lefff(lc $mot_mod)) {
			return 0 ;#pas inconnu car in lefff
		}elsif($morelex){
			return 0 if(is_in_morelex($mot)) ; #pas inconnu car in autres lexique
		}
	}
	return 1;
}
