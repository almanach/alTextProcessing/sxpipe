#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

# à passer en dernier

$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-ll$/ || /^-less_lists$/i) {$less_lists=1;}
    elsif (/^-ls$/ || /^-less_splits$/i) {$less_splits=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

if ($lang =~/^zh-eleve/) {
  $digit = qr/(?:[〇零一二三四五六七八九○0-9０-９])/o;
  $order = qr/(?:[十百千萬億兆万亿])/o;
  $point = qr/(?:[.．点點])/o;
  $zhnumber = qr/(?:[－-]?(?:十$digit|$digit$order)(?:$digit?$order?)*(?:$point$digit+)?(?:％|$order)?)/o;
  $mixednumber = qr/(?:[－-]?(?:(?:十$digit|$digit$order)(?:$digit?$order?)*|[0-9０-９]+)(?:$point$digit+)?(?:％|%|$order)?)/o;
  $number = qr/(?:[0-9０-９]+(?:$point[0-9０-９]+)?$order?$zhnumber?)/o;
} elsif ($lang eq "th") {
  $digit = qr/(?:[๐๑๒๓๔๕๖๗๘๙0-9])/o;
} else {
    $l        = qr/[^a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźžżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŽŻÑ\.}]/o;
    $maj      =qr/[^ A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŽŻÑ]/o;
    $numFR1   = qr/(?:\d{1,3}(?:(?:\s|_UNDERSCORE)+\d{3})*(?:(?:\s|_UNDERSCORE)*,(?:\s|_UNDERSCORE)*\d+)?)/o;
    $numFR2   = qr/(?:\d{1,3}(?:\.\d{3})*(?:(?:\s|_UNDERSCORE)*,(?:\s|_UNDERSCORE)*\d+)?)/o;
    $numNOSEP = qr/(?:\d+(?:(?:\s|_UNDERSCORE)*[,\.](?:\s|_UNDERSCORE)*\d+)?)/o;
    $num_ext = qr/(?:\d*(?:[Il]*\d+|\d+[Il])(?:\s*[,\.]\s*[\dIl]+)?)/o;
    $numUS    = qr/(?:\d{1,3}(?:\s*,\s*\d{3})*(?:\s*\.\s*(?:[12]\s*\/\s*[234]|\d+))?)/o;
     # Temporary fix for roman numbers in Medieval French
     # Descriptions are made according to the tokens of BFM
    if ($lang eq "ofr") {
      # Uppercase, no dot
      $numOFRclas1_9  = qr/(?:V|V?[IJ]{1,4})/o; # I... VIIII
      $numOFRclas1  = qr/(?:I)/o; # I supp J
      $numOFRclas2_9  = qr/(?:V|V?[IJ]{2,4})/o; # II... VIIII
      $numOFRclas10  = qr/(?:I{0,4}X{1,3}$numOFRclas1_9?)/o; # ...X...
      $numOFRclas50  = qr/(?:X?LX{0,2}$numOFRclas1_9?)/o; # ...L...
      $numOFRclas100 = qr/(?:D?C+L?X?$numOFRclas1_9?)/o; # C...
      $numOFRclas100e = qr/(?:(DC+L?X?$numOFRclas1_9?|D?C+LX?$numOFRclas1_9?|D?C+L?X$numOFRclas1_9?))/o; # C...
      # Lowercase, no dot (only exceptions)
      # $numOFRmin     = qr/(?:c|i|iiii|ix|viii|xv)/o;
      $numOFRmin     = qr/(?:(c|iij|iiii|ix|viii|xv)[.§]?)/o;
      $numOFRmine    = qr/(?:(cc+|iij|iiii|ix|viii|xv)[.§]?)/o;
      # Left and right dots
      $numOFRdotlr   = qr/(?:[.§][ij]{0,4}\s?[Mm]?\s?[Cc]{0,4}\s?([Xx]?[Ll])?[XxVvIiJjï]*(\sV?I{0,3})?[.§]?(\s?[lmcdxiX]+[.§])?)/o;
      # Left dot (one exception)
      $numOFRleft    = qr/(?:[.§]XLVII)/o;
      # Right dot
      $numOFRright   = qr/(?:(m|[Cc]+|l?[IiJj]{1,4}|[Xx]+[Ii][Xx])[.§])/o;
      # Mix of upper case and lower case, no dot
      $numOFRmix     = qr/(?:(?:$numOFRclas1_9|$numOFRclas10|$numOFRclas50|$numOFRclas100)(?:xx|ix))/o;
      $numOFRmixe    = qr/(?:(?:$numOFRclas1_9|$numOFRclas10|$numOFRclas50|$numOFRclass100e)(?:xx|ix))/o;
      # Ambedui
      $numOFRamb     = qr/(?:(?:[Aa]mbe|[AaEe]n)\s?[.§]ii[.§])/o;
    } else {
      $rom1_10 = qr/(?:III?|I?V|VIII?|VI|I?X|I)/o;
      $rom2_10 = qr/(?:III?|I?V|VIII?|VI|I?X)/o;
      $rom10_maxrom = qr/(?:(?:M+(?:C?D)?)?(?:X{1,4}|X?L|LX{1,4}|X?C{1,4}))/o;
      $numROM    = qr/(?:$rom10_maxrom?$rom1_10)/o;
      $numROMnotI    = qr/(?:$rom10_maxrom?$rom2_10|${rom10_maxrom}I)/o;
    }
    $num      = qr/(?:$numFR1|$numNOSEP|$numFR2|$numUS)/o;
    $num_ext   = qr/(?:$numFR1|$numNOSEP|$numFR2|$numUS|$num_ext)/o;
    $multinum = qr/$num(?:\s*[\-\/\.\­]\s*$num)?/o;
    $multinum_ext = qr/$num_ext(?:\s*[\-\/\.\­]\s*$num_ext)?/o;
    $multinumL = qr/$num[\-\­]?[a-zA-Z](?:\s*[\-\/\.\­]\s*$num[\-\­]?[a-zA-Z])?/o;
    $multinumROMnotI = qr/$numROMnotI(?:\s*[\-\/\.\­]\s*(?:$numROM|$multinum))?/o;
    $multinumROMI = qr/I\s*[\-\/\.\­]\s*(?:$numROM|$multinum)/o;
    $numOFR1 = qr/(?:$numOFRclas1)/o;
    $numOFR2 = qr/(?:$numOFRclas10|$numOFRclas2_9|$numOFRclas50|$numOFRclas100e|$numOFRmine|$numOFRmixe|$numOFRamb|$numOFRdotlr|$numOFRleft|$numOFRright)/o;
    $numOFR = qr/(?:$numOFRclas10|$numOFRclas1_9|$numOFRclas50|$numOFRclas100|$numOFRmin|$numOFRmix|$numOFRamb|$numOFRdotlr|$numOFRleft|$numOFRright)/o;
    #$numOFR = qr/(?:$numOFRclas10|$numOFRclas1_9|$numOFRclas50|$numOFRclas100)/o;
    $fraction = qr/(?:[1-9][0-9]*\s*\/\s*[1-9][0-9]*)/o;
    $num_left_ctxt = qr/(?:[Aa]rt\s*\.?|nr\s*\.?|num\s*\.?|pkt\s*\.?|ust\s*\.?|č\s*\.?)/o;
    if ($lang eq "sk") {
      $numsuff  = qr/(?:\.)(?=\s+[^\s])/io;
      $num_right_ctxt = qr/(?:rok[aoue]*)(?:[\s\"\½,;\.:\)]|$)/o;
    } elsif ($lang eq "pl") {
      $numsuff  = qr/(?:\.)(?=\s+[^\s])/io;
      $num_right_ctxt = qr/(?:lata?|rok[aoue]*)(?:[\s\"\½,;\.:\)]|$)/o;
    } elsif ($lang eq "en") {
      $numsuff  = qr/(?:th|st|[nr]d)(?=[\s\"\½,;\.:\)])/io;
      $num_right_ctxt = qr/(?:years?)(?:[\s\"\½,;\.:\)]|$)/o;
    } else {
      $numsuff  = qr/(?:i?[eè]mes?|[eè]|ers?|[eè]res?|nds?|ndes?|aines?|e)(?=\s|[\"\½,;\.:\)]$l)/io;
      $numsuff1  = qr/(?:ers?|[eè]res?)(?=\s|[\"\½,;\.:\)]$l)/io;
      $num_right_ctxt = qr/(?:ans?)(?:[\s\"\½,;\.:\)]|$)/o;
    }
    $romnum_lctxt     = qr/(?:.[^0-9a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ{}\-]|[^0-9a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ{}]-)/o;
}
while (<>) {
  # formattage
  chomp;
  if (/ (_XML|_MS_ANNOTATION) *$/) {
    print "$_\n";
    next;
  }
  
  s/  +/ /g;
  s/^/  /;
  s/$/  /;
  s/([^0-9],)([0-9]+,[0-9])/$1 $2/g;
  while (s/{([^{}]+)}\s*_[^ {}]+\s*([^{}]*} _SPECWORD)/\1\2/g) {}
  
  if ($lang =~/^zh-eleve/){
    s/␠/_STRANGESPACE/g;
    while (s/({[^{}]*[^{}␠])([0-9０-９○]|$digit|$order)/\1␠\2/go) {}
    s/(?<![{␠])($mixednumber|$zhnumber|[0-9０-９○]{2,})/ \{$1\} _NUM /go;
    s/(第$digit)/{$1} _ORDINAL/go;
    s/第 \{([^\}]+)\} _NUM/{第$1} _ORDINAL/go;
    s/␠//g;
    s/_STRANGESPACE/␠/g;
  } elsif ($lang eq "th"){
    s/␠/_STRANGESPACE/g;
    while (s/({[^{}]*[^{}␠])($digit)/\1␠\2/go) {}
    s/(?<![{␠])($digit+(?:,${digit}{3})*(?:\.$digit+)?)/ \{$1\} _NUM /go;
    s/␠//g;
    s/_STRANGESPACE/␠/g;
  } else {
    if ($lang eq "fr") {
      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])(brevets?(?: (?:[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñ-]+)?)?) ((?:n[°o](?: de publication)? ?)?[0-9][0-9 \/\.-]+[0-9])/$1$2 {$3} _PATENTID/gi;
      while (s/(_PATENTID[\s:;\?\!\(\)\[\]\"\+\*\-]+)((?:du \{[^}]+\} ?_DATE[^ ]* )?(?:et|,)) ((?:n[°o](?: de publication)? ?)?[0-9][0-9 \/\.-]+[0-9])/$1$2 {\3} _PATENTID/g) {
      }
      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])([A-Z]{1,2}) \{([^{}]+\} _PATENTID)/\1\{\2 \3/g;
    }

    if ($lang eq "en") {
      s/(?<=[^\}]) (\d+\+)( [^\d]|[\.,;:\?\!\(\)\[\]]|$)/ {\1} _NUM\2/go;
      s/(?<=[^\}]) (\d+(?:[km]|bn))( [^\d]|[\.,;:\?\!\(\)\[\]]|$)/ {\1} _NUM\2/go;
    }
    
    s/(?<=[^\}]) (\#\d+)([ \.,;:\?\!\(\)\[\]])/ {\1} _NUM\2/go;
    
    # tentative de protection pour les séquences de maj et chiffres mélangés
    if ($no_sw) {
      s/(?<=[^\}])(\s)([A-Z]+\d+[A-Z\d\/-]*)([ \.,;:\?\!\(\)\[\]])/\1__{{\2}}__\3/g;
      s/(?<=[^\}])(\s)(\d+[A-Z]+[A-Z\d\/-]*)([ \.,;:\?\!\(\)\[\]])/\1__{{\2}}__\3/g;
      s/(?<= )([:;\?\!\(\)\[\]\"\+\*-]+)([A-Z]+\d+[A-Z\d\/-]*)([ \.,;:\?\!\(\)\[\]])/__{{\1\2}}__\3/g;
      s/(?<= )([:;\?\!\(\)\[\]\"\+\*-]+)(\d+[A-Z]+[A-Z\d\/-]*)([ \.,;:\?\!\(\)\[\]])/__{{\1\2}}__\3/g;
    } else {
      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])([A-Z]+\d+[A-Z\d\/-]*)([ \.,;:\?\!\(\)\[\]])/\1__{{\2}}__\3/g;
      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])(\d+[A-Z]+[A-Z\d\/-]*)([ \.,;:\?\!\(\)\[\]])/\1__{{\2}}__\3/g;
    }

    s/({[^{}]+}\s*_(?:PATENTID|NUM(?:_[^_]+_)?))/"__{{".replace_spaces_by_tmpspace($1)."}}__"/ge;
    
    if ($lang eq "fr" || $lang eq "en") {
      if ($no_sw) {
	s/(?<=[^\}])(\s)(M) (6)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(France) ([2345]|24)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(Europe) ([12])([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}]) ([Dd]')(Europe) ([12])([\s\.,;:\?\!\(\)\[\]\/\-])/ $1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(RTL) ([29])([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(TF) (1)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(George) (V)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(20) ([Mm]inutes)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(Seveso) ([12]|I|II)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(Cap) (21)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])(\s)(Rue) (89)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
      } else {
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(M) (6)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(France) ([2345]|24)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(Europe) ([12])([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}]) ([Dd]')(Europe) ([12])([\s\.,;:\?\!\(\)\[\]\/\-])/ $1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(RTL) ([29])([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(TF) (1)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(George) (V)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(20) ([Mm]inutes)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(Seveso) ([12]|I|II)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(Cap) (21)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
	s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(Rue) (89)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\{$2 $3} __{{$2_$3}}__$4/go;
      }
    }
    while (s/\{([^{}]+)\} __\{\{[^{}]+\}\}__([^{}]*)}\s*(_SPECWORD)/\1\2} \3/g) {
    }
    s/(?<=[^\}]) G ([789]|20)([\s\.,;:\?\!\(\)\[\]])/ {G $1} G$1$2/go;
    s/(?<=[^\}]) (4[xX]4)([\s\.,;:\?\!\(\)\[\]])/ {\1} 4x4\2/go;
    s/(?<=[^\}]) ([Nn])( ?)([+-])( ?)(\d)([\s\.,;:\?\!\(\)\[\]])/ {\1\2\3\4\5} \1\3\5/go;
    
    # things that must not be affected by gl_number.pl should be "protected", i.e., put between __{{ and }}__
    s/([\s\.,;:\?\!\(\)\[\]\/\-'\"])(k?m[23]|M6|G[789]|G20|France[2345]|France24|TF1|Europe[12]|Rue89|Cap21|4[xX]4|[Nn][\+-]\d|\S+_\d\S*)([\s\.,;:\?\!\(\)\[\]\/\-\"])/$1\__\{\{$2\}\}__$3/g;
    s/(\&#\d+;)/\__\{\{$1\}\}__/g;
    s/(moteurs?)(V[0-9]+)([\s\.,;:\?\!\(\)\[\]])/\1 _REGLUE___{{\2}}__\3/go;
    s/(moteurs?\s+)(V[0-9]+)([\s\.,;:\?\!\(\)\[\]])/\1__{{\2}}__\3/go;
    s/(?<=[^\}]) (i\. *e\.?|e\. *g\.?|d\. *h\.?|z\. *b\.?|o\. *ä\.?|m\. *e\.?|u\. *s\. *w\.?)([\s\.,;:\?\!\(\)\[\]])/ __{{\1}}__\2/goi;
    s/(?<=[^\}]) (France\.9)([\s\.,;:\?\!\(\)\[\]])/ __{{\1}}__\2/go;
    s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(EE-LV)([\s\.,;:\?\!\(\)\[\]\/\-\"])/\1__{{\2}}__\3/go;
    s/(?<=[^\}]) ([Dd]')(EE-LV)([\s\.,;:\?\!\(\)\[\]\/\-\"])/ \1__{{\2}}__\3/go;
    s/(?<=[^\}]) (\d{1,2}(?: ?: ?\d{1,2})? *[AaPp]\.? ?[Mm]\.?)/ __{{\1}}__/go;

    if ($lang eq "fr") {
      s/(Ian)([\s\.,;:\?\!\(\)\[\]])/__{{\1}}__ \2/go; # protection required
    }
    s/(\S*[A-Za-z-]\S*_UNDERSCORE\S+)/__{{\1}}__/go;
    s/(\S+_UNDERSCORE\S*[A-Za-z-]\S*)/__{{\1}}__/go;
    s/__\{\{__\{\{(.*?)\}\}__\}\}__/__{{\1}}__/go;
    
    # reconnaissance
    $listnumid = qr/(?:$numROM|\d{1,2})(?:\s*\.\s*(?:$numROM|\d{1,2}|[A-Za-z]))*(?:\s*\.)?(?=[^0-9])/o;
    $listnumidB = qr/(?:[A-Za-z])(?:\s*\.\s*(?:$numROM|\d{1,2}|[A-Za-z]))*(?:\s*\.)?(?=[^0-9])/o;
    $listnumidC = qr/(?:[A-Za-z])(?:\s*\.\s*(?:$numROM|\d{1,2}|[A-Za-z]))*(?:\s*\.)?(?=[^0-9])/o;
    $listnumprefix = qr/((?:^|[;:,]|[^\d]\.|_META_TEXTUAL_[^ ]+) +)/o;
    $listnumprefix2 = qr/((?:^|, |[;:] ?|[^\d]\.|_META_TEXTUAL_[^ ]+ ) *)/o;
    $listnumprefix2b = qr/((?:^|, |[;:] ?|_META_TEXTUAL_[^ ]+ ) *)/o;
    $listnumprefix3 = qr/((?:^|[;:,]|_META_TEXTUAL_[^ ]+) +)/o;

    s/^ +([\-\­=]+\s*&gt;)/\{$1\}_META_TEXTUAL_PONCT/go; # [début de ligne] ->
    if ($lang ne 'ofr') {
      if ($no_sw) {
	s/$listnumprefix((?:$numROM|\d+)\s*[\.\-\­]\s*$listnumid)(\s*[\.\)\]\/\]\-\­])?(?=[^0-9}])/$1\{$2$3\}_META_TEXTUAL_GN/go; # [début de ligne] I.2 texte
	s/$listnumprefix([\-\­])(\s+)($listnumid|$listnumidB)(\s+)([\.\)\]\/\]\-\­])(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_PONCT$3\{$4\}_META_TEXTUAL_GN$5\{$6\}_META_TEXTUAL_PONCT/go; # [début de ligne] - 1) texte
	unless ($less_lists) {
	  s/$listnumprefix([\-\­])(\s+)($listnumid|$listnumidB)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_PONCT$3\{$4$5\}_META_TEXTUAL_GN/go; # [début de ligne] - 1) texte
	  s/$listnumprefix([\-\­])(\s+)($listnumid|$listnumidB)([\-\­])\s+(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_PONCT$3\{$4$5\}_META_TEXTUAL_GN /go; # [début de ligne] - 1) texte
	  s/$listnumprefix([\-\­])($listnumid|$listnumidB)(\s+)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2$3\}_META_TEXTUAL_GN$4\{$5\}_META_TEXTUAL_PONCT/go; # [début de ligne] -1 - texte
	  s/$listnumprefix([\-\­])($listnumid|$listnumidB)(\s+)([\-\­])\s+(?=[^0-9}])/$1\{$2$3\}_META_TEXTUAL_GN$4\{$5\}_META_TEXTUAL_PONCT /go; # [début de ligne] -1 - texte
	  s/$listnumprefix([\-\­])($listnumid|$listnumidB)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2$3$4\}_META_TEXTUAL_GN/go; # [début de ligne] -1) texte
	  s/$listnumprefix([\-\­])($listnumid|$listnumidB)([\-\­])\s+(?=[^0-9}])/$1\{$2$3$4\}_META_TEXTUAL_GN /go; # [début de ligne] -1) texte
	  s/$listnumprefix($listnumid|$listnumidB)(\s+)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_GN$3\{$4\}_META_TEXTUAL_PONCT/go; # [début de ligne] 1 ) texte
	  s/$listnumprefix($listnumid|$listnumidB)(\s+)([\-\­])\s+(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_GN$3\{$4\}_META_TEXTUAL_PONCT /go; # [début de ligne] 1 ) texte
	  s/$listnumprefix($listnumid|$listnumidB)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2$3\}_META_TEXTUAL_GN/go; # [début de ligne] 1) texte
	  s/$listnumprefix($listnumid|$listnumidB)([\-\­])\s+(?=[^0-9}])/$1\{$2$3\}_META_TEXTUAL_GN /go; # [début de ligne] 1) texte
	}
      } else {
	s/$listnumprefix2((?:$numROM|\d+)\s*[\.\-\­]\s*$listnumid)(\s*[\.\)\]\/\]\-\­])(?=[^0-9}])/$1\{$2$3\}_META_TEXTUAL_GN/go; # [début de ligne] I.2) texte
	s/$listnumprefix2((?:$numROM|\d+)\s*[\.\-\­]\s*$listnumid) /$1\{$2\}_META_TEXTUAL_GN /go; # [début de ligne] I.2 texte
	unless ($less_lists) {
	  s/$listnumprefix2([\-\­])(\s*)($listnumid|$listnumidB)(\s*)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_PONCT$3\{$4\}_META_TEXTUAL_GN$5\{$6\}_META_TEXTUAL_PONCT/go; # [début de ligne] - 1) texte
	  s/$listnumprefix2([\-\­])(\s*)($listnumid|$listnumidB)(\s*)([\-\­])\s+(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_PONCT$3\{$4\}_META_TEXTUAL_GN$5\{$6\}_META_TEXTUAL_PONCT /go; # [début de ligne] - 1) texte
	  s/$listnumprefix2($listnumid)(\s*)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_GN$3\{$4\}_META_TEXTUAL_PONCT/go; # [début de ligne] 1) texte
	  s/$listnumprefix2($listnumid)(\s*)([\-\­])\s+(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_GN$3\{$4\}_META_TEXTUAL_PONCT /go; # [début de ligne] 1) texte
	  s/$listnumprefix($listnumidB)(\s*)([\.\)\]\/\]])(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_GN$3\{$4\}_META_TEXTUAL_PONCT/go; # [début de ligne] 1) texte
	  s/$listnumprefix($listnumidB)(\s*)([\-\­])\s+(?=[^0-9}])/$1\{$2\}_META_TEXTUAL_GN$3\{$4\}_META_TEXTUAL_PONCT /go; # [début de ligne] 1) texte
	}
      }
    }
    # rattrapage de sur-corrections
    if ($lang eq "fr") {
      s/$listnumprefix\{([Aa])\}_META_TEXTUAL_GN(\s*)\{(-)\}_META_TEXTUAL_PONCT(\s*t-)/$1$2$3$4$5/g;
      # ci-dessus = horreur
    }
    s/(\([^\)]*)\{([^{}]+)\}_META_TEXTUAL_GN\{\)\}_META_TEXTUAL_PONCT/$1$2\)/g;
    
    # remet M. en mode no word segmentation au lieu de _META_TEXTUAL_GN _SENT_BOUND
    if ($no_sw) {
      s/{(Mr\.?|M\.|Mme|Miss|Mrs\.?|Sir|Lady|Sgt\.?)}_META_TEXTUAL_GN/\1/g;
    }
    
    s/((?:Mr\.?|M\.|Mme|Miss|Mrs\.?|Sir|Lady|Sgt\.?) )\{([A-ZÉÃÀÂÊÛÎÔÄËÜÏÖÇ])\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT/$1$2$3./g;
    s/$listnumprefix\{([G-ZÉÃÀÂÊÛÎÔÄËÜÏÖÇ])\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT/$1$2./g; # \1 est dans listnumprefix
    if ($lang =~ /^(?:fr|en|es|pt|it|ro)$/) {
      s/ ((?:pp|[pnv])\.?) \{(\d+)\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT/ $1 $2$3./g;
    } elsif ($lang eq "de") {
      s/ ([snv]\.?) \{(\d+)\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT/ $1 $2$3./g;
    } elsif ($lang =~ /^(sk|cz)$/) {
      s/ ([čv]\.?) \{(\d+)\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT/ $1 $2$3./g;
    }

    if ($lang eq "en") { # le mot "a" ne peut terminer une phrase... donc on va dire artificiellement que " a[.\)...]" désigne tjs un _META_TEXTUAL_truc
      s/ ([Aa])(\.)( *)(?=[^0-9mM ])/ \{$1\}_META_TEXTUAL_GN\{$2\}_META_TEXTUAL_PONCT\3/go; # a. ATTENTION, TRES RISQUÉ
      unless ($less_lists) {
	s/ ([Aa])([\)\]])( *)(?=[^0-9mM ])/ \{$1\}_META_TEXTUAL_GN\{$2\}_META_TEXTUAL_PONCT\3/go; # a. ATTENTION, TRES RISQUÉ
      }
      # ... sauf dans certains contextes gauches
      s/((?:Mr\.?|M\.|Mme|Miss|Mrs\.?|Sir|Lady) )\{([Aa])\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT/\1\2\3./g;
      # ... et droite
      s/\{([Aa])\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT(\.)/\1.\2/g;
      s/\{A\}_META_TEXTUAL_GN\{\.\}_META_TEXTUAL_PONCT([A-Z]\.)/A.\1/g;
    }
    
    if ($less_lists) {
      s/$listnumprefix2b([\-\­\*])([^ ]*[^ -]\s)/$1\{$2\}_META_TEXTUAL_PONCT$3/go; # [début de ligne] - texte
      s/^(\s*)\+(\s)/$1\{\+\}_META_TEXTUAL_PONCT$2/go; # [début de ligne] + texte
    } else {
      s/$listnumprefix([\-\­\*])([^ ]*[^ -]\s)/$1\{$2\}_META_TEXTUAL_PONCT$3/go; # [début de ligne] - texte
      s/$listnumprefix\+(\s)/$1\{\+\}_META_TEXTUAL_PONCT$2/go; # [début de ligne] + texte
    }
    s/$listnumprefix3\.(\s)/$1\{\.\}_META_TEXTUAL_PONCT$2/go; # [début de ligne] . texte

    # QQUES CAS TORDUS
    s/(?<=[^0-9\{])(\d+\s*(?:[,\.]\s*|\s+)[123]\s*\/\s*[234])([\s\"\½;:\)]|[\.,][^0-9]|$)/\{$1\}_NUM$2/go; #    9,1/4
    s/(?<=[^0-9A-Z\{_])([A-Z\d]*\d[A-Z\d]*\s*[\/-]\s*(?:[\d\/-]\s[\d\/-]|[\dA-Z\/-])*[\dA-Z])([\s\"\½;:\)]|[\.,][^0-9]|$)/\{$1\}_NUM$2/go; #    (fractions)   2-F4      A3-249/93	0-190/93
    s/(?<=[^0-9\{])(\d+(?:[.,]\d+)?\s*x\s*\d+(?:[.,]\d+)?)([\s\"\½;:\)]|[\.,][^0-9]|$)/\{$1\}_NUM$2/go; # 130x180
    #    s/(?<=[^0-9\{])($fraction)/\{$1\}_NUM/go; # 1/2
    
    # Limit ofr
    if ($lang eq "ofr") {
      #s/(?<=$romnum_lctxt)($multinumROMnotI)(?=(?:$l|\.\s+$maj))/\{$1\}_ROMNUM/go; # IV
      #s/(?<=$romnum_lctxt)($multinumROMI)(?=(?:$l|\.\s+$maj))/\{$1\}_ROMNUM/go; # I
      #s/(?<=$romnum_lctxt)($numOFR)(?!\s*')(?=(?:$l|\.\s+$maj))/\{$1\}_ROMNUM/go; # I
      s/(?<=$romnum_lctxt)($numOFR)(?!\s*')(?=(?:$l|\.\s+$maj))/romnum_handle($1)/geo; # I
      s/(?<=$romnum_lctxt)($numOFR2$numsuff)/\{$1\}_ROMNUM/go; # IVème
      s/(?<=$romnum_lctxt)($numOFR1$numsuff1)/\{$1\}_ROMNUM/go; # IVème
    } else {
        s/(?<=$romnum_lctxt)($multinumROMnotI)(?=(?:$l|\.\s+$maj))/\{$1\}_ROMNUM/go; # IV
        s/(?<=$romnum_lctxt)($multinumROMI)(?=(?:$l|\.\s+$maj))/\{$1\}_ROMNUM/go; # I
        s/(?<=$romnum_lctxt)($multinumROMnotI$numsuff)/\{$1\}_ROMNUM/go; # IVème
        s/(?<=$romnum_lctxt)($multinumROMI$numsuff)/\{$1\}_ROMNUM/go;  # Ier
        # overmatching
        s/{VIE}_ROMNUM/VIE/g;
        s/{MCI}_ROMNUM/MCI/g;
        s/{CV}_ROMNUM/CV/g;
        s/U.{V}_META_TEXTUAL_GN\{.\}_META_TEXTUAL_PONCT/U.V./g;
        s/U.{V}_ROMNUM/U.V/g;
        s/{I-I}_ROMNUM-/I-I-/g if $lang eq "en";
        s/{(x)}_ROMNUM(-(?:ray|rated|shaped))/$1$2/gi;
        s/{(.)}_ROMNUM(-(?:shaped))/$1$2/gi;
      }
    
    s/(?<=[^0-9\{])($multinum\s*$numsuff)/\{$1\}_NUM/go; # 2ème
    if ($lang eq "pl") {
      s/(?<=[^0-9\{])($multinum_ext\s*(?:mld|tys|mln|bln)(?:\s*\.)?)/\{$1\}_NUM/go; # 2ème
    }
    s/(?<=[^0-9\{])($multinumL)(\s*)(?=[^0-9\-\­\/,\.a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ])/\{$1\}_NUM$2/go; # 2a
    
    s/($num_left_ctxt\s*)([A-Z])([\s\"\½,;\.:\)]|$)/$1\{$2\} _NUM$3/go;
    s/ \{M\}\s*_(?:NUM|META_TEXTUAL_GN)(\s*)\. / M$1\. /go;
    s/ \{M\}\s*_(?:NUM|META_TEXTUAL_GN)(\s*)\{\.\}\s*_META_TEXTUAL_PONCT / M$1\. /go;
    s/($num_left_ctxt\s*)([0-9]+\s*[A-Za-e])([\s\"\½,;\.:\)]|$)/$1\{$2\} _NUM$3/go;
    s/(?<=\s)([0-9]*[Il][0-9\.,\s]*[0-9][0-9\.,\s]*?)(\s*$num_right_ctxt)/\{$1\} _NUM$2/go;
    s/(?<=[^0-9\{])([0-9]*[0-9\.,\s]+[Il])(\s*$num_right_ctxt)/\{$1\} _NUM$2/go;

    if ($less_splits) {
      while (s/(?<=[,\.\-\/\­\$€¥£ ])($multinumL)(\s*[,\.\-\/\­])([^0-9]|$)/\{$1\}_NUM$2$3/go) {} 
      s/(?<=[,\.\-\/\­\$€¥£ ])($multinum)(\s*)(?=[,\.\-\/\­ ])/\{$1\}_NUM$2/go; # 2
      while (s/(?<=[,\.\-\/\­\$€¥£ ])($multinum)(\s*[,\.\-\­\/])([^0-9]|\s+$)/\{$1\}_NUM$2$3/go) {}
    } else {
      while (s/(?<=[^0-9\{])($multinumL)(\s*[,\.\-\/\­])([^0-9]|$)/\{$1\}_NUM$2$3/go) {} 
      s/(?<=[^0-9\{])($multinum)(\s*)(?=[^0-9\-\­\/,\.\}])/\{$1\}_NUM$2/go; # 2
      while (s/(?<=[^0-9\{])($multinum)(\s*[,\.\-\­\/])([^0-9]|\s+$)/\{$1\}_NUM$2$3/go) {}
    } 
    # REGROUPE AVEC LE PRECEDENT   s/(?<=[^0-9\{])($multinum) +$/\{$1\}_NUM/go; # 2 [fin de ligne]

    # sécurités pour éviter les _NUM emboités et les reconnaissances de trucs déjà reconnus à l'avance comme 4x4
    while (s/(?<!_){([^{}]*){([^{}]*)}_NUM/{$1$2/g) {
    }
    s/{([^}]+)}\s*{([^}]+)}_NUM/{$1} $2/g;
    
    if ($lang eq "fr") {
      s/(?<=[^0-9\{])\{([^\}]+)\}_NUM(\s*virgule\s*)\{([^\}]+)\}_NUM/\{$1$2$3\}_NUM/go; # 2 virgule 5
    }
    
    s/(?<=\s)([0-9]+)(?=\s)/\{$1\}_NUM/go; # sécurité
    
    s/ \}/\}/go;

    # unlock protections
    s/__\{\{//g;
    s/\}\}__//g;
    
    if ($lang =~ /^(ja|zh|tw)$/) {
      if ($no_sw) {
	s/([①-⑫])/{\1}_META_TEXTUAL_GN /g;
	s/({&#226;&#145;&#16[0-8];})\s*_ETR/{$1}_META_TEXTUAL_GN/g;
	s/([●※＊■◆→⇒◇◎★☆〇])/{$1}_META_TEXTUAL_PONCT/g;
	s/(?<![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])(○)(?![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])/{\1}_META_TEXTUAL_PONCT/g;
	s/([\（\(]){([1-9１-９])}_NUM([\）\)])/{$1$2$3}_META_TEXTUAL_PONCT _UNSPLIT__META_TEXTUAL_GN _UNSPLIT__META_TEXTUAL_PONCT/g;
      } else {
	s/([①-⑫])/ {\1}_META_TEXTUAL_GN /g;
	s/({&#226;&#145;&#16[0-8];})\s*_ETR/{\1}_META_TEXTUAL_GN/g;
	s/([●※＊■◆→⇒◇◎★☆〇])/ {\1}_META_TEXTUAL_PONCT /g;
	s/(?<![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])(○)(?![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])/ {\1}_META_TEXTUAL_PONCT /g;
	s/([\（\(]){([1-9１-９])}_NUM([\）\)])/{$1}_META_TEXTUAL_PONCT {$2} _META_TEXTUAL_GN {$3} _META_TEXTUAL_PONCT/g;
      }
      s/({&#226;&#151;&#143;})\s*_ETR/{\1}_META_TEXTUAL_PONCT/g;
      if ($no_sw) {
	s/ \{([1-9１-９]+)\}_NUM(\s*)([\.\)、\）。，：:])/ {$1}_META_TEXTUAL_GN$2\{$3}_META_TEXTUAL_PONCT /g;
	s/(^|。)(\s*)(第?[一二])([\.\)、\）。，：:])/$1$2\{$3$4}_META_TEXTUAL_GN _UNSPLIT__META_TEXTUAL_PONCT /g;
      } else {
	s/ \{([1-9１-９]+)\}_NUM\s*([\.\)、\）。，：:])/ {$1}_META_TEXTUAL_GN {$2}_META_TEXTUAL_PONCT /g;
	s/(^|。)\s*(第?[一二])([\.\)、\）。，：:])/$1 {$2}_META_TEXTUAL_GN {$3}_META_TEXTUAL_PONCT /g;
      }
    } elsif ($lang =~ /^th$/) {
      if ($no_sw) {
	s/^(\s*)\{(๑๐|๑?[๑๒๓๔๕๖๗๘๙])\} _NUM ([\.])/$1\{$2$3\}_META_TEXTUAL_GN _UNSPLIT__META_TEXTUAL_PONCT /g;
      } else {
	s/^(\s*)\{(๑๐|๑?[๑๒๓๔๕๖๗๘๙])\} _NUM ([\.])/$1\{$2\}_META_TEXTUAL_GN {$3}_META_TEXTUAL_PONCT /g;
      }
    }
    if ($lang =~ /^(ja)/) {
      s/(·)/ {\1}_META_TEXTUAL_PONCT /g; # diverses bullets unicode (utilisé en mandarin comme séparateur de type ponctuation (très) faible)
    }
    if ($lang eq "km") {
      s/([\P{Latin}\)])[ ​]*-[ ​]*(\P{Latin})/$1 {-}_META_TEXTUAL_PONCT $2/g;
      s/([\P{Latin}\)])[ ​]*\{(\d+)\}_NUM([,\-])[ ​]*(\P{Latin})/$1 {$2}_META_TEXTUAL_GN {$3}_META_TEXTUAL_PONCT $4/g;
    }
    s/(•)/ {\1}_META_TEXTUAL_PONCT /g; # diverses bullets unicode
    s/({(?:&#226;&#128;&#162;|&#194;&#183;)})\s*_ETR/\1_META_TEXTUAL_PONCT/g; # diverses bullets unicode yarecodées
  }
  unless ($lang =~ /^zh-eleve/) {
    s/_TMP_SPACE/ /g;
    s/_TMP_COMMENT_B/{/g;
    s/_TMP_COMMENT_E/}/g;
    
    # sortie
    $ne = qr/(?:_(?:ROM)?NUM(?:_[^_]+_)?|_META_TEXTUAL_PONCT|_META_TEXTUAL_GN|PATENTID)/o;
    if ($no_sw) {
      s/{([^\s}]+)}\s*($ne)([^_\s\}]+){([^\s\}]+)}([^ ])/{$1$3$4} $2 _UNSPLIT_$3 _UNSPLIT_$5/g; # traiter certaines unexpected situations (e.g., 90+3 )
      s/\s*($ne)([^_\s\}])/$1 _REGLUE_$2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}\s*($ne)/$1\{$2$3\} $2 _UNSPLIT_$4/g;
      if (s/{_REGLUE_.*?}//) {
	print STDERR "Warning: gl_number encoutered an unexpected situation (input data is approx: $_)\n";
      }
    } else {
      s/_REGLUE_/_PROTECTREGLUE_/g;
      s/($ne)([^\s_])/$1 $2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}($ne)/$1$2 \{$3\}$4/g;
      s/(?<=[^} ]) _REGLUE_//g;
      s/_PROTECTREGLUE_/_REGLUE_/g;
    }
    s/_SPECIAL_//g;
    
    s/^(.*_META_TEXTUAL_[^ ]+)/\1 _UNSPLIT__SENT_BOUND/g;
    s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne) _REGLUE_/$1$2 _UNSPLIT_/g;
    s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne)/$1$2/g;  
  }
  
  while (s/{([^{}]+)}\s*_[^ {}]+\s*([^{}]*} *_SPECWORD)/\1\2/g) {}
  
  s/^ //;
  s/ $//;
  print "$_\n";
}

sub replace_spaces_by_tmpspace {
  my $s = shift;
  $s =~ s/ /_TMP_SPACE/g;
  $s =~ s/{/_TMP_COMMENT_B/g;
  $s =~ s/}/_TMP_COMMENT_E/g;
  return $s;
}

sub romnum_handle {
  my $s = shift;
  # print STDERR "romnum handle '$s'\n";
  $s =~ /^\s*\.+\s*$/ and return $s;
  return "\{$s\}_ROMNUM";
}
