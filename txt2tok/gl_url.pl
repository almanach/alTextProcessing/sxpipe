#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


# ATTENTION: à exécuter APRES gl_email.pl

$| = 1;

$lang="fr";

while (1) {
  $_=shift;
  if (/^$/) {last}
  elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1}
  elsif (/^-sc$/ || /^-spelling[-_]correction$/i) {$do_spelling_correction=1;}
  elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;}
  elsif (/^-l(?:ang)?$/) {$lang=shift}
}

if ($lang eq "sk") {
  $safeExt     = qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|bo|ci|co|in|im|me)/o;
  $unsafeExt   = qr/(?:je|by|do|je|ma|mu|my|na|no|tj|to|za|info|jobs|museum|name|asia)/o;
} elsif ($lang eq "pl") {
  $safeExt     = qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|br|bs|bt|bv|bw|bz|ca|cc|cd|cf|cg|ch|ck|cl|cm|cn|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mv|mw|mx|mz|nc|ne|nf|ng|ni|nl|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tk|tm|tn|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|zm|zw|me)/o;
  $unsafeExt   = qr/(?:bo|by|ci|co|do|im|in|je|ma|mu|my|na|no|tj|to|za|info|jobs|museum|name|asia)/o;
} elsif ($lang eq "en") {
  $safeExt     = qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|au|aw|az|ba|bb|bd|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|mz|na|nc|ne|nf|ng|ni|nl|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|de|et|eu)/o;
  $unsafeExt   = qr/(?:it|to|at|do|is|my|no|so|us|info|jobs|museum|name|asia|me)/o;
} else {
  $safeExt     = qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/o;
  $unsafeExt   = qr/(?:je|de|et|eu|be|info|jobs|museum|name|asia|me)/o;
}

$protocole = qr/https?|ftp|telnet|gopher/o;
$a         = qr/[a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;
$pre       = qr/(?<=[^\/／\{\@\.．0-9a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ_<－-])/o;
$post      = qr/(?=[^\'\/／\}a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ_>－-])/o;
$dirname   = qr/\s*[\/／]\s*(?:\s(?:_UNDERSCORE|_|\.|．|\~|\?|=)|(?:_UNDERSCORE|_|\.|．|\~|\?|=)\s|[\w－\-\.．\~_\?=%&#])+/o;
$dom1      = qr/${a}[\w－\-_]*[\.．]/o; # was: $dom2      = qr/${a}[\w\-_]{2,}\./o;
$dom2_allow_spaces      = qr/${a}[\w－\-_]+\s*[\.．]\s*/o;
$domN      = qr/${a}[\w－\-_]+[\.．](?:[\w－\-_]{2,}[\.．])+/o;
$domN_allow_spaces      = qr/${a}[\w－\-_]+\s*[\.．]\s*(?:[\w－\-_]{2,}\s*[\.．]\s*)+/o;
$POSTvar   = qr/(?:[a-zA-Z0-9\?=－\-\_]+\s*=(?:\s*[a-zA-Z0-9－\-\?=\_]+)?)/o;
$refOK     = qr/(?:$domN(?:$safeExt|$unsafeExt)|$dom1$safeExt)(?:$dirname)*(?:\s*[\/／])?(?:\s*\?\s*$POSTvar(?:\s*\&amp;\s*$POSTvar)*)?/o;
$refOK_allow_spaces     = qr/(?:$domN_allow_spaces(?:$safeExt|$unsafeExt)|$dom1$safeExt)/o;
$op        = qr/(?:\<|\&lt;)/o;
$cl        = qr/(?:\>|\&gt;)/o;

while (<>) {
  # formattage
  chomp;
  if (/ (_XML|_MS_ANNOTATION) *$/) {
    print "$_\n";
    next;
  }
  s/^\s*/ /o;
  s/\s*$/ /o;
  #    s/\&gt;/>/o;
  #    s/\&lt;/</o;

  # correction
  if ($do_spelling_correction && $lang !~ /^zh-/) {
    s/($protocole):\/([^\/])/$1:\/\/$2/go;
    if (!$no_sw) {
      s/($protocole):\s+\/([^\/])/$1:\/\/$2/go;
    }
    s/($protocole)\/\//$1:\/\//go;
    s/([^h])(ttps?):\/\//$1$2:\/\//go;
    if (!$no_sw) {
      s/($protocole)\s*:\s*\/\/\s*/$1:\/\//go;
      s/($protocole)(:\/\/$a+\.)\s/$1$2/go;
      s/($protocole)(:\/\/$a+\.$a+\.)\s([a-z]{2,})\b/$1$2$3/go;
      s/([\'\/\.\wàâäãéêèëîïöôùûüÿ-])($protocole):\/\//$1 $2:\/\//go;
      s/\. *(org|net|com|fr)\b/.$1/go;
      s/((?:$dom1$safeExt|$domN(?:$safeExt|$unsafeExt))[\w-\/\~]*)\s([\w-]*(?:\/[\w-\/]*|[\w-\/]*\.html?))\b/$1$2/go;
    }
    s/($protocole)(:\/\/[^\s,;]*)[âàäã]([^\s,;]*\.$a)/$1$2a$3/go;
    s/($protocole)(:\/\/[^\s,;]*)[éèêë]([^\s,;]*\.$a)/$1$2e$3/go;
    s/($protocole)(:\/\/[^\s,;]*)[îï]([^\s,;]*\.$a)/$1$2i$3/go;
    s/($protocole)(:\/\/[^\s,;]*)[ôö]([^\s,;]*\.$a)/$1$2o$3/go;
    s/($protocole)(:\/\/[^\s,;]*)[ûü]([^\s,;]*\.$a)/$1$2u$3/go;
    if (!$no_sw) {
      s/\btrad\.([^\s])/trad. $1/go;
    } else {
      s/\btrad\.([^\s])/trad. _REGLUE_$1/go;
    } 
  }

  # reconnaissance
  if ($no_sw) {
    ### minitel
    s/(^|\s)(36-?1[567]\s[^\s]+)(\s|$)/$1\{$2} _URL$3/;
    ### urls faciles (deux points dans le nom de domaine ou alors extension autre que .et et .de)
    s/(^|\s)($op?(?:(?:$protocole)\s*:\s*\/\/\s*)?$refOK$cl?)$post/$1\{$2\}_URL/go;
    ### urls de type (http://)?nomsanspoint.(et|de)/deschosesobligatoirement
    s/(^|\s)($op?(?:(?:$protocole)\s*:\s*\/\/\s*)?$dom1$unsafeExt(?:$dirname)+\/?$cl?)$post/$1\{$2\}_URL/go;
    ### urls de type http://nomsanspoint.(et|de)(/deschosesfacultatives)?
    s/(^|\s)($op?(?:(?:$protocole)\s*:\s*\/\/\s*)$dom1$unsafeExt(?:$dirname)*\/?$cl?)$post/$1\{$2\}_URL/go;
    s/(\s)\.\}_URL/\}_URL$1\./go || s/\.\}_URL/\}_URL\./go;
    ### twitter
    s/^ (.)([\@\#][a-zA-Z0-9])/ {$1$2}_URL/go; # so-called "fix-replies"
    s/(?<= )([\@\#][a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ0-9_]+)(?=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/\=\+\«\»\˝\`\.])/{\1}_URL/go; # "hashtags" and "replies"
    s/(?<= )(\.)(\@[a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ0-9_]+)(?=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/\=\+\«\»\˝\`\.])/{\1\2}_URL/go; # ".replies"
  } else {
    ### minitel
    s/(^|\s)(36-?1[567]\s[^\s]+)(\s|$)/$1\{$2} _URL$3/;
    ### urls faciles (deux points dans le nom de domaine ou alors extension autre que .et et .de)
    s/$pre($op?(?:(?:$protocole)\s*:\s*[\/／][\/／]\s*)?$refOK$cl?)$post/\{$1\}_URL/go;
    s/$pre($op?(?:$protocole)\s*:\s*[\/／][\/／]\s*$refOK_allow_spaces(?: [\/／])?$cl?)$post/\{$1\}_URL/go;
    ### urls de type (http://)?nomsanspoint.(et|de)/deschosesobligatoirement
    s/$pre($op?(?:(?:$protocole)\s*:\s*[\/／][\/／]\s*)?$dom1$unsafeExt(?:$dirname)+[\/／]?$cl?)$post/\{$1\}_URL/go;
    s/$pre($op?(?:$protocole)\s*:\s*[\/／][\/／]\s*$dom2_allow_spaces$unsafeExt(?: [\/／])?$cl?)$post/\{$1\}_URL/go;
    ### urls de type http://nomsanspoint.(et|de)(/deschosesfacultatives)?
    s/$pre($op?(?:(?:$protocole)\s*:\s*[\/／][\/／]\s*)$dom1$unsafeExt(?:$dirname)*[\/／]?$cl?)$post/\{$1\}_URL/go;
    s/(\s)([\.．])\}_URL/\}_URL$1$2/go || s/([\.．])\}_URL/\}_URL$1/go;
    ### twitter
    s/^ (.)([\@\#][a-zA-Z0-9])/ \1{\2}_URL/go; # so-called "fix-replies"
    s/(?<=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`])([\@\#][a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ0-9_]+)(?=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])/{\1}_URL/go; # "hashtags" and "replies"
    s/(?<= )(\.)(\@[a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ0-9_]+)(?=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])/{\1}_META_TEXTUAL_PONCT {\2}_URL/go; # ".replies"
  }
    
  # sortie
  if ($no_sw) {
    s/(_URL)([^\s])/$1 _REGLUE_$2/g;
  } else {
    s/({[^{}]*){([^{}]*)}\s*_URL([^{}]*})/\1\2\3/g;
    s/(_URL)([^\s])/$1 $2/g;
  }
  s/^\s*_<_\s*/_<_/go;
  s/\s*_<_\s*/ _<_/go;
  s/\s*_>_\s*/_>_ /go;
  s/^_\{/_/go;
  s/ _\{/ _/go;
  s/\}_[A-Z]+_ /_ /go;
  print "$_\n";
}

