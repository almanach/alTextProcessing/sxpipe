#!/usr/bin/env perl
# $Id$

$dico_file_name=shift;

$sxspeller_is_absent=`type sxspeller 2>&1;`; chomp($sxspeller_is_absent);
$sxspeller_is_absent=($sxspeller_is_absent=~/not found$/);
if (!$sxspeller_is_absent) {
    print STDERR "### WARNING: sxspeller is available. You should replace the call to ".__FILE__." by a call to sxspeller (typically sxspeller -cl -pl -cq -ch -pp -ps -c -nsc -d) to identify unknown words\n";
}

open(DICO,"<$dico_file_name") || die("L'ouverture du dico $dico_file_name a �chou�e: $!\n");

%dico=();

while(<DICO>) {
    chomp;
    $dico{$_}=1;
}

my $sep="";
while(<>) {
    chomp;
    $p="";
    $s=0;
    s/\'/\'_/g;
    s/^/ /;
    s/$/ /;
    while (s/([ \}])_((?:[A-Z������������]|__)+)_([^ _])/\1_\2\__\3/g) {}
    for (split(/(?<=[^ \}_])(?=(?: .|_[^_]))/,$_)) {
	s/^(.)//;
	$sep=$1;
	s/^ +//;
	s/ +$//;
	if (/^\{/) {
	    $s++;
	}
	if ($s || /^[A-Z������������]/ || /^[0-9\.\,\-\/]+$/ || $dico{$_} || $dico{"\"".$_."\""} || $_ eq "\"" || /^_/) {
	    $p = $p.$sep.$_;
	} else {
#	    print STDERR "$sep.<<<".$_.">>>\n";
	    $p = $p.$sep."<<<".$_.">>>";
	}
	if (/\}/) {
	    $s--;
	}
    }
    $p=~s/\'>>> <<</\'/g;
    $p=~s/^<<<([^ ]+\')>>> *([^ ]+)/<<<\1\2>>> /;
    $p=~s/ *<<<([^ ]+\')>>> *([^ ]+)/ <<<\1\2>>> /g;
    $p=~s/\'_/\'/g;
    $p=~s/ +/ /g;
    $p=~s/ *$/\n/;
    while ($p=~s/([ \}]_[A-Z������������]+_)_/\1/g) {print STDERR "$_\n";}
    print $p;
}
