#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

## numtruc to detect expressions 4-mère  or 3-foliacé

use strict;

my $no_sw=0;
my $suffix_mark="";
my $lang="fr";
my $more_nprefs = 0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-asm$/ || /^-add_suffix_mark$/i) {$suffix_mark="_-";}
    elsif (/^-more_nprefs?$/i) {$more_nprefs=1;}
}

$| = 1;


while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    if ($more_nprefs) {
      if ($no_sw) {
	s/\b(\s*)((?:(?:±\s*)?\d+[-­])+)(?=[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]{2,})/$1\{$2\}_NPREF/og;
      } else {
	s/\b(\s*)((?:(?:±\s*)?\d+[-­])+)(?=[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]{2,})/$1\{$2\}_NPREF/og;
      }
    } else {
      if ($no_sw) {
	s/\b(\s*)((?:(?:±\s*)?\d+[-­])+)(?=[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñ]{2,})/$1\{$2\}_NPREF/og;
      } else {
	s/\b(\s*)((?:(?:±\s*)?\d+[-­])+)(?=[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñ]{2,})/$1\{$2\}_NPREF/og;
      }
    }

    # sortie
    if ($no_sw) {
      s/(_NPREF|_NUM(?:_[^_]+_)?)([^_\s\}])/$1 _REGLUE_$suffix_mark$2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}(_NPREF|_NUM(?:_[^_]+_)?)/$1\{$2$3\} $2 _REGLUE_$4/g;
    } else {
      s/(_NPREF|_NUM(?:_[^_]+_)?)([^_\s])/$1 $suffix_mark$2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}(_NPREF|_NUM(?:_[^_]+_)?)/$1$2 \{$3\}$4/g;
    }

    print $_."\n";
}
