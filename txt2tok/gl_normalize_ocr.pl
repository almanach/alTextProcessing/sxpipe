#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";



while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) { print "$_\n"; next; }
    
    s/\{([^\}]+)\} ?_DATE_[ay][re][ta][ofr]/" _date" . "\{$1\}" . "\{" . ne_norm($1) . "\}"/ge; 
    s/\{([^\}]+)\} ?_ADRESSE/" _adresse" . "\{$1\}" . "\{" . ne_norm($1) . "\}"/ge; 

    s/\{([^\}]+)\} ?_NUM (à|et|ou|voire) \{([^\}]+)\} ?_NUM \{([^\}]+)\} ?_DIMENSION/" _num" . "\{$1\}" . "\{" . value_norm($1) . "\}" . " $2 " . " _num"."\{$3\}" . "\{" . value_norm($3) . "\} " . " _dimension" . "\{$4\}" . "\{" . unit_norm($4) . "\}"/ge; 

    s/\{([^\}]+)\} ?_NUM \{([^\}]+)\} ?_DIMENSION/" _num" ."\{$1\}" . "\{" . value_norm($1) . "\} " . " _dimension" . "\{$2\}" . "\{" . unit_norm($2) . "\}"/ge; 

    s/\{([^\}]+)\} ?_CHEM/" _chem" . "\{$1\}" . "\{" . chem_norm($1) . "\}"/ge; 

    s/\{([^\}]+)\} ?_CURRENCY/" _currency" . "\{$1\}" . "\{$1\}"/ge; 
    s/\{([^\}]+)\} ?_JURIDID/" _juridid" . "\{$1\}" . "\{$1\}"/ge; 

    print "$_\n";

}


sub exponent {
  my $s = shift;
  $s =~ tr/0123456789-/⁰¹²³⁴⁵⁶⁷⁸⁹⁻/;
  return $s;
}

sub lowindex {
  my $s = shift;
  $s =~ tr/0123456789-/₀₁₂₃₄₅₆₇₈₉⁻/;
  return $s;
}


# NE: date, adress:

sub ne_norm {
  my $s = shift;
  $s =~  s/([0-9])O/${1}0/g;
  $s =~  s/O([0-9])/0$1/g;
  $s =~  s/^[ÎÏ1iîlï][\'\"] /1er /g;
  $s =~  s/([0-9])[îiïÏlÎ]/${1}1/g;
  $s =~  s/^[ilÎÏïî] /1 /g;
  $s =~  s/^ii/11/g;
  $s =~  s/^ll/11/g;
  $s =~  s/[lîiÎÏï]([0-9])/1$1/g;

  $s =~  s/([0-9])G/${1}6/g;
  $s =~  s/^G /6 /g;
  $s =~  s/G([0-9])/6$1/g;

  $s =~  s/([0-9])j/${1}5/g;
  $s =~  s/^j /5 /g;
  $s =~  s/ j\. / 5. /g;
  $s =~  s/5([0-9])/5$1/g;

  $s =~  s/([VX])[lîiÎÏï]([^\w])/$1I$2/g;
  $s =~  s/([VXI])[lîiÎÏï]([^\w])/$1I$2/g;

  $s =~  s/([VX]+I{0,2})[^\w]/$1e/g;
  $s =~  s/([1-9][0-9]*)[°'"]/$1e/g;

  $s =~ s/ +/ /g;
  $s =~ s/ $//g;
 
  return "$s";
}


# dimension

sub value_norm {
  my $s = shift;
  $s =~  s/O/0/g;
  $s =~ s/(10) ([1-9][0-9]*) ?/$1.exponent($2)/e;
  $s =~ s/(10) ?- ?([1-9][0-9]*) ?$/$1.exponent("-".$2)/e;
  $s =~ s/ //g;
  return $s;
}

sub unit_norm {
  my $s = shift;
  $s =~ tr/1/l/;
  if ($s!~/[a-zéè]{4}/) {$s =~ s/ //g;}
  $s = exponent($s);
  return $s;
}

# chemical

sub chem_norm {
  my $s = shift;
  $s = lowindex($s);
  $s =~  s/0/O/g;
  $s =~ s/A1/Al/g;
  $s =~  s/NOx/NOₓ/g;
  $s =~ s/OZ$/O₂/;
  $s =~  s/-$/⁻/g;
  $s =~ s/\+$/⁺/g;
  $s =~ s/ +//g;
  return $s;
}




