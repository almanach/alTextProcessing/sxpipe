#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;
$afp = 0;
while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-l$/ || /^-lang$/i) {$lang=shift;}
    elsif (/^--?afp$/i) {$afp=1;}
    elsif (/^-xml$/i) {$xml=1;}
}
while (<>) {
  chomp;
  if ($lang =~ /^zh-/){
    s/_/_UNDERSCORE/g;
    s/{/ _ACC_O /g;
    s/}/ _ACC_F /g;
    s/\|/ _PIPE /go;
    s/\[/ _BRA_O /go;
    s/\]/ _BRA_F /go;
    s/\(/ _PAR_O /go;
    s/\)/ _PAR_F /go;

    sub normalise_latin{
        my $x = shift; 
        my @Xs = split("　",$x);
        my @Ys = map {
                my $x = $_ ; 
                my $y = $x ;
                $y =~ tr/[ａ-ｚ０-９Ａ-Ｚ]/[a-z0-9A-Z]/;
                "{$x} $y";} @Xs ; 
        return join(" ",@Ys);
    }; 
    s/((?:[ａ-ｚ０-９Ａ-Ｚ]+　?)+)/normalise_latin($1)/goe;
    s/　/ _ZHSPACE /go;
    print "$_\n";
    next;
  }

  if($afp)
    {
      s/^\s*((<p(ara)?)?\s*[^\/]{2,3}(\/[^\/]{2,3}){1,2}\s*(<\/p(ara)?>)?)\s*$/{$1} _XML/;
    }
  s/^\s*(<[\!\?]?[\w\.:_-]+(?: .*?)?\/?>(?:.*<\/[\w\.:_-]+>)?)\s*$/{$1} _XML/g;
  s/^\s*((?:<\/?[\w\.:_-]+[^>]*\/?>)+)\s*$/{$1} _XML/g;
  s/^\s*(<\!--[^>]+>)\s*$/{$1} _XML/g;
  s/^\s*(<\/[\w\.:_-]+>)\s*$/{$1} _XML/g;
  if (/(^| )(_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
    print "$_\n";
    next;
  }
  if ($lang !~ /^(fa|ckb)$/) {
    s/_/_UNDERSCORE/g;
  }

  s/{/_ACC_O/g;
  s/}/_ACC_F/g;

  s/(<fs type="anonymisation">.*?<\/fs>)/{__PROTECT:__$1__:PROTECT__} _NP/g;

  # Temporary fix to analyse roman numbers in Medieval French
  # s/\.([IiïjJVvXxLlDdMmCc]+(\s+[IiïJjVvXxLlDdMmCc]+){0,3})\./§\1§/g;
  # s/ ([Cc]+|m|[IiJj]+|l(ii)?|[Xx]+[Ii][Xx])\./ \1§/g;

  unless ($xml) {
    s/</&lt;/g;
    s/>/&gt;/g;
  }
  s/´/'/g;
  s/’/'/g;
  s/\t/   /g;
  s/\r$//g;
  s/\r/\n/g;
  s/((?:。|&#227;&#128;&#130;)+)([^ ]|$)/\1 \2/g; # le point en japonais: 。 (sinon gl_entities ne passe pas sur les paragraphes trop longs)

  s/__PROTECT:__(.*?)__:PROTECT__/restore_protected_content($1)/ge;

  print "$_\n";
  if (/_PAR_BOUND *$/) {
    print "\n";
  }
}

sub restore_protected_content {
  my $s = shift;
  $s =~ s/&lt;/</g;
  $s =~ s/&gt;/>/g;
  $s =~ s/_UNDERSCORE/_/g;
  return $s;
}
