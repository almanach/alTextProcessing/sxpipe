#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


# ATTENTION: à exécuter avant gl_number.pl


$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

$main_unit = qr/(?:[mkc]\.|(?:[khdcmμnpa]?[mgls]|cc|[khdcmμnpa]1|t\.p\.m\.|h(?:heures?)|minutes?|[a-zé]*secondes?|[a-zé]*litres?|[a-zé]*mètres?|ares?|hectares?)(?:[2-9²-⁹]| *carré| *cube)?)/;
$by_unit = qr/(?:\/(?:[khdcmμnpa]?[mgl1s]|h)(?:[2-9²-⁹]| *carré| *cube)?)/;
$unit = qr/(?:$main_unit(?:$by_unit(?:$by_unit)?)?|1$by_unit(?:$by_unit)?)/;

$num = qr/(?:0|10 *-?[1-9][0-9]*|0,[0-9]+|[1-9][0-9]*(?:,[0-9]+)?(?:\.10 *-?[1-9][0-9]*)?)/;

while (<>) {
  chomp;
  s/^\s*/  /;
  s/\s*$/  /;

=comment
  if (0) {    
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])(10 *-?[1-9][0-9]*) ($unit)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2." ".$3."}_DIMENSION_".dim_norm($2." ".$3)."_"/ge;
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])(0,[0-9]+ ?|[1-9][0-9]*(?:,[0-9]+)? ?)(°[CF])(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2.$3."}_DIMENSION_".dim_norm($2.$3)."_"/ge;
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])(0,[0-9]+ ?|[1-9][0-9]*(?:,[0-9]+)?(?:\.10 *-?[1-9][0-9]*)? ?) ($unit)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2." ".$3."}_DIMENSION_".dim_norm($2." ".$3)."_"/ge;
    
    s/(_DIMENSION_[^_]+_[\s:;\?\!\(\)\[\]\"\+\*\-]+)((?:et|ou|à)(?: environ)?)(10 *-?[1-9][0-9]* ?) ($unit)(?=[ \.,;:\?\!\(\)\[\]])/$1.$2."{".$3." ".$4."}_DIMENSION_".dim_norm($3." ".$4)."_"/ge;
    s/(_DIMENSION_[^_]+_[\s:;\?\!\(\)\[\]\"\+\*\-]+)((?:et|ou|à)(?: environ)?) (0,[0-9]+ ?|[1-9][0-9]*(?:,[0-9]+)? ?)(°[CF])(?=[ \.,;:\?\!\(\)\[\]])([ \.,;:\?\!\(\)\[\]])/$1.$2."{".$3.$4."}_DIMENSION_".dim_norm($3.$4)."_"/ge;
    s/(_DIMENSION_[^_]+_[\s:;\?\!\(\)\[\]\"\+\*\-]+)((?:et|ou|à)(?: environ)?)(0,[0-9]+ ?|[1-9][0-9]*(?:,[0-9]+)?(?:\.10 *-?[1-9][0-9]*)? ?) ($unit)(?=[ \.,;:\?\!\(\)\[\]])/$1.$2."{".$3." ".$4."}_DIMENSION_".dim_norm($3." ".$4)."_"/ge;
    
    s/{([^{}]+)} *_DIMENSION_([^_]+)_ {([^{}]+)} *_DIMENSION_([^_]+)_/{$1 $3}_DIMENSION_$2 $4_/g;
  } else {
=cut

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])($num)( (?:à|et|ou|voire) | ?- ?)($num) ($unit)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2."}_NUM".$3."{".$4."}_NUM"." {".$5."}_DIMENSION"/ge;
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])($num)( (?:à|et|ou|voire) | ?- ?)($num) ?(°[CFK]|\%)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2."}_NUM".$3."{".$4."}_NUM"." {".$5."}_DIMENSION"/ge;
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])($num) ($unit)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2."}_NUM"." {".$3."}_DIMENSION"/ge;
    if ($no_sw) {
      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])($num) (°[CFK]|\%)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2."}_NUM"." {".$3."}_DIMENSION"/ge;
    } else {
      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\"\+\*\-])($num) ?(°[CFK]|\%)(?=[ \.,;:\?\!\(\)\[\]])/$1."{".$2."}_NUM"." {".$3."}_DIMENSION"/ge;
    }
    s/{([^{}]+)} *_DIMENSION_([^_]+)_ {([^{}]+)} *_DIMENSION_([^_]+)_/{$1 $3}_DIMENSION_$2 $4_/g;

    # post-correction de cas particuliers
    s/(1res?|premi[eè]res?)(,) \{(\d+)}_NUM \{(secondes?)}_DIMENSION/$1$2 $3 $4/g;
    $ne = qr/(?:_(?:DIMENSION|NUM)(?:_[^_]+_)?)/o;
    if ($no_sw) {
      s/\s*($ne)([^\s\}_])/$1 _REGLUE_$2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}\s*($ne)/$1\{$2$3\} $2 _UNSPLIT_$4/g;
      if (s/{_REGLUE_.*?}//) {
        print STDERR "Warning: gl_dimensions encoutered an unexpected situation (input data is approx: $_)\n";
      }
    } else {
      s/_REGLUE_/_PROTECTREGLUE_/g;
      s/($ne)([^\s_])/$1 $2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}($ne)/$1$2 \{$3\}$4/g;
      s/(?<=[^} ]) _REGLUE_//g;
      s/_PROTECTREGLUE_/_REGLUE_/g;
    }
    s/_SPECIAL_//g;
  
    s/^(.*_META_TEXTUAL_[^ ]+)/\1 _UNSPLIT__SENT_BOUND/g;
    s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne) _REGLUE_/$1$2 _UNSPLIT_/g;
    s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne)/$1$2/g;
  
    s/^ +//;
    s/ +$//;
    print "$_\n";
}

