#!/usr/bin/env perl
# $Id$

# grammaire locale des m�ta-mots: le mot " mot " est un m�tamot, quel est le sens de " sgrumplumpf "
# et des mots quot�s: on les appelle les " schtroumpfs "

$| = 1;

$l = qr/(?:[����������������a-zA-Z������������])/;

while (<>) {
    # formattage
    chomp;
    s/^\s*/ /o;
    s/\s*$/ /o;

    # reconnaissance
    if (/} *{/) {
	print STDERR "Warning: found pattern /} {/ in input line $_. Pattern deleted\n";
    }
    s/ ([\"�] *)($l+)( *[\"�])/ {$1$2$3} $2/go;
    s/ (\� *)($l+)( *\�)/ {$1$2$3} $2/go;
    s/ ([\"�] *)($l+)((?: $l+)*) +($l+)( *[\"�]) / {$1$2} $2$3 {$4$5} $4 /go;
    s/ (\� *)($l+)((?: $l+)*) +($l+)( *\�) / {$1$2} $2$3 {$4$5} $4 /go;

    s/ (\' +)($l+)( +\')/ {$1$2$3} $1/go;
    s/ (\'+)($l+)((?: $l+)*) ($l+)( +\') / {$1$2} $2$3 {$4$5} $4 /go;

    s/(?<=[ {])([^ {}]+)} *{\1/$1/g; # {euh "} " la " -> {euh "} {" la "} la -> {euh " la "} la
    # sortie
    s/^ //o;
    s/ $//o;
    print "$_\n";
}
