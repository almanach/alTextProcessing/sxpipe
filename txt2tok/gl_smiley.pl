#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
}

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/^\s*/ /o;
    s/\s*$/ /o;

    # reconnaissance
    s/([;:=]\s*(?:'\s*)?(?:-+\s*)?(?:(?:\(|-LRB-)(?:\s*(?:\(|-LRB-))*|(?:\)|-RRB-)(?:\s*(?:\)|-RRB-))*|\[|\]|[DdPpOoSs]|\$+))(?=\s)/\{$1}_SMILEY/go;
    s/(?<=\s)([xX][dD]|wsh|mdr|MDR|ptdr+|PTDR+|--+'|(?:<|&lt;)3|(?:>|&gt;)(_|_UNDERSCORE)+(?:<|&lt;)|lol+|-(?:_UNDERSCORE)+-|\\o+\/|[Oo]_[Oo]|\^(?:_UNDERSCORE)*\^+['"]*)(?=\s)/\{$1}_SMILEY/go;
    s/(?<=\s)([\&\@\#][\!\?\&\@\#]+)(?=\s)/ {\1}_SMILEY/g;
#    s/([\!\?\.;])\s+([\!\?]{2,})(?=\s)/\1 {\2}_SMILEY/g; changé en le suivant
    s/([\!\?;])\s+([\!\?]{2,})(?=\s)/\1 {\2}_SMILEY/g;
    s/(?<=\s)([\!\?]+[\&\@\#][\!\?\&\@\#]*)(?=\s)/ {\1}_SMILEY/g;

    # correction de sur-reconnaissance
    s/^\s*(\([^\)]*){([^{}]*\))}_SMILEY([\s\.]*)$/\1\2\3/go;
    s/{([^}]*\[)}_SMILEY((?:[^:]|{[^{}]*})*\])/\1\2/go;
    s/{([^}]*\()}_SMILEY((?:[^:]|{[^{}]*})*\))/\1\2/go;

    # gestion des cas où un commentaire existait déjà (on est sûrs que les {...} _SMILEY internes ne sont pas contre un { ou un })
    while (s/{([^{}]+){([^{}]+)} *_SMILEY([^{}]+)}/{\1\2\3}/g) {}
    s/({[^{}]+}) *{[^{}]+} *_SMILEY/\1 _SMILEY/g;

    # sortie
    s/^ //o;
    s/ $//o;
    print "$_\n";
}

