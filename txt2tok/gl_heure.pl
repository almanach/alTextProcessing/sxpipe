#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

if ($lang =~ /^zh-eleve/){
    $h = qr/(?:点钟|點鐘|时|點|点)/o;
    $m = qr/(?:分钟|分鐘|分)/o;
    $s = qr/(?:秒)/o;
    $zhdigit = qr/(?:[〇零一二三四五六七八九○])/o;
    $num12 = qr/(?:$zhdigit|两|兩|[0-9０-９]|十[一二]?|1[０012])/o;
    $num24 = qr/(?:十?$zhdigit|两|兩|1?[0-9０-９]|二十[一二三四]?|2[０-４0-4])/o;
    $num60 = qr/(?:[0-9０-９]{1,2}|两|兩|(?:(?:二三四五六)?十)?$zhdigit?)/o;
    $hour = qr/(?:$num24$h)/o;
    $minute = qr/(?:$num60$m|半)/o;
    $second = qr/(?:$num60$s)/o;
    $dayperiod = qr/(?:凌晨|早上|上午|中午|下午|晚上)/o;
}
else {
    $heure = qr/(?:0?\d|[1l]\d|2[0-4])/o;
    $min   = qr/(?:[0-5]\d)/o;
    $sec   = qr/(?:[0-5]\d)/o;
    if ($lang eq "sk") {
      $hm    = qr/(?:$heure,[0-9]+\s*[hH](?:\s*eures?)?|$heure\s*[hH](?:\s*eures?)?(?:(?:\s+|:)?$min(?:\s*min(?:utes?)?)?)?)/oi;
      $ms    = qr/(?:$min(?:\s*min(?:utes?)?\s*|:)$sec(?:\s*sec(?:onde)?s?)?)/oi;
      $link  = qr/(?:\/|do|alebo)/oi;
    } elsif ($lang eq "pl") {
      $hmgodz= qr/(?:godz(?:\s*\.|in[^\s]+)?\s*$heure(?:(?:\s*min(?:\s*$min)?|(?:\s*[\.:]|\s+)$min))?)/oi;
      $hmnogodz= qr/(?:$heure(?:\s*\.)?(?:\s*min(?:\s\.)?\s*|\s+)$min)/oi; # |$min min(?: \.)?
      $hm    = qr/(?:$hmgodz|$hmnogodz)/oi;
      $ms    = qr/(?:$min(?:\s*min(?:utes?)?\s*|:)$sec(?:\s*sec(?:onde)?s?)?)/oi;
      $link  = qr/(?:\/|do|lub)/oi;
    } elsif ($lang eq "en") {
      if (0) {
        $hm    = qr/(?:$heure\s*[:\.]\s*$min(?:\s*[AaPp][Mm])?|$heure\s*[AaPp][Mm]|$heure,[0-9]+\s*[hH](?:\.|\s*ours?)?|$heure\s*[hH](?:\s*ours?)?(?:\s*$min(?:\s*min(?:ute)?s?)?)?)/oi;
      }
      $hm    = qr/(?:$heure\s*[:\.]\s*$min(?:\s*[:\.]\s*$sec)?|$heure,[0-9]+\s*[hH](?:\.|\s*eures?)?|$heure\s*[hH](?:\.|\s*eures?)?(?:\s*$min(?:\s*min(?:\.|(?:ute)?s?))?(?:\s*$sec(?:\s*sec(?:\.|(?:onde)?s?))?)?)?|$heure\s*[:\.]\s*$min(?:\s*[AaPp][Mm])?|$heure\s*[AaPp]\.? ?[Mm]\.?|$heure,[0-9]+\s*[hH](?:\s*ours?)?|$heure\s*[hH](?:\s*ours?)?(?:\s*$min(?:\s*min(?:ute)?s?)?)?)/oi;
      $ms    = qr/(?:$min(?:\s*min(?:\.|(?:ute)?s?)\s*|:)$sec(?:\s*sec(?:\.|(?:onde)?s?))?)/oi;
      $link  = qr/(?:[\/-]|to|or)/oi;
    } else {
       $hm    = qr/(?:$heure\s*[:\.]\s*$min(?:\s*[:\.]\s*$sec)?|$heure,[0-9]+\s*[hH](?:\.|\s*eures?)?|$heure\s*[hH](?:\.|\s*eures?)?(?:(?:\s+|:)?$min(?:\s*min(?:\.|(?:ute)?s?))?(?:\s*$sec(?:\s*sec(?:\.|(?:onde)?s?))?)?)?)/oi;
#      $hm    = qr/(?:$heure\s*h\s*$min(?:\s*[:\.]\s*$sec)?|$heure,[0-9]+\s*[hH](?:\s*eures?)?|$heure\s*[hH](?:\s*eures?)?(?:(?:\s+|:)?$min(?:\s*min(?:ute)?s?)?(?:\s*$sec(?:\s*sec(?:onde)?s?)?)?)?)/oi;
      $ms    = qr/(?:$min(?:\s*min(?:utes?)?\s*|:)$sec(?:\s*sec(?:onde)?s?)?)/oi;
      $link  = qr/(?:[\/-]|à|ou)/oi;
    }
}
$timezonename = qr/(?:AGST|AKDT|AKST|ALMT|AMST|ANAT|AQTT|ASST|AZOT|AZST|BRST|CAST|CEST|CLST|ChST|DAVT|EAST|EEST|EGST|EKST|ETST|FJST|FKST|GALT|GAMT|GILT|HADT|HAST|HOVT|IRDT|IRKT|IRST|KRAT|KUYT|LAST|LHDT|LHST|LINT|MAGT|MART|MAWT|MSST|NAST|NOVT|NZDT|NZST|OMST|OVST|PETT|PHOT|PMDT|PMST|PONT|PYST|RAST|RKST|SAMT|SAST|TAHT|ULAT|UYST|VLAT|WAST|WEST|WGST|WITA|YAKT|YAPT|YEKT|ZOST|ADT|AFT|AMT|ART|AST|AZT|BNT|BOT|BRT|BST|BTT|CAT|CCT|CDT|CET|CKT|CLT|COT|CST|CVT|CXT|EAT|ECT|EDT|EET|EGT|EST|FJT|FKT|FNT|GET|GFT|GMT|GST|GYT|HAA|HAC|HAE|HAP|HAR|HAT|HAY|HKT|HLV|HNA|HNC|HNE|HNP|HNR|HNT|HNY|ICT|IDT|IOT|IST|JST|KGT|KST|MDT|MHT|MMT|MSD|MSK|MST|MUT|MVT|MYT|NCT|NDT|NFT|NPT|NST|NUT|PDT|PET|PGT|PHT|PKT|PST|PWT|PYT|RET|SBT|SCT|SGT|SRT|SST|TFT|TJT|TKT|TLT|TMT|TVT|UYT|UZT|VET|VUT|WAT|WDT|WET|WFT|WGT|WIB|WIT|WST|ET|PT|WT|A|B|C|D|E|F|G|H|I|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z)/o;
$timezone = qr/(?:GMT\s*[\+-]\s*\d+(?:\s*(?:\(\s*$timezonename\s*\)|$timezonename))?|$timezonename)/o;
while (<>) {
  # formattage
  chomp;
  if (/ (_XML|_MS_ANNOTATION) *$/) {
    print "$_\n";
    next;
  }
  
  s/^\s*/ /o;
  s/\s*$/ /o;

  if ($lang =~ /^zh-eleve/){
    s/($dayperiod)?($hour)($minute)?($second)?/\{$1 $2 $3 $4\}_HEURE/go;
  }
  else {
    $rctxt=qr/(?=[^\}0-9a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ])/;
    $rctxt2=qr/(?=[^\}0-9a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ'])/;
    
    # reconnaissance
    # note that we refuse "m" as a left context, to prevent recognizing things when it's only m2, m3, km2, and so on
    if ($lang eq "fr") {
      s/(?<=[^0-9\{\-m])($hm(?:\s*$timezone)?)(\s+)\-(\s+)($hm(?:\s*$timezone)?)$rctxt/{$1}_HEURE$2\{-} à$3\{$4}_HEURE/go;
    } else {
      s/(?<=[^0-9\{\-m])($hm(?:\s*$timezone)?)(\s+)\-(\s+)($hm(?:\s*$timezone)?)$rctxt/{$1}_HEURE$2\{-} -$3\{$4}_HEURE/go;
    }
    s/(?<=[^0-9\{\-m])($hm(?:\s*$timezone)?)(\s+$link\s+)($hm(?:\s*$timezone)?)$rctxt/{$1}_HEURE$2\{$3}_HEURE/go;
    if ($lang eq "fr") {
      s/(?<=[^0-9\{\-m])($heure(?:\s*,\s*[0-9]+)?(?:\s*$timezone)?)(\s+)\-(\s+)($hm(?:\s*$timezone)?)$rctxt/{$1}_HEURE$2\{-} à$3\{$4}_HEURE/go;
    } else {
      s/(?<=[^0-9\{\-m])($heure(?:\s*,\s*[0-9]+)?(?:\s*$timezone)?)(\s+)\-(\s+)($hm(?:\s*$timezone)?)$rctxt/{$1}_HEURE$2 -$3\{$4}_HEURE/go;
    }
    s/(?<=[^0-9\{\-m])($heure(?:\s*,\s*[0-9]+)?(?:\s*$timezone)?)(\s+$link\s+)($hm(?:\s*$timezone)?)$rctxt/{$1}_HEURE$2\{$3\}_HEURE/go;
    s/(?<=[^0-9\{\-m])($hm\s*(?:$timezone\s*)?)\-( ?$hm(?:\s*$timezone)?)$rctxt/{$1-$2}_HEURE/go;
    #    s/(?<=[^0-9\{\-m])($hm ?)($link)( ?$hm)$rctxt/{$1$2$3}_HEURE/go; # non: 4 ligne plus haut fait ça en mieux
    s/(?<=[^0-9\{\-m])($heure(?:\s*,\s*[0-9]+)?\s*(?:$timezone\s*)?)\-( ?$hm(?:\s*$timezone)?)$rctxt/{$1-$2}_HEURE/go;
    s/(?<=[^0-9\{\-m])($heure(?:\s*,\s*[0-9]+)?\s*(?:$timezone\s*)?)($link)($hm(?:\s*$timezone)?)$rctxt/{$1$2$3}_HEURE/go;
    s/(?<=[^0-9\{\-m])($hm\s*$timezone)$rctxt2/{$1}_HEURE/go;
    s/(?<=[^0-9\{\-m])($hm)$rctxt/{$1}_HEURE/go;
#modif KG    s/(?<=[^0-9\{\-m])($heure\s*:(?:\s*$min(?:\s*min(?:ute)?s?)?)(?:\s*[AaPp][Mm])?(?:\s*$timezone)?)$rctxt/{$1}_HEURE/go;
    s/(?<=[^0-9\{\-m])($heure\s*h(?:\s*$min(?:\s*min(?:ute)?s?)?)(?:\s*[AaPp][Mm])?(?:\s*$timezone)?)$rctxt/{$1}_HEURE/go;
   
    s/(?<=[^0-9\{\-m])($ms)$rctxt/{$1}_HEURE/go;
   
    if ($no_sw) {
      s/(_HEURE)([^\s])/$1 _REGLUE_$2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}_HEURE/$1\{$2$3\} $2 _UNSPLIT__HEURE/g;
    } else {
      s/(_HEURE)([^\s])/$1 $2/g;
      s/(^|\s)([^\s\{]+){([^{}]*)}_HEURE/$1$2 \{$3\}_HEURE/g;
    }
   
    # temporaire
    if ($lang eq "pl") {
      s/_HEURE/_TIME/g;
    }
  }
  # sortie
  s/^ //o;
  s/ $//o;
  print "$_\n";
}
