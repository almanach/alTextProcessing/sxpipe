#!/usr/bin/env perl
# $Id$

use strict;
use Getopt::Long;
use Lingua::Matcher;

$| = 1;

my $unk_subtaxon = '\(\?\)';
my $any_subtaxon = 'spp?\.?';

my ($data, $no_sw);
GetOptions(
    'data=s' => \$data,
    'no_sw' => \$no_sw
);

my $build = sub {
    my ($string) = @_;

    my ($type, $name, $authors) = split(/\t/, $string);

    # additional forms
    if ($type eq 'species') {
        die unless $name =~ /^([A-Z][a-z]+)\s+([a-z-]+)/;
        my $genus   = $1;
        my $species = $2;
        my $initial = uc(substr($genus, 0, 1)) . '\.';
        $name = "(?:$genus|$initial)(?: $unk_subtaxon )? $species(?: $unk_subtaxon)?";
      } elsif ($type eq 'genus') {
        $name = "$name(?: $any_subtaxon)?";
      }

    # build final pattern
    my $pattern = $authors ? "$name(?: $authors)?" : "$name";

    return "\\b$pattern\\b";
    
};

my $correct = sub {
    my ($string) = @_;

    $string =~ tr/ / /s;

    return $string;
};

my $matcher = Lingua::Matcher->new(
    'SCIENTIFIC_NAME',
    $data ? $data : \*DATA,
    1,
    $build,
    ($no_sw ? undef : $correct)
);

$matcher->run();
