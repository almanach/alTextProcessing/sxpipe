#!/usr/bin/env perl
# $Id: gl_url.pl 4820 2012-08-03 13:58:55Z sagot $
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


# ATTENTION: à exécuter APRES gl_number.pl

$| = 1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-no_corr$/ || /^-no_correction$/i) {$no_corr=1;}
}

$lang="fr";

$chemelt0 = qr/(?:MII+|[BCFHIKNOPSUVWY]|R\'*)/;
$chemelt1 = qr/(?:R\'+|MII+|A[cglmrstu]|B[aehikr]|C[adeflmnorsu]|D[bsy]|E[rsu]|F[elmr]|G[ade]|H[efgos]|I[nr]|Kr|L[airuv]|M[dgnot]|N[abdeiop]|Os|P[abdmortu]|R[abefghnu]|S[bcegimnr]|T[abcehilm]|Xe|Yb|Z[nr])/;
$chemelt_maj = qr/(?:R\'+|MII+|[BCFHIKNOPSUVWY]|A[CGLMRSTU]|B[AEHIKR]|C[ADEFLMNORSU]|D[BSY]|E[RSU]|F[ELMR]|G[ADE]|H[EFGOS]|I[NR]|KR|L[AIRUV]|M[DGNOT]|N[ABDEIOP]|OS|P[ABDMORTU]|R[ABEFGHNU]|S[BCEGIMNR]|T[ABCEHILM]|XE|YB|Z[NR])/;
$chemelt = qr/(?:$chemelt0|$chemelt1)/;
$noisy_chemelt = qr/(?:0)/;
$chemical_abbreviation = qr/(?:DMF)/;
$chem_numindex = qr/(?:[2-9]|[1-9][0-9]+)/;

$postchem = qr/(?:(?: ?\. ?[0-9]{0,2})(?:[A-Z][a-z]?(?:I+|[0-9]{1,2}))+)/o;

$safe_chem_formula = qr/(?:${chemelt}$chem_numindex?( ?[\-=])?(?: ?${chemelt}$chem_numindex?( ?[\-=])?)*)/;
$safe_chem_formula_min2elts = qr/(?:${chemelt}$chem_numindex?( ?[\-=])?(?: ?${chemelt}$chem_numindex?( ?[\-=])?)+)/;
$noisy_chem_formula_elt = qr/${noisy_chemelt}$chem_numindex?( ?[\-=])?/;
$noisy_chem_formula = qr/(?:$safe_chem_formula ?- ?(?:$noisy_chem_formula_elt|$chemical_abbreviation) ?- ?$safe_chem_formula|$safe_chem_formula ?- ?(?:$noisy_chem_formula_elt|$chemical_abbreviation)|(?:$safe_chem_formula ?- ?)?(?:$noisy_chem_formula_elt|$chemical_abbreviation) ?- ?$safe_chem_formula)/;
$chem_formula = qr/(?:$noisy_chem_formula|$safe_chem_formula)/;
$chem_formula_min2elts = qr/(?:$noisy_chem_formula|$safe_chem_formula_min2elts)/;
$braketed_chem_formula = qr/(?:\( ?$chem_formula ?\)(?: ?$chem_numindex)?)/;
$chem_formula_with_brackets = qr/(?:$chem_formula(?:( ?[\-=])? ?$braketed_chem_formula( ?[\-=])? ?(?:$chem_formula( ?[\-=])?)?)+|$braketed_chem_formula(?:( ?[\-=])? ?(?:$chem_formula( ?[\-=])? ?)?$braketed_chem_formula( ?[\-=])? ?)*$chem_formula|$chem_formula_min2elts)/;
$chem_formula_with_2_levels_of_brackets = qr/(?:(?:[-=] ?)?(?:$chem_formula_with_brackets(?:( ?[\-=])? ?\[ ?$chem_formula_with_brackets ?\](?: ?$chem_numindex)? ?$chem_formula_with_brackets?)+|(?:\[ ?$chem_formula_with_brackets ?\](?: ?$chem_numindex)?( ?[\-=])? ?$chem_formula_with_brackets?( ?[\-=])? ?)+$chem_formula_with_brackets))/;
$generic_chem_formula = qr/(?:(?:$chem_formula_with_2_levels_of_brackets|$chem_formula_with_brackets|$chem_formula_min2elts)(?: ?$postchem| ?\d*?[\-\+])?)/;

$cheml1 = qr/(?<=[A-Z0-9])(?:[a-z])/;
$cheml2 = qr/(?:[-=]?[A-Z0-9])/;
$cheml3 = qr/(?:[\)])(?=[0-9])/;
$cheml4 = qr/(?:[\(])(?=[A-Z])/;
$cheml12 = qr/(?:$cheml1|$cheml2)/;
$cheml1234 = qr/(?:$cheml1|$cheml2|$cheml3|$cheml4)/;

$chem0 = qr/(?:(?:[A-Z][a-z]?[0-9]{1,2}|RI+)+)/;
$chembr = qr/(?:\([A-Z]$cheml12*\)[0-9]{1,2})/;
$chem1 = qr/(?:[A-Z]$cheml12*|$chembr)/;
$chem1long = qr/(?:[A-Z]$cheml12+|$chembr)/;
$chem2 = qr/(?:[A-Z][a-z][A-Z](?:[a-z0-9]$cheml1234*)?(?:$cheml1|[0-9]))/;
$chem3 = qr/(?:(?:[A-Z](?:$cheml1|$cheml2$cheml2?[0-9a-z]){3,}|$chembr)+$cheml2?)/;
$chem4 = qr/(?:[A-Z][a-z](?:[a-z0-9]$cheml1234*)?(?:$cheml1|[0-9]))/;
$chem = qr/(?:${chembr}$chem1|(?:$chem1?(?:$chem2|$chem3)|${chem1}$chem4|$chem1long)$postchem?)/;

$unsafe_known_chem = qr/(?:OZ|Z[1-9]|N2|R[0-9][0-9]?'?|H2)/;
$known_chem = qr/(?:CO2|H2O2?|NOx|NO[23]|CO|O2|H2|-?CHOH|-?COOH)/;
$known_rad = qr/-(?:CHOH|COOH)/;

while (<>) {
  chomp;
  s/^\s*/  /o;
  s/\s*$/  /o;

  $line_seems_to_contain_chemical_formulae = 0;
  # we first try (in dry-run mode) a few safe regexps, in order to know whether we can be certain that the current sentence contains chemical formulae.
  if (/(?<=[\s\.\,\:\=])((\(?$chemelt0\)?|\(?$chemelt1\)?)+$chem_numindex((\(?$chemelt1+$chem_numindex\)?)|(\(?$chemelt0+$chem_numindex\)?))+(\(?($chemelt0|$chemelt1)*\)?$chem_numindex?\)?)*$chemelt0*[\+−-]?)(?=[\s\,\.\:\=])/
      || /(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-])($known_chem|$known_rad)(?=[ \.,;:\?\!\(\)\[\]])/
      || /_CHEM/ # for testing purposes (add "_CHEM" to your test input line, and line_seems_to_contain_chemical_formulae will be 1
     ) {
    $line_seems_to_contain_chemical_formulae = 1;
  }

=comment
  if ($line_seems_to_contain_chemical_formulae) {
    #... We can be certain that the current sentence contains chemical formulae, so let's apply less safe regexps first (they might match more formulae and/or longer formulae than safer regexps)
    s/groupe(-$generic_chem_formula)(?=[ \.,;:\?\!\(\)\[\]])/"{groupe".$1."} groupe _UNSPLIT__CHEM_".chem_norm($1)."_"/ge;

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\|])($generic_chem_formula)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM_".chem_norm($2)."_"/ge;

    s/(groupe)($known_rad)(?=[\s\,\.\:\=])/"{".$1.$2."} $1 _UNSPLIT__CHEM_".chem_norm($2)."_"/ge;

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-\|])(C[1-9][0-9]*-C?[1-9][0-9]*)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM_".chem_norm2($2)."_"/ge;

    s/(?<=[^\}])(\s)(02)(?=\s)/$1."{".$2."} _CHEM_".chem_norm2("O2")."_"/ge;
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-\|])(H20)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM_".chem_norm2("H2O")."_"/ge;
    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-\|])(C02)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM_".chem_norm2("CO2")."_"/ge;

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-\|])($unsafe_known_chem)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM_".chem_norm($2)."_"/ge;
  }
=cut

  if ($line_seems_to_contain_chemical_formulae) {
    #... We can be certain that the current sentence contains chemical formulae, so let's apply less safe regexps first (they might match more formulae and/or longer formulae than safer regexps)
    s/groupe(-$generic_chem_formula)(?=[ \.,;:\?\!\(\)\[\]])/"{groupe".$1."} groupe _UNSPLIT__CHEM"/ge;

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\|])($generic_chem_formula)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM"/ge;
    s/(groupe)($known_rad)(?=[\s\,\.\:\=])/"{".$1.$2."} $1 _UNSPLIT__CHEM"/ge;

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-\|])(C[1-9][0-9]*-[1-9][0-9]*)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM"/ge;

    s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\"\+\*,\-\|])($unsafe_known_chem)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM"/ge;
  }


  # we apply these safer regexps in all cases

      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\|\"\+\*,\-\|])(métal(?: [a-zé]+valent)?|formules?(?: (?:chimique|générale)s?)?) ($chem0$postchem?)(?=[ \.,;:\?\!\(\)\[\]\|])/$1.$2." {".$3."} _CHEM"/ge;

      s/(?<=[\s\.\,\:\=])((\(?$chemelt0\)?|\(?$chemelt1\)?)+$chem_numindex((\(?$chemelt1+$chem_numindex\)?)|(\(?$chemelt0+$chem_numindex\)?))+(\(?($chemelt0|$chemelt1)*\)?$chem_numindex?\)?)*$chemelt0*[\+−-]?)(?=[\s\,\.\:\=])/"{".$1."} _CHEM"/ge;

      s/(?<=[^\}])([\s:;\?\!\(\)\[\]\/\|\"\+\*,\-\|])($known_chem|$known_rad)(?=[ \.,;:\?\!\(\)\[\]\|])/$1."{".$2."} _CHEM"/ge;
      
#   }
# }

  while (s/(_CHEM_[^_]+_)([\s:;\?\!\(\)\[\]\"\+\*,\-\|]+(?:et|ou)| ?,| \+) ((?:[A-Z][a-z]?(?:I+|[0-9]{1,2}))+$postchem?)([ \.,;:\?\!\(\)\[\]])/$1.$2." {".$3."} _CHEM"/ge) {}
  while (s/((?:[A-Z][a-z]?(?:I+|[0-9]{1,2}))+$postchem?)([\s:;\?\!\(\)\[\]\"\+\*,\-\|]+(?:et|ou)| ?,| \+) *({[^{}]+} *_CHEM_[^_]+_)([ \.,;:\?\!\(\)\[\]])/"{".$1."} _CHEM".$2." ".$3.$4/ge) {}


  while (s/({[^{}]*){([^{}]*)} _CHEM_[^_]+_/$1$2/g) {}

  $ne = qr/(?:_CHEM(?:_[^_]+_)?)/o;
  if ($no_sw) {
    s/\s*($ne)([^\s\}_])/$1 _REGLUE_$2/g;
    s/(^|\s)([^\s\{]+){([^{}]*)}\s*($ne)/$1\{$2$3\} $2 _UNSPLIT_$4/g;
    if (s/{_REGLUE_.*?}//) {
      print STDERR "Warning: gl_chemicals encoutered an unexpected situation (input data is approx: $_)\n";
    }
  } else {
    s/_REGLUE_/_PROTECTREGLUE_/g;
    s/($ne)([^\s_])/$1 $2/g;
    s/(^|\s)([^\s\{]+){([^{}]*)}($ne)/$1$2 \{$3\}$4/g;
    s/(?<=[^} ]) _REGLUE_//g;
    s/_PROTECTREGLUE_/_REGLUE_/g;
  }
  s/_SPECIAL_//g;
  
  # Nettoyage de surdétections
  s/\{([^{}]+)\}\s*_CHEM_(?:[^_]*[₀₁₂₃₄₅₆₇₈₉]{3}[^_]*)_/\1/g;
  
  s/^(.*_META_TEXTUAL_[^ ]+)/\1 _UNSPLIT__SENT_BOUND/g;
  s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne) _REGLUE_/$1$2 _UNSPLIT_/g;
  s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne)/$1$2/g;
  
  s/^ +//;
  s/ +$//;
  print "$_\n";
}

