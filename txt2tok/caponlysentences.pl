#!/usr/bin/env perl
# $Id$
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my %lc_map = (
	      'А' => 'а',
	      'Б' => 'б',
	      'В' => 'в',
	      'Г' => 'г',
	      'Д' => 'д',
	      'Е' => 'е',
	      'Ё' => 'ё',
	      'Ж' => 'ж',
	      'З' => 'з',
	      'И' => 'и',
	      'Й' => 'й',
	      'І' => 'і',
	      'Ì' => 'ì',
	      'К' => 'к',
	      'Л' => 'л',
	      'М' => 'м',
	      'Н' => 'н',
	      'О' => 'о',
	      'П' => 'п',
	      'Р' => 'р',
	      'С' => 'с',
	      'Т' => 'т',
	      'У' => 'у',
	      'Ф' => 'ф',
	      'Х' => 'х',
	      'Ц' => 'ц',
	      'Ч' => 'ч',
	      'Ш' => 'ш',
	      'Щ' => 'щ',
	      'Ъ' => 'ъ',
	      'Ы' => 'ы',
	      'Ь' => 'ь',
	      'Ѣ' => 'ѣ',
	      'Э' => 'э',
	      'Ю' => 'ю',
	      'Я' => 'я',
	      'Ѳ' => 'ѳ',
	      'Ѵ' => 'ѵ',
	      'Ә' => 'ә',
	      'Ғ' => 'ғ',
	      'Қ' => 'қ',
	      'Ң' => 'ң',
	      'Ө' => 'ө',
	      'Ұ' => 'ұ',
	      'Ү' => 'ү',
	      'Һ' => 'һ',
	      'İ' => "i",
	      'Á' => 'á',
	      'À' => 'à',
	      'Â' => 'â',
	      'Ä' => 'ä',
	      'Ą' => 'ą',
	      'Ã' => 'ã',
	      'Ă' => 'ă',
	      'Å' => 'å',
	      'Ć' => 'ć',
	      'Č' => 'č',
	      'Ç' => 'ç',
	      'Ď' => 'ď',
	      'É' => 'é',
	      'È' => 'è',
	      'Ê' => 'ê',
	      'Ë' => 'ë',
	      'Ę' => 'ę',
	      'Ě' => 'ě',
	      'Ğ' => 'ğ',
	      'Ì' => 'ì',
	      'Í' => 'í',
	      'Î' => 'î',
	      'Ĩ' => 'ĩ',
	      'Ĭ' => 'ĭ',
	      'Ï' => 'ï',
	      'Ĺ' => 'ĺ',
	      'Ľ' => 'ľ',
	      'Ł' => 'ł',
	      'Ń' => 'ń',
	      'Ñ' => 'ñ',
	      'Ň' => 'ň',
	      'Ò' => 'ò',
	      'Ó' => 'ó',
	      'Ô' => 'ô',
	      'Õ' => 'õ',
	      'Ö' => 'ö',
	      'Ø' => 'ø',
	      'Ŕ' => 'ŕ',
	      'Ř' => 'ř',
	      'Ś' => 'ś',
	      'Š' => 'š',
	      'Ş' => 'ş',
	      'Ť' => 'ť',
	      'Ţ' => 'ţ',
	      'Ù' => 'ù',
	      'Ú' => 'ú',
	      'Û' => 'û',
	      'Ũ' => 'ũ',
	      'Ü' => 'ü',
	      'Ǔ' => 'ǔ',
	      'Ỳ' => 'ỳ',
	      'Ý' => 'ý',
	      'Ŷ' => 'ŷ',
	      'ÿ' => 'ÿ',
	      'Ź' => 'ź',
	      'Ẑ' => 'ẑ',
	      'Ż' => 'ż',
	      'Ž' => 'ž',
	     );


$lang = "fr";
$more_lowercasification = 0;

$| = 1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-ml$/) {$more_lowercasification = 1}
    elsif (/^-l$/) {$lang = shift}
}

if ($lang =~ /^(de|ja|ko|zh|tw|ar)$/) {
  while (<>) {
    print $_;
  }
} else {
  while (<>) {
    # formattage
    chomp;
    
    if (/ (_XML|_MS_ANNOTATION) *$/) {
      print "$_\n";
      next;
    }
    
    $orig = $_;
    $min=0; $maj=0; $trans=0;
    $decap='';
    $inside_comment = 0;
    $inside_token = 0;
    $inside_named_entity_id = 0;
    $nb_of_consecutive_nonmin_word = 0;
    $max_nb_of_consecutive_nonmin_word = 0;
    $leftcontext = "";
    while (s/^(.)//) {
      $c = $1;
      $orc = $c;
      $leftcontext .= $c;
      if ($inside_comment) {
	if ($c eq "<") {
	  $inside_token = 0;
	} elsif ($c eq ">") {
	  $inside_token = 1;
	} elsif ($c eq "}") {
	  $inside_comment = 0;
	}
	if ($inside_token == 0 || $inside_comment == 0) {
	  $decap.=$c;
	  next;
	}
      }
      if ($c eq "{") {$inside_comment = 1; $inside_token = 1; $decap.=$c; $inside_named_entity_id = 0; next;}
      if ($c eq " ") {
	$trans++;
	$inside_named_entity_id = 0;
	if ($decap !~ /}$/ && $inside_comment == 0) {
	  if ($leftcontext =~ /[ }][A-ZАБВГДЕЁЖЗИЙІÌКЛМНОПРСТУФХЦЧШЩЪЫЬѢЭЮЯѲѴӘҒҚҢӨҰҮҺİÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶÿŹẐŻŽ]+ $/o) {
	    $nb_of_consecutive_nonmin_word++;
	  } else {
	    if ($nb_of_consecutive_nonmin_word > $max_nb_of_consecutive_nonmin_word) {
	      $max_nb_of_consecutive_nonmin_word = $nb_of_consecutive_nonmin_word;
	    }
	    $nb_of_consecutive_nonmin_word = 0;
	  }
	}
      }
      if ($c eq "_" && $decap =~ /[} ]$/) {$inside_named_entity_id = 1}
      if ($inside_named_entity_id == 1) {$decap.=$c; next}
      if ($inside_comment || $c =~ /^[^A-ZАБВГДЕЁЖЗИЙІÌКЛМНОПРСТУФХЦЧШЩЪЫЬѢЭЮЯѲѴӘҒҚҢӨҰҮҺİÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶÿŹẐŻŽ]$/o) {
	  $decap.=$c; 
	  $min++ if $c =~ /^[a-zабвгдеёжзийіìклмнопрстуфхцчшщъыьѣэюяѳѵәғқңөұүһáàâäąãăåćčçďéèêëęěğìíîĩĭïĺľłńñňòóôõöøŕřśšşťţùúûũüǔỳýŷÿźẑżž]$/o;
	  next
      }
      unless ($decap =~ /[A-ZАБВГДЕЁЖЗИЙІÌКЛМНОПРСТУФХЦЧШЩЪЫЬѢЭЮЯѲѴӘҒҚҢӨҰҮҺİÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶÿŹẐŻŽa-zабвгдеёжзийіìклмнопрстуфхцчшщъыьѣэюяѳѵәғқңөұүһİáàâäąãăåćčçďéèêëęěğìíîĩĭïĺľłńñňòóôõöøŕřśšşťţùúûũüǔỳýŷÿźẑżž_0-9-]{4}[\.\?\!] $/o && /^[A-ZАБВГДЕЁЖЗИЙІÌКЛМНОПРСТУФХЦЧШЩЪЫЬѢЭЮЯѲѴӘҒҚҢӨҰҮҺİÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶÿŹẐŻŽa-zабвгдеёжзийіìклмнопрстуфхцчшщъыьѣэюяѳѵәғқңөұүһİáàâäąãăåćčçďéèêëęěğìíîĩĭïĺľłńñňòóôõöøŕřśšşťţùúûũüǔỳýŷÿźẑżž_0-9-]/o) {
	if ($lang eq "tr" && $c eq "I") {
	  $c = "ı";
	} elsif ($lang eq "en" && $c eq "I" && $decap =~ /(^| )$/ && /^[ ,]/) {
	  $c = "I";
	} else {
	  $c = ext_lc($c);
	}
      }
      if ($lang eq "gr" && $c eq "σ" && !/^([^\W_0-9-]|$)/) {$c = "ς"}
      if ($orc eq $c) {
	$min++
      } else {
	$maj++
      }
      $decap.=$c;
    }
    $_ = $orig;
    if ((5*$min < $maj && $trans > 1 && $maj > 10)
       ||
       ($more_lowercasification && $max_nb_of_consecutive_nonmin_word >= 3)) {
      $decap =~ s/_underscore/_UNDERSCORE/g;
      $decap =~ s/_acc_o/_ACC_O/g;
      $decap =~ s/_acc_f/_ACC_F/g;
      print "$decap\n";
    } else {
      print "$_\n";
    }
  }
}

sub ext_lc {
  my $in = shift;
  my $out = "";
  for (split (//, $in)) {
    if (defined($lc_map{$_})) {
      $out .= $lc_map{$_};
    } else {
      $out .= lc($_);
    }
  }
  return $out;
}

