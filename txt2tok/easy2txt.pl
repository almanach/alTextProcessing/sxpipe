#!/usr/bin/env perl
# $Id$

# Transforme un corpus EASy-segmenté (XML), quel qu'en soit l'encodage (UTF-8 ou non, qui est alors
# supposé comme étant latin-1), en une alternance de lignes de type {<E ID="Ennn"> } _XML (où nnn
# est un numéro de phrase) et de phrases encodées en latin-1 où l'espace dénote le changement de
# EASy-token

$| = 1;

use utf8;

binmode STDOUT, ":encoding(iso-8859-1)";
binmode STDERR, ":encoding(iso-8859-1)";

my $firstline = 1;

while (<>) {
  s/[­·]/-/g;
  s/œ/oe/g;
  s/€/EUR/g;
  chomp;
  if (s/^<(?:\?|\/?DOCUMENT|\!DOCTYPE)//) {
    if (/encoding\s*=\s*\"?([^\"\?]+)\"?.*$/) {
      if ($1 eq "UTF-8") {
	binmode STDIN, ":utf8";
      } else {
	binmode STDIN, ":encoding(iso-8859-1)";
      }
    }
  } elsif ($_!~/^\s*\{/) {
    s/<\/?Groupe[^<>]*>//g;
    s/<\/?relation[^<>]*>//g;
    s/<[^<>]*xlink[^<>]*>//g;
    s/<a-propager[^<>]*>//g;
    s/<s-o[^<>]*>//g;
    s/<\/E>//;
    next if /^\s*$/;
    s/<[^>]*>(.*)<\/[^>]*>/ _<_$1\_>_/g;
    s/_<_ +/_<_/g;
    s/ +_>_/_>_/g;
    while (s/_([^_]+) ([^ ]+_>_)/_$1\_$2/g) {
    }
    s/^ *//;
    if ($firstline) {
      s/(<E[^>]*>\n?)/{$1} _XML\n/g;
    } else {
      s/(<E[^>]*>\n?)/\n{$1} _XML\n/g;
    }
    s/_>_//g;
    s/_<_/ /g;
    s/ +/ /g;
    print $_;
    $firstline=0;
  } else {
    print $_."\n";
  }
}
print "\n";

