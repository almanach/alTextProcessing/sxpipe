#!/usr/bin/env perl
# -*- perl -*-
eval 'exec /usr/bin/env perl -S $0 ${1+"$@"}'
    if 0;

# Ce programme permet d'ajouter l'information du tagger port�e sur les token
# sur les mots simples inconnus
#
# Exemple:
# AVANT :
#<transition source="8" target="9">
#	<label type="in">
#	<token tag="N-C-ms" form="schmilblick" id="8">schmilblick</token>
#	<w tag="-Unknown-" entry="schmilblick" idref="8"/>
#	</label>
#</transition>

# APRES :
#<transition source="8" target="9">
#	<label type="in">
#	<token tag="N-C-ms" form="schmilblick" id="8">schmilblick</token>
#	<w tag="N-C-ms" entry="schmilblick" idref="8"/>	</label>
#</transition>


use strict;
use FindBin qw($Bin $Script);
use XML::Twig;

my @patterns=();

open(CONF,"<$Bin/Trans.conf") || die "can't open configuration file";
while (<CONF>) {
  next if /^#/;
  next if /^\s*$/;
  my ($regexp, $rempl) = split(/\t/, $_);
  chop($rempl);
  $rempl =~ s/\\(\d)/\$$1/og;
  push(@patterns,[$regexp,$rempl]);
}
close(CONF);

my $t = XML::Twig->new(
		       twig_handlers =>
		       {
			fsm => \&fsm_process,
			'w[@tag="-Unknown-"]' => \&unknown
		       },
		       pretty_print => 'indented',
		       output_encoding => 'ISO-8859-1',
		       output_filter => 'latin1'
		      );

$t->parse(\*STDIN);
$t->flush();

sub fsm_process {
  my ($t,$elt) = @_;
  $t->flush();
}

sub unknown {
  my $t = shift;
  my $w = shift;
  my $id = $w->att('tokens');
  my ($token) = $w->parent('fsm')->descendants("token[\@id=\"$id\"]");
  my $tag = $token->att('tag');
  foreach my $pattern (@patterns) {    
    eval "\$tag =~ s/$pattern->[0]/$pattern->[1]/g";
  }
  $w->set_att(tag=>$tag);
}

exit 0;

__END__
