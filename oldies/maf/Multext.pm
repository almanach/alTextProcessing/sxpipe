use strict;

package Multext;

use Exporter;
use vars qw(@ISA @EXPORT $VERSION);
use Class::Generate qw/class subclass/;
use UNIVERSAL qw( isa can ) ;
use Memoize;
use XML::Generator;
use DB_File qw/O_RDWR O_CREAT/;

$VERSION = 1.00;              # Or higher
@ISA     = qw(Exporter);
@EXPORT  = qw(multext2fs fs2string fs2xml generate_xml_lib);

tie my %cache => 'DB_File', "multext2fstag.cache", O_RDWR|O_CREAT, 0666;

memoize 'multext2fs', SCALAR_CACHE => [HASH => \%cache];

# Class Feature
class Feature => {
    name => '$',
    short => '$',
    set => '$',
    order => '$'
};

class FSet => {
    name => '$',
    values => '@'
};

class FValue => {
    name => '$',
    short => '$',
    char => '$'
};

my %features = ();
my %fset = ();
my %fval = ();
my %map = ();
my $forder=0; 

feature_register(
    name => 'cat',
    set  => {
	name => 'cat',
	values => [ qw/adj adv advneg x cl cla cld cln clng det n p ponct pp prep s v vm cc cs interj/ ]
    }
);

feature_register(
    name => 'det_type',
    set  => {
	name => 'det_type',
	values => [ qw/article:art:a
	demonstratif:dem:d
	indefini:indef:i
	possessif:poss:s
	interrogatif:int:t
	exclusif:excl:t/ ]
    }
);

feature_register(
    name => 'adj_type', 
    set => {
	name => 'adj_type',
	values => [ qw/qual::f ord::o indef::i poss::s/ ]
    }
);

feature_register(
    name => 'noun_type', 
    set  => {
	name => 'noun_type',
	values => [qw/common::c proper::p distinguished:dist:d/]
    }
);

feature_register(
    name => 'pron_type', 
    set  => {
	name => 'pron_type',
	values => [ qw/personal:pers:p
	demonstratif
	indefini
	possessif
	interrogatif
	relatif:rel:r
	reflexif:refl:x/ ]
    }
);

feature_register(
    name => 'aux', 
    set  => {
	name => 'aux',
	values => [ qw/avoir::m etre::a/ ]
    } 
);

feature_register(
    name => 'mode', 
    set  => {
	name => 'mode' ,
	values => [
	qw/indicatif:ind:i
	subjonctif:subj:s
	imperatif:imp:m
	conditionnal:cond :c
	infinitif:inf:n
	participe_passe:ppart:p/ ]
    }
);

feature_register(
    name => 'tense', 
    set  => {
	name => 'tense',
	values => [qw/present:pres:p imparfait:imp:i futur:fut:f passe:past:s/]
    }
);

feature_register(
    name => 'pers', 
    set  => {
	name => 'pers',
	values => [ qw/1 2 3/ ]
    }
);

feature_register(
    name => 'gender', 
    set  => {
	name => 'gender',
	values => [ qw/masc::m fem::f/ ] 
    }
);

feature_register(
    name => 'num', 
    set  => {
	name => 'num',
	values => [ qw/pl::p sing::s/ ]
    }
);

feature_register(
    name => 'case', 
    set  => {
	name => 'case',
	values => [ qw/accusatif:acc:j datif:dat:j genitif:gen nomimatif:nom:n oblique::o/ ]
    }
);

feature_register(
    name => 'restr',
    set  => {
	name => 'restr', 
	values => [ qw/moinshum plushum/ ]
    } 
);

feature_register(
    name => 'wh', 
    set  => {
	name => 'wh',
	values => [ qw/plus minus rel/ ]
    }
);

feature_register(
    name => 'bool', 
    set  => {
	name => 'bool',
	value => [qw/plus minus/]
    }
);

feature_register(
    name => 'numposs', 
    set  => 'num'
);

feature_register(
    name => 'degree', 
    set  => {
	name => 'degree',
	values => [ qw/pos::p comp::c/ ]
    }
);

feature_register(
    name => 'def', 
    set => 'bool' 
);

feature_register(
    name => 'res_type',
    set => {
	name => 'res_type',
	values => [qw/unit::u expression:exp:e foreign::f/]
    }
);

feature_register(
    name => 'sem',
    set => {
	name => 'sem',
	values => [qw/place::c inhabitant::h entity::s/]
    }
);

my $multext_converter = {
    'N' => {
	cat    => 'noun',
	decode => {
	    f => [qw/noun_type gender num sem/],
	    r => [1,2,3,5]
	}
    },
    'V' => {
	cat    => 'verb',
	decode => {
	    f => [qw/aux mode tense pers num gender/],
	    r => [1 .. 6]
	}
    },
    'A' => {
	cat    => 'adj',
	decode => {
	    f => [qw/adj_type degree gender num/],
	    r => [1 .. 4] 
	}
    },
    'P' => {
	cat    => 'pronoun',
	decode => {
	    f => [qw/pron_type pers gender num case numposs/],
	    r => [1 .. 6],
	    v => [qw/pron_type pers gender num case num/] 
	}
    },
    'D' => {
	cat    => 'det',
	decode => {
	    f => [qw/det_type pers gender num numposs def/],
	    r => [1 .. 7],
	    v => [qw/det_type pers gender num num bool/]
	}
    },
    'R' => {
	cat  => 'adv',
	skip => {
	    'n' => {cat => 'advneg'},
	    'd' => {cat => 'advneg'}
	}
    },
    'S' => {
	'p' => {cat => 'prep'}
    },
    'C' => {
	'c' => {cat => 'cc'},
	's' => {cat => 'cs'}
    },
    'M' => {
	'c' => {cat => ['det','adj']},
	decode => {
	    f => [qw/gender num/],
	    r => [2,3]
	}
    },
    'I' => {cat => 'interj'},
    'X' => {cat => 'noun'},
    'F' => {cat => 'ponct'}
};

build_map();
normalize_converter($multext_converter);

sub multext2fs {
    my ($multext) = @_;
    my $feat = {};
    if ($multext eq '-Unknown-'){
	$feat->{"cat"}=["uw"];
    } else {
	my @caracteres = split(/\s+/,$multext);
	use_converter($multext_converter, 0, \@caracteres, $feat);
    }
    return $feat;
}

sub fs2string {
    my ($feat) = @_;
    my @tags=();
    my @cats=();
    foreach my $attr (sort f_sort (keys %$feat)) {
	my $vals = $feat->{$attr};
	push(@tags,$attr."@".join('|',map(can($_,'short') ? $_->short : $_,@$vals))) if (@$vals);
    }
    return join(" ",@tags);
}    

sub fs2xml {
    my ($feat) = @_;
    my $xml = XML::Generator->new(pretty => 2);
    my @f=();
    foreach my $attr (sort f_sort (keys %$feat)) {
	my $vals = $feat->{$attr};
	next unless (@$vals);
	my @tmp=();
	foreach my $val (@$vals) {
	    push(@tmp,$xml->sym({value=>(can($val,'name') ? $val->name : $val) }));
	}
	push(@f, $xml->f({name=>$attr},(@$vals > 1) ? $xml->vAlt(@tmp) : @tmp));
    }
    return $xml->fs(@f);
}

sub generate_xml_lib {
    my $xml = XML::Generator->new(pretty => 2);
    my @flib=();
    my @vlib=();
    foreach my $key (sort keys %fset){
	my $fset = $fset{$key};
	my @tmp = ();
	foreach my $value ($fset->values()) {
	    my $vshort = $value->short();
	    push(@tmp,$xml->sym(
		{value=>$value->name(),
		id=>"$key\_$vshort"
	    }));
	}
	push(@vlib,$xml->vLib({type=>"$key"},@tmp));
    }
    foreach my $key (sort keys %features){
	my $feature = $features{$key};
	my $fset = $feature->set();
	foreach my $value ($fset->values()){
	    my $vshort = $value->short();
	    push(@flib,$xml->f(
		{name=>$key,
		id=>"$key\@$vshort",
		fVal=>$fset->name.'_'.$vshort
	    }));
	}
    }
    print $xml->tagset({name=>'multext'},
    @vlib,$xml->fLib({type=>'multext'},@flib));
    print "\n";
}

sub feature_register {
    my %args = @_;
    my $set = $args{set};
    my $name = $args{name};
    if (ref($set) ne 'HASH') {
	$set = fset_register(name=>$set,values=>[]);
    } else {
	$set = fset_register(
	    name=>$set->{name},
	    values => $set->{values} 
	);
    }

    $features{$args{name}} = Feature->new(
	name=>$name,
	set=>$set,
	order => $forder++,
	short=> $args{short} || $name
    );
}

sub fset_register {
    my %args = @_;
    my $name = $args{name};
    my $values = $args{values};
    return $fset{$name} if (exists $fset{$name});

    my $set = FSet->new(
	name => $name,
	values => [ map(fval_register($_),@$values) ]
    );
    $fset{$name} = $set;
    return $set;

}

sub fval_register {
    my $fval = shift;
    my ($name,$short,$char) = split(/:/,$fval);
    $short = $name unless ($short);
    $char = substr($name,0,1) unless ($char);
    #  print "Register fval $name $short $char\n";
    return $fval{$name} if (exists $fval{$name});
    $fval = FValue->new(
	'name'=> $name,
	'short'=> $short,
	'char'=> $char
    );
    $fval{$name} = $fval;
    return $fval;
}

sub build_map {
    foreach my $key (sort keys %fset){
	my $fset = $fset{$key};
	my %tmp = ();
	foreach my $value ($fset->values()){
	    my $vshort = $value->short;
	    my $vchar = $value->char;
	    $tmp{$vchar} = [] unless (exists $tmp{$vchar});
	    push(@{$tmp{$vchar}},$value); 
	}
	$map{$key}={ %tmp };
    }
}

sub normalize_converter {
    my $converter = shift;
    foreach my $key (keys %$converter) {
	next if ($key eq "feat");
	if ($key eq "cat") {
	    $converter->{cat} = [$converter->{cat}] 
	    unless (ref($converter->{cat}) eq 'ARRAY');
	    next;
	}
	if ($key eq "skip") {
	    normalize_converter($converter->{'skip'});
	    next;
	}
	if ($key eq "decode") {
	    $converter->{decode}{v} = $converter->{decode}{f} 
	    unless (exists($converter->{decode}{v}));
	    next;
	}
	normalize_converter($converter->{$key});
    }
}

sub use_converter {
    my ($converter,$index,$caracteres,$feat) = @_;

    if (exists($converter->{'cat'})){
	$feat->{'cat'} = $converter->{'cat'};
    } 

    if (defined($converter->{decode})) {
	my $decoder = $converter->{decode};
	for(my $i=0; $i < @{$decoder->{f}}; ++$i){
	    $feat->{$decoder->{f}[$i]} =
	    generic_decoder($map{$decoder->{v}[$i]},
	    $caracteres->[$decoder->{r}[$i]]);
	}
    }

    if (exists($converter->{feat})){
	foreach my $key (keys %{$converter->{feat}}) {
	    $feat->{$key} = $converter->{feat}{$key};
	}
    }

    if (exists($converter->{skip})) {
	use_converter($converter->{skip},$index+1,$caracteres,$feat);
	return;
    }

    my $char = $caracteres->[$index];
    if (exists($converter->{$char})) {
	use_converter($converter->{$char},$index+1,$caracteres,$feat);
    }
}

sub generic_val_decoder {
    my $map = shift;
    my $code = shift;
    my $val = $map->{$code};
    return (ref($val) eq 'ARRAY') ? @$val : $val;
}

sub generic_decoder {
    my $map = shift;
    my $char = shift;
    if ($char eq '-') {
	return [];
    } else {
	my @multext = split(/\|/,$char);
	return [ map {generic_val_decoder($map,$_)} @multext ];
    }
}

sub gds {
    my $caracteres = shift;
    my $indices = shift;
    my $fields = shift;
    my @values = ();
    while (@$indices) {
	my $index = shift(@$indices);
	my $field = shift(@$fields);
	push(@values,generic_decoder($map{$field},$caracteres->[$index]));
    }
    return @values;
}

sub f_sort {
    $features{$a}->order <=> $features{$b}->order;
}

1;
