#!/usr/bin/env perl
# $Id$

use Lingua::TagSet::Talana;
use strict;

my @buffer;

while (<>) {
    chomp;
    my ($word, undef, $cat, $stuff) = split(/\t/);

    # no such stuff
    next if $cat =~ /aux/;
    next if $stuff =~ /lightverb/;
    next if $word =~ /^\w$/ and $cat eq 'nc';

    # lemm
    my ($lemm) = $stuff =~ /pred='([\w-]+)(:?<.+>\w*)?'/;
    next if $lemm =~ /Se$/;
    $lemm = $word unless ($lemm && $lemm ne 'pro');

    # tag
    my $tag = ($stuff =~ /@(\w+)/g)[-1];
    if ($cat eq 'v') {
	$tag = Lingua::TagSet::Talana->tag2string("V--$tag");
    } elsif ($cat eq 'nc') {
	$tag = Lingua::TagSet::Talana->tag2string("N-C-$tag");
    } elsif ($cat eq 'np') {
	$tag = Lingua::TagSet::Talana->tag2string("N-P-$tag");
    } elsif ($cat eq 'adj') {
	$tag = Lingua::TagSet::Talana->tag2string("A--$tag");
    } elsif ($cat eq 'pro') {
	my $struc = Lingua::TagSet::Talana->tag2structure("PRO--$tag");
	my $type;
	if ($stuff =~ /dem = \+/) {
	    $type = 'dem';
	} elsif ($stuff =~ /define = -/) {
	    $type = 'ind';
	} else {
	    $type = 'pers';
	}
	$struc->set_feature(type => $type);
	$tag = $struc->to_string();
    } elsif ($cat eq 'det') {
	$tag = Lingua::TagSet::Talana->tag2string("D--$tag");
    } elsif ($cat eq 'prel') {
	$tag = Lingua::TagSet::Talana->tag2string("PRO-rel-$tag");
    } elsif ($cat eq 'pri') {
	$tag = Lingua::TagSet::Talana->tag2string("PRO-int-$tag");
    } elsif ($cat =~ /^cl[nargld]$/) {
	my $struc = Lingua::TagSet::Talana->tag2structure("CL--$tag");
	my $case;
	if ($stuff =~ /case = (\w+)/) {
	    $case = $1;
	} else {
	    $case = 'ref';
	}
	$struc->set_feature(case => $case);
	$tag = $struc->to_string();
    } elsif ($cat eq 'clneg') {
	$tag = 'cat@advneg';
    } elsif ($cat eq 'prep') {
	$tag = 'cat@prep';
    } elsif ($cat eq 'adv') {
	$tag = 'cat@adv';
    } elsif ($cat eq 'advneg') {
	$tag = 'cat@advneg';
    } elsif ($cat eq 'csu') {
	$tag = 'cat@cs';
    } elsif ($cat eq 'coo') {
	$tag = 'cat@cc';
    } elsif ($cat =~ /ponct[psw]/) {
	$tag = 'cat@ponct';
    } else {
	next;
    }

    if ($buffer[-1] && ($word ne $buffer[-1]->{word} || $lemm ne $buffer[-1]->{lemm})) {
	flush();
    }
    push(@buffer, { word => $word, lemm => $lemm, tag => $tag });

}
flush();

sub flush {

    # try to merge multiple entries
    for my $i (0 .. $#buffer - 1) {
	if ($buffer[$i]->{tag} eq $buffer[$i+1]->{tag}) {
	    $buffer[$i] = undef;
	} else {
	    my $struc1 = Lingua::Features::Structure->from_string($buffer[$i]->{tag});
	    my $struc2 = Lingua::Features::Structure->from_string($buffer[$i+1]->{tag});
	    if ($struc1->has_same_type($struc2)) {
		my $struc = Lingua::Features::Structure->union($struc1, $struc2);
		$buffer[$i] = undef;
		$buffer[$i+1]->{tag} = $struc->to_string();
	    }
	}
    }

    # print remaining entries
    foreach my $entry (@buffer) {
	next unless $entry;
	print join("\t", $entry->{word}, $entry->{lemm}, $entry->{tag}) . "\n";
    }

    # empty buffer
    @buffer = ();
}
