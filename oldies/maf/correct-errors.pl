#!/usr/bin/env perl
# $Id$
use Getopt::Long;
use Algorithm::Diff 'traverse_sequences';
use Dict::Lexed;
use Data::Dumper;
use strict;
use warnings;
use locale;

# global values
my (@files, @words, %words, %lexicon, %substitutions, %stats);
my $name      = 'correct-errors';
my $version   = '1.1';
# parameters
my (@wordfiles, $help);
my $verbose   = 0;
my $length    = 4;
my $frequency = 4;
my $distance  = 1;
my $weight    = 0.5;
GetOptions(
    "l|length=i"    => \$length,
    "f|frequency=i" => \$frequency,
    "d|distance=i"  => \$distance,
    "w|weight=i"    => \$weight,
    "W|wordfile=s"  => \@wordfiles,
    "h|help"        => \$help,
    "v|verbose+"    => \$verbose,
);

{
    # slurp mode
    local $/;

    if (@ARGV) {
	foreach my $file (@ARGV) {
	    print "reading $file\n" if $verbose > 2;
	    open(FILE, "<$file") or die "Can't open $file: $!";
	    push(@files, {
		path    => $file,
		content => <FILE>
	    }); 
	    close(FILE);
	}
    } else {
	@files = ({
	    path    => 'stdin',
	    content => <STDIN>
	}); 
    }
}

# tokenize text
print "tokenizing entry\n" if $verbose > 2;
foreach my $file (@files) {
    foreach my $word (split(/\b/, $file->{content})) {
	my @chars = split(//, $word);
	my $numbers = scalar grep { /[[:digit:]]/ } @chars;
	my $letters = scalar grep { /[[:alpha:]]/ } @chars;
	next unless $letters > $numbers;
	next unless length($word) >= $length;
	push(@words, $word);
    }
}

# index all words
print "indexing words\n" if $verbose > 2;
$stats{words_total} = 0;
foreach my $word (@words) {
    $words{$word}->{count}++;
    $stats{words_total}++;
}
$stats{words_unique} = keys %words;

# normalize capitalisation
print "normalizing words\n" if $verbose > 2;
foreach my $word (keys %words) {
    my $entry = $word;

    if ($word =~ /^[[:upper:]]+$/) {
	my $form = lc($word);
	my $Form = substr($word, 0, 1) . lc(substr($word, 1));
	if ($words{$Form} && $words{$form}) {
	    $entry =
		($words{$Form}->{count} > $words{$form}->{count}) ?
		$Form:
		$form;
	} elsif ($words{$Form}) {
	    $entry = $Form;
	} elsif ($words{$form}) {
	    $entry = $form;
	} else {
	    $entry = $form;
	}
    }

    if ($word =~ /^[[:upper:]][[:lower:]]+$/) {
	my $form = lc($word);
	my $Form = $word;
	if ($words{$form}) {
	    $entry = 
		($words{$Form}->{count} > $words{$form}->{count}) ?
		$Form:
		$form;
	} else {
	    $entry = $Form;
	}
    }

    $words{$word}->{entry} = $entry;
    $lexicon{$entry}->{count} += $words{$word}->{count};
}

Dict::Lexed->create_dict([ keys %lexicon ], '-p words');
my $dict = Dict::Lexed->new('-p words', '-s 1 -W 10');

print "searching neighbours\n" if $verbose > 2;
foreach my $entry (keys %lexicon) {
    $lexicon{$entry}->{neighbours} = [
	sort { $lexicon{$b}->{count} <=> $lexicon{$a}->{count} }
	$dict->suggest($entry)
    ];
}

print "searching corrections\n" if $verbose > 2;
foreach my $entry (keys %lexicon) {
    next unless $lexicon{$entry}->{count} == 1;

    my $neighbour = $lexicon{$entry}->{neighbours}->[0];

    next unless $neighbour;
    next unless $lexicon{$neighbour}->{count} > 1;

    $lexicon{$entry}->{correction} = $neighbour;
}

print "normalizing correction\n" if $verbose > 2;
foreach my $word (keys %words) {
    my $entry = $words{$word}->{entry};
    my $correction = $lexicon{$entry}->{correction};
    next unless $correction;

    if ($entry ne $word) {
	if ($word =~ /^[[:upper:]]+$/) {
	    $correction = uc($correction);
	} else {
	    $correction = ucfirst($correction);
	}
    }

    $words{$word}->{correction} = $correction;
    $substitutions{$word} = $correction;
}

dump_file('words.txt', \%words) if $verbose > 1;
dump_file('lexicon.txt', \%lexicon) if $verbose > 1;
dump_file('substitutions.txt', \%substitutions) if $verbose > 1;

# replace in original text
print "applying corrections\n" if $verbose > 2;
$stats{substitutions_total}  = 0;
if (%substitutions) {
    my $pattern = join('|', map { quotemeta $_ } keys %substitutions);
    foreach my $file (@files) {
	$stats{substitutions_total} += $file->{content} =~ s/\b($pattern)\b/$substitutions{$1}/eg;
    }
}
$stats{substitutions_unique} = keys %substitutions;

# stat reporting
if ($verbose > 0) {
    print <<EOF;
Words
total: $stats{words_total}
unique: $stats{words_unique}

Substitutions
total: $stats{substitutions_total}
unique: $stats{substitutions_unique}
EOF
}

# output text
foreach my $file (@files) {
    if ($file->{path} eq 'stdin') {
	print STDOUT $file->{content};
    } else {
	my $backup = $file->{path} . '.bak';
	rename $file->{path},$backup or die "Can't rename $file->{path} to $backup: $!";
	open(FILE, ">$file->{path}") or die "Can't write $file->{path}: $!";
	print FILE $file->{content};
	close(FILE);
    }
}


sub dump_file {
    my ($file, $variable) = @_;
    open(FILE, ">$file") or die "Can't open $file: $!";
    print FILE Dumper($variable);
    close(FILE);
}
