#!/usr/bin/perl -w

package Lingpipe;

use 5.006;
use strict;
use warnings;
use Carp;

use sigtrap;
use IO::Socket;
use Net::hostent;              # for OO version of gethostbyaddr
#use Carp;
use IPC::Open2;
use POSIX 'setsid';

use FindBin qw($Bin $Script);
use Net::Server::Fork;
use vars qw(@ISA $VERSION);

@ISA = qw(Net::Server::Fork);

$VERSION= '0.1.0';

######################################################################
# Define and read configuration file

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new({ CREATE => '_(cmd|options|option_list|desc|depend|wcmd|woptions)',
			      GLOBALS => { ARGCOUNT => ARGCOUNT_ONE,
					   EXPAND => EXPAND_ALL
					 },
			    },
			    "package=f",
			    "resources=f",
			    "path=f@",
			    "components=s",
			    "port|p=i" => { DEFAULT => 8997 },
			    "setsid!" => { DEFAULT => 1 },
			    "server!" => { DEFAULT => 0 },
			    "terminal|t=s" => { DEFAULT => 'normalize'},
			    "verbose|v!" => { DEFAULT => 0 },
			    "log_file=f" =>  {DEFAULT => '/tmp/lingpipe.log'},
			    "log_level=i" => {DEFAULT => 2},
                            "maxclients=i" => { DEFAULT => 2 },
			    "user=s" => {DEFAULT => 'nobody'},
			    "group=s" => {DEFAULT => 'nobody'},
			    "pid_file=f" => {DEFAULT => '/var/run/lingpiped.pid'},
			    "syslog_ident=s" => {DEFAULT => 'lingpiped' },
			    "admin=s" => {DEFAULT => 'lingpipe'}
			   );

my $conffile;

foreach ("./lingpipe.conf",
	 "./.lingpipe.conf",
	 "/etc/lingpipe.conf",
	 "$Bin/lingpipe.conf") {
  $conffile=$_, last if (POSIX::access($_,&POSIX::R_OK));
}

# read configuration file
if ($conffile) {
  $config->file("$conffile") 
    || die "can't open or process configuration file $conffile";
}

$config->args();		# parse remaining args

if ($config->server()){
  # Securize
  delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};
}

# setting path
$ENV{'PATH'} = join(':',@{$config->path()},"$Bin");

my $verbose = $config->verbose();

sub Lingpipe::build_pipe {
  my $self=shift;
  $self->{terminal} = shift;
  my @pipe=();
  foreach my $comp (@{$self->{components}}) {
    my %tmp = $config->varlist("^$comp"."_",1);
    my @tmp = $tmp{cmd};
    push(@tmp,$tmp{'options'}) if (defined $tmp{'options'});
    if (defined $tmp{wcmd}){
      my @wrapped = ($tmp{wcmd});
      push(@wrapped,$tmp{woptions})  if (defined $tmp{woptions});
      push(@tmp,'-c','"'.join(' ',@wrapped).'"');
    }
    push(@pipe,join(' ',@tmp));
    last if ($comp eq $self->{terminal});
  }
  $self->{pipe}=join(' | ',@pipe);
}

### set up some server parameters
sub configure_hook {
  my $self = shift;
  open(STDIN, '</dev/null') || die "Can't close STDIN [$!]";
  open(STDOUT,'>/dev/null') || die "Can't close STDOUT [$!]";
  #  open(STDERR,'>&STDOUT')   || die "Can't close STDERR [$!]";

  $self->{components} = [ split(/\s+/,$config->components()) ];
  $self->build_pipe($config->terminal());

}

### this occurs after the request has been processed
### this is server type specific (actually applies to all by INET)
sub post_accept {
  my $self = shift;
  my $prop = $self->{server};

  ### duplicate some handles and flush them
  ### maybe we should save these somewhere - maybe not
  if( defined $prop->{client} ){
    *MYSTDIN  = \*{ $prop->{client} };
    *MYSTDOUT = \*{ $prop->{client} } if ! $prop->{client}->isa('IO::Socket::SSL');
    MYSTDIN->autoflush(1);
    MYSTDOUT->autoflush(1);
    select(STDOUT);
  }else{
    $self->log(1,"Client socket information could not be determined!");
  }

  ### keep track of the requests
  $prop->{requests} ++;

}

### user customizable hook
sub post_process_request_hook {
  my $self = shift;
}

sub start_pipe {
    my $self = shift;
    open2(*READER,*WRITER,"$self->{pipe}")
	|| die "Couldn't start pipe";
    
    READER->autoflush(1);
    WRITER->autoflush(1);
}

sub process_request{
  my $self = shift;
  
  eval {
    local $SIG{ALRM} = sub { die "Timed Out!\n" };
    my $timeout = 60; # give the user 60 seconds to type a line
    my $previous_alarm = alarm($timeout);
    my $prompt='';

    while (<MYSTDIN>) {
      s/\r?\n$//;
      
      $prompt='yes', next unless /\S/;       # blank line
      
      if (/^quit/) {
	last;
      }
      
      if (/^admin\s*(\S+)\s*(\S+)/ && $1 eq $config->admin()) {
	my $cmd = $2;
	if ($cmd eq 'shutdown') {
	  print MYSTDOUT "Close server\n";
	  $self->server_close;
	}
	next;
      }
      
      if (/^terminal\s+(\S+)/) {
#	print MYSTDOUT "Set terminal component to $1\n";
	$self->build_pipe($1);
	next;
      }

      if (/^config\s+(.*)/) {
	$config->args([$1]);
#	print MYSTDOUT "Option is now ",$config->get('normalize_options'),"\n";
	next;
      }
      
      if (/^pipe/) {
	print MYSTDOUT <<EOF;
pipe structure:
     components @{$self->{components}}
     terminal $self->{terminal}
EOF
             next;
      }
      
      if (s/^sentence\s+//) {
	# this seems to be necessary
	my $sentence = $_;
	
	$self->start_pipe;
	
	print WRITER "<p>$sentence</p>";
	close WRITER;
	
	print MYSTDOUT $_ while (<READER>);
	close READER;
	
	next;
      }
      
      $self->usage();
      
    }  continue {
      alarm($timeout);
      if ($prompt) {
	print MYSTDOUT "Welcome to Lingpiped; type help for command list.\n";
	print MYSTDOUT "Command? ";
      }
    }

      if( $@=~/timed out/i ){
	print MYSTDOUT "Timed Out.\r\n";
	return;
      }
    
  }
}

sub usage {
  my $self=shift;
  print MYSTDOUT <<EOF ;

Welcome to Lingpiped, a linguistic pipeline server.

Contact: Eric de la Clergerie <Eric.De_La_Clergerie\@inria.fr>

Commands:
      quit
    | help
    | sentence <sentence>
    | pipe
    | terminal <component>

Pipe components:
EOF
    foreach my $comp (@{$self->{components}}) {
	my %tmp = $config->varlist("^$comp"."_",1);
        print MYSTDOUT "\t$comp\t-- $tmp{desc}\n";
    }

print MYSTDOUT <<EOF;

Options:
EOF


}

__PACKAGE__->run(
		 port => $config->port(),
		 log_file => $config->log_file(),
		 syslog_ident => $config->syslog_ident(),
		 log_level => $config->log_level(),
		 user => $config->user(),
		 group => $config->group(),
		 pid_file => $config->pid_file(),
		 setsid   => $config->setsid ? 1 : undef,
		 maxclients => $config->maxclients()
		 );

exit;

1;

__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

lingpipe - Perl script to run Linguistic Pipelines

=head1 SYNOPSIS

# Process from file
cat file.txt | ./lingpipe

# Process from string
echo "Yves aime Sabine" | ,/lingpipe

# Verbose mode. Stop after tagging
echo "Yves aime Sabine" | ,/lingpipe -v t tagger

=head1 DESCRIPTION

Script B<lingpipe> may be used to run pipelines of linguistic
components, such as tokenizer, taggers, morphological analyzer,
parsers, ...

Pipelines may be described in configuration files (see lingpipe.conf)
and tuned with command-line options

=head1 AUTHORS

Lionel Cl�ment, E<lt> Lionel.Clement@inria.frE<gt>

Eric de la Clergerie, E<lt>Eric.De_La_Clergerie@inria.frE<gt>

=head1 SEE ALSO

L<perl>
L<Event::XML::Sync>.

=cut
