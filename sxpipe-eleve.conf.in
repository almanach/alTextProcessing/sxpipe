sxpipe  = @datadir@
bindir = @bindir@
alexinadir = @alexinadir@
alexinatoolsdir = @alexinatoolsdir@

# Component list
components = \
	tag_meta \
	email \
	url \
        cjk_specials \
	date \
	heure \
	adresse \
	number \
	cjk_specials2 \
	remove_inner_1 \
	segment \

[force_utf8]
cmd = $alexinatoolsdir/yarecode -l $lang $* | $alexinatoolsdir/yadecode -l $lang $*

[normalize_arabic]
cmd = python $sxpipe/txt2txt/arabic_script_normalizer.py -l $lang

[char_correct]
cmd = $sxpipe/txt2txt/normalizer.pl -l $lang $*

[romanize]
cmd = $sxpipe/txt2txt/romanizer.pl -al -l $lang $*

[unromanize]
cmd = $sxpipe/txt2txt/romanizer.pl -la -l $lang $*

[recode-init_2]
cmd = $alexinatoolsdir/yarecode
options = -l1 -u

[tag_meta]
cmd = $sxpipe/txt2tok/tag_meta.pl
options = -l $lang $*
desc = protection des meta-caractères

[entities]
cmd = $sxpipe/txt2tok/gl_entities.pl
desc = reconnaissance des entites

[email]
cmd = $sxpipe/txt2tok/gl_email.pl
options = -l $lang $*
desc = reconnaissance des emails

[url]
cmd = $sxpipe/txt2tok/gl_url.pl
options = -l $lang $*
desc = reconnaissance des url
depend = email

[date]
cmd = $sxpipe/txt2tok/gl_date.pl
options = -l $lang $*
desc = reconnaissance des dates

[tel]
cmd = $sxpipe/txt2tok/gl_tel.pl
options = -l $lang
desc = reconnaissance des n° de téléphone

[heure]
cmd = $sxpipe/txt2tok/gl_heure.pl
options = -lang=$lang $*
desc = reconnaissance des heures

[adresse]
cmd = $sxpipe/txt2tok/gl_adresses.pl
options = -l $lang $*
desc = reconnaissance des adresses

[numprefix]
cmd = $sxpipe/txt2tok/gl_numtruc.pl
options = $*
desc = reconnaissance des prefixes numeriques (genre 4-place)

[number]
cmd = $sxpipe/txt2tok/gl_number.pl
options = -l $lang $*
desc = reconnaissance des nombres

[smiley]
cmd = $sxpipe/txt2tok/gl_smiley.pl
desc = reconnaissance des smileys

[qword]
cmd = $sxpipe/txt2tok/gl_qword.pl
desc = reconnaissance des mots cités

[mtponct]
cmd = $sxpipe/txt2tok/gl_mtponct.pl
options = $* -l $lang
desc = reconnaissance des ponctuations multitokens

[cjk_specials]
cmd = $sxpipe/txt2tok/gl_cjkspecials.pl
options = $* -l $lang
desc = repère le latin, la punct et autres groupes non-cjk

[cjk_specials2]
cmd = $sxpipe/txt2tok/gl_cjkspecials2.pl
options = $* -l $lang
desc = repère le latin, la punct et autres groupes non-cjk



[formattage]
cmd = $sxpipe/txt2tok/gl_format.pl
desc = reconnaissance des mots formattés (pseudo-gras type _mot_ ou *mot*)

[caponly]
cmd = $sxpipe/txt2tok/caponlysentences.pl
options = -l $lang
desc = minusculisation des phrases (presque) entièrement en majuscules

[segment]
cmd = $sxpipe/txt2tok/segmenteur.pl
options = -no_s -no_af -r $*  -alexinadir $alexinadir
desc = segmentation

# tokens to compound components
[tag_unknown]
cmd = $bindir/sxspeller-$lang
options = -cl -pl -cq -ch -pp -ps -c -nsc -d -lw 1
desc = marquage des tokens inconnus

[sigles]
cmd = $sxpipe/tok2cc/gl_sigles.pl
desc = reconnaissance des sigles

[etr]
cmd = $sxpipe/tok2cc/gl_etr.pl
options = -no_gl $* -l $lang
desc = reconnaissance des expressions langue êtrangère

[untag_unknown]
cmd = $sxpipe/tok2cc/untag_unknown.pl
desc = démarquage des tokens inconnus

[remove_inner_1]
cmd = $sxpipe/tok2cc/remove_inner_ne.pl
desc = desencapsulation des entitées nommées

[remove_inner_2]
cmd = $sxpipe/tok2cc/remove_inner_ne.pl
desc = desencapsulation des entitées nommées

[rebuild]
cmd = $sxpipe/tok2cc/rebuild_easy_tags.pl -l $lang

[correct]
cmd = $bindir/sxspeller
options = -cl -pl -cq -ch -pp -ps -c -slw 20 -lw 40 -t -sc -mw 300 -sww 140 -g
desc = correction effective

[normalize]
cmd = $sxpipe/tok2cc/normalize_blanks.pl | perl -CSDA -pe "s/(\S+ss)[éè](-je)/\1s\2/g; s/(\S+)[éè](-je)/\1e\2/g"
desc = normalisation des espaces et de pourcentages

# compound components to DAG
[text2dag]
cmd = $bindir/text2dag-$lang
options = -cl -pl -cq -ch -pp -ps -slw 20 -lw 40 -sc -mw 300 -sww 140 -mcn 3 -mcwn 1 -g
desc = correction ambiguë et reconnaissance ambiguë des mots composés, amalgames, et combinaisons d'icelles

[numbers]
cmd = $bindir/numbers -nt -nv
desc = reconnaissance non-déterministe des ordinaux et cardinaux

[np]
cmd = if [ -x /usr/local/bin/np-$lang ]; then /usr/local/bin/np-$lang -nt -nv ; elif [ -x /usr/local/bin/np-fr ]; then /usr/local/bin/np-fr; else cat; fi
desc = reconnaissance non-déterministe des noms propres

[uw]
cmd = $bindir/uw -nt -nv
desc = passage en _uw / _Uw des mots inconnus

[cleaner]
cmd = $sxpipe/comments_cleaner.pl

[epsilonize]
cmd = $sxpipe/epsilonize_metawords.pl | perl -pe "s/ _SPECWORD_/ /"
desc = met en alternative avec _EPSILON les métamots (quotes, crochets...)

[recode-final_2]
cmd = $alexinatoolsdir/yadecode -l fr -u

[tokenizer_postprocessing]
cmd = perl -pe "s/_(UNSPLIT|REGLUE)_//g; s/(?:{.*?} *)?_SENT_BOUND/\n/g; s/_underscore/_/gi; s/_acc_o/{/gi; s/_acc_f/}/gi; s/&gt;/>/g; s/&lt;/</g; s/([\(\|\)\[\]\?\+])/\\\\\1/g;" | perl -pe "s/  +/ /g; s/^ //; s/ $//;"
#cmd = perl -pe "s/_(UNSPLIT|REGLUE)_//g; s/(?:{.*?} *)?_SENT_BOUND/\n/g; s/{(.*?)} *([^ \n]+)/\1/g; s/_underscore/_/gi; s/_acc_o/{/gi; s/_acc_f/}/gi; s/&gt;/>/g; s/&lt;/</g;" | perl -pe "s/  +/ /g; s/^ //; s/ $//;"
