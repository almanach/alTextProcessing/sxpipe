#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
 
 
#########################################################
# Ce code a pour but d'extraire une base de données contenant 
# des règles de correction extraites à partir de corpus de fautes annotées
#########################################################

$|=1; 
while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
elsif (/^-db$/) {$dbpath=shift;}
}
if (-r $dbpath) {
  print STDERR "  Erasing previous database $dbpath\n";
  `rm $dbpath`;
}

# On crée la base de données chargée de stockée les règles
my $dbh = DBI->connect("dbi:SQLite:$dbpath", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(infixe_Err,infixe_Corr,lettrePref,lettreSuff,nbOcc,score);");
my $sth=$dbh->prepare('INSERT INTO data(infixe_Err,infixe_Corr,lettrePref,lettreSuff,nbOcc,score) VALUES (?,?,?,?,?,?)');
my %regles;

while (<>){

	m/^([^\t]+)\t(.+)$/;
	
	my $forme_err=decompose_accents("##".minusculise_accents(lc($1))."##");
	my $forme_corr=decompose_accents("##".minusculise_accents(lc($2))."##");
	my $forme_err2="##".minusculise_accents(lc($1))."##";

	if($forme_err eq $forme_corr or $forme_err."s" eq $forme_corr or $forme_err eq $forme_corr."s" ){
		next;
	}
	#on retire partie gauche commune
	$pref="";
	$pref_idem=1;
	while ($pref_idem){
		$forme_err=~ s/^(.)(.*)$/$2/; 
		$first_lettre_err=$1;
		$suite_err=$2;
		$forme_corr=~ s/^(.)(.*)$/$2/; 
		$first_lettre_corr=$1;
		$suite_corr=$2;
		if($first_lettre_corr eq $first_lettre_err){
			$pref.=$first_lettre_corr;
		}else{
			$pref_idem=0;
			$forme_err=$first_lettre_err.$suite_err;
			$forme_corr=$first_lettre_corr.$suite_corr;
		}
	}
	
	#on retire partie droite commune
	$suff="";
	$suff_idem=1;
	while ($suff_idem){
		$forme_err=~ s/^(.*)(.)$/$1/; 
		$suite_err=$1;
		$last_lettre_err=$2;
		$forme_corr=~ s/^(.*)(.)$/$1/; 
		$suite_corr=$1;
		$last_lettre_corr=$2;
		if($last_lettre_corr eq $last_lettre_err){
			$suff=$last_lettre_corr.$suff;
		}else{
			$suff_idem=0;
			$forme_err=$suite_err.$last_lettre_err;
			$forme_corr=$suite_corr.$last_lettre_corr;
		}
		if ($suite_corr eq "" or $suite_err eq ""){
			last;
		}
	}
	#on récupère le contexte gauche
	$pref=~m/(.)(.)$/;
	$first_letter_cg=$2;
	$type_second_letter_cg = getTypeLettre($1);
	
	#on récupère le contexte droit
	$suff=~m/^(.)(.)/;
	$first_letter_cd=$1;
	$type_second_letter_cd = getTypeLettre($2);
		
	#On normalise la faute
	if ($forme_err eq ""){
		$forme_err="_";
	}
	if ($forme_corr eq ""){
		$forme_corr="_";
	}
	
	#On gère les cas de dupplication de lettres fautifs et les cas inverses (ex: lunnete)
	($forme_err,$forme_corr) = dupplic_dedupplic_letter($forme_err,$forme_corr, $first_letter_cg, $first_letter_cd);
	
	#On gère les cas d'accentuation ou de cédille
	($forme_err,$forme_corr, $first_letter_cg,$type_second_letter_cg, $first_letter_cd,$type_second_letter_cd) = accentuation($forme_err,$forme_corr, $first_letter_cg, $first_letter_cd);
	
	
	#print "$forme_err - $forme_corr - $type_second_letter_cg$first_letter_cg - $first_letter_cd$type_second_letter_cd\t->\t";
	#On merge les contextes
	if (exists $regles{$forme_err}{$forme_corr}{$type_second_letter_cg}{$type_second_letter_cd}){
		#si existe, on merge les contextes
		my $ref_info_regle=$regles{$forme_err}{$forme_corr}{$type_second_letter_cg}{$type_second_letter_cd};
		my ($contextg,$contextd,$nbocc)	=@$ref_info_regle;
		my @contextg=@$contextg;
		my @contextd=@$contextd;
		
		my $first_letter_cgq=quotemeta($first_letter_cg);
		my $first_letter_cdq=quotemeta($first_letter_cd);
		
		#print "cg _$first_letter_cg à [@contextg] \t\t cd _$first_letter_cd à [@contextd] \t\t"; 	
		if ("@contextg" !~ m/$first_letter_cgq/ ){  #on ajoute new contexte gauche
			push(@contextg,"$first_letter_cg");
			#print "add  \t\t"; 
		}
		if ("@contextd" !~ m/$first_letter_cdq/ ){ #on ajoute new contexte droit
			push(@contextd,"$first_letter_cd");		
			#print "add  \t\t"; 	
		}
		#print "\n";
		$nbocc++;
		if ($forme_err2 eq "##oœuvre##"){
			print "$forme_err2 - $forme_err}{$forme_corr}{$type_second_letter_cg}{$type_second_letter_cd}=[@contextg,@contextd,$nbocc]\n" ; 
		}
		$regles{$forme_err}{$forme_corr}{$type_second_letter_cg}{$type_second_letter_cd}=[\@contextg,\@contextd,$nbocc,-1]
	}else{
		if ($forme_err2 eq "##oœuvre##"){
			print "$forme_err2 - $forme_err}{$forme_corr}{$type_second_letter_cg}{$type_second_letter_cd}=[[$first_letter_cg],[$first_letter_cd],1]\n" ; 
		}
		#si existe pas on crée une nouvelle entrée
		#print "existe pas création\t";
		$regles{$forme_err}{$forme_corr}{$type_second_letter_cg}{$type_second_letter_cd}=[[$first_letter_cg],[$first_letter_cd],1];
		#print "[$first_letter_cg]\t[$first_letter_cd]\n"
	}
}

print "regles chargées\n";

#On calcule les scores logarithmés et on retient le meilleurs et le moins bon score pour pouvoir ensuite normaliser ce score entre 0 et1
my $scoremin=10000;
my $scoremax=-10000;
for $err (keys %regles){
	for $corr (keys %{$regles{$err}}){
		for $type_second_letter_cg (keys %{$regles{$err}{$corr}}){
			for $type_second_letter_cd (keys %{$regles{$err}{$corr}{$type_second_letter_cg}}){
				my $infos_regles=$regles{$err}{$corr}{$type_second_letter_cg}{$type_second_letter_cd};
				my ($cg,$cd,$nbocc)=@$infos_regles;
				$nbocc = sprintf("%.3f",log($nbocc)) if ($nbocc !=0);
				
				$regles{$err}{$corr}{$type_second_letter_cg}{$type_second_letter_cd}=[$cg,$cd,$nbocc];

				$scoremin=$nbocc if ($scoremin>$nbocc);
				$scoremax=$nbocc if ($scoremax<$nbocc);
			}
		}
	}
}

print "log(score) done. min=$scoremin - max= $scoremax.";

#On normalise tous les scores entre 0 et1 et on rentre toutes les règles de corr dans la data base
for $err (keys %regles){
	for $corr (keys %{$regles{$err}}){
		for $type_second_letter_cg (keys %{$regles{$err}{$corr}}){
			for $type_second_letter_cd (keys %{$regles{$err}{$corr}{$type_second_letter_cg}}){
				my $infos_regles=$regles{$err}{$corr}{$type_second_letter_cg}{$type_second_letter_cd};
				my ($cg,$cd,$nbocc)=@$infos_regles;
				my @contextg = @$cg;
				my @contextd = @$cd;
				my $contextegauche=$type_second_letter_cg."[".quotemeta(join("",@contextg))."]";
				my $contextedroit="[".quotemeta(join("",@contextd))."]".$type_second_letter_cd; 
				#print "execute $err - $corr - $contextegauche, $contextedroit)\n";
				my $score = $nbocc;
				if ($nbocc !=0){
					$score = sprintf("%.3f",($nbocc-$scoremin)/($scoremax-$scoremin));
				}
				$sth->execute($err, $corr, $contextegauche, $contextedroit,$nbocc,$score);
			}
		}
	}
}

	
#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les index pour la base de données.
#------------------------------------------------------------------------------------------------------

$dbh->commit;
$sth->finish;
print STDERR "  Creating index...";
$dbh->do("CREATE INDEX indinfixe_Err ON data(infixe_Err);");
$dbh->do("CREATE INDEX indinfixe_Corr ON data(infixe_Corr);");
$dbh->do("CREATE INDEX indnbOcc ON data(nbOcc);");
$dbh->do("CREATE INDEX indContexte ON data(lettrePref,lettreSuff);");
$dbh->do("CREATE INDEX indInfixe ON data(infixe_Err,infixe_Corr);");
$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;



#------------------------------------------------------------------------------------------------------
# MÉTHODES
#------------------------------------------------------------------------------------------------------

sub getTypeLettre{
	my $letter = shift;
	if ($letter =~ m/[bBßcCçÇdDfFgGhHjJkKlLmMnNñÑpPqQrRsStTvVwWxXzZ¸]/){
		return "C";
	}elsif ($letter =~ m/[aAåÅâÂäÄāÃáÁàÀæÆeEêÊëÊéÉèÈiIîÎíÍìÌïÏoOôÔõÕóÓòÒöÖuUûÛüÜúÚùÙyYŷŶÿŸýÝœŒˆ¨˜´`]/){
		return "V"
	}elsif ($letter =~ m/#/){
		return "#";
	}
	return $letter;
}

sub dupplic_dedupplic_letter{
	#On gère les cas de dupplication de lettres fautifs et les cas inverses (ex: lunnete)
	my ($forme_err, $forme_corr, $letter_cg, $letter_cd)= @_;
	my $letter_cgq = quotemeta($letter_cg);
	my $letter_cdq = quotemeta($letter_cd);
	if($forme_err =~ m/^$letter_cgq/ and $forme_corr =~ m/^[^$letter_cgq]/){
		$forme_err =~ s/^$letter_cgq+/+_/;
		#$forme_corr =~ s/^/-_/;
	}elsif($forme_err =~ m/$letter_cdq$/ and $forme_corr =~ m/[^$letter_cdq]$/){
		$forme_err =~ s/$letter_cdq$/_+/;
		#$forme_corr =~ s/$/_-/;
	}
	if($forme_corr =~ m/^$letter_cgq/ and $forme_err =~ m/^[^$letter_cgq]/){
		$forme_corr =~ s/^$letter_cgq/+_/;
	}elsif($forme_corr =~ m/$letter_cdq$/ and $forme_err =~ m/[^$letter_cdq]$/){
		$forme_corr =~ s/$letter_cdq$/_+/;
	}
	$forme_err =~ s/^(\+)_(.+)$/$1$2/;
	$forme_err =~ s/^(.+)_(\+)$/$1$2/;
	$forme_corr =~ s/^(\+)_(.+)$/$1$2/;
	$forme_corr =~ s/^(.+)_(\+)$/$1$2/;
	return ($forme_err,$forme_corr);
}

sub accentuation{
	#On gère les cas de accentuation : ¨ ´ ` ˆ ˜ et de cedille ¸
	my ($forme_err,$forme_corr, $first_letter_cg, $first_letter_cd)= @_;
	#décoposition des accents:
	$forme_err = decompose_accents($forme_err);
	$forme_corr = decompose_accents($forme_corr);
	
	#merge contexte gauche
	$forme_err=~ s/^(.)(.*)$/$2/;
	my $first_letter_err=$1;
	$forme_corr=~ s/^(.)(.*)$/$2/;
	my $first_letter_corr=$1;
	if ($first_letter_err eq $first_letter_corr and $first_letter_err ne "_"){
		$type_second_letter_cg=getTypeLettre($first_letter_cg);
		$first_letter_cg=$first_letter_err;
		$forme_corr=~ s/^$/_/;
		$forme_err=~ s/^$/_/;
	}else{
		$forme_err =$first_letter_err.$forme_err;
		$forme_corr=$first_letter_corr.$forme_corr;
	}
	
	#merge contexte droit
	$forme_err=~ s/^(.*)(.)$/$1/;
	my $last_letter_err=$2;
	$forme_corr=~ s/^(.*)(.)$/$1/;
	my $last_letter_corr=$2;
	if ($last_letter_err eq $last_letter_corr and $last_letter_err ne "_"){
		$type_second_letter_cd=getTypeLettre($first_letter_cd);
		$first_letter_cd=$last_letter_err;
		$forme_corr=~ s/^$/_/;
		$forme_err=~ s/^$/_/;
	}else{
		$forme_err .=$last_letter_err;
		$forme_corr.=$last_letter_corr;
	}
	return ($forme_err,$forme_corr, $first_letter_cg, $type_second_letter_cg, $first_letter_cd, $type_second_letter_cd);
	#return (recompose_accents($forme_err),recompose_accents($forme_corr), $first_letter_cg,  $type_second_letter_cg, $first_letter_cd, $type_second_letter_cd);
	
}

sub decompose_accents{
	my $all_err = shift;
	$all_err =~ s/â/ˆa/go;
	$all_err =~ s/ä/¨a/go;
	$all_err =~ s/ā/˜a/go;
	$all_err =~ s/á/´a/go;
	$all_err =~ s/à/`a/go;
	$all_err =~ s/ë/¨e/go;
	$all_err =~ s/ê/ˆe/go;
	$all_err =~ s/é/´e/go;
	$all_err =~ s/è/`e/go;
	$all_err =~ s/î/ˆi/go;
	$all_err =~ s/ï/¨i/go;
	$all_err =~ s/í/´i/go;
	$all_err =~ s/ì/`i/go;
	$all_err =~ s/ô/ˆo/go;
	$all_err =~ s/ö/¨o/go;
	$all_err =~ s/õ/˜o/go;
	$all_err =~ s/ó/´o/go;
	$all_err =~ s/ò/`o/go;
	$all_err =~ s/û/ˆu/go;
	$all_err =~ s/ü/¨u/go;
	$all_err =~ s/ú/´u/go;
	$all_err =~ s/ù/`u/go;
	$all_err =~ s/ŷ/ˆy/go;
	$all_err =~ s/ÿ/¨y/go;
	$all_err =~ s/ý/´y/go;
	$all_err =~ s/ç/¸c/go;
	$all_err =~ s/ñ/˜n/go;
	return $all_err;
}

sub minusculise_accents{
	my $s = shift;
	$s =~ s/Â/â/go;	
	$s =~ s/Å/å/go;
	$s =~ s/Ä/ä/go;
	$s =~ s/Ã/ā/go;
	$s =~ s/Á/á/go;
	$s =~ s/À/à/go;
	$s =~ s/Ë/ë/go;
	$s =~ s/Ê/ê/go;
	$s =~ s/É/é/go;
	$s =~ s/È/è/go;
	$s =~ s/Î/î/go;
	$s =~ s/Ï/ï/go;
	$s =~ s/Í/í/go;
	$s =~ s/Ì/ì/go;
	$s =~ s/Ô/ô/go;
	$s =~ s/Ö/ö/go;
	$s =~ s/Õ/õ/go;
	$s =~ s/Ó/ó/go;
	$s =~ s/Ò/ò/go;
	$s =~ s/Û/û/go;
	$s =~ s/Ü/ü/go;
	$s =~ s/Ú/ú/go;
	$s =~ s/Ù/ù/go;
	$s =~ s/Ŷ/ŷ/go;
	$s =~ s/Ÿ/ÿ/go;
	$s =~ s/Ý/ý/go;
	$s =~ s/Ç/ç/go;
	$s =~ s/Ñ/ñ/go;
	return $s;
}	
