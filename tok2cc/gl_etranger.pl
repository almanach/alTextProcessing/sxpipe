#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;

my $has_tags = 0;

$|=1; 

my $share_dir = ""; # folder (typically /usr/local/share/sxpipe) that contains models and t-test data

while (1) {
$_=shift;
if (/^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script gl_etranger a pour but détecter les mots étranger dans un texte et de les annoter comme tel. Pour bien fonctionner il nécessite les options suivantes:
\t-d\tcorrespond au répertoire dans lequel sont stockés les modèles de langue appris et la base de données contenant la fréquence des mots\n";
}
elsif (/^-d$/) {$share_dir=shift;}
elsif (/^-ht$/) {$has_tags=1;}
}

die "### ERROR: gl_etranger.pl requires using option -d" if ($share_dir eq "");

my $unkfr = "";
my $unklw = "";
if ($has_tags) {
	$unkfr = "-UNKFR";
	$unklw = "-UNKLW";
}

## hash de cache
my %getTraitTtest_cache;

##on stocke les models extraits du corpus wacky n-gramm + ttest

my %model1_2=();
open FILE, "<$share_dir/model1a2";
binmode FILE, ":encoding(utf8)";
while (<FILE>) {
	chomp;
	my @keyval =  split(/ /, $_);
	$model1_2{$keyval[0]} = $keyval[1] ;
}
close FILE;

my %model2=();
open FILE, "<$share_dir/model2";
binmode FILE, ":encoding(utf8)";
while (<FILE>) {
	chomp;
	my @keyval =  split(/ /, $_);
	$model2{$keyval[0]} = $keyval[1] ;
}
close FILE;

my %model2_3=();
open FILE, "<$share_dir/model2a3";
binmode FILE, ":encoding(utf8)";
while (<FILE>) {
	chomp;
	my @keyval =  split(/ /, $_);
	$model2_3{$keyval[0]} = $keyval[1] ;
}
close FILE;

#on ouvre la base de donnée du ttest
my $ttest_dbh = DBI->connect("dbi:SQLite:$share_dir/traitsTtest.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$ttest_dbh->{sqlite_unicode}=1;
my $ttest_sth = $ttest_dbh->prepare('select value from data where key=?;');


while (<>) {
	# formattage
	chomp;
	if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
		print "$_\n";
		next;
	}
	s/^\s*/ /o;
	s/\s$/ /o;

	#Reconnaissance des mots étrangers
	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/([^\\])\)/$1 \)/g;

	my $tmp=$_." ";
	$_ = "";
	while ($tmp =~ s/^( *([^\}]*\} )([^ ]*?) +(?:\[\|.*?\|\] *)?([ \)]*) *)//) {
		my $motsxpipe = $1;
		my $original = $2;
		my $mot = $3;
		$mot =~ s/\/[^ \/]+$// if ($has_tags) ;
		#print "motsxpipe:$motsxpipe \nTMP: ".$tmp."\n";
		
		if ($mot =~ m/_INC_/) {
			my $motbis= $mot ;
			$mot=~ s/_INC_//g;
	
			##EXTRACTION DES TRAITS
			#extraction des unigrammes
			my @uni_ac_doublon = split(//, $mot);
		
			#extraction des bigrammes et trigrammes
			my %trait_unigramme=();
			my %trait_bigramme=();
			my %trait_trigramme=();
			my $id = 0;	
			my $idmax=$#uni_ac_doublon;
			my $lettre;
			
			for $lettre (@uni_ac_doublon) {
				$trait_unigramme{"gram_$lettre"}=1 ;
				if ($id!=$idmax) {
					$trait_bigramme{"gram_$lettre$uni_ac_doublon[$id+1]"}=1 ;
					$trait_trigramme{"gram_$lettre$uni_ac_doublon[$id+1]$uni_ac_doublon[$id+2]"}=1 if ($id!=($idmax+1) );
				}
				$id+=1;
			}
			my %trait_bigramme_sans_dollard = %trait_bigramme;
			$trait_bigramme{"gram_\$$uni_ac_doublon[0]"}=1;
			$trait_trigramme{"gram_\$\$$uni_ac_doublon[0]"}=1;
			$trait_trigramme{"gram_\$$uni_ac_doublon[0]$uni_ac_doublon[1]"}=1;
			$trait_bigramme{"gram_$uni_ac_doublon[$#uni_ac_doublon]\$"}=1;
			$trait_trigramme{"gram_$uni_ac_doublon[$#uni_ac_doublon-1]$lettre$uni_ac_doublon[$#uni_ac_doublon]\$"}=1;
			$trait_trigramme{"gram_$uni_ac_doublon[$#uni_ac_doublon]\$\$"}=1;

			##CALCUL SCORE POUR TROIS MODELS
			my @traitmodel2=(keys(%trait_bigramme));
			my @traitmodel2_3=(keys(%trait_bigramme_sans_dollard),keys(%trait_trigramme));
			my @traitmodel1_2=(keys(%trait_bigramme),keys(%trait_unigramme));	 
			if (is_in_ttest($mot)) {
				push(@traitmodel2,getTraitTtest($mot));
				push(@traitmodel2_3,getTraitTtest($mot));
				push(@traitmodel1_2,getTraitTtest($mot));	 
			}
			my $score_model2 = getscore(\@traitmodel2,\%model2);
			my $score_model2a3 = getscore(\@traitmodel2_3,\%model2_3);
			my $score_model1a2 = getscore(\@traitmodel1_2,\%model1_2);
	
			#on compare ses résultats en fonction des différents models
			my $nbEn=0;
			my $nbFR=0;
			my @allScore=($score_model1a2,$score_model2,$score_model2a3);
			my $score_model;
			for $score_model (@allScore) {
				#Si maxEnt > 0.2 on l'annote comme mot étranger
				if ($score_model>0.2) {
					$nbEn++;
				} elsif ($score_model<0) {	
					$nbFR++;
				}
			}

			$mot = quotemeta($mot);
			my $tag = "";
			if ($has_tags) {
				$motsxpipe =~ s/(\/[^\/ ]+) *$//;
				$tag = $1;
			} else {
				$motsxpipe =~ s/ *$//;
			}
			#si au moins deux scores etr alors mots étranger
			if ($nbEn>1) {
				$motsxpipe =~ s/ $motbis/ _ETR$tag$unklw / ;
			#si on a moins de deux score fr c'est que le résultat est ambigue
			} elsif ($nbFR<2) {
				my $sxpipe_etr = $motsxpipe; 
				my $motbisq = quotemeta($motbis);
				$sxpipe_etr =~ s/ $motbisq/ _ETR/ ;
				if ($motsxpipe =~ m/^ *\(/) {
					$sxpipe_etr =~ s/^ *\(// ;
					$motsxpipe = "$motsxpipe$tag$unkfr | $sxpipe_etr$tag$unklw " ;
				} elsif ($motsxpipe =~ m/^\|/) {
					$motsxpipe =~ s/\) ?$//;
					$motsxpipe = "$motsxpipe$tag$unkfr $sxpipe_etr$tag$unklw " ;
				} else {
					$motsxpipe =~ s/ *\) *$/ / if ($motsxpipe =~ m/\) *$/) ;
					$motsxpipe = "( $motsxpipe$tag$unkfr | $sxpipe_etr$tag$unklw ) " ;
				}
			#sinon mot fr
			} else {
				$motsxpipe .= "$tag$unkfr ";
			}
		}
		$_.=$motsxpipe;
	}

	# sortie 
	s/ +/ /go;
	s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
	s/( ?[^\\])\)($|[^<] ?)/$1\) $2/go; 
	s/ +/ /go;

	s/^ //;
	s/ $//;
	print "$_\n";
}


#on ferme la base de donnée des ttests
$ttest_sth->finish;
$ttest_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.


##########################################################################################################
# MÉTHODES
##########################################################################################################

sub getscore {
	my ($alltraits, $modelref)=@_;
	my %model = %$modelref;
	my @traits = @$alltraits;
	my $score = $model{"**BIAS**"};
	my $trait;
	for $trait (@$alltraits){
		$score+=$model{$trait} if (defined($model{$trait}));
	}
	return $score;
}

sub is_in_ttest {
	my $word = $_;
	return $is_in_ttest_cache{$word} if (defined($is_in_ttest_cache{$word}));
	$ttest_sth->execute(lc $word);
	$reponse = $ttest_sth->fetchrow;
	#print "$reponse \n";
	if ($reponse !~ m\.\){
		$is_in_ttest_cache{$word} = 0;
		return 0;
	}
	$is_in_ttest_cache{$word} = 1;
	return 1;
}

sub getTraitTtest {
	my $word = shift;
	return $getTraitTtest_cache{$word} if (defined($getTraitTtest_cache{$word})) ;
	$ttest_sth->execute($word);
	$reponse = $ttest_sth->fetchrow;
	$getTraitTtest_cache{$word} = $reponse;
	return $reponse;
}
