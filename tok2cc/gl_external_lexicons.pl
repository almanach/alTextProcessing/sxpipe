#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;
use DBI;
use Encode;

$| = 1;

my $external_lexicons_directory = "./";

while (1) {
$_=shift;
if (/^$/) {last;}
elsif (/^-d$/) {$external_lexicons_directory=shift;}
elsif (/^-el$/) {$external_lexicons=shift;}
}
 
for (split /,/ , $external_lexicons) {
  $dbh{$_} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$_.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $_.dat not found";
  $sth{$_} = $dbh{$_}->prepare('select lemma,tag,inflclass from data where wordform=?;') || die "Could not prepare request on $_.dat";
}

while (<>) {
  chomp;
  while (s/^(.*?)_INC_([^ ]+)//) {
print $1;
%analyses = ();
for (keys %sth) {
  $sth{$_}->execute($2);
  while (@row = $sth{$_}->fetchrow_array) {
$row[2] =~ s/:/!/g;
push @{$analyses{Encode::decode('utf8',$row[0]).":".$row[2].":".$row[1]}}, $_;
  }
  $sth{$_}->finish;
}
if (scalar %analyses) {
  print "_LEX_".$2;
  print " [|".join(";",map {join(",",sort @{$analyses{$_}}).":".$_} sort {$a cmp $b} keys %analyses)."|]";
}else{
  print "_INC_".$2;
}
  }
  print "$_\n";
}
