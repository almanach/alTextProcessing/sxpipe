#!/usr/bin/env perl

$| = 1;

$maybe_skipXML = 0;

while (<>) {
  chomp;
  s/^ +//;
  s/ +$//;
  if (/E([0-9]+)F[0-9]+">[^<]*<\/F>\s*}\s*M\.$/) {
    print $_;
    $lastE = $1;
    $maybe_skipXML = 1;
  } elsif (/E([0-9]+)">\s*}\s*_XML$/ && $maybe_skipXML) {
    if ($1 == $lastE + 1) {
      print " ";
      next;
    } else {
      print "\n".$_."\n";
      $maybe_skipXML = 0;
    }
  } else {
    print $_."\n";
    $maybe_skipXML = 0;
  }
}
