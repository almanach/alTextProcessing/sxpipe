#!/usr/bin/env perl
# $Id$


binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$| = 1;

$lang="fr";

$title     = qr/(?:(?:M|Mme|Mlle|Dr|Me|Pr|Mrs|Mr|Miss|Mgr)\.?|Monsieur|Madame|Mademoiselle|Monseigneur|Docteur|Ma[iî]tre|Professeur|[vV]icomte|[Cc]omte|[Dd]uc|[Bb]aron|[vV]icomtesse|[Cc]omtesse|[Dd]uchesse|[Rr]oi|[Rr]eine|[Pp]rince|[Pp]rincesse|[Ss]ir|[Ll]ady|[Cc]hevalier|[Aa]miral|[Gg]énéral|[Ll]ieutenant-[Cc]olonel|[Ll]ieutenant|[Cc]apitaine|[Cc]ommandant|[Ss]ergent|[Cc]aporal|[Br]rigadier)|[PpMm]ère|[pP]an[^ ]*(?: +(?:[pP]oseł|[pP]ośle|[pP]osł[^ ]+))|(?:[pP]oseł|[pP]ośle|[pP]osł[^ ]+)/o;
$titleabbr_sans_pt     = qr/(?:(?:M|Mme|Mlle|Dr|Me|Pr|Mrs|Miss|Mgr))/o;
$article = qr/(?:la|le|les)/o;
$particule = qr/(?:de|du|von|van|of|ben|bin|ibn|del\'?|della|d\'|des|den|der)/o;

while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
    }

    @words=();@comments=();
    $currc="";
    $level=0;
    if (/>>> <<</ || /(^| )($title|[A-Z][a-z]?\.)( |$)/) {
	s/(?<=[^ ])\{/ \{/g;
	s/\}(?=[^ ])/\} /g;
	for (split) {
	    if (/^\{/) {
		$level++;
	    }
	    if ($level>0) {
		if ($currc ne "") {$currc.=" "}
		s/^ *\{ *//; 
		$currc.=$_;
		$currc=~s/ *\} *//;
	    } else {
		push(@words,$_);
		push(@comments,$currc);
		$currc="";
	    }
	    if (/\}$/) {
		$level--;
	    }
	}
	$tags="";
	for (@words) {
	    if (/^_(?:UNSPLIT|REGLUE)/o) {
		$tags.="S"; # special!
	    } elsif (/^(?:\<\<\<)?$titleabbr_sans_pt(?:\>\>\>)?$/o) {
		$tags.="T";
	    } elsif (/^(?:\<\<\<)?$title(?:\>\>\>)?$/o) {
		$tags.="t";
	    } elsif (/^(?:\<\<\<)?\.(?:\>\>\>)?$/o) {
		$tags.="P";
	    } elsif (/^(?:\<\<\<)?([A-ZÀÉÈÊËÂÄÔÖÛÜÇ]\.|_Uw|Ch\.|Th\.)(?:\>\>\>)?$/o) {
		$tags.="C";
	    } elsif (/^(?:\<\<\<)?([A-ZÀÉÈÊËÂÄÔÖÛÜÇ]|_Uw)/o) {
		$tags.="c";
	    } elsif (/^(?:\<\<\<)?$article$(?:\>\>\>)?/o) {
		$tags.="a";
	    } elsif (/^(?:\<\<\<)?$particule$(?:\>\>\>)?/o) {
		$tags.="d";
	    } elsif (/^\"$/o) {
		$tags.="g";
	    } elsif (/^(?:\<\<\<)?[A-ZÀÉÈÊËÂÄÔÖÛÜÇ]\.?(?:\>\>\>)?$/o) {
		$tags.="i";
	    } elsif (/^\.$/o) {
		$tags.="p";
	    } elsif (/^\-$/o) {
		$tags.="h";
	    } elsif (/^[\?\!;:\/\*\(\)]$/o|| /^\[$/o || /^\]$/o ) {
		$tags.="b";
	    } elsif (/^_SENT_BOUND$/o) {
		$tags.="b";
	    } else {
		$tags.="n";
	    }
	}
	$prevtags="";
	while ($prevtags ne $tags) {
	    $prevtags=$tags;
	    $tags =~ s/.S/sS/go; # S = special ; s = before special
	    $tags=~s/TP/tt/go;
	    $tags=~s/T/t/go;
	    $tags=~s/Cc/uu/go;
	    $tags=~s/C/c/goi;
	    $tags=~s/atchcc/uuuuuu/goi;
	    $tags=~s/tchcc/uuuuu/goi;
	    $tags=~s/atchc/uuuuu/goi;
	    $tags=~s/tchc/uuuu/goi;
	    $tags=~s/atcc/uuuu/goi;
	    $tags=~s/tcc/uuu/goi;
	    $tags=~s/atdcc/uuuuu/goi;
	    $tags=~s/tdcc/uuuu/goi;
	    $tags=~s/atddcc/uuuuuu/goi;
	    $tags=~s/tddcc/uuuuu/goi;
	    $tags=~s/atc/uuu/goi;
	    $tags=~s/tc/uu/goi;
	    $tags=~s/atdc/uuuu/goi;
	    $tags=~s/tdc/uuu/goi;
	    $tags=~s/atcdc/uuuuu/goi;
	    $tags=~s/tcdc/uuuu/goi;
	    $tags=~s/uuhu([^usS])/uuuu$1/goi;
	    $tags=~s/uuucb/uuuub/goi;
	    $tags=~s/uuuc/uuuu/goi; # ??????
	    $tags=~s/uuuccb/uuuuub/goi;
	    $tags=~s/tuu/uuu/goi;
	}
	#posttraitement
	while ($tags=~s/[ue]u/ee/go) {}
	$i=0;
	$state=0;
	$e="";
	for (split(//,$tags)) {
	    $w=$words[$i];
	    $c=$comments[$i];
	    $origw=$w;
	    $w=~s/^<<<(.*)>>>$/$1/o;
#	    if ($c eq "" && $w!~/^[\(\|\)]/) {$c=$w} POURQUOI CETTE EXCLUSION ICI???
	    if ($c eq "") {$c=$w}
	    if (/^e$/ && $state==0) {
		if ($i>0) {print " ";}
		print "{";
		print $c;
		$e.=$c." ";
		$state=1;
	    } elsif (/^[^e]$/ && $state==1) {
		$e=~s/ $//;
		if ($c ne "") {
		    print "} _NP {$c} $origw";
		} else {
		    print "} _NP $origw";
		}
		$state=0;
		$e="";
	    } else {
		if ($i>0) {print " ";}
		if ($state==1) {
		    print $c;
		    $e.=$c." ";
		} else {
		    if ($c ne "" && $origw !~ /^_(?:UNSPLIT|REGLUE)/) {
			print "{$c} $origw";
		    } else {
			print "$origw";
		    }
		}
	    }
	    if ($i==$#words) {
		if ($state==1){
		    print "} _NP";
		}
		print "\n";
	    }
	    $i++;
	}
    } else {
#	s/<<<//g;
#	s/>>>//g;
	print "$_\n";
    }
}
