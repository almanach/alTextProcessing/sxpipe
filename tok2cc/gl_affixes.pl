#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;
use DBI;
use Encode;

$| = 1;

my $external_lexicons_directory = "./";
while (1) {
	$_=shift;
	if (/^$/) {last;}
	elsif (/^-d$/) {$external_lexicons_directory=shift;}
	elsif (/^-el$/) {$external_lexicons=shift;}
	elsif (/^-l$/) {$lang=shift;}
}

$prefixes = "((agri|anti|après|archi|contre|cyber|dé|demi|dés|e|ex|extra|grand|hyper|im|in|inter|intra|mal|maxi|méga|méta|mi|mini|multi|non|outre|para|péri|pluri|poly|post|pré|quart|quasi|re|ré|sans|semi|sous|sub|super|supra|sur|télé|tiers|trans|ultra|uni|vice)-?|co-)";
$suffixes = "(iste|isme|isation)";

if ($external_lexicons ne "") {
	for (split ',', $external_lexicons) {
		$dbh{$_} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$_.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $_.dat not found";
		$sth{$_} = $dbh{$_}->prepare('select distinct lemma,tag,inflclass from data where wordform=?;') || die "Could not prepare request on $_.dat";
	}
} else {
	$dbh{$lang} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $lang.dat not found";
	$sth{$lang} = $dbh{$lang}->prepare('select distinct lemma,tag,inflclass from data where wordform=?;') || die "Could not prepare request on $lang.dat";
}

while (<>) {
	chomp;
	while (s/^(.*?)_INC_([^ ]+)//) {
		print $1;
		my $token = $2;
		my $analysis_found = 0;
		$affix = '';
		my @affix_hyps = ();
		if (!$analysis_found) {
			if ($token =~ /^$prefixes(.*)$/) {
				$affix = $1;
				for (keys %sth) {
					# Recherche du suffixe dans le Lefff
					$sth{$_}->execute($3);
					if (@row = $sth{$_}->fetchrow_array) {
						$row[2] =~ s/:/!/g;
						push(@affix_hyps, $affix.':'.$affix.decode('utf8',$row[0]).':'.decode('utf8',$row[2]).':'.decode('utf8',$row[1]));
					}
				}
				if (@affix_hyps) {
	  				$analysis_found = 1;
	  				print '_PREFIX_'.$affix.' [|'.join(';',@affix_hyps).'|]';
				}
			}
		}
		if (!$analysis_found) {
			if ($token =~ /^(.*)$suffixes$/) {
				$affix = $2;
				for (keys %sth) {
					# Recherche du préfixe dans le Lefff
					$sth{$_}->execute($1);
					if (@row = $sth{$_}->fetchrow_array) {
						$row[2] =~ s/:/!/g;
						$tag = 'NC_ms'; $flex = 'nc-2m' if ($affix =~ /-?isme/);
						$tag = 'NC_s'; $flex = 'nc-2' if ($affix =~ /-?iste/) ;
						$tag = 'NC_fs'; $flex = 'nc-2f' if ($affix =~ /-?isation/);
						push(@affix_hyps, $affix.':'.decode('utf8',$row[0]).$affix.':'.decode('utf8',$row[2]).':'.decode('utf8',$row[1]));
					}
				}
				if (@affix_hyps) {
					$analysis_found = 1;
					print '_SUFFIX_'.$affix.' [|'.join(';',@affix_hyps).'|]';
				}
			}
		}
		print _INC_.$token if not $analysis_found;
	}
	print "$_\n";
}

