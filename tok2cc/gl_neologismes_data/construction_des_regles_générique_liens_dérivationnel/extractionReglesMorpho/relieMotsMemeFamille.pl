#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
 
$|=1; 

 
while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-regles/) {$dbRegles=shift;}   #prend base de données de POS
    elsif (/^-dico/) {$dico=shift;}   #prend dico format .lex encodé en UTF8
    elsif (/^-sortie/) {$sortie=shift;}   #prend base de données qu'on veut sortir
    
}

#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------

# On ouvre la base de règles
my $regles_dbh = DBI->connect("dbi:SQLite:$dbRegles", "", "", {RaiseError => 1, AutoCommit => 1});
my $regles_getall2= $regles_dbh->prepare('SELECT pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle FROM data WHERE pref1=? AND suff1=? AND pos1=?;');

# On ouvre la base de règles dans laquelle on veut écrire

if (-r $sortie) {
  print STDERR "  Erasing previous database $sortie\n";
  `rm $sortie`;
}
my $sortie_dbh = DBI->connect("dbi:SQLite:$sortie", "", "", {RaiseError => 1, AutoCommit => 1});
$sortie_dbh->do("CREATE TABLE data(mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle);");
my $sortie_sth = $sortie_dbh->prepare('INSERT INTO data(mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');
#$sortie_dbh->do("CREATE TABLE data(mot1,mot2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle);");
#my $sortie_sth = $sortie_dbh->prepare('INSERT INTO data(mot1,mot2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle) VALUES (?,?,?,?,?,?,?,?,?,?)');



#on conserve les mots du dico
my %lefff;
my @mots;
open (Fic, "<$dico") || die "Problème à l\'ouverture fichier contenant les mots : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp;
	m/^([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*).*$/;	#à titre indicatif: $mot = $1; $cat = $2; $catflex = $3;
	my $cat = $2;
	if ($cat eq "advm" or $cat eq "advp"){
		$cat="adv";
	} 
	if ($cat ne "cf"){
		$lefff{$1}{$cat."_".$3}=1; #on stocke le mot et sa pos_classeflex
		push(@mots, $1);
		#print "$1 - $2 - $3\n";
	}
}	
close (Fic);
print STDERR "dico End \n";

my $l = 0;

#------------------------------------------------------------------------------------------------------
# Parcours des mots du dico pour créer des paires de candidats avec leurs regex correspondantes.
#------------------------------------------------------------------------------------------------------
my $nbmots = @mots;
my $numMot=0;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);
print "\n";
my %allMotForAnalogie;
#my @mots = keys (%dico);#("agenceurs","aubryste","balladurette","blablater","bloggueuse","blogueur","boguer","bootable","bouffeur","bravitude","buger","bugger","bugguer","buguer","bunkerisé","choucrouterie","choupinette","cliquables","compoter","déboulonneurs","débroussailleurs","décodifiez","décompactage","décongestionnants","décrocheurs","dégroupage","désinfectables","désintrouvabilisable","desintrouvabiliser","désobéisseurs","déssouder","déstockeur","déstockeur","diagnostiqueurs","discriminateur","divulgable","écloseurs","embouteilleurs","enneigeurs","énormitude","filoutage","flemmingite","footeux","geekement","hameçonnage","harmonisantes","introuvabiliser","islamophile","keumette","légumer","limitateur","métropolisation","parraineur","pathologisé","pixelisation","pogotais","portabilité","précommande","précommandé","prostater","prostitueurs","rebooter","recycleurs","refourguer","rematérialisée","reminéralisant","répétitivité","reposter","rescotchage","reseter","sarkozisme","sarkozyens","scroller","stockeurs","surbooker","surréservation","surtaxement","taggé","téléphonistes","trouvable","ultrafacile","upgrader","victimisant","victimiser","visitables","voyagiste","abitacle","abonment","adhére","agreabl","aimmerais","alor","alore","aniversaire","appuier","avaiet","baculer","bazards","beaucoups","béneficier","bnéficier","colegue","collectione","communicat","competance","concervé","conges","connaitre","conprenez","continura","coutchouc","deconsseille","defense","derangé","desinscription","effectivment","efficaté","ensolleillée","enterement","envelloppe","éssaye","esseiller","eviter","excatement","exceptionels","foctioner","foyfait","hore","inaproprié","intéréssé","interesser","interressante","interresse","inutillisable","inversemblable","irreprochables","journe","melheureusement","momemtanement","obsolésence","pexeliser","proceder","proseder","prosedure","puisqueun","pulverisateur","racheterais","recerche","recois","reconaissante","reconnaîs","recrédité","réçu","récuperé","reduit","reflechiser","regle","regler","rejete","releves","represente","reservation","resiliation","resiliée","resoudre","revoi","revoire","secateur","sechage","secrétes","serenite","sisthematiquement","advantage","adventure","after","apple","artist","author","battles","beautiful","believe","boost","callcenter","cheek","color","crazy","default","delete","desperate","dude","error","friends","hour","house","kitchen","login","lost","nautic","nobody","now","obvious","offline","pink","planet","players","pride","print","problem","protect","purchase","pursuit","quick","raster","ratchet","raw","reasonable","recipe","response","rub","safe","save","secure","select","serious","shameless","shape ","shark ","shoulder","sign","signs","smart","smiles","soldier","some","someone","spain","spies","strange ","stranger ","street ","stuff","subject","subscription ","survival","tablet","task","term","trader","unavailable","username","utilities","vanquish","void","wait","were","winner","winners","without");
my $verbose = 0;#1;

for $mot (@mots){ #(keys %lefff){
	
	for $pos1 (keys ($lefff{$mot})){
	$numMot+=1;
	$mot = "#$mot#";
	print STDERR "\r $numMot/$nbmots";
	#On extrait les préfixes et suffixes du mot inc
	my ($prefref,$suffref)= getAffixes($mot);
	my @prefInc = @$prefref;
	my @suffInc = @$suffref;
	push(@prefInc,"");
	push(@suffInc,"");
	
	my %allPropositionForMot; 


	#pour chaque part of speech que peut avoir le mot :
	my $posMot="$pos1";
		
	#On extrait toutes les données correspondants à ces suffixes ou préfixes dans notre base de données 
	my ($pref, $suff);
		
	for $pref (@prefInc){
		for $suff (@suffInc){
			if($verbose){
				print $pref."\t".$suff."\t".$posMot."\t.....\n";
			}
			$regles_getall2->execute("$pref","$suff", "$posMot");
			while (($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type) =  $regles_getall2->fetchrow){
				($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type)=encode_UTF8($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type);	

				my $newMot = $mot;
				$newMot =~ s/$pref1(.*)$suff1/$pref2$1$suff2/;
	
				#on stocke le mot généré par la regex s'il est connu du dico et si sa pos concorde.
				$newMot =~ m/^#(.*)#$/;
					
				if($verbose){
					print "\t".$newMot."\n";
				}
				if (exists($lefff{$1}) and exists($lefff{$1}{$pos2})) {
					#print "\t\t1.-> $nbOcc\t$mot\t$newMot\t$pos1\t$pos2\t-$type- $mot,$pref1,$suff1,$lettrePref,$lettreSuff\n";	
					if (bonContext($mot,$pref1,$suff1,$lettrePref,$lettreSuff)){ 
						
						my $catgramMot=$pos1;
						my $catgramLemme=$pos2;
						$catgramMot=~ s/_.*$//;
						$catgramLemme=~ s/_.*$//;	
						#print "\t\t2.-> $nbOcc\t$mot\t$newMot\t$pos1\t$pos2\t-$type-\n";
											
						if (($catgramMot eq $catgramLemme) and ($type eq "F")){#(exists $dicomotLemme{$mot}{$newmot}) and -> si on veut lemmatiser
							#print "\t\t-> $nbOcc\t$mot\t$newMotdiese\t$pos1\t$pos2\t$pref1 - $suff1\t$pref2 - $suff2 -$typeRegle-\n";
			 				$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}{$type}=$nbOcc; 
						}elsif ($type eq "D"){
			 				$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}{$type}=$nbOcc; 
							#print "\t\t-> $nbOcc\t$mot\t$newMotdiese\t$pos1\t$pos2\t$pref1 - $suff1\t$pref2 - $suff2 -$typeRegle-\n";
						}
						if($verbose){
							print "-> $nbOcc\t$mot\t$newMot\t$pos1\t$pos2\t$pref1 - $suff1\t$pref2 - $suff2\n";
							
						}
					}
				}
			}
		}
	}	
	#on filtre les règles qu'on veut conserver et on les ajoute à %allMotForAnalogie
	my $allregles=gestBestRegle(\%allPropositionForMot);
	@allRegles = @$allregles;
	for $regle (@allRegles){
		@regle1 = @$regle;
		#print "$regle1[0]}{$regle1[1]}{$regle1[2]}{$regle1[3]}{$regle1[4]}{$regle1[5]}{$regle1[6]}{$regle1[7]\n";
		$allMotForAnalogie{$regle1[0]}{$regle1[1]}{$regle1[2]}{$regle1[3]}{$regle1[4]}{$regle1[5]}{$regle1[6]}{$regle1[7]}{$regle1[8]}+=1;  
	} 
}
}
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);


	
#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------
$regles_getall2->finish;
$regles_dbh->disconnect;

$sortie_sth->finish;
%$sortie_dbh->commit;
print STDERR "  Creating index...";
$sortie_dbh->do("CREATE INDEX idmot1 ON data(mot1);");
$sortie_dbh->do("CREATE INDEX idmot2 ON data(mot2);");
$sortie_dbh->do("CREATE INDEX idpos1 ON data(pos1);");
$sortie_dbh->do("CREATE INDEX idpos2 ON data(pos2);");
print STDERR "done\n";
$sortie_dbh->disconnect;
print STDERR "\r  Loading data...$l\n";
################################################################################
#  MÉTHODES 
################################################################################

sub getAffixes{
	my ($motInc) = @_;
	my $id = 1;
	my (@pref, @suff);
	while ($id!=length($motInc)){
		$motInc =~ m/^(.{$id})(.*)$/;
		push(@pref,$1);
		push(@suff,$2);
		$id+=1; 
	}
	return (\@pref, \@suff);
}

sub	encode_UTF8{	
	my ($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type)= @_;
	$pos1 = Encode::decode("utf-8",$pos1);
	$pos2 = Encode::decode("utf-8",$pos2);
	$pref1 = Encode::decode("utf-8",$pref1);
	$suff1 = Encode::decode("utf-8",$suff1);
	$pref2 = Encode::decode("utf-8",$pref2);
	$suff2 = Encode::decode("utf-8",$suff2);
	$lettrePref = Encode::decode("utf-8",$lettrePref);
	$lettreSuff = Encode::decode("utf-8",$lettreSuff);
	$nbOcc = Encode::decode("utf-8",$nbOcc);
	$type = Encode::decode("utf-8",$type);
	return ($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type);
}

sub gestBestRegle{
	my ($allPropositionForMot)=@_;
	%allPropositionForMot = %$allPropositionForMot;
	my @listeRegle;
	my $bestScore = 1;
	while ( $bestScore>0){#($#listeRegle<2)and
		my @bestRegle = ("","","","","","","","");
		$bestScore = 0;
		my ($bestMot1,$bestMot2);
		#my @best=("$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff);
		for $mot (keys (%allPropositionForMot)){
			for $newMot (keys %{$allPropositionForMot{$mot}}){
				for $pos1 (keys %{$allPropositionForMot{$mot}{$newMot}}){
					for $pos2 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}}){ 
						for $prefixe1 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}}){
							for $suffixe1 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}}){
								for $prefixe2 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}{$suffixe1}}){
									for $suffixe2 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}}){
										for $lettre1 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}}){
											for $lettre2 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}}){
												for $type (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}{$lettre2}}){
													my $nbocc= $allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}{$lettre2}{$type};
													my $newScore= getScore($nbocc,$prefixe1,$prefixe2,$suffixe1,$suffixe2,$mot,$newMot);
													#print "$nbocc \t $mot-$newMot, $pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2\n";
													
													if ($newScore>$bestScore){
														$bestScore= $newScore;
														@bestRegle=($pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$type);
														$bestMot1=$mot;
														$bestMot2=$newMot;
														if ($nbocc>=80){
															#print "$nbocc \t$mot, $newMot, $pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2 $type\n";
															$sortie_sth->execute($mot,$newMot, $pos1, $pos2 , $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc,$type)
															#$sortie_sth->execute($newMot,$mot, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc,$type)
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if ($bestScore>0){
			push(@listeRegle,\@bestRegle); 
			delete($allPropositionForMot{$bestMot1}{$bestMot2}{$bestRegle[0]}{$bestRegle[1]}{$bestRegle[2]}{$bestRegle[3]}{$bestRegle[4]}{$bestRegle[5]}{$bestRegle[6]}{$bestRegle[7]}{$bestRegle[8]});
			#print "$bestScore !!!!!!!!!!!!!!!!!!!!!!!{$bestMot1}{$bestMot2}{$bestRegle[0]}{$bestRegle[1]}{$bestRegle[2]}{$bestRegle[3]}{$bestRegle[4]}{$bestRegle[5]}{$bestRegle[6]}{$bestRegle[7]}\n";
			#print "$bestMot1-$bestMot2  $bestRegle[0]-$bestRegle[1]  $bestRegle[2]-$bestRegle[3]  $bestRegle[4]-$bestRegle[5]  $bestRegle[6]-$bestRegle[7]\n";

		}
	}
	return \@listeRegle;
}

sub getScore{
	my ($nbocc,$prefixe1,$prefixe2,$suffixe1,$suffixe2,$mot,$newMot)=@_;
	$mot =~ m/^$prefixe1(.*)$suffixe1$/;
	if ($nbocc > 10 and length($1)>3){
		return 1;
	}
	return 0;
	#return 1;
}

sub bonContext{
	my ($mot,$pref1,$suff1,$lettrePref,$lettreSuff)=@_;
#		print "$pref1$lettrePref.*$lettreSuff$suff1      #$mot#\n";
	
	if ($lettrePref eq "A"){
		$lettrePref="";
	} elsif ($lettrePref eq "C"){
		$lettrePref=qr/[bcçdfghjklmnpkrstvwxz]/;
	} elsif ($lettrePref eq "V"){
		$lettrePref=qr/[aàâäeéèêëiîïoôöuùûüy]/; 
	}
	if ($lettreSuff eq "A"){
		$lettreSuff="";
	} elsif ($lettreSuff eq "C"){
		$lettreSuff=qr/[bcçdfghjklmnpkrstvwxz]/;
	} elsif ($lettreSuff eq "V"){
		$lettreSuff=qr/[aàâäeéèêëiîïoôöuùûüy]/; 
	}
	#print "$pref1$lettrePref.*$lettreSuff$suff1   $mot\n";
	if ($mot=~ m/^$pref1$lettrePref.*$lettreSuff$suff1/){
		return 1;
	}
	return 0
}
