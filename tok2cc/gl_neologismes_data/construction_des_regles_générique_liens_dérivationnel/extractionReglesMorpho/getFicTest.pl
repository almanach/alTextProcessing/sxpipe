#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;
use Encode;
 
$|=1; 

while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-regles/) {$dbRegles=shift;}   #prend base de données de POS
}

#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------

# On ouvre la base de règles (mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle)
my $regles_dbh = DBI->connect("dbi:SQLite:$dbRegles", "", "", {RaiseError => 1, AutoCommit => 1});
my $regles_getline= $regles_dbh->prepare('SELECT rowid,* FROM data WHERE rowid=? and typeRegle="F";');
my $regles_getnbline= $regles_dbh->prepare('SELECT count (*) FROM data;');


#---------------------------------------------------------------------------------------------------------------------
# On selectionne les lignes de manière aléatoire.
#---------------------------------------------------------------------------------------------------------------------
#on récupère les numéros de ligne
$regles_getnbline->execute();
my $nblinedb = $regles_getnbline->fetchrow;
my %numLine;
my $nb_line_add=0;
while ($nb_line_add<500){
	my $newNum = sprintf("%.0f", rand($nblinedb));
	if (!$numLine{$newNum}){
		$numLine{$newNum}=1;
		$regles_getline->execute($newNum);
		while (($numligne,$mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle) =  $regles_getline->fetchrow){
			@ligne=encode_UTF82($numligne,$mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
			print "@ligne\n";
			$nb_line_add++;
		} 
	}
}


#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------
$regles_getline->finish;
$regles_getnbline->finish;
$regles_dbh->disconnect;

sub	encode_UTF82{	
	my ($numligne, $mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle)= @_;
	$numligne = Encode::decode("utf-8",$numligne);
	$mot1 = Encode::decode("utf-8",$mot1);
	$mot2 = Encode::decode("utf-8",$mot2);
	$pos1 = Encode::decode("utf-8",$pos1);
	$pos2 = Encode::decode("utf-8",$pos2);
	$pref1 = Encode::decode("utf-8",$pref1);
	$suff1 = Encode::decode("utf-8",$suff1);
	$pref2 = Encode::decode("utf-8",$pref2);
	$suff2 = Encode::decode("utf-8",$suff2);
	$lettrePref = Encode::decode("utf-8",$lettrePref);
	$lettreSuff = Encode::decode("utf-8",$lettreSuff);
	$nbOcc = Encode::decode("utf-8",$nbOcc);
	$typeRegle = Encode::decode("utf-8",$typeRegle);
	return ($numligne,$mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
}
