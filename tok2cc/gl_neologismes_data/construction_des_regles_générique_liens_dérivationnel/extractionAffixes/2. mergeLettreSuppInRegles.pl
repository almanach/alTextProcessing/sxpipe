#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
#use strict;
#use warnings;
use Encode;
 
$|=1;

# Prend les règles extraites et rassemble celle qui sont identique tout en mergeant leur caractère supplémentaire
# ex : a -> ait [m]
#  a -> ait [t]
#  => a -> ait [mt]

while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
elsif (/^-f$/) {$fileAffixes=shift;}   
}

if ($fileAffixes eq "") {
  die "### ERROR: 2.mergeLettreSuppInRegles.pl requires using option -f followed by the file that contains affix";
}

#on stocke les affixes donnés en options
my %affixes;
open (Fic, "<$fileAffixes") || die "Problème à l\'ouverture $fileAffixes : $!";
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp; 
 	m/^ *([0-9]+) (.*\t.*)\t\-(.*)\-$/;
 	
#	m/^ *(.*\t.*)\t\-(.*)\-\t([0-9]+)$/;
	$affixes{$2}{$3}=$1;	
#	print " ".$2."\t-$3-\t$1\n";	
}
close (Fic);


my $affixe;
for $affixe (keys(%affixes)){
	my @affixeLetters=keys %{$affixes{$affixe}};
	my $count=0;
	my $letter;
	for $letter (@affixeLetters){
		$count += $affixes{$affixe}{$letter};
	}
	print " ".$count."\t$affixe\t[".join("",@affixeLetters)."]\n";
}

