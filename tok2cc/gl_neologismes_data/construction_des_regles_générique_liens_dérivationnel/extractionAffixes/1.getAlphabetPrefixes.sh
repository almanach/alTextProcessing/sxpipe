#!/bin/sh
mydb=$1

date 

perl ExtractRegleMorphoPrefixe.pl -d $mydb > pref
echo -n "Extraction de règles ok - "
date
cat pref|grep -iE "^#	" > prefixeAlphabet/prefixeDiese
echo -n "diese ok - "
date
cat pref|grep -iE "^#ß" > prefixeAlphabet/prefixeß
echo -n "ß ok - "
cat pref|grep -iE "^#[aàäâ]" > prefixeAlphabet/prefixeA
echo -n "A ok - "
date
cat pref|grep -iE "^#[b]" > prefixeAlphabet/prefixeB
echo -n "B ok - "
date
cat pref|grep -iE "^#[cç]" > prefixeAlphabet/prefixeC
echo -n "C ok - "
date
cat pref|grep -iE "^#[d]" > prefixeAlphabet/prefixeD
echo -n "D ok - "
date
cat pref|grep -iE "^#[eéèêë]" > prefixeAlphabet/prefixeE
echo -n "E ok - "
date
cat pref|grep -iE "^#[f]" > prefixeAlphabet/prefixeF
echo -n "F ok - "
date
cat pref|grep -iE "^#[g]" > prefixeAlphabet/prefixeG
echo -n "G ok - "
date
cat pref|grep -iE "^#[h]" > prefixeAlphabet/prefixeH
echo -n "H ok - "
date
cat pref|grep -iE "^#[iîï]" > prefixeAlphabet/prefixeI
echo -n "i ok - "
date
cat pref|grep -iE "^#[j]" > prefixeAlphabet/prefixeJ
echo -n "j ok - "
date
cat pref|grep -iE "^#[k]" > prefixeAlphabet/prefixeK
echo -n "k ok - "
date
cat pref|grep -iE "^#[l]" > prefixeAlphabet/prefixeL
echo -n "l ok - "
date
cat pref|grep -iE "^#[m]" > prefixeAlphabet/prefixeM
echo -n "m ok - "
date
cat pref|grep -iE "^#[n]" > prefixeAlphabet/prefixeN
echo -n "n ok - "
date
cat pref|grep -iE "^#[oôö]" > prefixeAlphabet/prefixeO
echo -n "o ok - "
date
cat pref|grep -iE "^#[p]" > prefixeAlphabet/prefixeP
echo -n "p ok - "
date
cat pref|grep -iE "^#[q]" > prefixeAlphabet/prefixeQ
echo -n "q ok - "
date
cat pref|grep -iE "^#[r]" > prefixeAlphabet/prefixeR
echo -n "r ok - "
date
cat pref|grep -iE "^#[s]" > prefixeAlphabet/prefixeS
echo -n "s ok - "
date
cat pref|grep -iE "^#[t]" > prefixeAlphabet/prefixeT
echo -n "t ok - "
date
cat pref|grep -iE "^#[uûü]" > prefixeAlphabet/prefixeU
echo -n "u ok - "
date
cat pref|grep -iE "^#[v]" > prefixeAlphabet/prefixeV
echo -n "v ok - "
date
cat pref|grep -iE "^#[w]" > prefixeAlphabet/prefixeW
echo -n "w ok - "
date
cat pref|grep -iE "^#[x]" > prefixeAlphabet/prefixeX
echo -n "x ok - "
date
cat pref|grep -iE "^#[y]" > prefixeAlphabet/prefixeY
echo -n "y ok - "
date
cat pref|grep -iE "^#[z]" > prefixeAlphabet/prefixeZ
echo -n "z ok - "
date
cat prefixeAlphabet/prefixeDiese|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedDiese
echo -n "trie diese ok - "
date
cat prefixeAlphabet/prefixeß|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedß
echo -n "trie ß ok - "
date
cat prefixeAlphabet/prefixeA|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedA
echo -n "trie a ok - "
date
cat prefixeAlphabet/prefixeB|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedB
echo -n "trie b ok - "
date
cat prefixeAlphabet/prefixeC|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedC
echo -n "trie c ok - "
date
cat prefixeAlphabet/prefixeD|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedD
echo -n "trie d ok - "
date
cat prefixeAlphabet/prefixeE|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedE
echo -n "trie e ok - "
date
cat prefixeAlphabet/prefixeF|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedF
echo -n "trie f ok - "
date
cat prefixeAlphabet/prefixeG|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedG
echo -n "trie g ok - "
date
cat prefixeAlphabet/prefixeH|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedH
echo -n "trie h ok - "
date
cat prefixeAlphabet/prefixeI|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedI
echo -n "trie i ok - "
date
cat prefixeAlphabet/prefixeJ|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedJ
echo -n "trie j ok - "
date
cat prefixeAlphabet/prefixeK|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedK
echo -n "trie k ok - "
date
cat prefixeAlphabet/prefixeL|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedL
echo -n "trie l ok - "
date
cat prefixeAlphabet/prefixeM|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedM
echo -n "trie m ok - "
date
cat prefixeAlphabet/prefixeN|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedN
echo -n "trie n ok - "
date
cat prefixeAlphabet/prefixeO|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedO
echo -n "trie o ok - "
date
cat prefixeAlphabet/prefixeP|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedP
echo -n "trie p ok - "
date
cat prefixeAlphabet/prefixeQ|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedQ
echo -n "trie q ok - "
date
cat prefixeAlphabet/prefixeR|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedR
echo -n "trie r ok - "
date
cat prefixeAlphabet/prefixeS|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedS
echo -n "trie s ok - "
date
cat prefixeAlphabet/prefixeT|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedT
echo -n "trie t ok - "
date
cat prefixeAlphabet/prefixeU|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedU
echo -n "trie u ok - "
date
cat prefixeAlphabet/prefixeV|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedV
echo -n "trie v ok - "
date
cat prefixeAlphabet/prefixeW|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedW
echo -n "trie w ok - "
date
cat prefixeAlphabet/prefixeX|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedX
echo -n " triex ok - "
date
cat prefixeAlphabet/prefixeY|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedY
echo -n "trie y ok - "
date
cat prefixeAlphabet/prefixeZ|sort|uniq -c > prefixeAlphabetSorted/prefixeSortedZ
echo -n "trie z ok - "
date
cat prefixeAlphabetSorted/* |grep -iE " +[0-9][0-9]">prefixes_10_occs_min
date
