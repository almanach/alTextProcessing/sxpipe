#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
#use strict;
#use warnings;
use LWP::UserAgent;
use Encode;
use IPC::Open2;
use URL::Encode; 
use Clone;
use File::Temp qw/ tempfile tempdir /;;

$|=1; 

#ligne de commande:
# ligne de commande KendecServer/kendecserver/kendserver 127.0.0.1 8080 /home/marion/app/kenlm/languageModel_binary/
#puis 
# echo "ceci est un tessssst peug etre ok."|sxpipe-normalisation |perl /home/marion/workspace_eclipse/correcteur/sxpipe/tok2cc/corr_filtrage_bis.pl -lm frwiki_2gram -nbphrase 3 -serveur http://localhost:8080/decode
my $lm="frwiki_2gram.binary";
my $nbbestphrase=3; 
my $serveur="http://localhost:8080/decode";
my $lambda_sxp=10;
my $lambda_ana=10;

while (1) {
	$_=shift;
	if (!$_ || /^$/) {last;}
	elsif (/^-lm$/) {$lm=shift;} #par defaut frwiki_3gram_xaa.binary
	elsif (/^-nbphrase/) {$nbbestphrase=shift;} #par defaut 3
	elsif (/^-serveur/) {$serveur=shift;} #par defaut "http://localhost:8080/decode"
	elsif (/^-lsxp/) {$lambda_sxp=shift;} #par defaut 10
	elsif (/^-lana/) {$lambda_ana=shift;} #par defaut 10
}

#On récupère la sortie sxpipe.
my @allphrase;
my $allphrasestring;
while(<>){
	if($_ !~ m/^[ \n]*$/){
		$allphrasestring.=$_;
		chomp;
		push(@allphrase,$_);
	}
}

print STDERR "av dag2dag\n";

#my $ficTMP ="/home/marion/Bureau/Filtrage/sortie_sxp4filtrage.tmp";
#_____________________________________________________________________________________

$allphrasestring= deconcat_motSXP_agglu($allphrasestring);

$dir = tempdir( CLEANUP => 1 );
($fh, $filename) = tempfile( DIR => $dir );
open (FICTMP,">$filename");
print (FICTMP "$allphrasestring\n");
close FICTMP;

#On le passe dans Dag2udag
$resDag2udag=decode('UTF-8', `cat $filename | dag2udag `);
close($fh);
my @resDag2udag=split("\n",$resDag2udag);

#print STDERR "ap dag2dag\n";

#______________________________________________________________________________________

#On récupère les résultats Lazy pour nos dags
print STDERR "avant Lazy\n";
#print "@resDag2udag";
my $resLazyref = getanalyseLazy(\@resDag2udag, $lm,$nbbestphrase, $serveur,$lambda_sxp,$lambda_ana);

my @resLazy=@$resLazyref; #tableau contenant pour chaque phrase dags sortis par lazy mergés sous forme de string
print STDERR "getLazyfini\n"; #@resLazy\n\n\n";
#print STDERR "allPhraseSxp: @allphrase\n";

#Pour chaque phrase: on conserve les x meilleures propositions.
for $phraseSxpipe (@allphrase){
#
#	print "\n---------------------------------------------------\n".$phraseSxpipe."\n";
	my @pile_mots = (); 
	my $newphrase="";	
	$resLazyPhraseString= shift (@resLazy);
	my $nbocc=@resLazy;
	
	@resLazyPhrase = split(/\n0/,$resLazyPhraseString);
	# méthode qui modifie les scores des @resLazyPhrase afin qu'on puisse les récupérer par la suite.	
	$resLazyPhraseref=get_real_score(\@resLazyPhrase);
	
	@resLazyPhrase=@$resLazyPhraseref;
	my $debutsxp=1;
	while ($phraseSxpipe =~ s/^ *(([^\}]*\} )([^ ]*?) *((?:\[\|([^\]]*?)\|\] *)? +[\)\|]? *|$))//) { 
		my $motsxpipe = $1;
		my $mot = $3;
		$mot=~ s/^_//;
#		print "_____________________________________________________\n";
#		print "\t$mot\n";
#		print "@resLazyPhrase\n";
		if($phraseSxpipe eq "" and $debutsxp){ #On gère bug lazy qui ignore phrases qui ne contiennent qu'un mot non amb
			my $lazy_tmp = join("\n",@resLazyPhrase);
			$lazy_tmp =~ s/^ *0*0\t<s>\t[^\n]+\n//;
			if ($lazy_tmp eq ""){
				#print "\n → fioup1 lc($mot) ne '') \n";
				next;
			}
			$lazy_tmp=~m/^ +[0-9]+\t([^\t]+)\t[0-9]+$/;
			if(lc($mot) ne lc($1)){
				#print "\n → fioup2 lc($mot) ne lc($1) \n";
				unshift (@resLazy,$resLazyPhraseString);
				next;
			}
		}
		
		#Si mot à traiter est ambigü:
		if ($motsxpipe=~ m/^ *\(/){
#			print "ambig\n";
			
			($phraseSxpipe,$newmotsxpipe, $resLazyPhrase) = filtre_sxpipe_ambig($motsxpipe.$phraseSxpipe,\@pile_mots,\@resLazyPhrase);
#			print "------------------------------\n$phraseSxpipe,\n$newmotsxpipe, \n@$resLazyPhrase\n";
			@resLazyPhrase=@$resLazyPhrase;
			$newphrase.= $newmotsxpipe;
			@pile_mots = ();
		#Si mot à traiter est non ambigü:
		}else{
#			print "non_ambig\n";
			if ($mot=~m/_/){
				my $score =0;
				for $m (split("_",$mot)){
					my ($newscore,$allresLazyPhrase_ref) = get_moyenne_score (\@resLazyPhrase, $m);
					@resLazyPhrase=@$allresLazyPhrase_ref;
					$score = ($score+$newscore)/2;
				}
				$motsxpipe =~s/<\/F>/◀Filtre_$score<\/F>/;
				
			}else{
				my ($score,$allresLazyPhrase_ref) = get_moyenne_score (\@resLazyPhrase, $mot);
				@resLazyPhrase=@$allresLazyPhrase_ref;
				$motsxpipe =~s/<\/F>/◀Filtre_$score<\/F>/;
			}
#			print "__ @resLazyPhrase\n";
			$newphrase.= $motsxpipe;
		}
		$debutsxp=0;
	}	
	$newphrase =~ s/ +/ /go;
	print "$newphrase\n";
}

#############################################################################################
#										MÉTHODES
#############################################################################################

sub deconcat_motSXP_agglu{
	my $PhraseSxp = shift;
	while ($PhraseSxp=~m/ ((( *\{[^}]+\})+)( +[^_ ]+_[^ ]+) +(\[\|[^\|]+\|\])?)/){
#		print "$1, $2, $3, $4, $5 \n";
		my $allSXP = $1;
		my $forme = $3;
		my $tokenAgglu = $4;
		my $commentaire = $5;
		my @tokens =  split('_', $tokenAgglu);
		my $newSxp=" ";
		for $token (@tokens){
			$newSxp.=$forme." ".$token.$commentaire." " if ($token ne "");
		}
#		print  "avant : $allSXP\n";
#		print  "après : $newSxp\n";
		$newSxp=~s/ +/ /g;
		$newSxpq = quotemeta($allSXP);
		$PhraseSxp=~s/$newSxpq/$newSxp/;
#		print "$PhraseSxp\n____________________________________________\n";
	}
	return $PhraseSxp;
}

sub getanalyseLazy{ #prend format dag2udag et le met au format lazy
	my ($ref_udag, $lm,$nbbestphrase,$serveur,$lambda_sxp,$lambda_ana)=@_;
	@resDag2udag2=@$ref_udag;
	# Create a user agent object
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1 ");
	
	
	#On initialise les variables propres à la construction de notre graphe.
	my $nbTransition = 0;
	my $etatFinal;
	my %graphe;
	my @resLazy=();
	#on parcourt le dag passé
	for $ligne (@resDag2udag2) {
		#chomp($ligne);		
		#On récupère contenu d'une dag et on la converti au format lazy
		if ($ligne ne "##DAG END"){
			next if ($ligne eq "##DAG BEGIN");			
			if ($ligne =~ m/_End_Of_Sentence_/){
				$ligne =~ m/^([0-9]+)\t _End_Of_Sentence_ \t([0-9]+)$/;
				$etat_init=$1-1;
				$etat_arrive=$2-1;
				$nbTransition ++ if (! exists $graphe{$etat_arrive}{$etat_init}{"</s>"});
				$graphe{$etat_arrive}{$etat_init}{"</s>"}=0; 
				$etatFinal=$etat_arrive;				
				next;
			}
			$ligne =~ m/^([0-9]+)\t\{[^>]+>([^<]+)<[^\}]+\} ([^\t]+) (\[[^\]]+\] ?)?\t([0-9]+)$/;
			$etat_init=$1-1;
			$etat_arrive=$5-1;
			$contenu=$3;
			$contenu =~ s/_/ /g;
			my $poidsCoor = 0;
			#on prend en compte le poids de correction assigné}
			if ($2 =~ m/◀Corr_SXP_-?([0-9]+)/){
				$poidsCoor= "poids_correction_Sxp=".(1/$1)*$lambda_sxp if ($1 != 0);
				$poidsCoor= "poids_correction_Sxp=".0 if ($1 == 0);
			}
			if ($2 =~ m/◀Corr_ANA_([0-9\.]+)/){
				$poidsCoor= "poids_correction_Ana=".($1)*$lambda_ana if ($1 != 0);
				$poidsCoor= "poids_correction_Ana=".0 if ($1 == 0);
			}
			$nbTransition ++ if (! exists $graphe{$etat_arrive}{$etat_init}{$contenu});
			$graphe{$etat_arrive}{$etat_init}{$contenu}=$poidsCoor;  #Les poids de corrections sont ajoutés en option
			$etatFinal=$etat_arrive;
			next;
		}	
		#Lorsqu'on a terminé de récupérer les infos des dags on met en forme lazy
		my $texteLazy = getformatLazy($etatFinal,$nbTransition,\%graphe);
		
		#on réinitialise les données
		$nbTransition = 0;
		$etatFinal = "";
		%graphe = ();

		#Une fois le dag récupéré au format lazy, on l'envoie au serveur et on lit sa réponse
		if($texteLazy !~ m/^2 2/){
			$res = getResRequete($ua,$texteLazy, $lm,$nbbestphrase,$serveur);
			if ($res->is_success) {
				push(@resLazy,$res->content);			
			}else {
				print $res->status_line, "\n";
			}
		}
	} 
	my @newresLazy=();
	for $res (@resLazy){
		push (@newresLazy,decode('utf-8', "$res"));
	}
	return \@newresLazy;
}

sub getformatLazy{
	#méthode qui prend dag sxpipe et les converti au format lazy.
	my ($etatFinal,$nbTransition,$grapheref)=@_;
	%graphe = %$grapheref;
	#my $texte = "[".$etatFinal."] </s> |||\n";
	$texte="1\n".$texte;
	my $i=$etatFinal+1;
	while ($i>0){
		$i--;
		if (exists $graphe{$i}){
			my $nbtrans=0;
			for $init (keys %{$graphe{$i}}){
				for $contenu (keys %{$graphe{$i}{$init}}){					
					if (not exists $graphe{$i}{$init}{$contenu} or $graphe{$i}{$init}{$contenu} eq "0"){
						$texte = "[$init] $contenu ||| \n".$texte;
					}else{
						my $score=$graphe{$i}{$init}{$contenu};
						$texte = "[$init] $contenu ||| ".$score."\n".$texte;
					}
					$nbtrans++;			
				}		
			}
			$texte ="$nbtrans\n".$texte;
		}
	}
	$texte="<s> ||| \n".$texte;
	$texte="1\n".$texte;
	$nbetat=$etatFinal+1;
	$nbTransition+=1;
	#print $texte."\n";
	return "$nbetat $nbTransition\n".$texte;
}

sub getResRequete{
	#méthode qui lance la requête lazy -> renvoie son résultat.
	my ($ua,$texteLazy, $lm, $nbbestphrase, $serveur) = @_;
	# Création de la requête  	
	my $req = HTTP::Request->new(POST => $serveur);#'http://localhost:8080/decode');
	$req->content_type('application/x-www-form-urlencoded');
	my $requete = $lm."\n" #Le nom du LM
				.$nbbestphrase."\n" #nombre N en décimal de phrases souhaitées en retour
				."5\n" #nombre de threads en décimal
				."5\n" # paramètre beam en décimal
				."\@----part----@\n" #séparateur de section égal à "@----part----@"
				."LanguageModel=1\n" #poids des features
				."LanguageModel_OOV=-5\n"
				."WordPenalty=1\n"
				."poids_correction_Ana=1\n"#Score!!!!!!
				."poids_correction_Sxp=1\n"#Score!!!!!!						
				."\@----part----@\n" #séparateur de section 
				.$texteLazy #treilli au format Lazy (reconnu par kendec)
				."\@----part----@\n";  #séparateur de section 
	$req->content(encode('utf-8', $requete));
	#On passe la requête et on récupère la réponse 
	return $ua->request($req);
}

sub normalize1{
	my ($txt)=@_;
	$txt =~ s/\[\|/[[/go;
	$txt =~ s/\|\]/]]/go;
	$txt =~ s/Ana\(/Ana[/go;
	$txt =~ s/Affixe\(/Affixe[/go;
	$txt =~ s/Assoc\(/Assoc[/go;
	$txt =~ s/([^ \\])\)/$1]@]/go;
	$txt =~ s/^\( *([^\)]+[^\\])\)//; 
	return ($txt,$1);
}

sub normalize2{
	my ($txt)=@_;
	$txt =~ s/\[\[/[|/go;
	$txt =~ s/\]\]/|]/go;
	$txt =~ s/Ana\[/Ana(/go;
	$txt =~ s/Affixe\[/Affixe(/go;
	$txt =~ s/Assoc\[/Assoc(/go;
	$txt =~ s/#\]/#)/go;
	$txt =~ s/\]@\]/)/go;
	return $txt;
}

sub filtre_sxpipe_ambig{
	my ($phraseSxpipe,$pile_mots,$allresLazyPhrase)=@_;
	
	my %motLazyKeep=();
	my @newphraseLazy=();
	
	#on récup amb et on la suppr;
	my $chaine_ambig = "";
	($phraseSxpipe,$chaine_ambig) = normalize1($phraseSxpipe);
	$phraseSxpipe = normalize2($phraseSxpipe);

	@allAmbigSxpipe=split(/\|/,$chaine_ambig);
	@pile_mots = @$pile_mots;	#On récupère les mots déjà parcouru
	@allresLazyPhrase=@$allresLazyPhrase; 	#On récupère les analyses lazy
	#On gère les cas où il n'y a pas le nb de forme concerné dans une amb en les stockant
	my $nb_mot_amb_ref= createHashNbMotAmbig(@allAmbigSxpipe);

	#on suppr ce qui a déjà été lu sans ambiguité
	my $i = 0;
	for $phraseLazy (@allresLazyPhrase){
		my $bool_out_of_pile = 0;
		@pile_mots_tmp=@pile_mots;
		$phraseLazy =~ s/^0*0\t<s>\t[^\t]+\n//;
		my $nb_mot=1;
		while($phraseLazy =~ m/^([0-9]+)\t(.+)\t([0-9\-\.]+)(\n|$)/ ){ # $noeud = $1 & $tok = $2
			$score= $3;
			#print "$2 \n"; 
			if (($2 eq $pile_mots_tmp[0] or quotemeta($2) eq $pile_mots_tmp[0]) and not $bool_out_of_pile){
				$phraseLazy =~ s/^([0-9]+)\t(.+)\t([0-9\-\.]+)(\n|$)//;
				shift(@pile_mots_tmp);				
			}else{
				$phraseLazy =~ s/^([0-9]+)\t(.+)\t([0-9\-\.]+)(\n|$)//;
				my $tok = $2;
				if (not $bool_out_of_pile){
					#print "___\n";
					$nb_mot = getNbMotAmbig($tok,$nb_mot_amb_ref,$phraseLazy) ;
					#print $nb_mot."\n";
					
				}
				
				$bool_out_of_pile =1;
				if (exists ($motLazyKeep{$2})){
					$moyenneScore=($score+$motLazyKeep{$tok})/2;
					$motLazyKeep{$2}=$moyenneScore;
				}else{
					$motLazyKeep{$2}=$score;
				}
				$nb_mot-=1;
				if ($nb_mot <= 0){
					push (@newphraseLazy,$phraseLazy);
					last;
				}
			}
		}
	}
	my $newAmb = get_new_amb(\@allAmbigSxpipe,\%motLazyKeep);
	
	return ($phraseSxpipe,normalize2($chaine_ambig),\@newphraseLazy) if ($newAmb =~ m/^ *\( +$/);
	return ($phraseSxpipe,$newAmb,\@newphraseLazy)
}

sub get_new_amb{
	#méthode qui renvoie l'ambiguité qui vient d'être crée
	my ($allAmbigSxpipe_ref,$motLazyKeep_ref)=@_;
	my @allAmbigSxpipe=@$allAmbigSxpipe_ref;
	my %motLazyKeep=%$motLazyKeep_ref;
	my $newAmb="";	
	my $nbambigSxp=@allAmbigSxpipe;
	#print "nbambigSxp: $nbambigSxp\n";
	if($nbambigSxp>1){
		for $AmbigSxpipe (@allAmbigSxpipe){
			#print "\t\t- $AmbigSxpipe\n";
			$AmbigSxpipe =~ s/\[\[/[|/go;
			$AmbigSxpipe =~ s/\]\]/|]/go;
			$AmbigSxpipe=~m/^[^\}]+\} ([^ ]*?) /;
			$corr=$1;
			my $corr2=$corr;
			$corr2=~s/_/ /g;
			my @a = keys %motLazyKeep;
			
			if (exists($motLazyKeep{$corr})or exists($motLazyKeep{$corr2})){
				$score=$motLazyKeep{$1};
				$AmbigSxpipe=~s/<\/F>/◀Filtre_$score<\/F>/;
				$newAmb.=$AmbigSxpipe."|";
			}elsif($AmbigSxpipe =~ m/\{.+\{/){ 
				$corr = $AmbigSxpipe;
				$corr =~ s/ *\{[^\}]+\}//g;
				$corr =~ s/^ +//;
				$corr =~ s/ +$//;
				$corr =~ s/ +/+/g;
				if (exists($motLazyKeep{$corr})){
					$score=$motLazyKeep{$1};
					$AmbigSxpipe=~s/<\/F>/◀Filtre_$score<\/F>/;
					$newAmb.=$AmbigSxpipe."|";
				}
			}elsif($corr2=~m/ /){
				my $bool_trouve=1;
				for $elmt_corr (split(' ',$corr2)){
					if(!exists ($motLazyKeep{$elmt_corr})){
						$bool_trouve=0;
						last;
					}
				}
				if($bool_trouve){
					$score=$motLazyKeep{$1};
					$AmbigSxpipe=~s/<\/F>/◀Filtre_$score<\/F>/;
					$newAmb.=$AmbigSxpipe."|";
				}
			}
		}
		if ($newAmb eq ""){
			$newAmb = join (' | ', @allAmbigSxpipe)." \| ";
		}
		my $existeAmb = $newAmb =~m/\|[^|]+\|/; 
		$newAmb=~s/^/\( / if ($existeAmb);
		$newAmb=~s/ \| *$/ \) / if ($existeAmb);
		$newAmb=~s/ \| *$/ / if (!$existeAmb);
		
	}else{
		$newAmb=$allAmbigSxpipe[0];
		$newAmb =~ s/\[\[/[|/go;
		$newAmb =~ s/\]\]/|]/go;
		$newAmb=~m/^[^\}]+\} ([^ ]*?) /;
		$corr=$1;
		my $corr2=$corr;
		$corr2=~s/_/+/g;
		if (exists($motLazyKeep{$corr})or exists($motLazyKeep{$corr2})){
			$score=$motLazyKeep{$1};
			$newAmb=~s/<\/F>/◀Filtre_$score<\/F>/;
		}elsif($newAmb =~ m/\{.+\{/){ 
			$corr = $newAmb;
			$corr =~ s/ *\{[^\}]+\}//g;
			$corr =~ s/^ +//;
			$corr =~ s/ +$//;
			$corr =~ s/ +/+/g;
			if (exists($motLazyKeep{$corr})){
				$score=$motLazyKeep{$1};
				$newAmb=~s/<\/F>/◀Filtre_$score<\/F>/;
			}
		}
	}
	
	return normalize2($newAmb);
}

sub createHashNbMotAmbig {
	#renvoie une hash contenant le nombre de mot pouvant être concernés dans une ambiguité
	@allAmbigSxpipe = @_;
	my %nb_mot_amb;
	for $AmbigSxpipe (@allAmbigSxpipe){
		$AmbigSxpipe_tmp = $AmbigSxpipe;
		$AmbigSxpipe_tmp =~ s/\{[^\}]+\}//g;
		$AmbigSxpipe_tmp =~ s/\[\|[^\]]+\|\]//g;		
		$AmbigSxpipe_tmp =~ s/  +/ /g;
		$AmbigSxpipe_tmp =~ s/^ *//g;		
		$AmbigSxpipe_tmp =~ s/ *$//g;
		$AmbigSxpipe_tmp =~ s/_/ /g;
		my @tmp = split(" ", $AmbigSxpipe_tmp);
		my $nb_mot_amb_tmp = @tmp;
		$nb_mot_amb{$tmp[0]}{$nb_mot_amb_tmp}=\@tmp;
		#print "{$tmp[0]}{$nb_mot_amb_tmp}= @tmp \n";
	}
	return \%nb_mot_amb;
}

sub getNbMotAmbig{
	#On renvoie le nombre de mot concerné par l'ambiguité
	my ($tok,$nb_mot_amb_ref,$phraseLazy) = @_;
	my %nb_mot_amb=%$nb_mot_amb_ref;
	my %nb_mot_amb_bis = %{ Clone::clone ($nb_mot_amb_ref) };
	my $nb_mot =1 ;
	my @ScoresPossibles = sort {$b <=> $a} keys %{$nb_mot_amb_bis{$tok}};
	if (scalar(@ScoresPossibles) >= 2){ #si plusieurs scores possibles
		my $scoreMax= $ScoresPossibles[0];
		my $phraseLazy_tmp = "0\t$tok\t0\n".$phraseLazy; 
		my $i = 0;
		$nb_mot =-1;
		while ($i<$scoreMax and $phraseLazy_tmp =~ s/^([0-9]+)\t(.+)\t([0-9\-\.]+)(\n|$)//){
			my $mot_attendu = $2;
			$i +=1;
			for $scorePossible (@ScoresPossibles){
				my $ref_allMot2Possibles = $nb_mot_amb_bis{$tok}{$scorePossible};
				my @allMot2Possibles = @$ref_allMot2Possibles;
				my $size_allMotsPossibles = @allMot2Possibles;
				if ($size_allMotsPossibles>0){
					my $mot4score=shift(@allMot2Possibles);
					delete($nb_mot_amb_bis{$tok}{$scorePossible}) if ($mot4score ne $mot_attendu);
					$nb_mot_amb_bis{$tok}{$scorePossible}=\@allMot2Possibles if ($mot4score eq $mot_attendu);
				}
			}
			my @ScoresPossibles_bis = keys %{$nb_mot_amb_bis{$tok}};
			my $nb_ScoresPossibles_bis = @ScoresPossibles_bis;
			if ($nb_ScoresPossibles_bis==1){
				$nb_mot=$ScoresPossibles_bis[0];
				last;
			}
		}
		if($nb_mot==-1){
			my @ScoresPossibles_ter = sort {$b <=> $a} keys %{$nb_mot_amb_bis{$tok}};
			$nb_mot=$ScoresPossibles_ter[0];			
		}
		
	}else{ #si qu'un score possible
		$nb_mot=$ScoresPossibles[0];		
	}	
	return $nb_mot;
}

sub get_real_score{
	#renvoie le score lazy pour un mot donné
	my $phrasesref = shift;
	@phrases = @$phrasesref;
	@newphrases=();
	for $phrase (@phrases){
		my $lastScore = 0;
		my $newphrase="";
		$phrase="0".$phrase;
		while($phrase =~ s/(^[0-9]+\t.+\t([0-9\-\.]+)(\n|$))// ){
			$newmot=$1;
			$score= $2;
			$fin = $3;
			$newscore = sprintf("%.3f",sprintf("%.3f",$score)-($lastScore));
			$lastScore=sprintf("%.3f",sprintf("%.3f",$score));
			$newmot=~ s/$score$fin/$newscore$fin/;
			$newphrase.=$newmot;
		}
		push(@newphrases,$newphrase);
	}
	return \@newphrases;
}

sub get_moyenne_score {
	my $allresLazyPhrase = shift;
	my $mot = shift;
	@allresLazyPhrase=@$allresLazyPhrase;
	my $newScore=0;
	for $phraseLazy (@allresLazyPhrase) {
		$phraseLazy =~ s/^0*0\t<s>\t[^\t]+\n//;
		if($phraseLazy =~ m/^([0-9]+)\t(.+)\t([0-9\-\.]+)(\n|$)/){
			my $motLazy=$2;
			$motLazy =~s/\)/\\\)/g;
			#$motLazy =~s/&amp;lt;//;
			if($motLazy ne $mot and quotemeta($motLazy) ne $mot and quotemeta($mot) ne $motLazy){
				print STDERR "\t\t$motLazy ne $mot → no score!!!\n";
				#print "\t\t$motLazy ne $mot → no score!!!\n";
				
				return 0;				
			}
		}
		$phraseLazy =~ s/^([0-9]+)\t(.+)\t([0-9\-\.]+)(\n|$)//;
		my $tok = $2;
		my $score = $3;
		if ( $score!=0 ) {
			$moyenneScore = ( $score + $newScore ) / 2;
			$newScore     = $moyenneScore;
		}else {
			$newScore = $score;
		}		
	}	
	return ($newScore,\@allresLazyPhrase);
}
