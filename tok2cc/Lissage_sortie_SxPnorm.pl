#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug"; 
use utf8;
#use strict;
#use warnings;
use Encode;
#------------------------------------------------------------------------------------------------------
$|=1; 


while(<>){
	# formattage
	chomp;
	if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
		print "$_\n";
		next;
	}
	s/^\s*/ /o;
	s/\s$/ /o;

	my $tmp=$_." ";
	$_ = "";
	#print "1) $tmp\n";
	while ($tmp =~ s/^ *((\(([\|]* *\{[^\}]+\} [^ ]+ (\[\|[^\|]*\|\])? *)+\))|(\{[^\}]+\} +[^ ]+ +(\[\|[^\|]*\|\])?))//) {
		my $motsxpipe=$1;
		my $motsxpipebis = "";
		#print "motsxpipe: $motsxpipe \nTMP: $tmp\n";
		#Si ambiguité
		if ($motsxpipe=~m/^\(/){
			$motsxpipe=~s/^ *\( *//;
			$motsxpipe=~s/ *\) *$//;
			$motsxpipebis= lissage_ambig($motsxpipe);
		}else{#sinon
			$motsxpipebis = supp_etiquette($motsxpipe);
		}
		$_ .=$motsxpipebis;			 
		
	}
	
		# sortie 
	s/__adv / /g;
	s/__prep / /g;
	s/__det / /g;
	s/__csu / /g;

	s/  +/ /go;
	s/Affixe *\( *([^\)]+?) *\) */Affixe\($1\)/g;
	s/Assoc *\( *([^\)]+?) *\) */Assoc\($1\)/g;
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;
	s/^ //;
	s/ $//;
	#print "2) $_\n-------------------------------------------------------------\n";
	print "$_\n";
}

##---------------------------------------------------------------------
## Méthodes
##---------------------------------------------------------------------

sub supp_etiquette{
	my $forme = shift;
	#print "bla: $forme !!!\n";
	$forme =~ m/(^ *\{[^>]+>([^◀<]+)[^\}]+\} ([^ ]+( *\[\|[^\|]*\|\])?) *)$/ ;
	my $motsxpipe = $1; #forme sxpipe entière
	my $original = $2; #mot_original
	my $mot = $3; #seconde partie de la forme sxpipe (après les {} )
	my $motq = quotemeta($mot); #seconde partie de la forme sxpipe (après les {} )
	#print "motsxpipe: ".$motsxpipe." - ".$mot."!!!!!!!!!!!!!!!\n";
	if ($motq=~/SENT_BOUND/){
		return ""
	}
	#print $motq.": ";
	if ($motq=~/^_INC/){
		$motsxpipe =~ s/ _INC_/ /;
		my $etiquette ="◀INC";
		if ($motsxpipe =~ m/ \[\|/){
			$motsxpipe =~ s/ (\[\|[^\]]+\]) *$//;
			$etiquette.=$1;
		}
		$motsxpipe =~ s/<\/F>/$etiquette<\/F>/go;
		if($motsxpipe=~m/^\{[^\}]+\} *$/){ #si bug du module np on remet token initial comme représantant de la forme
			$motsxpipe =~ m/^ *\{[^>]+>([^◀<]+)[◀<]/;
			my $nextok = $1;
			$motsxpipe .= " $1 ";
		}
	}elsif($motsxpipe=~ m/^.*<\/F>.*<\/F>.*$/ and $mot=~m/^_(.+)$/){
		#print "hihi\n";
		$mot=~m/^_(.+)$/;
		my $etiquette = $1;
		$etiquette =~ s/ \[\|/\[\|/;
		$original =$motsxpipe;
		$original =~ s/◀[^<]+<\/F>/<\/F>/g; #on supp les label
		if ($mot=~m/^_(URL|EMAIL|SMILEY)/){
			$original =~ s/<\/F> <F id="?E[0-9]+F[0-9]+"?>//g; #on supp espace si  url
		}else{
			$original =~ s/<\/F> <F id="?E[0-9]+F[0-9]+"?>/_/g; #on insère espace si pas url
		}
		$original =~ s/<\/F>//go;
		$original =~ s/<F id="?E[0-9]+F[0-9]+"?>//g;
		$original =~ s/^ *\{//;
		$original =~ s/\}.*$/ /;
		$motsxpipe =~ s/<\/F>/◀$etiquette<\/F>/;
		$motsxpipe =~ s/$motq/$original/;
	}elsif ($mot=~/^_(.+)/){
		my $etiquette = $1;
		$etiquette =~ s/ \[\|/\[\|/;
		$motsxpipe =~ s/<\/F>/◀$etiquette<\/F>/;
		$motsxpipe =~ s/$motq/$original/;
	}
	$motsxpipe =~ s/ *$/ /;
#	print "\t".$motsxpipe."\n";
	
	return $motsxpipe;	
}

sub lissage_ambig{
	
	my $forme_ambig = shift;
	my $containCorrAna=0;
	$containCorrAna=1 if ($forme_ambig=~m/Corr_ANA_/);
	
	my %allCorr;
	$forme_ambig=~s/([^\[\\]) *\| *([^\]])/$1 \| $2/g;
	my @all_ambig = split(' \| ', $forme_ambig);
	
	#On récupère toutes les analyses possibles
	for $ambig (@all_ambig){
#		print $ambig."\n";
		if ($ambig=~ m/^[^\{]*\{[^\{]+$/){ #Si ambig ne contient qu'une forme:
			my $ambig = supp_etiquette($ambig);
			next if ($ambig eq "");
			$ambig =~ m/^ *\{[^>]+>([^◀<]+)(◀[^<]+)?[^\}]+\} +([^ ]+) *(\[\|[^\|]*\|\])? *$/;
			my $original = $1; #mot_original
			my $label = $2; #type annotation
			my $mot = quotemeta($3); #seconde partie de la forme sxpipe (après les {} )
			$label=~s/^◀//;
			#print " →→→ $original - $mot - $label - $ambig - $containCorrAna\n";
			if ($label=~ m/Corr_SXP/){
				$allCorr{$mot}{$ambig}=1 if (not $containCorrAna);
				$allCorr{$mot}{$ambig}=1 if ($label=~ m/Corr_ANA/);
				#print "1\n";
			}else{				
				$allCorr{$mot}{$ambig}=1;
				#print "2\n";
			}
			#print "1 $mot: $ambig\n";
		} else { #si plusieurs formes:
			my $ambig_tmp = "";
			#print "→ $ambig\n";
			while ($ambig=~ s/^( *[\|\{ ]+[^\(\{]*)//){
				#print "\t→".$1."\n";
				$ambig_tmp.=supp_etiquette($1);
			} 
			$ambig_tmp.=$ambig;
#			print "2 multimot: $ambig_tmp\n";
			$allCorr{"multimot"}{$ambig_tmp}=1;
		}
	}
	
	my @amb_lissee=();
	
	#On trie les analyses trouvées en fonction des étiquettes assignées
	for $analyse (keys (%allCorr)){
#		print "3 $analyse\n";
		$nb_descr_sxp=keys %{$allCorr{$analyse}};
		if($nb_descr_sxp == 1){
			push (@amb_lissee, keys %{$allCorr{$analyse}});
		}else{
			if($analyse eq "multimot"){
				for $descr_sxp (keys %{$allCorr{$analyse}}){
					push (@amb_lissee, $descr_sxp);
				}
#				print "multi: @amb_lissee\n";
			}else{
				my $base_sxp="";
				my $all_descr_sxp="";
#				@aaa = keys %{$allCorr{$analyse}};
#				print "!!!!$analyse --- @aaa\n";
				for $descr_sxp (keys %{$allCorr{$analyse}}){
#					print "--- $descr_sxp \n";
					$descr_sxp =~ m/◀([^<]+)<\/F>/;
#					print "--- → $1 \n";
					my $label = $1;
					$all_descr_sxp.="+".$label;
					my $labelq = quotemeta($label);
					$descr_sxp =~ s/◀$labelq//;
					$base_sxp = $descr_sxp;
#					print "→→→→→→ $all_descr_sxp --- $base_sxp\n";
				}			
#				print "A→→→→→→ $base_sxp ---- $all_descr_sxp\n";
				$all_descr_sxp =~ s/^\+/◀/;
#				print "B→→→→→→ $base_sxp ---- $all_descr_sxp\n";
				$base_sxp =~ s/<\/F>/$all_descr_sxp<\/F>/;
				push (@amb_lissee, $base_sxp);
#				print "non multi: $base_sxp - @amb_lissee\n";

			}
		}
	}
#	print "5 @amb_lissee\n";
	my $nbambig = @amb_lissee;
	return " ( ".join(" | ",@amb_lissee)." ) " if ($nbambig>1);
	return $amb_lissee[0];
}









