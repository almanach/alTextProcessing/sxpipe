#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


while(<>){
 chomp;
 if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
 }
 s/^\s*/ /o;
 s/\s*$/ /o;

 s/([^\\])\(([^<])/$1 \( $2/go;
 s/\(([^< ])/\( $1/go;
 s/([^\\])\)([^<])/$1 \)$2/go;
 s/$/ /go;

 my $tmp = "$_ ";
 $_="";
 chomp $tmp;
 while ($tmp =~ s/^(\s*(?:{[^{}]+})?\s*)([^ ]+)//) { #pour chaque mot sxpipe

 	$content = $1;
	$cat = $2;
#	print "- $content ---- $cat -\n";
	$content_corr = $content;

	if ($prev1) {   # remove space after opening paren
		$content =~ s/^ +//; undef $prev1;
	}

	my @toks = split/>/, $content;

	foreach my $tok (@toks) {
		if ($tok =~ /<\/F$/) {
			if ($cat =~ /(_DATE_[ay][re][ta][ofr]|_ADRESSE)/) {
				$tmp_tok = ne_norm ($tok, $prev_tok);
			} elsif ($cat =~ /_CHEM/) {
				$tmp_tok = chem_norm ($tok);
			} elsif ($cat =~ /_NUM *$/) {
				$tmp_tok = value_norm ($tok);
			} elsif ($cat =~ /_DIMENSION/) {
				$tmp_tok = unit_norm ($tok);
			} else {
				$tmp_tok = $tok;
			}
			unless ($tmp_tok eq $tok) {$corr=1;}
			$tmp_cont .= ("$tmp_tok" . ">");
			$prev_tok = $tok;
		} else {
			$tmp_cont .= ("$tok" . ">");
		}
	}
	$tmp_cont =~ s/>$//;		
	if ($cat =~ /^[^\\]?\)/) {$tmp_cont =~ s/ $//;}  # remove space before closing paren
	$tmp_cont .= $cat;
	if ($cat =~ /^[^\\]?\(/) {$prev1 =1;}
	else {undef $prev1;}
	if ($corr==1) {
		$tmp_cont =~ s/(\{[^\{]+)$/\($content$cat \| $1/;
		$tmp_cont =~ s/$/\)/;
		$tmp_cont =~ s/\( /\(/;
	}
	undef $corr;
}
	

 $tmp_cont =~ s/ +/ /go;
 $tmp_cont =~  s/^ //;
 $tmp_cont =~  s/ $//;
	
 print "$tmp_cont\n";
 undef $tmp_cont;
}



sub exponent {
  my $s = shift;
  $s =~ tr/0123456789-/⁰¹²³⁴⁵⁶⁷⁸⁹⁻/;
  return $s;
}

sub lowindex {
  my $s = shift;
  $s =~ tr/0123456789-/₀₁₂₃₄₅₆₇₈₉⁻/;
  return $s;
}

sub ne_norm {
  my ($s,$prevs) = @_;
  $s =~  s/([0-9])O/${1}0/g;
  $s =~  s/O([0-9])/0$1/g;
#  $s =~  s/^[ÎÏ1iîlï][\'\"] /1er /g; # tokenization pb, split:
  if ($prevs =~ /^[ÎÏ1iîlï]</) {$s =~ s/[\'\"]/er/;}
  $s =~  s/([0-9])[îiïÏlÎ]/${1}1/g;
  $s =~  s/^[ilÎÏïî]</1/g;
  $s =~  s/^ii/11/g;
  $s =~  s/^ll/11/g;
  $s =~  s/[lîiÎÏï]([0-9])/1$1/g;

  $s =~  s/([0-9])G/${1}6/g;
  $s =~  s/^G</6/g;
  $s =~  s/G([0-9])/6$1/g;

  $s =~  s/([0-9])j/${1}5/g;
  $s =~  s/^j</5/g;
  $s =~  s/^j\.</5./g;
  $s =~  s/5([0-9])/5$1/g;

  $s =~  s/([VX])[lîiÎÏï]([^\w])/$1I$2/g;
  $s =~  s/([VXI])[lîiÎÏï]([^\w])/$1I$2/g;

  $s =~  s/([VX]+I{0,2})[^\w]/$1e/g;
  $s =~  s/([1-9][0-9]*)[°'"]/$1e/g;

  $s =~ s/ +/ /g;
  $s =~ s/ $//g;
 
  return "$s";
}


# dimension

sub value_norm {
  my $s = shift;
  $s =~  s/O/0/g;
  $s =~ s/(10) ([1-9][0-9]*) ?/$1.exponent($2)/e;
  $s =~ s/(10) ?- ?([1-9][0-9]*) ?$/$1.exponent("-".$2)/e;
  $s =~ s/ //g;
  return $s;
}

sub unit_norm {
  my $s = shift;
  $s =~ tr/1/l/;
  if ($s!~/[a-zéè]{4}/) {$s =~ s/ //g;}
  $s = exponent($s);
  return $s;
}

# chemical

sub chem_norm {
  my $s = shift;
  $s = lowindex($s);
  $s =~  s/0/O/g;
  $s =~ s/A1/Al/g;
  $s =~  s/NOx/NOₓ/g;
  $s =~ s/OZ$/O₂/;
  $s =~  s/-$/⁻/g;
  $s =~ s/\+$/⁺/g;
  $s =~ s/ +//g;
  return $s;
}




