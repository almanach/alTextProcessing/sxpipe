# sxpipe reloaded

# Language
lang    = si

# Path to different
sxpipe  = @datadir@/sxpipe
bindir = @bindir@

# Verbose flag
verbose

# Component list
components = \
	recode-u8l2 \
	tag_meta \
	email \
        url \
	date \
	tel \
	heure \
	adresse \
	smiley \
	mtponct \
	numprefix \
	number \
	formattage \
	remove_inner_1 \
	segment \
	tag_unknown \
	sigles \
	np \
	etr \
	untag_unknown \
	remove_inner_2 \
	rebuild \
	normalize \
	text2dag \
	epsilonize \
	recode-l2u8

# raw text to tokens
[recode-u8l2]
cmd = iconv
options = -f UTF-8 -t L2
desc = passe de l'UTF-8 au Latin 2

[tag_meta]
cmd = $sxpipe/txt2tok/tag_meta.pl
desc = protection des meta-caract�res

[email]
cmd = $sxpipe/txt2tok/gl_email.pl
options = $*
desc = reconnaissance des email

[url]
cmd = $sxpipe/txt2tok/gl_url.pl
options = $*
desc = reconnaissance des url
depend = email

[date]
cmd = $sxpipe/txt2tok/gl_date.pl
options = $* -l=$lang
desc = reconnaissance des dates

[tel]
cmd = $sxpipe/txt2tok/gl_tel.pl
options = $* -l=$lang
desc = reconnaissance des n� de t�l�phone

[heure]
cmd = $sxpipe/txt2tok/gl_heure.pl
options = $* -l=$lang
desc = reconnaissance des heures

[adresse]
cmd = $sxpipe/txt2tok/gl_adresses.pl
options = $* -l=$lang
desc = reconnaissance des adresses

[numprefix]
cmd = $sxpipe/txt2tok/gl_numtruc.pl
options = $*
desc = reconnaissance des prefixes numeriques (genre 4-place)

[number]
cmd = $sxpipe/txt2tok/gl_number.pl
options = $* -l=$lang
desc = reconnaissance des nombres

[smiley]
cmd = $sxpipe/txt2tok/gl_smiley.pl
desc = reconnaissance des smileys

[mtponct]
cmd = $sxpipe/txt2tok/gl_mtponct.pl
options = $* -l=$lang
desc = reconnaissance des ponctuations multitokens

[qword]
cmd = $sxpipe/txt2tok/gl_qword.pl
desc = reconnaissance des mots cit�s

[formattage]
cmd = $sxpipe/txt2tok/gl_format.pl
desc = reconnaissance des mots formatt�s (pseudo-gras type _mot_ ou *mot*)

[segment]
cmd = $sxpipe/txt2tok/segmenteur.pl
options = -af=@silexdir@/pctabr -p=r $*
desc = segmentation

# tokens to compound components
[tag_unknown]
cmd = $bindir/sxspeller$lang
options = -cl -pl -cq -ch -pp -ps -c -nsc -d -lw 1
desc = marquage des tokens inconnus

[sigles]
cmd = $sxpipe/tok2cc/gl_sigles.pl
desc = reconnaissance des sigles

[np]
cmd = $sxpipe/tok2cc/gl_np.pl
desc = reconnaissance des noms propres

[etr]
cmd = $sxpipe/tok2cc/gl_etr.pl
options = -no_gl -l=$lang
desc = reconnaissance des expressions langue �trang�re

[untag_unknown]
cmd = $sxpipe/tok2cc/untag_unknown.pl
desc = d�marquage des tokens inconnus

[remove_inner_1]
cmd = $sxpipe/tok2cc/remove_inner_ne.pl
desc = desencapsulation des entit�es nomm�es

[remove_inner_2]
cmd = $sxpipe/tok2cc/remove_inner_ne.pl
desc = desencapsulation des entit�es nomm�es

[rebuild]
cmd = $sxpipe/tok2cc/rebuild_easy_tags.pl

[correct]
cmd = sxspeller$lang
options = -cl -pl -cq -ch -pp -ps -c -slw 20 -lw 50 -t -sc -mw 200 -sww 100
desc = correction effective

[normalize]
cmd = $sxpipe/tok2cc/normalize_blanks.pl
desc = normalisation des espaces et de pourcentages

# compound components to DAG
[text2dag]
cmd = $sxpipe/cc2dag/wraptext2dag.pl
options = -cl -pl -cq -ch -pp -ps -slw 20 -lw 40 -sc -mw 100 -sww 100 -mcn 2 -l $lang
desc = analyse des phrases

[epsilonize]
cmd = $sxpipe/epsilonize_metawords.pl
desc = met en alternative avec _EPSILON les m�tamots (quotes, crochets...)

[unfold]
cmd = $sxpipe/cc2dag/wrapunfolddag.pl
desc = d�pliage des DAGs

[recode-l2u8]
cmd = iconv
options = -f L2 -t UTF-8
desc = passe du Latin 2 � l'UTF-8
