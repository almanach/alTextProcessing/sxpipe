#include "sxversion.h"
#include "sxunix.h"

#define SX_DFN_EXT_VAR2
#include "udag_scanner.h"
#include "varstr.h"

#include lex_h

extern SXINT cur_lang_id;
extern SXINT cur_lang_mask;

extern SXINT get_SEMLEX_lahead (void);

static VARSTR wstr;

#if 0
static double
get_semlex (SXINT pq, SXINT t) {
  SXINT *tok_no_stack, *top_tok_no_stack, *t_stack, lgth;
  double tmp_weight, weight = (double)-1;
  SXINT tok_no = idag.pq2tok_no [pq];
  struct sxtoken *semlex_ptok;

  if (tok_no < 0) {
      tok_no_stack = idag.tok_no_stack - tok_no;
      top_tok_no_stack = tok_no_stack + *tok_no_stack;
      tok_no_stack++;
  }
  else {
    /* truc */
    top_tok_no_stack = tok_no_stack = &tok_no;
  }

  while (tok_no_stack <= top_tok_no_stack) {
    tok_no = *tok_no_stack++;
    t_stack = idag.local_t_stack + idag.tok_no2t_stack_hd [tok_no];
    semlex_ptok = &(SXGET_TOKEN (tok_no+1));

    if (semlex_ptok->lahead == get_SEMLEX_lahead ()) {
      tmp_weight = atof (sxstrget (semlex_ptok->string_table_entry));
    } else {
      tmp_weight = DEFAULT_LOCAL_WEIGHT;
    }

    lgth = t_stack [0];

    while (lgth-- > 0) {
      if (*++t_stack == t && tmp_weight > weight) {
	weight = tmp_weight;
	break;
      }
    }    
  }

  return weight;
}
#endif /* 0 */

static SXINT uw_ste, Uw_ste;
static VARSTR vstr2;

SXVOID
uw_init (void) {
  wstr = varstr_alloc (256);
  vstr2 = varstr_alloc (256);

  uw_ste = sxstrsave ("_uw");
  Uw_ste = sxstrsave ("_Uw");
}

SXVOID
uw_final (void) {
  varstr_free (wstr), wstr = NULL;
  varstr_free (vstr2), vstr2 = NULL;
}

static void
uw_extract_trans (SXINT p, void (*output_trans) (SXINT, SXINT, SXINT))
{
  SXINT *tok_no_stack, *top_tok_no_stack, tok_no, lgth, bot_pq, top_pq, q, t, length, *t_stack;
  char *tmp_str;
  struct sxtoken *ptok;

  if (p == idag.final_state)
    return;

  bot_pq = idag.p2pq_hd [p];
  top_pq = idag.p2pq_hd [p+1];

  while (bot_pq < top_pq) {
    q = idag.pq2q [bot_pq];
    t_stack = idag.t_stack + idag.pq2t_stack_hd [bot_pq];
    tok_no = idag.pq2tok_no [bot_pq];
  
    if (tok_no < 0) {
      tok_no_stack = idag.tok_no_stack - tok_no;
      top_tok_no_stack = tok_no_stack + *tok_no_stack;
      tok_no_stack++;
    }
    else {
      /* truc */
      top_tok_no_stack = tok_no_stack = &tok_no;
    }
    
    while (tok_no_stack <= top_tok_no_stack) {
      tok_no = *tok_no_stack++;
      t_stack = idag.local_t_stack + idag.tok_no2t_stack_hd [tok_no];
      
      lgth = t_stack [0];
      
      while (lgth-- > 0) {
	ptok = &(SXGET_TOKEN (tok_no));
	if ((*++t_stack & cur_lang_mask) == 0) { /* mot inconnu dans la langue courante */
	  tmp_str = sxstrget (ptok->string_table_entry);
	  if (p == idag.init_state) {
	    if (*tmp_str == sxtolower (*tmp_str))
	      ptok->lahead = -1;
	    else {
	      ptok->lahead = -3;
	    }
	  } else {
	    if (*tmp_str == sxtolower (*tmp_str))
	      ptok->lahead = -1;
	    else
	      ptok->lahead = -2;
	  }
	}
	(*output_trans) (p, tok_no, q);
      }    
    }
      
    bot_pq++;
  }
}

static char*
uw_trans_name (SXINT tok_no)
{
  struct sxtoken *ptok;

  varstr_raz (vstr2);

  ptok = &(SXGET_TOKEN (tok_no));

  if (ptok->lahead >= 0) {
    if (ptok->comment) {
      vstr2 = varstr_catenate (vstr2, ptok->comment);
      vstr2 = varstr_catchar (vstr2, ' ');
    }
    vstr2 = varstr_catenate (vstr2, sxstrget (ptok->string_table_entry));
  } else {
    if (ptok->lahead == -3) { /* _uw | _Uw */
      vstr2 = varstr_catchar (vstr2, '(');
      if (ptok->comment) {
	vstr2 = varstr_catenate (vstr2, ptok->comment);
	vstr2 = varstr_catchar (vstr2, ' ');
      }
      vstr2 = varstr_catenate (vstr2, "_Uw | ");

      if (ptok->comment) {
	vstr2 = varstr_catenate (vstr2, ptok->comment);
	vstr2 = varstr_catchar (vstr2, ' ');
      }
      vstr2 = varstr_catenate (vstr2, "_uw)");
    } else {
      if (ptok->comment) {
	vstr2 = varstr_catenate (vstr2, ptok->comment);
	vstr2 = varstr_catchar (vstr2, ' ');
      }
      if (ptok->lahead == -1)
	vstr2 = varstr_catenate (vstr2, "_uw");
      else
	vstr2 = varstr_catenate (vstr2, "_Uw");
    }
  }

  return varstr_tostr (vstr2);
}

static SXBOOLEAN
uw_tag_it (SXINT what)
{
  if (what == SXACTION) {
    wstr = dag2re (varstr_raz (wstr), idag.init_state, idag.final_state,
		   idag.eof_code, uw_extract_trans, uw_trans_name);  
    printf ("%s\n", varstr_tostr (wstr));
  }

  return SXTRUE;
}


SXBOOLEAN        (*main_parser)(SXINT) = uw_tag_it;
