#define sxgetchar sxgetchar
#include "sxversion.h"
#include "sxunix.h"

#include <math.h>

#define SX_DFN_EXT_VAR2
#include "udag_scanner.h"
#include "varstr.h"

extern VARSTR              cur_input_vstr;

extern SXINT is_train;

static VARSTR wvstr;


#define DEFAULT_LOCAL_WEIGHT (double)1

SXSHORT
sxgetchar (void)
{
  SXSHORT cur_char = ((sxsrcmngr.infile != NULL)
		      ? getc (sxsrcmngr.infile)
		      : (sxsrcmngr.instring [++sxsrcmngr.strindex] == SXNUL
			 ? (--sxsrcmngr.strindex, EOF)
			 : sxsrcmngr.instring [sxsrcmngr.strindex]));

  if (cur_char != EOF && cur_input_vstr)
    cur_input_vstr = varstr_catchar (cur_input_vstr, (char) cur_char);

  return cur_char;
}

#ifdef lex_h
#include lex_h
#else /* lex_h */
#include "XxY.h"
#include "fsa.h"
#define EOF_CODE 0
#endif /* lex_h */
static double *t2occ;
static char **t2string;

#if MAXNGRAM == 1
static SXINT *p2best_pq, *p2best_q, *p2best_t, *p2best_length;
#else /* MAXNGRAM == 1 */
#if MAXNGRAM == 2
static SXINT occ_id_max = EOF_CODE;
static XxY_header prev_t__t2occ_id_hd;
static double *pqt2best_weight, *pqt2local_weight;
static SXINT *pqt2best_prev_pqt, *pqt2best_length;
#else
#error SxTagger handles only 1-gram and 2-gram data. Please use another data file.
#endif /* MAXNGRAM == 2 */
#endif /* MAXNGRAM == 1 */

#ifndef weights_h
#if MAXNGRAM == 2
static double *occ_id2occ;
#endif /* MAXNGRAM == 2 */
#endif /* !weights_h */

#ifdef weights_h
#include weights_h
#else
static double weight [1] = {1.0};
static double weight_max = 1.0;
static struct XxY_elem prev_t__t2occ_id_hd_display [1] = {
/* 0 */ {0, 0},
};
static SXINT prev_t__t2occ_id_hd_hash_lnk [1] = {
/* 0 */ 0,
};
static SXINT prev_t__t2occ_id_hd_hash_display [1] = {
/* 0 */ 0,
};
static XxY_header prev_t__t2occ_id_hd =
{"prev_t__t2occ_id_hd", /* name */
prev_t__t2occ_id_hd_hash_display, /* hash_display */
prev_t__t2occ_id_hd_hash_lnk, /* hash_lnk */
1, /* hash_size */
1, /* top */
1, /* size */
X_root_80, /* free_buckets */
0, /* has_free_buckets */
NULL, /* (*system_oflw) () */
NULL, /* (*user_oflw) () */
NULL, /* (*cmp) () */
NULL, /* (*scrmbl) () */
NULL, /* (*suppress) () */
NULL, /* (*assign) () */
NULL, /* stat_file */
1, /* is_locked */
SXTRUE, /* is_static */
SXTRUE, /* is_allocated */
prev_t__t2occ_id_hd_display, /* display */
{NULL, NULL}, /* X_hd [2] */
{NULL, NULL}, /* lnk_hd [2] */
{NULL, NULL}, /* lnk [2] */
{SXTRUE, SXTRUE} /* X_hd_is_static [2] */
}; /* End XxY_header prev_t__t2occ_id_hd */;
#endif /* weights_h */

static double min_prob;

static SXBA q_already_visited;

extern SXINT get_SEMLEX_lahead (void);

static double
get_semlex (SXINT pq, SXINT t) {
  SXINT *tok_no_stack, *top_tok_no_stack, *t_stack, lgth;
  double tmp_weight, weight = (double)-1;
  SXINT tok_no = idag.pq2tok_no [pq];
  struct sxtoken *semlex_ptok;

  if (tok_no < 0) {
      tok_no_stack = idag.tok_no_stack - tok_no;
      top_tok_no_stack = tok_no_stack + *tok_no_stack;
      tok_no_stack++;
  }
  else {
    /* truc */
    top_tok_no_stack = tok_no_stack = &tok_no;
  }

  while (tok_no_stack <= top_tok_no_stack) {
    tok_no = *tok_no_stack++;
    t_stack = idag.local_t_stack + idag.tok_no2t_stack_hd [tok_no];
    semlex_ptok = &(SXGET_TOKEN (tok_no+1));

    if (semlex_ptok->lahead == get_SEMLEX_lahead ()) {
      tmp_weight = atof (sxstrget (semlex_ptok->string_table_entry));
    } else {
      tmp_weight = DEFAULT_LOCAL_WEIGHT;
    }

    lgth = t_stack [0];

    while (lgth-- > 0) {
      if (*++t_stack == t && tmp_weight > weight) {
	weight = tmp_weight;
	break;
      }
    }    
  }

  return weight;
}

static void
print_pqt (SXINT pqt)
{
  SXINT t, p, q;

  t = pqt2t (pqt);
  p = pqt2p (pqt);
  q = pqt2q (pqt);

  printf ("%ld--%s-->%ld", p, t2string [t], q);
}

#if MAXNGRAM == 1
static double *p2best_weight;

static void
compute_best_path_to_final_state (SXINT p)
{
  double tmp_weight;
  SXINT bot_pq, top_pq, q, t, lgth, *t_stack;

  if (p == idag.final_state) {
    p2best_weight [p] = 0;
    p2best_pq [p] = 0;
    p2best_q [p] = 0;
    p2best_t [p] = 0;
    p2best_length [p] = 0;

    return;
  }

  bot_pq = idag.p2pq_hd [p];
  top_pq = idag.p2pq_hd [p+1];

  p2best_weight [p] = -1;

  while (bot_pq < top_pq) {
    q = range2q (bot_pq);
    t_stack = idag.t_stack + idag.pq2t_stack_hd [bot_pq];

    lgth = t_stack [0];

    while (lgth-- > 0) {
      t = *++t_stack;

      compute_best_path_to_final_state (q);
      tmp_weight = weight [t] + p2best_weight [q];
      if (p2best_length [q])
	tmp_weight *= 1 / (1 + 1 / p2best_length [q]);

      if (tmp_weight > p2best_weight [p]) {
	p2best_weight [p] = tmp_weight;
	p2best_pq [p] = bot_pq;
	p2best_q [p] = q;
	p2best_t [p] = t;
	p2best_length [p] = p2best_length [q] + 1;
      }
    }

    bot_pq++;
  }
}

static void
output_best_path (SXINT p)
{
  if (p == idag.final_state)
    return;

  SXINT lgth, t, tok_no;
  SXINT *tok_no_stack, *top_tok_no_stack, *t_stack;
  struct sxtoken *ptok;
  char *str, *comment;

  tok_no = idag.pq2tok_no [p2best_pq [p]];

  if (tok_no < 0) {
      tok_no_stack = idag.tok_no_stack - tok_no;
      top_tok_no_stack = tok_no_stack + *tok_no_stack;
      tok_no_stack++;
  }
  else {
    /* truc */
    top_tok_no_stack = tok_no_stack = &tok_no;
  }

  t = p2best_t [p];

  while (tok_no_stack <= top_tok_no_stack) {
    tok_no = *tok_no_stack++;
    t_stack = idag.local_t_stack + idag.tok_no2t_stack_hd [tok_no];

    lgth = t_stack [0];

    while (lgth-- > 0) {
      if (*++t_stack == t) {
	tok_no_stack = top_tok_no_stack + 1;
	break;
      }
    }    
  }

  ptok = &(SXGET_TOKEN (tok_no));
  str = sxstrget (ptok->string_table_entry);
  varstr_raz (wvstr);
  wvstr = varstr_lcatenate_with_escape (wvstr, str, strlen (str), "%()[]{|\\");
  str = varstr_tostr (wvstr);
  comment = ptok->comment;

  printf ("%ld\t%s %s/%s(%ld)\t%ld\n", p, comment ? comment : "", str, t2string [t], t, p2best_q [p]);
  
  output_best_path (p2best_q [p]);
}
#endif /* MAXNGRAM == 1 */

#if MAXNGRAM == 2
static void
compute_best_path_to (SXINT q)
{
  double tmp_weight;
#ifndef ONT_USE_SEMLEX_AS_WEIGHT
  double semlex;
#endif /* ONT_USE_SEMLEX_AS_WEIGHT */
  SXINT bot_pq, top_pq, p, t, pq, lgth, *t_stack, *prev_pq_stack, *top_prev_pq_stack;
  SXINT pqt, prev_t__t, size;
  SXINT prev_t, prev_pq, length, *prev_t_stack, prev_lgth, prev_pqt;

  size = SXBASIZE (q_already_visited);
  while (q >= size) {
    size *= 2;
    q_already_visited = sxba_resize (q_already_visited, size);
  }

  if (!SXBA_bit_is_reset_set (q_already_visited, q))
    return;

  if (q != idag.init_state) {
    prev_pq_stack = idag.q2pq_stack + idag.q2pq_stack_hd [q];
    top_prev_pq_stack = prev_pq_stack + *prev_pq_stack;
    
    while (++prev_pq_stack <= top_prev_pq_stack) {
      prev_pq = *prev_pq_stack;
      p = range2p (prev_pq);
      compute_best_path_to (p);
    }

    /* pile des transitions qui arrivent en q */
    prev_pq_stack = idag.q2pq_stack + idag.q2pq_stack_hd [q];
    top_prev_pq_stack = prev_pq_stack + *prev_pq_stack;
  }

  if (q != idag.final_state) {
    /* étude de celles qui en repartent */
    for (pq = idag.p2pq_hd [q]; pq < idag.p2pq_hd [q+1]; pq++) {
#if EBUG
      if (pq > idag.last_pq) {
	fprintf (sxstderr, "q = %ld, pq = %ld, idag.last_pq = %ld\n", q, pq, idag.last_pq);
	sxtrap ("viterbi_tagger.c", "compute_best_path_to (found pq greater than idag.last_pq)");
      }
#endif EBUG
      /* on étudie une par une les transitions sortantes, pour leur donner leur poids (optimal) */
      t_stack = idag.t_stack + idag.pq2t_stack_hd [pq];
    
      lgth = t_stack [0];
    
      while (lgth-- > 0) {
	t = *++t_stack;
	pqt = pq_t2pqt (pq, t);

	pqt2best_weight [pqt] = -1;
	if (pqt2local_weight [pqt] == 0) {
#ifdef ONT_USE_SEMLEX_AS_WEIGHT
	  pqt2local_weight [pqt] = 1;
#else /* ONT_USE_SEMLEX_AS_WEIGHT */
	  semlex = get_semlex (pq, t);
	  pqt2local_weight [pqt] = 1 / (1 + pow (10,semlex));
#endif /* ONT_USE_SEMLEX_AS_WEIGHT */
#if EBUG
	  print_pqt (pqt), printf (" : %e\n", pqt2local_weight [pqt]);
#endif /* EBUG */
	}

	if (q == idag.init_state) {
	  if (prev_t__t = XxY_is_set (&prev_t__t2occ_id_hd, 0, t))
	    tmp_weight = pqt2local_weight [pqt] * occ_id2occ [prev_t__t]/weight [0];
	  else if (weight [t] > 0)
	    tmp_weight = pqt2local_weight [pqt] * (log10(1+weight [t])/10)/weight [0];
	  else 
	    tmp_weight = pqt2local_weight [pqt] * min_prob;
	
	  pqt2best_weight [pqt] = tmp_weight;
	  pqt2best_prev_pqt [pqt] = 0;
	  pqt2best_length [pqt] = 1;
	}
	else {
	  prev_pq_stack = idag.q2pq_stack + idag.q2pq_stack_hd [q];
	  top_prev_pq_stack = prev_pq_stack + *prev_pq_stack;

	  while (++prev_pq_stack <= top_prev_pq_stack) {
	    /* on essaye toutes les séquences prev_pq,prev_t - pq,t, i.e. prev_pqt - pqt */
	    prev_pq = *prev_pq_stack;
	    prev_t_stack = idag.t_stack + idag.pq2t_stack_hd [prev_pq];
    
	    prev_lgth = prev_t_stack [0];

	    while (prev_lgth-- > 0) {
	      prev_t = *++prev_t_stack;
	      prev_pqt = pq_t2pqt (prev_pq, prev_t);
      
	      if (prev_t__t = XxY_is_set (&prev_t__t2occ_id_hd, prev_t, t)) {
#if EBUG
		if (weight [prev_t] == 0) {
		  fprintf (sxstderr, "Problematic sequence: %s %s\n", t2string[prev_t], t2string [t]);
		  sxtrap ("compute_best_path_to", "found t1, t2 s.t. weight [t1] == 0 but sequence t1 t2 is known");
		}
#endif /* EBUG */
		tmp_weight = pqt2best_weight [prev_pqt] * pqt2local_weight [pqt] * occ_id2occ [prev_t__t]/weight [prev_t];
	      }
	      else {
		if (weight [t] > 0) {
		  if (weight [prev_t] > 0)
		    tmp_weight = pqt2local_weight [pqt] * pqt2best_weight [prev_pqt] * (log10(1+weight [t])/10)/weight [prev_t];
		  else
		    tmp_weight = pqt2local_weight [pqt] * pqt2best_weight [prev_pqt] * (log10(1+weight [t])/10);
		}
		else
		  tmp_weight = pqt2local_weight [pqt] * pqt2best_weight [prev_pqt] * min_prob;
	      }

	      length = pqt2best_length [prev_pqt] + 1;

	      tmp_weight *= 1 / (1 + 1 / length);

#if EBUG
	      if (!is_train) {
		print_pqt (prev_pqt), printf (", "), print_pqt (pqt);
		printf (" : %e\n", tmp_weight);
	      }
#endif /* EBUG */

	      if (tmp_weight >= pqt2best_weight [pqt]) {
		pqt2best_weight [pqt] = tmp_weight;
		pqt2best_prev_pqt [pqt] = prev_pqt;
		pqt2best_length [pqt] = length;
	      }
	    }
	  }
	}
      }
    }
  }
  else {
    while (++prev_pq_stack <= top_prev_pq_stack) {
      /* on est sur l'état final */
      prev_pq = *prev_pq_stack;
      prev_t_stack = idag.t_stack + idag.pq2t_stack_hd [prev_pq];
    
      prev_lgth = prev_t_stack [0];

      while (prev_lgth-- > 0) {
	prev_t = *++prev_t_stack;
	prev_pqt = pq_t2pqt (prev_pq, prev_t);
      
	tmp_weight = pqt2best_weight [prev_pqt];

	if (tmp_weight >= pqt2best_weight [0]) {
	  pqt2best_weight [0] = tmp_weight;
	  pqt2best_prev_pqt [0] = prev_pqt;
	  pqt2best_length [0] = length;
	}
      }
    }
  }
}

static void
output_best_path (SXINT q)
{
  SXINT lgth, t, p, pq, tok_no, pqt, prev_pq;
  SXINT *tok_no_stack, *top_tok_no_stack, *t_stack;
  struct sxtoken *ptok;
  char *str, *comment;
  static SXINT last_pqt;

  if (q == idag.init_state)
    return;
  else if (q == idag.final_state)
    last_pqt = 0;

  last_pqt = pqt = pqt2best_prev_pqt [last_pqt];
  pq = pqt2pq (pqt);
  t = pqt2t (pqt);

  p = range2p (pq);
  
  output_best_path (p);

  tok_no = idag.pq2tok_no [pq];

  if (tok_no < 0) {
      tok_no_stack = idag.tok_no_stack - tok_no;
      top_tok_no_stack = tok_no_stack + *tok_no_stack;
      tok_no_stack++;
  }
  else {
    /* truc */
    top_tok_no_stack = tok_no_stack = &tok_no;
  }

  while (tok_no_stack <= top_tok_no_stack) {
    tok_no = *tok_no_stack++;
    t_stack = idag.local_t_stack + idag.tok_no2t_stack_hd [tok_no];

    lgth = t_stack [0];

    while (lgth-- > 0) {
      if (*++t_stack == t) {
	tok_no_stack = top_tok_no_stack + 1; /* et donc la boucle while (tok_no_stack <=
						top_tok_no_stack) s'arrêtera après le break
						ci-dessous */
	break;
      }
    }    
  }

  ptok = &(SXGET_TOKEN (tok_no));
  str = sxstrget (ptok->string_table_entry);
  varstr_raz (wvstr);
  wvstr = varstr_lcatenate_with_escape (wvstr, str, strlen (str), "%()[]{|\\+*?");
  str = varstr_tostr (wvstr);
  comment = ptok->comment;

  //  printf ("%ld\t%s %s\t%ld\n", p, comment ? comment : "", str, q);
  printf ("%s %s ", comment ? comment : "", str);
  if (tok_no < idag.toks_buf_top) {
    ptok = &(SXGET_TOKEN (tok_no+1));

    if (ptok->lahead == get_SEMLEX_lahead ()) {
      /* Oui */ 
      printf ("[|%s|] ", sxstrget (ptok->string_table_entry));
    }
  }
}

#if 0
SXINT max_pqt;

static void
pqXt2pqt_oflw (SXINT old_size, SXINT new_size)
{
#if EBUG
  fprintf (sxstderr, "pqXt2pqt_hd has been resized (%ld -> %ld)\n", old_size, new_size);
#endif /* EBUG */
  
  if (new_size + 1 > max_pqt) {
    max_pqt = new_size + 1;
    pqt2best_prev_pqt = (SXINT *) sxrealloc (pqt2best_prev_pqt, max_pqt, sizeof (SXINT));
    pqt2best_weight = (double *) sxrealloc (pqt2best_weight, max_pqt, sizeof (double));
    pqt2local_weight = (double *) sxrecalloc (pqt2local_weight, max_pqt, sizeof (double));
    pqt2best_length = (SXINT *) sxrealloc (pqt2best_length, max_pqt, sizeof (SXINT));
  }
}
#endif /* 0 */
#endif /* MAXNGRAM == 2 */

static SXBOOLEAN
viterbi_tag_it (void)
{
#if MAXNGRAM == 1
  p2best_weight = (double *) sxalloc (idag.final_state + 1, sizeof (double));
  p2best_q = (SXINT *) sxalloc (idag.final_state + 1, sizeof (SXINT));
  p2best_pq = (SXINT *) sxalloc (idag.final_state + 1, sizeof (SXINT));
  p2best_t = (SXINT *) sxalloc (idag.final_state + 1, sizeof (SXINT));
  p2best_length = (SXINT *) sxalloc (idag.final_state + 1, sizeof (SXINT));
#endif /* MAXNGRAM == 1 */
#if MAXNGRAM == 2
  SXINT last_pqt;

  fill_idag_pqt2pq();
  last_pqt = MAX_pqt();

  pqt2best_weight = (double *) sxcalloc (last_pqt + 1, sizeof (double));
  pqt2local_weight = (double *) sxcalloc (last_pqt + 1, sizeof (double));
  pqt2best_prev_pqt = (SXINT *) sxalloc (last_pqt + 1, sizeof (SXINT));
  pqt2best_length = (SXINT *) sxalloc (last_pqt + 1, sizeof (SXINT));

#endif /* MAXNGRAM == 2 */

  t2string = idag_get_t2string();

#if MAXNGRAM == 1
  compute_best_path_to_final_state (idag.init_state);
  output_best_path (idag.init_state);
#endif /* MAXNGRAM == 1 */
#if MAXNGRAM == 2
  //fill_idag_infos();
  fill_idag_q2pq_stack ();
  compute_best_path_to (idag.final_state);
  output_best_path (idag.final_state);
  printf ("\n");
#endif /* MAXNGRAM == 2 */

  sxba_empty (q_already_visited);

#if MAXNGRAM == 1
  sxfree (p2best_weight);
  sxfree (p2best_pq);
  sxfree (p2best_q);
  sxfree (p2best_t);
  sxfree (p2best_length);
#endif /* MAXNGRAM == 1 */
#if MAXNGRAM == 2
  sxfree (pqt2best_prev_pqt);
  sxfree (pqt2best_weight);
  sxfree (pqt2local_weight);
  sxfree (pqt2best_length);
#endif /* MAXNGRAM == 2 */

  return SXTRUE;
}


static SXBA p_already_visited;
static SXBA prev_pqt_already_visited;

static void
walk_paths (SXINT prev_pqt)
{
  double tmp_weight;
  SXINT bot_pq, top_pq, prev_pq, p, q, pqt, prev_t, t, lgth, *t_stack, size, occ_id;

  if (prev_pqt == 0) {
    prev_pq = 0;
    prev_t = 0;
    p = idag.init_state;
  }
  else {
    prev_pq = pqt2pq (prev_pqt);
    prev_t = pqt2t (prev_pqt);
    p = range2q (prev_pq);
  }

  if (p == idag.final_state)
    return;

  size = SXBASIZE (p_already_visited);
  if (p >= size)
    p_already_visited = sxba_resize (p_already_visited, 2*size);

  size = SXBASIZE (prev_pqt_already_visited);
  if (prev_pqt >= size)
    prev_pqt_already_visited = sxba_resize (prev_pqt_already_visited, 2*size);
  else {
    if (SXBA_bit_is_set (prev_pqt_already_visited, prev_pqt))
      return;
  }

  bot_pq = idag.p2pq_hd [p];
  top_pq = idag.p2pq_hd [p+1];

  while (bot_pq < top_pq) {
    t_stack = idag.t_stack + idag.pq2t_stack_hd [bot_pq];
    lgth = t_stack [0];

    while (lgth-- > 0) {
      t = *++t_stack;

      if (t == 0) {
	//	fprintf (sxstderr, "Unknown word at position %ld\n", p);
	t = EOF_CODE;
      }

      if (!SXBA_bit_is_set (p_already_visited, p))
	t2occ [t]++;
    }

    bot_pq++;
  }

  SXBA_1_bit (p_already_visited, p);

  bot_pq = idag.p2pq_hd [p];
  top_pq = idag.p2pq_hd [p+1];

  while (bot_pq < top_pq) {
    t_stack = idag.t_stack + idag.pq2t_stack_hd [bot_pq];

    lgth = t_stack [0];

    while (lgth-- > 0) {
      t = *++t_stack;
      pqt = pq_t2pqt (bot_pq, t);

      XxY_set (&prev_t__t2occ_id_hd, prev_t, t, &occ_id);
      occ_id2occ [occ_id]++;
      if (occ_id_max < occ_id)
	occ_id_max = occ_id;

      SXBA_1_bit (prev_pqt_already_visited, prev_pqt);
      walk_paths (pqt);
      
    }

    bot_pq++;
  }
}

static void
output_dtable (double *table, char *name, SXINT max, XxY_header *XxY_hd_ptr)
{
  SXINT t, x, y;

  printf ("static double %s [%ld] = {\n", name, max);

  for (t = 0; t < max; t++) {
    if (XxY_hd_ptr == NULL)
      printf ("/* %ld %s */ %e,\n", t, t < EOF_CODE ? t2string [t] : "(uw)", table [t]);
    else {
      x = XxY_X (*XxY_hd_ptr, t);
      y = XxY_Y (*XxY_hd_ptr, t);
      printf ("/* %ld %s %s */ %e,\n", t, 
	      x == EOF_CODE ? "(uw)" : (x > 0 ? t2string [x] : ""), 
	      y == EOF_CODE ? "(uw)" : (y > 0 ? t2string [y] : ""),
	      table [t]);
    }
  }

  printf ("};\n");
}

static SXBOOLEAN
viterbi_train_it (void)
{
  t2string = idag_get_t2string();
  fill_idag_pqt2pq();

  t2occ [0] ++;
  walk_paths (0);

  sxba_empty (p_already_visited);
  sxba_empty (prev_pqt_already_visited);

  return SXTRUE;
}

SXBOOLEAN
viterbi_train_or_tag_it (SXINT what)
{
  if (what == SXACTION) {
    cur_input_vstr = varstr_alloc (128);

    wvstr = varstr_alloc (128);

    if (is_train)
      viterbi_train_it ();
    else {
      viterbi_tag_it ();
    }

    varstr_free (wvstr), wvstr = NULL;
  }

  return SXTRUE;
}

#ifndef weights_h
static void
prev_t__t2occ_id_oflw (SXINT old_size, SXINT new_size)
{
#if EBUG
  fprintf (sxstderr, "prev_t__t2occ_id_hd has been resized (%ld -> %ld)\n", old_size, new_size);
#endif /* EBUG */
  
  occ_id2occ = (double *) sxrealloc (occ_id2occ, new_size + 1, sizeof (double));
}
#endif /* !weights_h */

void
init (void)
{
  if (is_train) {
    t2occ = (double *) sxcalloc (EOF_CODE + 1 /* tmax+1  +  1, pour les unknown words*/, sizeof (double));
    p_already_visited = sxba_calloc (128);
    prev_pqt_already_visited = sxba_calloc (128);
    
#if MAXNGRAM == 2
#ifndef weights_h
    XxY_alloc (&prev_t__t2occ_id_hd, "prev_t__t2occ_id_hd", occ_id_max, 1, 0 /* pas de foreach */, 0, prev_t__t2occ_id_oflw, NULL);
    occ_id2occ = (double *) sxalloc (XxY_size (prev_t__t2occ_id_hd) + 1, sizeof (double));
#endif /* !weights_h */
#endif /* MAXNGRAM == 2 */
    
  }
  else {
    //#ifdef weights_h
    min_prob = (0.1 / weight [0]) / weight [0];
    q_already_visited = sxba_calloc (128);
    //#endif /* weights_h */
    XxY_reuse (&prev_t__t2occ_id_hd, "prev_t__t2occ_id_hd", NULL, NULL); /* size == top */
  }
}

static SXINT
count_char_in_string (char* str, char c)
{
  SXINT ret_val = 0;

  while (str = strchr (str, c)) {
    str++;
    if (*str == '_')
      str++; /* on ne compte pas les séquences __ */
    else
      ret_val ++;
  }

  return ret_val;
}

void
finalize (void)
{
  if (is_train) {
    SXINT t;
    double weight_max;

    for (t = 0; t <= EOF_CODE; t++) {
      if (t < EOF_CODE)
	t2occ [t] += 0.1 * count_char_in_string (t2string [t], '_');
      if (weight_max < t2occ [t]) 
	weight_max = t2occ [t];
    }
    output_dtable (t2occ, "weight", EOF_CODE + 1, NULL);
    printf ("static double weight_max = %e;\n", weight_max);
#if MAXNGRAM == 2
    XxY_lock (&prev_t__t2occ_id_hd); /* size == top */
    XxY_to_c (&prev_t__t2occ_id_hd, stdout, "prev_t__t2occ_id_hd", SXTRUE /* static */);    
    output_dtable (occ_id2occ, "occ_id2occ", occ_id_max, &prev_t__t2occ_id_hd);
#endif /* MAXNGRAM == 2 */
    
    sxfree (t2occ);
    sxfree (p_already_visited);
  }

  varstr_free (cur_input_vstr), cur_input_vstr = NULL;
}

SXBOOLEAN        (*main_parser)(SXINT) = viterbi_train_or_tag_it;

