#!/usr/bin/env perl
# -*- coding: latin-1 -*-

use strict;

my $usage = <<END;
RAW-FORMAT TEXT -> PROCESSABLE TEXT, using '&x<ddd>;' entities for non transcodable chars; for some non-latin writing systems, preliminary translitteration is performed (this is the case for language using the Arabic script and some Indian languages); for some other non-latin writing systems (e.g., Russian cyrillic), preliminary translitteration is possible, using option '-tr'

Standard use: cat text_file | yarecode -l \$lang

Other use: in a UTF-8 text, replacing &x<d+>; sequences by the corresponding UTF-8 char: cat text_file | yarecode -u -n8b
(see explanations below for this choice of options)

Add option -u if input is known to be UTF-8 (default = yarecode will try to guess whether the input is in UTF-8 or in the default 8-bit encoding associated with language \$lang, except for languages 'ar', 'fa' and 'ckb', where UTF-8 is assumed)
Add option -l1 (-l2, -kr) if input is known to be in latin1 (latin2, koi-8r)
Add option -n8b if you want UTF-8 output, instead of the default 8-bit encoding associated with language \$lang
If UTF-8 is not explicitely required for the input and/or the output or guessed for the input, but no 8-bit encoding can be found (e.g., only the language is provided, but it is not associated with an 8-bit Alexina lexicon), then UTF-8 is used by default
END

$|=1;

my $latin_type = 0;
my $lang = "";
my $unicode_input = -1;
my $unicode_output = 0;
my $guess_encoding = 1;
my $translitterate = -1;

my %lang2encoding = (
		  "fr" => "iso-8859-1",
		 );

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-l=(.*)$/) {$lang = $1} elsif (/^-l$/) {$lang = shift}
    elsif (/^-l1$/) {$unicode_input = 0 unless $unicode_input == 1; $guess_encoding = 0;$latin_type = 1}
    elsif (/^-l2$/) {$unicode_input = 0 unless $unicode_input == 1; $guess_encoding = 0; $latin_type = 2}
    elsif (/^-no_8bit$/ || /^-n8b?$/ || /^-nr$/) {$unicode_output = 1}
    elsif (/^-kr$/) {$unicode_input = 0 unless $unicode_input == 1; $guess_encoding = 0; $latin_type = -8}
    elsif (/^-u$/) {$unicode_input = 1; $guess_encoding = 0;}
    elsif (/^-tr$/) {$translitterate = 1}
    elsif (/^-g$/) {$guess_encoding = 1}
    elsif (/^-h$/) {print STDERR $usage; exit (0)}
}

if ($lang eq "fa" || $lang eq "ckb") {
  $translitterate = 1;
  $guess_encoding = 0;
  unless ($unicode_input == 0) {
    $unicode_input = 1;
  }
} elsif ($translitterate == -1) {
  $translitterate = 0;
}

if ($lang ne "" && $latin_type == 0 && $unicode_output == 0) {
#  if ($latin_type != -1) {print STDERR "### WARNING (yarecode): you mentioned both a latin-n encoding type and a language ($lang); output will be in the language's latin-n encoding, ignoring -t option\n"}
  if ($latin_type = `cat @alexinadir@/$lang/encoding 2>/dev/null`) {
    chomp($latin_type);
  } elsif (defined($lang2encoding{$lang})) {
    $latin_type = $lang2encoding{$lang};
  } else {
    $unicode_input = 1;
    $unicode_output = 1; # i.e., the output will remain UTF-8, but entities (e.g., &#1508;&#1495;&#1493;&#1514;) will be processed and correctly replaced by UTF-8 chars
  }
}

if ($latin_type =~ s/^(?:(?:iso-?)?(?:l(?:atin)?|8859-?))?([12])$/\1/i) {
  $latin_type = $1;
} elsif ($latin_type =~ /^koi8-r$/i) {
  $latin_type = -8;
} elsif ($unicode_input < 1 || $unicode_output < 1) {
  print STDERR "Unhandled or unknown 8-bit encoding $latin_type ($latin_type) for the input and/or output. Defaulting to UTF-8\n";
  $unicode_input = 1;
  $unicode_output = 1;
}

binmode STDIN , ":encoding(iso-8859-1)"; 
binmode STDOUT , ":encoding(iso-8859-1)"; 

my %tmap;
if ($lang eq "ru") {
  %tmap =  (
		    "&#208;&#176;" => "a",
		    "&#208;&#177;" => "b",
		    "&#208;&#178;" => "v",
		    "&#208;&#179;" => "g",
		    "&#208;&#180;" => "d",
		    "&#208;&#181;" => "e",
		    "&#209;&#145;" => "�",
		    "&#208;&#182;" => "�",
		    "&#208;&#183;" => "z",
		    "&#208;&#184;" => "i",
		    "&#208;&#185;" => "j",
		    "&#208;&#186;" => "k",
		    "&#208;&#187;" => "l",
		    "&#208;&#188;" => "m",
		    "&#208;&#189;" => "n",
		    "&#208;&#190;" => "o",
		    "&#208;&#191;" => "p",
		    "&#209;&#128;" => "r",
		    "&#209;&#129;" => "s",
		    "&#209;&#130;" => "t",
		    "&#209;&#131;" => "u",
		    "&#209;&#132;" => "f",
		    "&#209;&#133;" => "x",
		    "&#209;&#134;" => "c",
		    "&#209;&#135;" => "�",
		    "&#209;&#136;" => "�",
		    "&#209;&#137;" => "�",
		    "&#209;&#138;" => "�",
		    "&#209;&#139;" => "y",
		    "&#209;&#140;" => "�",
		    "&#209;&#141;" => "�",
		    "&#209;&#142;" => "�",
		    "&#209;&#143;" => "�",
		    "&#208;&#144;" => "A",
		    "&#208;&#145;" => "B",
		    "&#208;&#146;" => "V",
		    "&#208;&#147;" => "G",
		    "&#208;&#148;" => "D",
		    "&#208;&#149;" => "E",
		    "&#208;&#129;" => "�",
		    "&#208;&#150;" => "�",
		    "&#208;&#151;" => "Z",
		    "&#208;&#152;" => "I",
		    "&#208;&#153;" => "J",
		    "&#208;&#154;" => "K",
		    "&#208;&#155;" => "L",
		    "&#208;&#156;" => "M",
		    "&#208;&#157;" => "N",
		    "&#208;&#158;" => "O",
		    "&#208;&#159;" => "P",
		    "&#208;&#160;" => "R",
		    "&#208;&#161;" => "S",
		    "&#208;&#162;" => "T",
		    "&#208;&#163;" => "U",
		    "&#208;&#164;" => "F",
		    "&#208;&#165;" => "X",
		    "&#208;&#166;" => "C",
		    "&#208;&#167;" => "�",
		    "&#208;&#168;" => "�",
		    "&#208;&#169;" => "�",
		    "&#208;&#170;" => "�",
		    "&#208;&#171;" => "Y",
		    "&#208;&#172;" => "�",
		    "&#208;&#173;" => "�",
		    "&#208;&#174;" => "�",
		    "&#208;&#175;" => "�",
		   );
} elsif ($lang eq "fa" || $lang eq "ckb" || $lang eq "ar") {
  %tmap =  (
		    "-" => "-",
		    "." => ".",
		    "&#216;&#140;" => ",",
		    "&#216;&#155;" => ";",
		    "&#216;&#159;" => "?",
		    "&#216;&#161;" => "�", # &#195;&#160; supprimer tout court
		    "&#216;&#162;" => "�",  ### diff l2 u8
		    "&#216;&#163;" => "�", # mauvais, &#195;&#160; remplacer par &#216;&#167;
		    "&#216;&#164;" => "�", # mauvais, &#195;&#160; remplacer par &#217;&#136;
		    "&#216;&#165;" => "E", # mauvais, &#195;&#160; remplacer par &#216;&#167;
		    "&#216;&#166;" => "'", # mauvais en fin de mot, &#195;&#160; supprimer
		    "&#216;&#167;" => "a",
		    "&#216;&#168;" => "b",
		    "&#216;&#169;" => "T", # mauvais, &#195;&#160; remplacer par &#217;&#135; 
		    "&#216;&#170;" => "t",
		    "&#216;&#171;" => "�",
		    "&#216;&#172;" => "j",
		    "&#216;&#173;" => "�",  ### diff l2 u8
		    "&#216;&#174;" => "x",
		    "&#216;&#175;" => "d",
		    "&#216;&#176;" => "�",
		    "&#216;&#177;" => "r",
		    "&#218;&#149;" => "�", # sorani
		    "&#216;&#178;" => "z",
		    "&#216;&#179;" => "s",
		    "&#216;&#180;" => "�",
		    "&#216;&#181;" => "�",
		    "&#216;&#182;" => "�",
		    "&#216;&#183;" => "�",
		    "&#216;&#184;" => "�",  ### diff l2 u8
		    "&#216;&#185;" => "'",
		    "&#216;&#186;" => "q",
		    "&#217;&#128;" => "=", # &#195;&#160; supprimer
		    "&#217;&#129;" => "f",
		    "&#218;&#164;" => "v", # sorani
		    "&#217;&#130;" => "�",
		    "&#217;&#131;" => "K", # &#195;&#160; remplacer par k
		    "&#217;&#132;" => "l",
		    "&#218;&#181;" => "�", # sorani
		    "&#217;&#133;" => "m",
		    "&#217;&#134;" => "n",
		    "&#217;&#135;" => "e",
		    "&#217;&#136;" => "w",
		    "&#219;&#134;" => "o",
		    "&#217;&#137;" => "�", #y
		    "&#217;&#138;" => "Y", #y
		    "&#217;&#139;" => "�",
		    "&#217;&#148;" => "`", # &#195;&#160; remplacer par un &#219;&#140; en fin de mot
		    "&#217;&#171;" => "�",
		    "&#217;&#177;" => "�", # &#195;&#160; remplacer par &#216;&#167;
		    "&#217;&#190;" => "p",
		    "&#218;&#134;" => "�",
		    "&#218;&#152;" => "�",
		    "&#218;&#169;" => "k",
		    "&#218;&#175;" => "g",
		    "&#218;&#190;" => "h", # remplacer par &#217;&#135;
		    "&#219;&#128;" => "X", # remplacer par &#217;&#135;&#219;&#140; en fin de mot (ailleurs = erreur)
		    "&#219;&#135;" => "~", # remplacer par &#217;&#136;
		    "&#219;&#140;" => "y",
		    "&#219;&#142;" => "�",
		    "&#219;&#176;" => "0",
		    "&#219;&#177;" => "1",
		    "&#219;&#178;" => "2",
		    "&#219;&#179;" => "3",
		    "&#219;&#180;" => "4",
		    "&#219;&#181;" => "5",
		    "&#219;&#182;" => "6",
		    "&#219;&#183;" => "7",
		    "&#219;&#184;" => "8",
		    "&#219;&#185;" => "9",
		    "&#226;&#128;&#140;" => "_"
		   );
}

my %map;
if ($latin_type == 1) {
  %map = (
	  "&#195;&#169;" => '�',
	  "&#195;&#160;" => '�',
	  "&#195;&#170;" => '�',
	  "&#195;&#167;" => '�',
	  "&#195;&#174;" => '�',
	  "&#195;&#185;" => '�',
	  "&#195;&#168;" => '�',
	  "&#195;&#137;" => '�',
	  "&#195;&#187;" => '�',
	  "&#195;&#162;" => '�',
	  "&#195;&#180;" => '�',
	  "&#195;&#175;" => '�',
	  "&#195;&#138;" => '�',
	  "&#195;&#128;" => '�',
	  "&#195;&#148;" => '�',
	  "&#195;&#188;" => '�',
	  "&#195;&#142;" => '�',
	  "&#195;&#136;" => '�',
	  "&#195;&#135;" => '�',
	  "&#195;&#186;" => '�',
	  "&#195;&#171;" => '�',
	  "&#195;&#179;" => '�',
	  "&#195;&#163;" => '�',
	  "&#195;&#161;" => '�',
	  "&#195;&#181;" => '�',
	  "&#195;&#173;" => '�',
	  "&#195;&#177;" => '�',
	  "&#195;&#130;" => '�',
	  "&#195;&#134;" => '�',
	  "&#195;&#139;" => '�',
	  "&#195;&#143;" => '�',
	  "&#195;&#156;" => '�',
	  "&#195;&#166;" => '�',
	  "&#195;&#129;" => '�',
	  "&#195;&#146;" => '�',
	  "&#195;&#191;" => '�',
	  "&#195;&#153;" => '�',
	  "&#195;&#154;" => '�',
	  "&#195;&#149;" => '�',
	  "&#195;&#155;" => '�',
	  "&#195;&#157;" => '�',
	  "&#195;&#164;" => '�',
	  "&#195;&#182;" => '�',
	  "&#195;&#178;" => '�',
	  "&#195;&#133;" => '�',
	  "&#195;&#172;" => '�',
	  "&#195;&#190;" => '�',
	  "&#195;&#144;" => '�',
	  "&#195;&#132;" => '�',
	  "&#195;&#152;" => '�',
	  "&#195;&#150;" => '�',
	  "&#195;&#184;" => '�',
	  "&#195;&#165;" => '�',
	  "&#195;&#159;" => '�',
	  "&#195;&#151;" => '�',
	  "&#194;&#176;" => '�',
	 );
} elsif ($latin_type == 2) {
  %map = (
	  "&#196;&#132;" => '�',
	  "&#203;&#152;" => '�',
	  "&#197;&#129;" => '�',
	  "&#196;&#189;" => '�',
	  "&#197;&#154;" => '�',
	  "&#197;&#160;" => '�',
	  "&#197;&#158;" => '�',
	  "&#197;&#164;" => '�',
	  "&#197;&#185;" => '�',
	  "&#197;&#189;" => '�',
	  "&#197;&#187;" => '�',
	  "&#196;&#133;" => '�',
	  "&#203;&#155;" => '�',
	  "&#197;&#130;" => '�',
	  "&#196;&#190;" => '�',
	  "&#197;&#155;" => '�',
	  "&#203;&#135;" => '�',
	  "&#197;&#161;" => '�',
	  "&#197;&#159;" => '�',
	  "&#197;&#165;" => '�',
	  "&#197;&#186;" => '�',
	  "&#203;&#157;" => '�',
	  "&#197;&#190;" => '�',
	  "&#197;&#188;" => '�',
	  "&#197;&#148;" => '�',
	  "&#195;&#129;" => '�',
	  "&#195;&#130;" => '�',
	  "&#196;&#130;" => '�',
	  "&#195;&#132;" => '�',
	  "&#196;&#185;" => '�',
	  "&#196;&#134;" => '�',
	  "&#195;&#135;" => '�',
	  "&#196;&#140;" => '�',
	  "&#195;&#137;" => '�',
	  "&#196;&#152;" => '�',
	  "&#195;&#139;" => '�',
	  "&#196;&#154;" => '�',
	  "&#195;&#141;" => '�',
	  "&#195;&#142;" => '�',
	  "&#196;&#142;" => '�',
	  "&#196;&#144;" => '�',
	  "&#197;&#131;" => '�',
	  "&#197;&#135;" => '�',
	  "&#195;&#147;" => '�',
	  "&#195;&#148;" => '�',
	  "&#197;&#144;" => '�',
	  "&#195;&#150;" => '�',
	  "&#195;&#151;" => '�',
	  "&#197;&#152;" => '�',
	  "&#197;&#174;" => '�',
	  "&#195;&#154;" => '�',
	  "&#197;&#176;" => '�',
	  "&#195;&#156;" => '�',
	  "&#195;&#157;" => '�',
	  "&#197;&#162;" => '�',
	  "&#195;&#159;" => '�',
	  "&#197;&#149;" => '�',
	  "&#195;&#161;" => '�',
	  "&#195;&#162;" => '�',
	  "&#196;&#131;" => '�',
	  "&#195;&#164;" => '�',
	  "&#196;&#186;" => '�',
	  "&#196;&#135;" => '�',
	  "&#195;&#167;" => '�',
	  "&#196;&#141;" => '�',
	  "&#195;&#169;" => '�',
	  "&#196;&#153;" => '�',
	  "&#195;&#171;" => '�',
	  "&#196;&#155;" => '�',
	  "&#195;&#173;" => '�',
	  "&#195;&#174;" => '�',
	  "&#196;&#143;" => '�',
	  "&#196;&#145;" => '�',
	  "&#197;&#132;" => '�',
	  "&#197;&#136;" => '�',
	  "&#195;&#179;" => '�',
	  "&#195;&#180;" => '�',
	  "&#197;&#145;" => '�',
	  "&#195;&#182;" => '�',
	  "&#195;&#183;" => '�',
	  "&#197;&#153;" => '�',
	  "&#197;&#175;" => '�',
	  "&#195;&#186;" => '�',
	  "&#197;&#177;" => '�',
	  "&#195;&#188;" => '�',
	  "&#195;&#189;" => '�',
	  "&#197;&#163;" => '�',
	  "&#203;&#153;" => '�',
	  "&#194;&#176;" => '�',
	 );
} elsif ($latin_type == -8) {
  %map = (
	  "&#208;&#144;" => '�',
	  "&#208;&#145;" => '�',
	  "&#208;&#146;" => '�',
	  "&#208;&#147;" => '�',
	  "&#208;&#148;" => '�',
	  "&#208;&#149;" => '�',
	  "&#208;&#150;" => '�',
	  "&#208;&#151;" => '�',
	  "&#208;&#152;" => '�',
	  "&#208;&#153;" => '�',
	  "&#208;&#154;" => '�',
	  "&#208;&#155;" => '�',
	  "&#208;&#156;" => '�',
	  "&#208;&#157;" => '�',
	  "&#208;&#158;" => '�',
	  "&#208;&#159;" => '�',
	  "&#208;&#160;" => '�',
	  "&#208;&#161;" => '�',
	  "&#208;&#162;" => '�',
	  "&#208;&#163;" => '�',
	  "&#208;&#164;" => '�',
	  "&#208;&#165;" => '�',
	  "&#208;&#166;" => '�',
	  "&#208;&#167;" => '�',
	  "&#208;&#168;" => '�',
	  "&#208;&#169;" => '�',
	  "&#208;&#173;" => '�',
	  "&#208;&#174;" => '�',
	  "&#208;&#175;" => '�',
	  "&#208;&#176;" => '�',
	  "&#208;&#177;" => '�',
	  "&#208;&#178;" => '�',
	  "&#208;&#179;" => '�',
	  "&#208;&#180;" => '�',
	  "&#208;&#181;" => '�',
	  "&#208;&#182;" => '�',
	  "&#208;&#183;" => '�',
	  "&#208;&#184;" => '�',
	  "&#208;&#185;" => '�',
	  "&#208;&#186;" => '�',
	  "&#208;&#187;" => '�',
	  "&#208;&#188;" => '�',
	  "&#208;&#189;" => '�',
	  "&#208;&#190;" => '�',
	  "&#208;&#191;" => '�',
	  "&#209;&#128;" => '�',
	  "&#209;&#129;" => '�',
	  "&#209;&#130;" => '�',
	  "&#209;&#131;" => '�',
	  "&#209;&#132;" => '�',
	  "&#209;&#133;" => '�',
	  "&#209;&#134;" => '�',
	  "&#209;&#135;" => '�',
	  "&#209;&#136;" => '�',
	  "&#209;&#137;" => '�',
	  "&#209;&#138;" => '�',
	  "&#209;&#139;" => '�',
	  "&#209;&#140;" => '�',
	  "&#209;&#141;" => '�',
	  "&#209;&#142;" => '�',
	  "&#209;&#143;" => '�',
	  "&#209;&#145;" => '�',
	 );
}

if ($lang eq "kmr") {
  $map{"&#197;&#158;"} = '�';
  $map{"&#197;&#159;"} = '�';
}

my $switch_encoding = "�";

$switch_encoding = Encode::encode("utf-8", Encode::decode('iso-8859-2', $switch_encoding));
my $recoded_switch_encoding = $switch_encoding;
$recoded_switch_encoding =~ s/([\x{80}-\x{FFFF}])/'&#' . ord($1) . ';'/gse;

my @buffer = ();
if ($guess_encoding) {
  while (<>) {
    push @buffer, $_;
    if ($lang eq "" || $lang eq "fr" || $lang eq "en" || $lang eq "es" || $lang eq "it" || $lang eq "kmr") {
      if (/(�[������������������])/ || /€/) {
	$unicode_input = 1; $latin_type = 1; last;
      } elsif (/[���������������������������]/) {
	$unicode_input = 0; $latin_type = 1; last;
      }
    } elsif ($lang eq "fa" || $lang eq "ckb" || $lang eq "pl" || $lang eq "sk" || $lang eq "si") {
      if (/�/ || /�/ || /�/) {
	$unicode_input = 1; $latin_type = 2; last;
      } else {
	$unicode_input = 0; $latin_type = 2; last;
      }
    } elsif ($lang eq "ru") {
      if ($translitterate) {
	if (/�/) {
	  $unicode_input = 1; $latin_type = 2; last;
	} else {
	  $unicode_input = 0; $latin_type = 2; last;
	}
      } else {
	if (/�/) {
	  $unicode_input = 1; $latin_type = -8; last;
	} else {
	  $unicode_input = 0; $latin_type = -8; last;
	}
      }
    }
    last if ($#buffer >= 20); # we abandon
  }
}

if ($unicode_input > 0) {
  for (@buffer) {
    print u8_to_8bits($_);
  }
  while (<>) {
    print u8_to_8bits($_);
  }
} else {
  for (@buffer) {
    print $_;
  }
  while (<>) {
    print $_;
  }
}

sub translchar {
  my $x = shift;
  if (my $c = $tmap{$x}) {
    return $c;
  } else {
    return $x;
  }
}

sub getchar {
  my $x = shift;
  if (my $c = $map{$x}) {
    return $c;
  } else {
    return $x;
  }
}

sub l1chr {
  my $x = shift;
  $x = chr ($x);
  Encode::_utf8_off($x);
  return $x;
}

sub restore_chr {
  my $s = shift;
  $s =~ s/&#(\d+);/chr($1)/gse;
  return $s;
}

sub u8_to_8bits {
  my $s = shift;

  $_ = Encode::decode('iso-8859-1', $s);
  if ($translitterate) {
    my $o = "";
    # translitterating + non-mandatory recoding
    s/&#(\d+);/l1chr($1)/gse;	# in case some &#NNNN; are already there... (e.g. in Excel file dumps)
    s/([\x{21}-\x{FFFF}])/'&#' . ord($1) . ';'/gse;
    s/(&#123; *&#60;&#70; &#105;&#100;&#61;&#34;.*?&#62;)/restore_chr($1)/ge; # { *<F.*?>
    s/(&#60;&#47;&#70;&#62; *&#60;&#70; &#105;&#100;&#61;&#34;.*?&#62;)/restore_chr($1)/ge; # </F> *<F.*?>
    s/(&#60;&#47;&#70;&#62; *&#125;)/restore_chr($1)/ge; # </F> *}
    while ($_ ne "") {
      if (/^(&#\d+;&#\d+;&#\d+;&#\d+;)/ms && defined($tmap{$1})) {
	s/^(&#\d+;&#\d+;&#\d+;&#\d+;)//ms;
	$o .= $tmap{$1};
      } elsif (/^(&#\d+;&#\d+;&#\d+;)/ms && defined($tmap{$1})) {
	s/^(&#\d+;&#\d+;&#\d+;)//ms;
	$o .= $tmap{$1};
      } elsif (/^(&#\d+;&#\d+;)/ms && defined($tmap{$1})) {
	s/^(&#\d+;&#\d+;)//s;
	$o .= $tmap{$1};
      } elsif (s/^(.)//ms) {
	$o .= $1;
      }
    }
    $_ = $o;
    if ($unicode_output) {
      $_ = Encode::encode("utf-8", Encode::decode('iso-8859-2', $_));
      s/$recoded_switch_encoding/_#_#_PROTECTED_SWITCH_ENCODING_CHAR_#_#_/gs;
      s/&#(\d+);/$switch_encoding.chr($1).$switch_encoding/gse;    
      s/$switch_encoding$switch_encoding//g;
      s/_#_#_PROTECTED_SWITCH_ENCODING_CHAR_#_#_/$recoded_switch_encoding/g;
    }
  } else {    
    # (non-mandatory) recoding
    if ($latin_type == -8) {
      s/(?<!;)&#171;/"/gs;
      s/(?<!;)&#187;/"/gs;
    }
    s/&#(\d+);/l1chr($1)/gse;	# in case some &#NNNN; are already there... (e.g. in Excel file dumps)
    s/([\x{80}-\x{FFFF}])/'&#' . ord($1) . ';'/gse;
    if ($latin_type == 1 || $latin_type == 2) {
      s/(&#194;&#176;)/getchar($1)/gse;
      s/(&#(?:19[567]|203);&#\d+;)/getchar($1)/gse;
    } elsif ($latin_type == -8) {
      s/(&#(?:20[89]);&#\d+;)/getchar($1)/gse;
    }
    if ($unicode_output) {
      s/ &#224; / � /g; #TMP
      s/([a-z ])&#233;([a-z ])/$1�$2/g; #TMP
      s/&#(\d+);/chr($1)/gse;    
    } else {
      s/&#226;&#128;&#153;/\'/gs;
      s/&#226;&#128;&#152;/\'/gs;
      s/&#194;&#180;/\'/gs;
      s/&#197;&#147;/oe/gs;
      if ($latin_type == 1) {
	s/&#194;&#171;/\�/gs;
	s/&#194;&#187;/\�/gs;
	s/&#194;&#186;/�/gs;
	s/&#194;&#167;/�/gs;
	s/&#210;&#169;/�/gs;
	s/&#194;&#191;/�/gs;
	s/&#194;&#161;/�/gs;
	s/&#194;&#181;/�/gs;
	s/&#194;&#172;/�/gs;
      }
      s/&#194;&#160;/�/gs;
      s/&#226;&#136;&#146;/-/gs;	# signe math�matique "moins"
#      s/&#226;&#128;&#148;/-/gs;	# tiret long
#      s/&#226;&#128;&#147;/-/gs;	# tiret semi-long
      s/&#194;&#173;//gs;		# cesure
      s/&#226;&#128;&#166;/.../gs;
      s/&#226;&#128;&#156;/"/gs;
      s/&#226;&#128;&#157;/"/gs;
      s/&#239;&#191;&#189;/'/gs;
      s/&#197;&#146;/OE/gs;
      s/&#226;&#148;&#128;/-/gs;
      s/&#226;&#128;&#137;/ /gs;
      s/&#226;&#128;&#149;/-/gs;
      s/&#226;&#128;&#145;/-/gs;
      s/&#194;&#146;/'/gs;
    }
  }
  return $_;
}

