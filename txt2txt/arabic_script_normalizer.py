# -*- coding: utf-8 -*-
# Convert Arabic Presentation Forms into Cannonical Arabic Characters
# Note: According to current Unicode standard, Arabic presentation forms do not have a joining class, and thus do not join to ordinary Arabic characters. The current script conservs this behaviour.
# input should be utf-8

import sys, codecs, unicodedata
import optparse, re 

usage = "usage: %prog [options] < input_file"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-l", "--language", action="store", help="language (only fa, ar and ckb are processed by this arabic script normalizer)", default="fr")
(options, args) = parser.parse_args()

ZWJ = u'\u200d'
ZWNJ = u'\u200c'


arJoinClass = {
  0x0600:'U', # ARABIC NUMBER SIGN
  0x0601:'U', # ARABIC SIGN SANAH
  0x0602:'U', # ARABIC FOOTNOTE MARKER
  0x0603:'U', # ARABIC SIGN SAFHA
  0x0608:'U', # ARABIC RAY
  0x060B:'U', # AFGHANI SIGN
  0x0621:'U', # HAMZA
  0x0622:'R', # MADDA ON ALEF
  0x0623:'R', # HAMZA ON ALEF
  0x0624:'R', # HAMZA ON WAW
  0x0625:'R', # HAMZA UNDER ALEF
  0x0626:'D', # HAMZA ON YEH
  0x0627:'R', # ALEF
  0x0628:'D', # BEH
  0x0629:'R', # TEH MARBUTA
  0x062A:'D', # TEH
  0x062B:'D', # THEH
  0x062C:'D', # JEEM
  0x062D:'D', # HAH
  0x062E:'D', # KHAH
  0x062F:'R', # DAL
  0x0630:'R', # THAL
  0x0631:'R', # REH
  0x0632:'R', # ZAIN
  0x0633:'D', # SEEN
  0x0634:'D', # SHEEN
  0x0635:'D', # SAD
  0x0636:'D', # DAD
  0x0637:'D', # TAH
  0x0638:'D', # ZAH
  0x0639:'D', # AIN
  0x063A:'D', # GHAIN
  0x063B:'D', # KEHEH WITH 2 DOTS ABOVE
  0x063C:'D', # KEHEH WITH 3 DOTS BELOW
  0x063D:'D', # FARSI YEH WITH INVERTED V
  0x063E:'D', # FARSI YEH WITH 2 DOTS ABOVE
  0x063F:'D', # FARSI YEH WITH 3 DOTS ABOVE
  0x0640:'C', # TATWEEL
  0x0641:'D', # FEH
  0x0642:'D', # QAF
  0x0643:'D', # KAF
  0x0644:'D', # LAM
  0x0645:'D', # MEEM
  0x0646:'D', # NOON
  0x0647:'D', # HEH
  0x0648:'R', # WAW
  0x0649:'D', # ALEF MAKSURA
  0x064A:'D', # YEH
  0x066E:'D', # DOTLESS BEH
  0x066F:'D', # DOTLESS QAF
  0x0671:'R', # HAMZAT WASL ON ALEF
  0x0672:'R', # WAVY HAMZA ON ALEF
  0x0673:'R', # WAVY HAMZA UNDER ALEF
  0x0674:'U', # HIGH HAMZA
  0x0675:'R', # HIGH HAMZA ALEF
  0x0676:'R', # HIGH HAMZA WAW
  0x0677:'R', # HIGH HAMZA WAW WITH DAMMA
  0x0678:'D', # HIGH HAMZA YEH
  0x0679:'D', # TEH WITH SMALL TAH
  0x067A:'D', # TEH WITH 2 DOTS VERTICAL ABOVE
  0x067B:'D', # BEH WITH 2 DOTS VERTICAL BELOW
  0x067C:'D', # TEH WITH RING
  0x067D:'D', # TEH WITH 3 DOTS ABOVE DOWNWARD
  0x067E:'D', # TEH WITH 3 DOTS BELOW
  0x067F:'D', # TEH WITH 4 DOTS ABOVE
  0x0680:'D', # BEH WITH 4 DOTS BELOW
  0x0681:'D', # HAMZA ON HAH
  0x0682:'D', # HAH WITH 2 DOTS VERTICAL ABOVE
  0x0683:'D', # HAH WITH MIDDLE 2 DOTS
  0x0684:'D', # HAH WITH MIDDLE 2 DOTS VERTICAL
  0x0685:'D', # HAH WITH 3 DOTS ABOVE
  0x0686:'D', # HAH WITH MIDDLE 3 DOTS DOWNWARD
  0x0687:'D', # HAH WITH MIDDLE 4 DOTS
  0x0688:'R', # DAL WITH SMALL TAH
  0x0689:'R', # DAL WITH RING
  0x068A:'R', # DAL WITH DOT BELOW
  0x068B:'R', # DAL WITH DOT BELOW AND SMALL TAH
  0x068C:'R', # DAL WITH 2 DOTS ABOVE
  0x068D:'R', # DAL WITH 2 DOTS BELOW
  0x068E:'R', # DAL WITH 3 DOTS ABOVE
  0x068F:'R', # DAL WITH 3 DOTS ABOVE DOWNWARD
  0x0690:'R', # DAL WITH 4 DOTS ABOVE
  0x0691:'R', # REH WITH SMALL TAH
  0x0692:'R', # REH WITH SMALL V
  0x0693:'R', # REH WITH RING
  0x0694:'R', # REH WITH DOT BELOW
  0x0695:'R', # REH WITH SMALL V BELOW
  0x0696:'R', # REH WITH DOT BELOW AND DOT ABOVE
  0x0697:'R', # REH WITH 2 DOTS ABOVE
  0x0698:'R', # REH WITH 3 DOTS ABOVE
  0x0699:'R', # REH WITH 4 DOTS ABOVE
  0x069A:'D', # SEEN WITH DOT BELOW AND DOT ABOVE
  0x069B:'D', # SEEN WITH 3 DOTS BELOW
  0x069C:'D', # SEEN WITH 3 DOTS BELOW AND 3 DOTS ABOVE
  0x069D:'D', # SAD WITH 2 DOTS BELOW
  0x069E:'D', # SAD WITH 3 DOTS ABOVE
  0x069F:'D', # TAH WITH 3 DOTS ABOVE
  0x06A0:'D', # AIN WITH 3 DOTS ABOVE
  0x06A1:'D', # DOTLESS FEH
  0x06A2:'D', # FEH WITH DOT MOVED BELOW
  0x06A3:'D', # FEH WITH DOT BELOW
  0x06A4:'D', # FEH WITH 3 DOTS ABOVE
  0x06A5:'D', # FEH WITH 3 DOTS BELOW
  0x06A6:'D', # FEH WITH 4 DOTS ABOVE
  0x06A7:'D', # QAF WITH DOT ABOVE
  0x06A8:'D', # QAF WITH 3 DOTS ABOVE
  0x06A9:'D', # KEHEH
  0x06AA:'D', # SWASH KAF
  0x06AB:'D', # KAF WITH RING
  0x06AC:'D', # KAF WITH DOT ABOVE
  0x06AD:'D', # KAF WITH 3 DOTS ABOVE
  0x06AE:'D', # KAF WITH 3 DOTS BELOW
  0x06AF:'D', # GAF
  0x06B0:'D', # GAF WITH RING
  0x06B1:'D', # GAF WITH 2 DOTS ABOVE
  0x06B2:'D', # GAF WITH 2 DOTS BELOW
  0x06B3:'D', # GAF WITH 2 DOTS VERTICAL BELOW
  0x06B4:'D', # GAF WITH 3 DOTS ABOVE
  0x06B5:'D', # LAM WITH SMALL V
  0x06B6:'D', # LAM WITH DOT ABOVE
  0x06B7:'D', # LAM WITH 3 DOTS ABOVE
  0x06B8:'D', # LAM WITH 3 DOTS BELOW
  0x06B9:'D', # NOON WITH DOT BELOW
  0x06BA:'D', # DOTLESS NOON
  0x06BB:'D', # DOTLESS NOON WITH SMALL TAH
  0x06BC:'D', # NOON WITH RING
  0x06BD:'D', # NYA
  0x06BE:'D', # KNOTTED HEH
  0x06BF:'D', # HAH WITH MIDDLE 3 DOTS DOWNWARD AND DOT ABOVE
  0x06C0:'R', # HAMZA ON HEH
  0x06C1:'D', # HEH GOAL
  0x06C2:'D', # HAMZA ON HEH GOAL
  0x06C3:'R', # TEH MARBUTA GOAL
  0x06C4:'R', # WAW WITH RING
  0x06C5:'R', # WAW WITH BAR
  0x06C6:'R', # WAW WITH SMALL V
  0x06C7:'R', # WAW WITH DAMMA
  0x06C8:'R', # WAW WITH ALEF ABOVE
  0x06C9:'R', # WAW WITH INVERTED SMALL V
  0x06CA:'R', # WAW WITH 2 DOTS ABOVE
  0x06CB:'R', # WAW WITH 3 DOTS ABOVE
  0x06CC:'D', # FARSI YEH
  0x06CD:'R', # YEH WITH TAIL
  0x06CE:'D', # FARSI YEH WITH SMALL V
  0x06CF:'R', # WAW WITH DOT ABOVE
  0x06D0:'D', # YEH WITH 2 DOTS VERTICAL BELOW
  0x06D1:'D', # YEH WITH 3 DOTS BELOW
  0x06D2:'R', # YEH BARREE
  0x06D3:'R', # HAMZA ON YEH BARREE
  0x06D5:'R', # AE
  0x06DD:'U', # ARABIC END OF AYAH
  0x06EE:'R', # DAL WITH INVERTED V
  0x06EF:'R', # REH WITH INVERTED V
  0x06FA:'D', # SEEN WITH DOT BELOW AND 3 DOTS ABOVE
  0x06FB:'D', # DAD WITH DOT BELOW
  0x06FC:'D', # GHAIN WITH DOT BELOW
  0x06FF:'D', # HEH WITH INVERTED V
  0x200C:'U', # ZERO WIDTH NON-JOINER
  0x200D:'C'  # ZERO WIDTH JOINER 
  }

def getJoinClass(ch):
  # This gives the joining type of Arabic letters and control characters. Other joining scripts (like Syriac) will return U as well
  c = ord(ch)
  if arJoinClass.has_key(c):
    return arJoinClass[c]
  category = unicodedata.category(ch)
  if category=='Mn' or category=='Me' or category=='Cf':
    return 'T'
  else:
    return 'U'

def hex2dec(s):
  # return the integer value of a hexadecimal string s
  return int(s, 16)

def isArPresForm(ch):
  c = ord(ch)
  return (((c>=0xfb50) and (c<=0xfdfc)) or ((c>=0xfe70) and (c<=0xfefc)))


if (not re.match ("(fa|ckb|ar)", options.language)):
  sys.stdout = codecs.getwriter('latin-1')(sys.stdout)
  for line in sys.stdin:
    line = line.decode('latin-1')
    sys.stdout.write(line)
else:
  sys.stdout = codecs.getwriter('utf8')(sys.stdout)
  lastJoinClass = 'U' # joinClass of last character read
  prevJoin = False # does the last character read want joining
  prevAPF = False # is the last character read an APF

  for line in sys.stdin:
	line = line.decode('utf-8')
	line = unicodedata.normalize('NFC', line) # apply cannonical composition
	for ch in line:
		#print(ord(ch))
		#print ch + ' ' + unicodedata.name(ch, '') + ' ' + unicodedata.category(ch)
		if isArPresForm(ch): #convert this character
		  joinClass = 'X' # handled by algorithm
		  curAPF = True
		  replaceby = unicodedata.normalize('NFKC', ch)
		  
		  form = (unicodedata.decomposition(ch)).partition(' ')[0]
		  
		  if (form == '<final>' or form == '<medial>'):
		    curJoin = True
		  else:
		    curJoin = False
		  if (form == '<initial>' or form=='<medial>'):
		    nextJoin=True
		  else:
		    nextJoin=False
		    
		else:
		  curAPF=False
		  joinClass=getJoinClass(ch)
		  #print joinClass
		  if (joinClass=='T'):
		    sys.stdout.write(ch)
		    continue # a transparernt character (outside APF block necessarily) does not affect joining in any way
		  replaceby = ch
		  if (joinClass!='C'): # never join a non-APF to an APF character
			  curJoin = False  
			  nextJoin = False
		  else: # except a JoinCausing character (tatweel, ZWJ) which anyhow does not change shape
		  	curJoin = True
		  	nextJoin = True
		
		
		  
		  		    
		      
		if (curAPF or prevAPF): # special care is only needed if we handle APFs
		  if (prevJoin):
		    if (not curJoin):
		      if (joinClass!='U'):
			replaceby = ZWNJ + replaceby
		      if (lastJoinClass=='D'): # this is just to counter the unlikely event of a previous tatweel (joinClass 'C')
			replaceby = ZWJ + replaceby
		    # if (curJoin): do nothing
		  else:
		    if (curJoin and joinClass!='C'): # no need to add a ZWJ to a JoinCauser character
		      replaceby = ZWJ + replaceby
		    if joinClass!='U' and (lastJoinClass=='D' or lastJoinClass=='C'):
			replaceby = ZWNJ + replaceby
		  sys.stdout.write(replaceby) 
		else: 
		  sys.stdout.write(ch)
		  
		
		# now calculate lastJoinClass
		if (joinClass=='X'): # we replaced an APF by a non-APF
		  lastJoinClass = getJoinClass(replaceby[-1])
		  if (lastJoinClass=='T'): # last character was a harakat
		    lastJoinClass = getJoinClass(replaceby[-2])
		else:
		  lastJoinClass=joinClass
		  
		prevJoin = nextJoin
		prevAPF = curAPF
		
		  
	
	if (prevJoin): # file ends with a joining character
		sys.stdout.write(ZWJ)
          
          
          
          
          
          
          
          

# Discover Arabic characters by their name (start by Arabic)
# Compatibility decomposition renders the compatibility characters (NFKD) 
# use the data of the decomposition name to get the contextual information
# the joining_data must be read separately
# canonical composition should be executed first in any case (NFC)
          
