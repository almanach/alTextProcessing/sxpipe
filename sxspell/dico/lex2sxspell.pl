#!/usr/bin/env perl

use warnings;			# can't use -w directly with env perl

my $letter = "A-Za-z���������������������������������������������������������������";
my $digit = "0-9";
my $sign1 = quotemeta("_-'");
my $sign2 = quotemeta("./|");

while (<>) {
    next if /^#/;
    next if /^\/\//;
    chomp;
    s/^ +//;
    s/^\"([^\t]+)\"$/$1/;
    s/ *' */'/g;
    s/ *- */-/g;
    s/ +/ /g;
    if (/^_/) {
      s/\\//g;
      s/"/\\"/g;
      print "\"$_\"\n";
    } else {
##      foreach (split(/(?<=.')|\s|(?<=[^ _])_(?=[^_])/)) {
#      foreach (split(/(?<=.')|\s/)) {
      foreach (split(/(?<=.')|\s/)) {
	s/\\//g;
	s/"/\\"/g;
	s/ '([^']+)' / "$1" /g;
	s/([^\.]\.)\.$/$1/;
	print "\"$_\"\n";
      }
    }
}
