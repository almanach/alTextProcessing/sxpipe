/* ********************************************************
   *                                                      *
   *                                                      *
   * Copyright (c) 1985 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *                                                      *
   *                                                      *
   ******************************************************** */




/* ********************************************************
   *                                                      *
   *     Produit de l'�quipe Langages et Traducteurs.     *
   *                                                      *
   ******************************************************** */


/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* 26-10-04 16:30 (pb):	 Ajout de cette rubrique "modifications"	*/
/************************************************************************/


#ifndef lint
#define WHAT	"@(#)named_entity_action.c\t- SYNTAX [unix] - Mardi 26 octobre 2004"
static struct what {
  struct what	*whatp;
  char		what [sizeof (WHAT)];
} what = {&what, WHAT};

#endif

/*   I N C L U D E S   */
#include "sxunix.h"
#include "named_entity_td.h"
#include "sxba.h"
#include "varstr.h"

extern struct sxtables	named_entity_tables;

/*  S T A T I C     V A R I A B L E S   */
static BOOLEAN	is_error;
static SXBA     prdct1_code_set, prdct2_code_set, prdct3_code_set;


/*   P R O C E D U R E S   */
int
named_entity_action (what, action_no)
    int		what, action_no;
{
  struct sxtoken	*ptok;
  int                   top, new_top, cur, lgth;
  static VARSTR         vstr;
  static BOOLEAN        is_first_word;
  char                  *pcom, *tcom;
  BOOLEAN               is_first;

  switch (what) {
  case OPEN:
    break;

  case INIT:
    vstr = varstr_alloc (128);
    fputs ("\n", stdout);
    is_first_word = TRUE;
    break;

  case ACTION:
    switch (action_no) {
    case 0:
      return;

    case 1:
    case 2:
    case 5:
      ptok = &(SXSTACKtoken (SXSTACKtop ()));
      printf ("%s%s%s", is_first_word ? "" : " ", ptok->comment ? ptok->comment : "", (is_first_word && ptok->comment) ? " " : "");

    default:
      break;
    }

    switch (action_no) {
    case 1:
      /* <word>			= et ; 1 */
      /* <word>			= de ; 1 */
      /* <word>			= %STRING ; 1 */
      /* <word>			= %WORD ; 1 */
      printf ("%s%s",
	      is_first_word ? "" : " ",
	      sxgenericp (sxplocals.sxtables, ptok->lahead)
	      ? sxstrget (ptok->string_table_entry)
	      : sxttext (sxplocals.sxtables, ptok->lahead));
      is_first_word = FALSE;
      return;

    case 2:
      /* <number>		= z�ro ; 2 */
      /* <number>		= <1..10^15-1> ; 2 */
      /* <number>		= <10^6+> ; 2 */
      printf ("%s_NUMBER", is_first_word ? "" : " ");
      is_first_word = FALSE;
      return;

    case 3:
      /* <axiome>		= <named_entities> ; 3 */
      fputs ("\n", stdout);
      return;

    case 4:
      /* <A0> = <A1> <A2> ... <Ap> ; 4 */
      /* Les commentaires associes a` A2, ... , Ap sont concatenes sur le commentaire associe' a` A1 */
      new_top = SXSTACKnewtop ();
      top =  SXSTACKtop ();

      if (top > new_top) {
	for (cur = new_top; cur <= top; cur++) {
	  if ((pcom = SXSTACKtoken (cur).comment) && (pcom = strchr (pcom, (int)'\n')))
	    break;
	}

	varstr_raz (vstr);

	if (cur <= top) {
	  /* Au moins un des commentaires contient un \n */
	  varstr_catenate (vstr, "\n");
	}

	for (cur = new_top; cur <= top; cur++) {
	  if ((pcom = SXSTACKtoken (cur).comment) && (pcom = strchr (pcom, (int)'{')))
	    break;
	}

	if (cur <= top) {
	  /* Au moins un des commentaires est un majeur */
	  varstr_catenate (vstr, "{");
	  is_first = TRUE;

	  for (cur = new_top; cur <= top; cur++) {
	    if ((pcom = SXSTACKtoken (cur).comment) && (pcom = strchr (pcom, (int)'{'))) {
	      if (is_first)
		is_first = FALSE;
	      else
		varstr_catenate (vstr, " ");

	      tcom = ++pcom;

	      if (tcom = strrchr (tcom, (int)'}')) {
		/* from_right */
		/* On vire le "}" fermant */
		varstr_lcatenate (vstr, pcom, tcom-pcom);
	      }
	    }
	  }
	    
	  varstr_catenate (vstr, "}");
	}

	if ((lgth = varstr_length (vstr)) > 0) {
	  pcom = SXSTACKtoken (new_top).comment;

	  if (pcom)
	    sxfree (pcom);

	  pcom = SXSTACKtoken (new_top).comment = sxalloc (lgth+1, sizeof (char));
	  strcpy (pcom, varstr_tostr (vstr));
	}
      }

      return;

    case 5:
      /* <number>		= %INTEGER ; 5 */
      printf ("%s_NUM", is_first_word ? "" : " ");
      is_first_word = FALSE;
      return;

    default:
      fputs ("The function \"named_entity_action\" is out of date with respect to its specification.\n", sxstderr);
      abort ();
    }

    break;

  case ERROR:
    is_error = TRUE;
    break;

  case FINAL:
    varstr_free (vstr);
    break;

  case CLOSE:
  case SEMPASS:
    break;

  default:
    fputs ("The function \"named_entity_action\" is out of date with respect to its specification.\n", sxstderr);
    abort ();
  }

  return 0;
}


int
named_entity_parsact (what, prdct_no)
    int		what, prdct_no;
{
  int        next_lahead;
  static int eof_code;

  switch (what) {
  case OPEN:
  case CLOSE:
    break;

  case INIT:
    eof_code = sxeof_code (sxplocals.sxtables);

    prdct1_code_set = sxba_calloc (eof_code+1);
    SXBA_1_bit (prdct1_code_set, un_code);
    SXBA_1_bit (prdct1_code_set, une_code);
    SXBA_1_bit (prdct1_code_set, onze_code);
    SXBA_1_bit (prdct1_code_set, mille_code);
    SXBA_1_bit (prdct1_code_set, million_code);
    SXBA_1_bit (prdct1_code_set, millions_code);
    SXBA_1_bit (prdct1_code_set, milliard_code);
    SXBA_1_bit (prdct1_code_set, milliards_code);
#if 0
    SXBA_1_bit (prdct1_code_set, unieme_code);
    SXBA_1_bit (prdct1_code_set, uniemes_code);
    SXBA_1_bit (prdct1_code_set, onzieme_code);
    SXBA_1_bit (prdct1_code_set, onziemes_code);
    SXBA_1_bit (prdct1_code_set, millionieme_code);
    SXBA_1_bit (prdct1_code_set, millioniemes_code);
    SXBA_1_bit (prdct1_code_set, milliardieme_code);
    SXBA_1_bit (prdct1_code_set, milliardiemes_code);
#endif /* 0 */

    prdct2_code_set = sxba_calloc (eof_code+1);
    SXBA_1_bit (prdct2_code_set, million_code);
    SXBA_1_bit (prdct2_code_set, milliard_code);
    SXBA_1_bit (prdct2_code_set, millions_code);
    SXBA_1_bit (prdct2_code_set, milliards_code);
#if 0
    SXBA_1_bit (prdct2_code_set, millionieme_code);
    SXBA_1_bit (prdct2_code_set, milliardieme_code);
#endif /* 0 */

    prdct3_code_set = sxba_calloc (eof_code+1);
    SXBA_1_bit (prdct3_code_set, cent_code);
    SXBA_1_bit (prdct3_code_set, cents_code);
    SXBA_1_bit (prdct3_code_set, mille_code);

    break;

  case FINAL:
    sxfree (prdct1_code_set), prdct1_code_set = NULL;
    sxfree (prdct2_code_set), prdct2_code_set = NULL;
    sxfree (prdct3_code_set), prdct3_code_set = NULL;
    break;

  case PREDICATE:
    switch (prdct_no) {
    case 1:
      /* &1 : TRUE <=> ssi le terminal suivant est : un[e], onze, mille ou milli[on|ard]s */
      next_lahead = sxget_token (sxplocals.ptok_no+1)->lahead;
      return SXBA_bit_is_set (prdct1_code_set, next_lahead);

    case 2:
      /* &2 : FALSE <=> ssi le terminal suivant est : milli[on|ard][s] 
	 ou si le terminal suivant est cent[s] ou mille et le terminal d'apres n'est
	 pas %WORD */
      next_lahead = sxget_token (sxplocals.ptok_no+1)->lahead;

      if (SXBA_bit_is_set (prdct2_code_set, next_lahead))
	/* le terminal suivant est : milli[on|ard][s] */
	return FALSE;

      if (SXBA_bit_is_set (prdct3_code_set, next_lahead) /* le terminal suivant est cent[s] ou mille */) {
	next_lahead = sxget_token (sxplocals.ptok_no+2)->lahead;

	if (next_lahead != WORD_code)
	  return FALSE;
      }

      return TRUE;

    case 3:
      /* TRUE <=> le ssi terminal suivant est %WORD */
      next_lahead = sxget_token (sxplocals.ptok_no+1)->lahead;

      return next_lahead == WORD_code;

    default:
      break;
    }

    break;

  case ERROR:
    is_error = TRUE;
    break;

  default:
    fputs ("The function \"named_entity_parsact\" is out of date with respect to its specification.\n", sxstderr);
    abort ();
  }

  return 0;
}


static VOID	gripe (sxtables, act_no)
    struct sxtables	*sxtables;
    int		act_no;
{
  sxput_error (sxsvar.lv.terminal_token.source_index,
	       "%sInternal inconsistency: Action %d not yet implemented.\nExecution aborted.", sxtables->err_titles [2]);
  SXEXIT (3);
}


BOOLEAN
named_entity_scan_act (code, act_no)
    int		code;
    int		act_no;
{
  char	string [14];
  char	c, *str1, *str2;

  switch (code) {
  default:
  case PREDICATE:
    gripe (&named_entity_tables, code);

  case BEGIN:
  case END:
    break;

  case OPEN:
  case CLOSE:
    break;

  case INIT:
  case FINAL:
    break;

  case ACTION:
    switch (act_no) {
    default:
      gripe (&named_entity_tables, act_no);

    case 1:
      /* Les mots clefs ne sont pas sensibles a la casse */
      /* |quatre-vingts| == 13 C'est le +long mot clef */
      if (sxsvar.lv.ts_lgth == 1 || sxsvar.lv.ts_lgth > 13)
	break;

      str1 = sxsvar.lv_s.token_string, str2 = string;

      do {
	c = *str1++;
	*str2++ = sxtolower (c);
      } while (c != NUL);

      if ((*sxsvar.SXS_tables.check_keyword) (string, sxsvar.lv.ts_lgth)) {
	strncpy (sxsvar.lv_s.token_string, string, sxsvar.lv.ts_lgth);
      }

      break;
    }

    break;
  }

  return TRUE;
}
