/* ********************************************************
   *                                                      *
   *                                                      *
   * Copyright (c) 1985 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *                                                      *
   *                                                      *
   ******************************************************** */




/* ********************************************************
   *                                                      *
   *     Produit de l'�quipe Langages et Traducteurs.     *
   *                                                      *
   ******************************************************** */


/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* 26-10-04 16:30 (pb):	 Ajout de cette rubrique "modifications"	*/
/************************************************************************/


#ifndef lint
#define WHAT	"@(#)ieme_action.c\t- SYNTAX [unix] - Mardi 26 octobre 2004"
static struct what {
  struct what	*whatp;
  char		what [sizeof (WHAT)];
} what = {&what, WHAT};

#endif

/*   I N C L U D E S   */
#include "sxunix.h"
#include "ieme_td.h"
#include "sxba.h"
#include "varstr.h"

/*  S T A T I C     V A R I A B L E S   */
static BOOLEAN	is_error;
static SXBA     prdct1_code_set, prdct2_code_set;


/*   P R O C E D U R E S   */
int
ieme_action (what, action_no)
    int		what, action_no;
{
  struct sxtoken	*ptok;
  int                   top, new_top, cur, lgth;
  static VARSTR         vstr;
  static BOOLEAN        is_first_word;
  char                  *pcom, *tcom;
  BOOLEAN               is_first;

  switch (what) {
  case OPEN:
    break;

  case INIT:
    vstr = varstr_alloc (128);
    fputs ("\n", stdout);
    is_first_word = TRUE;
    break;

  case ACTION:
    switch (action_no) {
    case 0:
      return;

    case 1:
    case 2:
    case 5:
      ptok = &(SXSTACKtoken (SXSTACKtop ()));
      printf ("%s%s%s", is_first_word ? "" : " ", ptok->comment ? ptok->comment : "", (is_first_word && ptok->comment) ? " " : "");

    default:
      break;
    }

    switch (action_no) {
    case 1:
      /*
	<word>			= et ; 1
	<word>			= de ; 1
	<word>			= %STRING ; 1
	<word>			= %WORD ; 1
	<word>			= %PONCT ; 1
      */
      printf ("%s%s",
	      is_first_word ? "" : " ",
	      sxgenericp (sxplocals.sxtables, ptok->lahead)
	      ? sxstrget (ptok->string_table_entry)
	      : sxttext (sxplocals.sxtables, ptok->lahead));
      is_first_word = FALSE;
      return;

    case 2:
      /*
	<number>		= <_NUMBERs> ; 2
	<number>		= <_NUMBERs> et &1 <ieme> ; 2
	<number>		= <_NUMBERs> de &1 <ieme> ; 2
	<number>		= <_NUMBERs> <ieme> ; 2
	<number>		= <_NUMBERs> <aine> ; 2
	<number>		= <ieme> ; 2
	<number>		= <aine> ; 2
      */
      printf ("%s_NUMBER", is_first_word ? "" : " ");
      is_first_word = FALSE;
      return;

    case 3:
      /*
	<axiome>		= <entities> ; 3
      */
      fputs ("\n", stdout);
      return;

    case 4:
      /* <A0> = <A1> <A2> ... <Ap> ; 4 */
      /* Les commentaires associes a` A2, ... , Ap sont concatenes sur le commentaire associe' a` A1 */
      new_top = SXSTACKnewtop ();
      top =  SXSTACKtop ();

      if (top > new_top) {
	for (cur = new_top; cur <= top; cur++) {
	  if ((pcom = SXSTACKtoken (cur).comment) && (pcom = strchr (pcom, (int)'\n')))
	    break;
	}

	varstr_raz (vstr);

	if (cur <= top) {
	  /* Au moins un des commentaires contient un \n */
	  varstr_catenate (vstr, "\n");
	}

	for (cur = new_top; cur <= top; cur++) {
	  if ((pcom = SXSTACKtoken (cur).comment) && (pcom = strchr (pcom, (int)'{')))
	    break;
	}

	if (cur <= top) {
	  /* Au moins un des commentaires est un majeur */
	  varstr_catenate (vstr, "{");
	  is_first = TRUE;

	  for (cur = new_top; cur <= top; cur++) {
	    if ((pcom = SXSTACKtoken (cur).comment) && (pcom = strchr (pcom, (int)'{'))) {
	      if (is_first)
		is_first = FALSE;
	      else
		varstr_catenate (vstr, " ");

	      tcom = ++pcom;

	      if (tcom = strrchr (tcom, (int)'}')) {
		/* from_right */
		/* On vire le "}" fermant */
		varstr_lcatenate (vstr, pcom, tcom-pcom);
	      }
	    }
	  }
	    
	  varstr_catenate (vstr, "}");
	}

	if ((lgth = varstr_length (vstr)) > 0) {
	  pcom = SXSTACKtoken (new_top).comment;

	  if (pcom)
	    sxfree (pcom);

	  pcom = SXSTACKtoken (new_top).comment = sxalloc (lgth+1, sizeof (char));
	  strcpy (pcom, varstr_tostr (vstr));
	}
      }

      return;

    case 5:
      /*
       * @5 sort _DATE
       <number>		= <date> ; 5
      */
      printf ("%s_DATE", is_first_word ? "" : " ");
      is_first_word = FALSE;

      return;

    default:
      fputs ("The function \"ieme_action\" is out of date with respect to its specification.\n", sxstderr);
      abort ();
    }

    break;

  case ERROR:
    is_error = TRUE;
    break;

  case FINAL:
    varstr_free (vstr);
    break;

  case CLOSE:
  case SEMPASS:
    break;

  default:
    fputs ("The function \"ieme_action\" is out of date with respect to its specification.\n", sxstderr);
    abort ();
  }

  return 0;
}


int
ieme_parsact (what, prdct_no)
    int		what, prdct_no;
{
  int        next_lahead;
  static int eof_code;

    switch (what) {
    case OPEN:
    case CLOSE:
	break;

    case INIT:
      eof_code = sxeof_code (sxplocals.sxtables);

      prdct1_code_set = sxba_calloc (eof_code+1);

      SXBA_1_bit (prdct1_code_set, zeroieme_code);
      SXBA_1_bit (prdct1_code_set, zeroiemes_code);
      SXBA_1_bit (prdct1_code_set, unieme_code);
      SXBA_1_bit (prdct1_code_set, uniemes_code);
      SXBA_1_bit (prdct1_code_set, deuxieme_code);
      SXBA_1_bit (prdct1_code_set, deuxiemes_code);
      SXBA_1_bit (prdct1_code_set, troisieme_code);
      SXBA_1_bit (prdct1_code_set, troisiemes_code);
      SXBA_1_bit (prdct1_code_set, quatrieme_code);
      SXBA_1_bit (prdct1_code_set, quatriemes_code);
      SXBA_1_bit (prdct1_code_set, cinquieme_code);
      SXBA_1_bit (prdct1_code_set, cinquiemes_code);
      SXBA_1_bit (prdct1_code_set, sixieme_code);
      SXBA_1_bit (prdct1_code_set, sixiemes_code);
      SXBA_1_bit (prdct1_code_set, septieme_code);
      SXBA_1_bit (prdct1_code_set, septiemes_code);
      SXBA_1_bit (prdct1_code_set, huitieme_code);
      SXBA_1_bit (prdct1_code_set, huitiemes_code);
      SXBA_1_bit (prdct1_code_set, neuvieme_code);
      SXBA_1_bit (prdct1_code_set, neuviemes_code);
      SXBA_1_bit (prdct1_code_set, dixieme_code);
      SXBA_1_bit (prdct1_code_set, dixiemes_code);
      SXBA_1_bit (prdct1_code_set, onzieme_code);
      SXBA_1_bit (prdct1_code_set, onziemes_code);
      SXBA_1_bit (prdct1_code_set, douzieme_code);
      SXBA_1_bit (prdct1_code_set, douziemes_code);
      SXBA_1_bit (prdct1_code_set, treizieme_code);
      SXBA_1_bit (prdct1_code_set, treiziemes_code);
      SXBA_1_bit (prdct1_code_set, quatorzieme_code);
      SXBA_1_bit (prdct1_code_set, quatorziemes_code);
      SXBA_1_bit (prdct1_code_set, quinzieme_code);
      SXBA_1_bit (prdct1_code_set, quinziemes_code);
      SXBA_1_bit (prdct1_code_set, seizieme_code);
      SXBA_1_bit (prdct1_code_set, seiziemes_code);
      SXBA_1_bit (prdct1_code_set, vingtieme_code);
      SXBA_1_bit (prdct1_code_set, vingtiemes_code);
      SXBA_1_bit (prdct1_code_set, trentieme_code);
      SXBA_1_bit (prdct1_code_set, trentiemes_code);
      SXBA_1_bit (prdct1_code_set, quarantieme_code);
      SXBA_1_bit (prdct1_code_set, quarantiemes_code);
      SXBA_1_bit (prdct1_code_set, cinquantieme_code);
      SXBA_1_bit (prdct1_code_set, cinquantiemes_code);
      SXBA_1_bit (prdct1_code_set, soixantieme_code);
      SXBA_1_bit (prdct1_code_set, soixantiemes_code);
      SXBA_1_bit (prdct1_code_set, quatre_vingtieme_code);
      SXBA_1_bit (prdct1_code_set, quatre_vingtiemes_code);
      SXBA_1_bit (prdct1_code_set, centieme_code);
      SXBA_1_bit (prdct1_code_set, centiemes_code);
      SXBA_1_bit (prdct1_code_set, millieme_code);
      SXBA_1_bit (prdct1_code_set, milliemes_code);
      SXBA_1_bit (prdct1_code_set, millionieme_code);
      SXBA_1_bit (prdct1_code_set, millioniemes_code);
      SXBA_1_bit (prdct1_code_set, milliardieme_code);
      SXBA_1_bit (prdct1_code_set, milliardiemes_code);

      prdct2_code_set = sxba_calloc (eof_code+1);

      SXBA_1_bit (prdct2_code_set, NUMBER_code);
      break;

    case FINAL:
      sxfree (prdct1_code_set), prdct1_code_set = NULL;
      sxfree (prdct2_code_set), prdct2_code_set = NULL;
      break;

    case PREDICATE:
      switch (prdct_no) {
      case 1:
	/* &1 : TRUE ssi le terminal suivant est un debutant de <ieme> */
	next_lahead = sxget_token (sxplocals.ptok_no+1)->lahead;
	return SXBA_bit_is_set (prdct1_code_set, next_lahead);

      case 2:
	/* &2 : TRUE ssi le terminal suivant est un _NUMBER */
	next_lahead = sxget_token (sxplocals.ptok_no)->lahead;
	return SXBA_bit_is_set (prdct2_code_set, next_lahead);

      default:
	break;
      }

      break;

    case ERROR:
	is_error = TRUE;
	break;

    default:
	fputs ("The function \"ieme_parsact\" is out of date with respect to its specification.\n", sxstderr);
	abort ();
    }

    return 0;
}
