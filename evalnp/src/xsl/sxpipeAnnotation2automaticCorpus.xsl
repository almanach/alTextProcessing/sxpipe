<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Dec 9, 2009</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <!-- prend en entrée le xml sorti de l'annotateur -->
  <xsl:output indent="yes" method="xml" encoding="utf-8"/>
    
  <xsl:param name="total_dev">
      <xsl:value-of select="count(/child::*/item[@eval_type='dev']/para/Person)+count(/child::*/item[@eval_type='dev']/para/Location)+count(/child::*/item[@eval_type='dev']/para/Organization)+count(/child::*/item[@eval_type='dev']/para/Company)"/>
  </xsl:param>  
    
    <xsl:param name="total_test">
      <xsl:value-of select="count(/child::*/item[@eval_type='test']/para/Person)+count(/child::*/item[@eval_type='test']/para/Location)+count(/child::*/item[@eval_type='test']/para/Organization)+count(/child::*/item[@eval_type='test']/para/Company)"/>
    </xsl:param>  
    
    <xsl:param name="total">
        <xsl:value-of select="count(//Person)+count(//Location)+count(//Organization)+count(//Company)"/>
    </xsl:param>
    
    
    
  <xsl:template match="/child::*">
    <sxpipe_corpus>
<!--        <total_dev><xsl:value-of select="$total_dev"/></total_dev>
        <total_test><xsl:value-of select="$total_test"/></total_test>
        <total><xsl:value-of select="$total"/></total>-->
      <xsl:apply-templates select="./item"/>
    </sxpipe_corpus>
  </xsl:template>
  
  
    <xsl:template match="item">
        <item>
            <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
            <xsl:attribute name="ref"><xsl:value-of select="./@ref"/></xsl:attribute>       
            <xsl:attribute name="eval_type"><xsl:value-of select="./@eval_type"/></xsl:attribute>                   
            <xsl:apply-templates select="./para"/>
        </item>
    </xsl:template>
      
  <!-- TEMPLATES FOR PARAGRAPHS DISPLAY -->
    <xsl:template match="para">
        <xsl:choose>
            <xsl:when test="./@signature">
                <para>
                    <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
                    <xsl:attribute name="signature"><xsl:value-of select="./@signature"/></xsl:attribute>                    
                </para>
            </xsl:when>
            <xsl:otherwise>                
                <para>
                    <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>      
                    <xsl:apply-templates/>
                </para> 
            </xsl:otherwise>
        </xsl:choose>       
    </xsl:template>
  
    <xsl:template match="a"/>
        
    
  
    <!-- TEMPLATES FOR NAMED ENTITIES DISPLAY -->
    <!--<xsl:template match="PERSON|Person|LOCATION|Location|ORGANIZATION|Organization">-->
    <xsl:template match="ENAMEX">
                <ENAMEX>
                    <xsl:attribute name="eid"><xsl:value-of select="./@eid"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
                    <xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
                    <xsl:value-of select="."/>                
                </ENAMEX>        
    </xsl:template>    
    
    <!--<xsl:template match="NUMEX|TIMEX">
        <xsl:copy-of select="."/>
    </xsl:template>-->
    
    
    
    <!-- TODO: MISC ENTITIES -->
    <!--<xsl:template match="Product|Work|Number|...">
    <xsl:element name="{local-name()}">
      <xsl:attribute name="name">
          <xsl:value-of select="./@name"/>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(child::text())"/>
    </xsl:element>
    </xsl:template>-->
</xsl:stylesheet>






