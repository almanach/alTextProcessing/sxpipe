<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jan 12, 2010</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <!-- take the manually corrected corpus and extract paras that are manually corrected without entities annotations == raw -->
    
    <xsl:output indent="yes" method="xml" encoding="utf-8"/>
<!--    <xsl:strip-space elements="*"/>-->
        
    <!-- TEMPLATES -->
    <xsl:template match="/child::*">
        <raw_corpus>
            <xsl:comment>CORPUS GOLD BRUT POUR L'EVALUATION DE SYSTEMES D'EXTRACTION D'ENTITES NOMMEES ET DE DETECTION DE REFERENCE</xsl:comment>
            <xsl:comment>AUTEUR : R.STERN [ALPAGE (INRIA-PARIS7) / AFP]</xsl:comment>
            <xsl:comment>stern.rosa@gmail.com</xsl:comment>
            <xsl:comment>DECEMBRE 2010</xsl:comment>
            <xsl:apply-templates select="./news_item"/>    
        </raw_corpus>
    </xsl:template>

    <xsl:template match="news_item">
        <xsl:if test="./@corrected='true'">
            <item>
                <xsl:attribute name="rank">
                    <xsl:value-of select="1 + count(preceding-sibling::news_item[@corrected='true'])"/>
                </xsl:attribute>
                <xsl:attribute name="eval_type">
                    <xsl:choose>
                        <xsl:when test="count(preceding-sibling::news_item[@corrected='true']) mod 2">test</xsl:when>
                        <xsl:otherwise>dev</xsl:otherwise>
                    </xsl:choose>    
                </xsl:attribute>                
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@ref"/>
                </xsl:attribute>
                <!--<xsl:apply-templates select="./news_head"/>-->
                <xsl:apply-templates select="./news_text"/>
            </item>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="news_head">
        <head>
            <xsl:value-of select="."/>
        </head>
    </xsl:template>
    
    <xsl:template match="news_text">
        <!--<text><xsl:apply-templates select="./para"/></text>-->
        <xsl:apply-templates select="./para"/>
    </xsl:template>
    
    <xsl:template match="para">
        <xsl:if test="not(./@double)">
            <xsl:choose>
                <!--<xsl:when test="position()=last()">-->
                <xsl:when test="./@signature='true'">
                    <para>
                        <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
                        <xsl:attribute name="signature">
                            <xsl:variable name="myString" select="normalize-space(child::text())"/>
                            <xsl:variable name="myNewString">
                                <xsl:call-template name="replaceCharsInString">
                                    <xsl:with-param name="stringIn" select="string($myString)"/>
                                    <xsl:with-param name="charsIn" select="'/'"/>
                                    <xsl:with-param name="charsOut" select="'__'"/>
                                </xsl:call-template>              
                            </xsl:variable>
                            <xsl:variable name="myNewNewString">
                                <xsl:call-template name="replaceCharsInString">
                                    <xsl:with-param name="stringIn" select="string($myNewString)"/>
                                    <xsl:with-param name="charsIn" select="'-'"/>
                                    <xsl:with-param name="charsOut" select="'_'"/>
                                </xsl:call-template>              
                            </xsl:variable>
                            <!-- $myNewString is a result tree fragment, which should be OK. -->
                            <!-- If you really need a string object, do this: -->
                            <xsl:variable name="myNewRealString" select="string($myNewNewString)"/>
                            <xsl:value-of select="$myNewRealString"/>
                            <!--<xsl:value-of select="normalize-space(child::text())"/>-->
                        </xsl:attribute>
                    </para>
                </xsl:when>
                <xsl:otherwise>
                    <para>
                        <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
                        <xsl:apply-templates/>
                    </para>        
                </xsl:otherwise>
            </xsl:choose>  
        </xsl:if>
    </xsl:template>
    
<!-- here is the template that does the replacement -->
<xsl:template name="replaceCharsInString">
  <xsl:param name="stringIn"/>
  <xsl:param name="charsIn"/>
  <xsl:param name="charsOut"/>
  <xsl:choose>
    <xsl:when test="contains($stringIn,$charsIn)">
      <xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
      <xsl:call-template name="replaceCharsInString">
        <xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
        <xsl:with-param name="charsIn" select="$charsIn"/>
        <xsl:with-param name="charsOut" select="$charsOut"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$stringIn"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
    

</xsl:stylesheet>
